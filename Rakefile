# frozen_string_literal: true

pwd = Rake.application.original_dir

desc "Linting in :path folder"
task :lint, [:path] do |t, args|
  args.with_defaults(path: pwd)
  sh "ameba #{args.path} --fix"
  sh "standardrb --fix #{args.path}"
end

desc "Format the sources in :path folder"
task :fmt, [:path] do |t, args|
  args.with_defaults(path: pwd)
  sh "crystal tool format #{args.path}"
  sh "go fmt #{args.path}/..."
end

namespace :fmt do
  desc "Check if the code formatted in :path folder"
  task :check, [:path] do |t, args|
    args.with_defaults(path: pwd)
    sh "crystal tool format --check #{args.path}"
    sh "standardrb #{args.path}"
  end
end

task test: ["test:all"]
namespace :test do
  desc "Run tests"
  task :all, [:path] do |t, args|
    args.with_defaults(path: pwd)
    option_list = ENV.fetch("TESTOPTS", "")
    option_list = "-- #{option_list}" if option_list.size > 0

    files = FileList["#{args.path}/**/*_test.cr"].reject { |f| f.include?("/lib/") }.join(" ")
    sh "crystal run --error-trace #{files} #{option_list}" if files.size > 0

    files = FileList["#{args.path}/**/*_spec.cr"].reject { |f| f.include?("/lib/") }.join(" ")
    sh "crystal spec --error-trace #{files}" if files.size > 0

    files = FileList["#{args.path}/**/*_test.go"].reject { |f| f.include?("/vendor/") }.join(" ")
    sh "go test #{args.path}/..." if files.size > 0
  end

  desc "Run benchmarks"
  task :benchmark, [:path] do |t, args|
    args.with_defaults(path: pwd)
    files = FileList["#{args.path}/**/*_benchmark.cr"].reject { |f| f.include?("/lib/") }.join(" ")
    sh "crystal run --release #{files}"
  end
end
