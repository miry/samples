packer {
  required_plugins {
    qemu = {
      version = "~> 1"
      source  = "github.com/hashicorp/qemu"
    }
  }
}

variable "password" {
  type    = string
  default = "s0m3password"
}

# Sample config that was not tested for Ubuntu 20.04
source "qemu" "ubuntu-2004" {
  iso_url          = "http://releases.ubuntu.com/20.04/ubuntu-20.04.6-live-server-amd64.iso"
  iso_checksum     = "file:http://releases.ubuntu.com/20.04/SHA256SUMS"
  output_directory = "output"
  shutdown_command = "echo '${var.password}' | sudo -S shutdown -P now"
  #disk_size         = "5000M"
  #format            = "qcow2"
  #accelerator       = "kvm"
  memory         = 2048
  http_directory = "http"
  ssh_username   = "ubuntu"
  ssh_password   = var.password
  ssh_timeout    = "10m"
  vm_name        = "ubuntu"
  #net_device        = "virtio-net"
  #disk_interface    = "virtio"
  boot_wait = "3s"
  # Old ubuntu uses Graphical loader:
  boot_command = ["<enter><enter><f6><esc><wait>autoinstall ds=nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/<enter>"]
}

source "qemu" "ubuntu-2204" {
  iso_url          = "https://releases.ubuntu.com/22.04.4/ubuntu-22.04.4-live-server-amd64.iso"
  iso_checksum     = "file:https://releases.ubuntu.com/22.04.4/SHA256SUMS"
  output_directory = "output-ubuntu-2204"
  shutdown_command = "echo '${var.password}' | sudo -S shutdown -P now"
  #disk_size         = "5000M"
  #format            = "qcow2"
  accelerator    = "kvm"
  http_directory = "http"
  ssh_username   = "ubuntu"
  ssh_password   = var.password
  ssh_timeout    = "30m"
  vm_name        = "ubuntu"
  cpus           = 4
  memory         = 4096
  #net_device        = "virtio-net"
  #disk_interface    = "virtio"
  boot_wait         = "3s"
  boot_key_interval = "150ms"
  headless          = true
  boot_command = [
    "c<wait>",
    "linux /casper/vmlinuz ",
    "autoinstall 'ds=nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/' console=ttyS0<enter><wait>",
    "<wait>",
    "initrd /casper/initrd<enter>",
    "<wait>",
    "<wait>",
    "boot<enter><wait>"
  ]
}

build {
  name    = "ubuntu-2004"
  sources = ["source.qemu.ubuntu-2004"]
}

build {
  name    = "ubuntu-2204"
  sources = ["source.qemu.ubuntu-2204"]
}

