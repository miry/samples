## Build Ubuntu virtual machine

```
PACKER_LOG=1 packer build ubuntu.pkr.hcl
```

For Mac:

```
PACKER_LOG=1 packer build ubuntu.mac.pkr.hcl
```


Then run:

```
qemu-system-x86_64 -name ubuntu-server \    
    -drive file=output/ubuntu.qcow2,if=virtio,cache=writeback,discard=ignore,format=qcow2 \
    -m 4096M \
    -smp 4 \
    -device virtio-net,netdev=user.0  \
    -netdev user,id=user.0,hostfwd=tcp::4141-:22
```

More details in my article [Getting started with Packer in 2024](https://dev.to/miry/getting-started-with-packer-in-2024-56d5).
