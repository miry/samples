# server.cr
require "http/server"

def run(host = "127.0.0.1", port = 8080, local = ".")
  server = HTTP::Server.new([
    HTTP::ErrorHandler.new,
    HTTP::LogHandler.new,
    HTTP::CompressHandler.new,
    HTTP::StaticFileHandler.new(local),
  ])

  address = server.bind_tcp host, port
  puts "Listening on http://#{address}"
  server.listen
end

run
