package main

import (
	"fmt"
	"log"
	"net/http"

	"codeberg.org/miry/samples/rack/app"
	"codeberg.org/miry/samples/rack/handlers"
	"codeberg.org/miry/samples/rack/mid"
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
		}
	}()

	a := &app.App{
		Version: "0.0.1",
	}

	http.HandleFunc("/version", func(w http.ResponseWriter, r *http.Request) { fmt.Fprintf(w, "%s", a.Version) })
	http.HandleFunc("/track", mid.Chain(a, handlers.Track, "track"))

	log.Fatal(http.ListenAndServe(":8080", nil))
}
