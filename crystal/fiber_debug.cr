# Original: https://forum.crystal-lang.org/t/finding-where-a-fiber-is-halt/2946
#
# Example of usage:
#
#     Signal::QUIT.trap do
#       Fiber.unsafe_each do |f|
#         f.print_backtrace if !f.running? && !f.dead?
#       end
#     end

class Fiber
  property call_stack : Exception::CallStack?

  def print_backtrace
    puts "Fiber's backtrace (name: #{name}):"
    if call_stack = @call_stack
      call_stack.printable_backtrace[1..].each { |l| puts l }
    else
      puts "(running, unable to get backtrace)"
    end
    puts
  end
end

class Crystal::Scheduler
  protected def resume(fiber : Fiber) : Nil
    @current.call_stack = Exception::CallStack.new
    previous_def
    @current.call_stack = nil
  end
end
