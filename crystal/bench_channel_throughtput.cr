# Benchmark throughput of channels in same thread
# Usage:
#   time crystal run --no-debug --release bench_channel_throughtput.cr
#   time crystal run --no-debug --release -Dpreview_mt bench_channel_throughtput.cr
#   time crystal run --no-debug --release -Dgc_none bench_channel_throughtput.cr
#   time crystal run --no-debug --release -Dpreview_mt -Dgc_none bench_channel_throughtput.cr
#
# Output sample of the fastest build:
#   $ time crystal run --no-debug --release -Dgc_none bench_channel_throughtput.cr
#   exp 20: 1,048,576 iterations took 84,479,708 ns. 81.0 ns/op
#   exp 23: 8,388,608 iterations took 681,324,458 ns. 82.0 ns/op
#   exp 24: 16,777,216 iterations took 356,331,041 ns. 22.0 ns/op   <- NOTE: Strange that for bigger numbers it works faster!
#   exp 25: 33,554,432 iterations took 706,687,250 ns. 22.0 ns/op
#   exp 26: 67,108,864 iterations took 421,953,500 ns. 7.0 ns/op    <- NOTE: Strange that for bigger numbers it works faster!
#   crystal run --release -Dgc_none bench_channel_throughtput.cr  50,83s user 0,43s system 99% cpu 51,552 total

def bench(niters)
  # Non buffered channel. It shoud lock on each read and write to force change the running Fibers.
  ch = Channel(String).new
  spawn(name: "echo", same_thread: true) do
    loop do
      ch.send(ch.receive)
    end
  end

  # warming
  16777216.times do
    ch.send("oops")
    ch.receive
  end

  # GC.collect
  # puts GC.stats

  # benchmarking
  start = Time.monotonic
  niters.times do
    ch.send("oops")
    ch.receive
  end
  elapsed = Time.monotonic - start
  nsop = (elapsed.nanoseconds / niters).ceil.to_i

  puts "#{niters.format} iterations took #{elapsed.nanoseconds.format} ns. #{nsop.humanize} ns/op"
end

scale = 1
(1..30).each do |i|
  scale = 1 << i
  printf("exp %02d: ", i)
  bench(scale)
end
