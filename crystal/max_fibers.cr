# Proving: https://crystal-lang.org/reference/1.11/guides/concurrency.html#a-fiber
#
# Crystal initialise Fiber right away.
# It reserves stack for each Fiber and memory is growing drasticaly with more fibers

# memory_usage returns RSS in KB (1024 unit)
def memory_usage
  `ps -p #{Process.pid} -o rss`.split("\n")[1].strip.to_i
end

def memory_mesure(&)
  start = memory_usage
  yield
  finish = memory_usage
  finish - start
end

def run
  niters = 250_000
  done = Atomic(UInt32).new(0)

  elapsed_memory = memory_mesure do
    # Estimation default stack 4KB
    # Residental memory: 4 KB * 250_000 fibers = 1 GB
    niters.times do
      spawn same_thread: true do
        done.add(1)
      end
    end
  end * 1024
  puts "initialize #{niters} fibers RSS memory change: #{elapsed_memory.humanize_bytes} vs estimated #{(niters * 4 * 1024).humanize_bytes}"
  while done.get != niters
    print "."
    Fiber.yield
  end
  puts
end

puts "before experiment RSS: #{(memory_usage * 1024).humanize_bytes}"
run
puts "after experiment RSS: #{(memory_usage * 1024).humanize_bytes}"
