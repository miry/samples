require "benchmark"

BENCHMARK_SIZE = 10_000

def hashtable_benchmark
  Benchmark.ips(warmup: 2, calculation: 10) do |x|
    x.report("initialize Hash") {
      Hash(Int32, Int32).new(default_value: 0, initial_capacity: BENCHMARK_SIZE)
    }
    x.report("initialize Array") {
      Array(Int32).new(BENCHMARK_SIZE, 0)
    }
    x.report("initialize StaticArray") {
      StaticArray(Int32, BENCHMARK_SIZE).new(0)
    }
  end

  arr = Array(Int32).new(BENCHMARK_SIZE) { |i| i }
  hash = Hash(Int32, Int32).new(default_value: 0, initial_capacity: BENCHMARK_SIZE)
  static = StaticArray(Int32, BENCHMARK_SIZE).new(0)

  BENCHMARK_SIZE.times do |i|
    arr << i
    hash[i] = i
  end

  Benchmark.ips(warmup: 2, calculation: 10) do |x|
    indx = rand(BENCHMARK_SIZE)
    x.report("access to Hash") { hash[indx] }
    x.report("access to Array") { arr[indx] }
    x.report("access to StaticArray") { static[indx] }
  end

  Benchmark.ips(warmup: 2, calculation: 10) do |x|
    indx = rand(BENCHMARK_SIZE)
    x.report("write to Hash") { hash[indx] = indx }
    x.report("write to Array") { arr[indx] = indx }
    x.report("write to StaticArray") { static[indx] = indx }
  end
end

hashtable_benchmark
