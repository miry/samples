package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"
)

func main() {
	err := run("basic function unexpected error as string")
	if err != nil {
		log.Printf("exit error: %+v (string)", err)
	}

	err = run(errors.New("basic function unexpected error as error"))
	if err != nil {
		log.Printf("exit error: %+v (error)", err)
	}

	err = run(123)
	if err != nil {
		log.Printf("exit error: %+v (int)", err)
	}

	err = run_go()
	if err != nil {
		log.Printf("exit error: %+v", err)
	}
	os.Exit(0)
}

func run(raise_err interface{}) (err error) {
	// Catch panic, then convert to error and return it as normal exit
	defer func() {
		if recover_err := recover(); recover_err != nil {
			log.Printf("recover error: %+v", recover_err)
			switch v := recover_err.(type) {
			case string:
				err = errors.New(v)
			case error:
				err = v
			default:
				err = errors.New(fmt.Sprint(v))
			}
		}
	}()

	log.Print("Panicking!")
	panic(raise_err)
	log.Print("Code after panic.")

	return
}

func run_go() (err error) {
	blocker := make(chan struct{})
	go func() {
		defer func() {
			if recover_err := recover(); recover_err != nil {
				log.Printf("recover error from child goroutine: %+v", recover_err)
				switch v := recover_err.(type) {
				case string:
					err = errors.New(v)
				case error:
					err = v
				default:
					err = errors.New(fmt.Sprint(v))
				}
			}
		}()

		log.Println("Panicking!")
		panic("goroutine unexpected error")
		blocker <- struct{}{}
	}()

	select {
	case <-blocker:
		panic("this branch should not happen")
	case <-time.After(time.Second):
		fmt.Printf("goroutine timeouted! err = `%+v`\n", err)
	}

	return
}
