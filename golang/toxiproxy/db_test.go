package main

import (
	"fmt"
	"log"
	"net"
	"sync"
	"testing"
	"time"

	toxiServer "github.com/Shopify/toxiproxy/v2"
	toxiproxy "github.com/Shopify/toxiproxy/v2/client"
	pg "github.com/go-pg/pg/v10"
	"github.com/prometheus/client_golang/prometheus"
)

var db *pg.DB
var toxi *toxiproxy.Client
var proxies map[string]*toxiproxy.Proxy

func DB() *pg.DB {
	if db == nil {
		var err error
		db, err = setupDB(":35432", "sample_test")
		if err != nil {
			log.Panicf("Could not connect to DB: %+v", err)
		}
	}
	return db
}

func connectDB(addr string) *pg.DB {
	return pg.Connect(&pg.Options{
		Addr:     addr,
		User:     "postgres",
		Database: "sample_test",
	})
}

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	fmt.Println("=== SETUP")
	runToxiproxyServer()
	populateProxies()
}

func populateProxies() {
	if toxi == nil {
		toxi = toxiproxy.NewClient("localhost:8474")
	}

	var err error
	_, err = toxi.Populate([]toxiproxy.Proxy{{
		Name:     "postgresql",
		Listen:   "localhost:35432",
		Upstream: "localhost:5432",
		Enabled:  true,
	}})
	if err != nil {
		panic(err)
	}

	proxies, err = toxi.Proxies()
	if err != nil {
		panic(err)
	}
}

func runToxiproxyServer() {
	var err error
	timeout := 5 * time.Second

	// Check if there is instance run
	conn, err := net.DialTimeout("tcp", "localhost:8474", timeout)
	if err == nil {
		conn.Close()
		return
	}

	go func() {
		metricsContainer := toxiServer.NewMetricsContainer(prometheus.NewRegistry())
		server := toxiServer.NewServer(metricsContainer)
		server.Listen("localhost", "8474")
	}()

	for i := 0; i < 10; i += 1 {
		conn, err := net.DialTimeout("tcp", "localhost:8474", timeout)
		if err == nil {
			conn.Close()
			return
		}
	}
	panic(err)
}

func TestSlowDBConnection(t *testing.T) {
	db := DB()

	// Add 1s latency to 100% of downstream connections
	proxies["postgresql"].AddToxic("latency_down", "latency", "downstream", 1.0, toxiproxy.Attributes{
		"latency": 10000,
	})
	defer proxies["postgresql"].RemoveToxic("latency_down")

	err := process(db)
	if err != nil {
		t.Fatalf("got error %v, wanted no errors", err)
	}
}

func TestOutageResetPeer(t *testing.T) {
	db := DB()

	// Add broken TCP connection
	proxies["postgresql"].AddToxic("reset_peer_down", "reset_peer", "downstream", 1.0, toxiproxy.Attributes{
		"timeout": 10,
	})
	defer proxies["postgresql"].RemoveToxic("reset_peer_down")

	err := process(db)
	if err == nil {
		t.Fatalf("expect error")
	}
}

func setConnectionCut(t *testing.T, proxy *toxiproxy.Proxy) {
	var err error
	_, err = proxy.AddToxic(
		"CUT_CONNECTION_DOWNSTREAM",
		"bandwidth",
		"downstream",
		1,
		toxiproxy.Attributes{"rate": 0},
	)
	if err != nil {
		t.Fatalf("proxy[%s]: could not cut connection downstream: %+v", proxy.Name, err)
	}
	_, err = proxy.AddToxic(
		"CUT_CONNECTION_UPSTREAM",
		"bandwidth",
		"upstream",
		1,
		toxiproxy.Attributes{"rate": 0},
	)
	if err != nil {
		t.Fatalf("proxy[%s]: could not cut connection upstream: %+v", proxy.Name, err)
	}
}

func TestMultipleToxics(t *testing.T) {
	DB()
	dbs := [5]*pg.DB{}

	for i := 0; i < len(dbs); i++ {
		proxy_name := fmt.Sprintf("pg_%d", i)
		db_port := fmt.Sprintf(":200%d", i)
		log.Printf(" >>> proxy[%s] Setup proxy on port %s\n", proxy_name, db_port)

		client := toxiproxy.NewClient("localhost:8474")
		proxy, err := client.CreateProxy(proxy_name, db_port, "localhost:5432")
		if err != nil {
			t.Fatalf("proxy[%s]: Setup got error: %+v", proxy_name, err)
		}

		for j := 0; j < 100; j++ {
			_, err = proxy.AddToxic(
				fmt.Sprintf("%s latency downstream%d", proxy.Name, j),
				"latency",
				"downstream",
				1.0,
				toxiproxy.Attributes{
					"latency": 6000,
					"jitter":  10,
				},
			)
			if err != nil {
				t.Fatalf("proxy[%s]: could not add downstream toxic [%d]: %+v", proxy_name, j, err)
			}

			_, err = proxy.AddToxic(
				fmt.Sprintf("%s latency_upstream%d", proxy.Name, j),
				"latency",
				"upstream",
				1.0,
				toxiproxy.Attributes{
					"latency": 6000,
					"jitter":  10,
				},
			)
			if err != nil {
				t.Fatalf("proxy[%s]: could not add upstream toxic [%d]: %+v", proxy_name, j, err)
			}
		}

		toxics, err := proxy.Toxics()
		if err != nil {
			t.Fatalf("proxy[%s]: could not get toxics: %+v", proxy_name, err)
		}
		log.Printf(" >>>> proxy[%s]: has toxics: %d\n", proxy_name, len(toxics))

		dbs[i] = connectDB(db_port)

		go func(proxy_name string, db *pg.DB) {
			log.Printf(" >>>> proxy[%s]: loading with queries\n", proxy_name)
			for {
				process(db)
			}
		}(proxy_name, dbs[i])
	}

	log.Printf(">> Sleep for 130s\n")
	time.Sleep(130 * time.Second)

	proxies, err := toxi.Proxies()
	if err != nil {
		t.Fatalf(">> Could not get toxiproxy proxies: %+v", err)
	}

	log.Println(">> Cutting connections")
	for _, proxy := range proxies {
		setConnectionCut(t, proxy)
	}

	log.Printf(">> Sleep for 60s\n")
	time.Sleep(60 * time.Second)

	log.Printf(">> Remove toxics\n\n")
	wg := sync.WaitGroup{}

	for _, proxy := range proxies {
		toxics, err := proxy.Toxics()
		if err != nil {
			t.Fatalf(" >>> proxy[%s] could not get toxics: %+v", proxy.Name, err)
		}

		for _, toxic := range toxics {
			toxic_name := toxic.Name
			proxy_name := proxy.Name
			log.Printf(" >>> proxy[%s]: Removing toxic %s\n", proxy_name, toxic_name)
			wg.Add(1)
			go func(proxy *toxiproxy.Proxy, toxic_name string, wg *sync.WaitGroup) {
				err = proxy.RemoveToxic(toxic_name)
				wg.Done()
				if err != nil {
					log.Printf(" >>> ERROR: proxy[%s]: could not remove toxic %s: %+v", proxy_name, toxic_name, err)
				}
				log.Printf(" >>> proxy[%s]: Removing toxic %s - DONE\n", proxy_name, toxic_name)
			}(proxy, toxic_name, &wg)
		}
	}

	wg.Wait()
	log.Printf(">> Remove proxies\n\n")
	for _, proxy := range proxies {
		err = proxy.Delete()
		if err != nil {
			t.Fatalf(" >>> proxy[%s] could not be removed: %+v", proxy.Name, err)
		}
	}
	log.Println(">> Done")
}
