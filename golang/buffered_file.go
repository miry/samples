// Task:
// Creating a decorator for I/O writes involves implementing an interface with two methods: write and flush.
// Our side mission? Ensuring thread safety.
// Sample abstract code:
//
//  f = File.new("path")
//  b = BufferedFile.new(f, buffer_size: 2048)
//  b.write("string")
//  b.flush

package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sync"
	"time"
	"unsafe"
)

func StringToBytes(s string) []byte {
	return unsafe.Slice(unsafe.StringData(s), len(s))
}

type BufferedFile struct {
	buffer *bufio.Writer
	mu     sync.Mutex
}

func NewBufferedFile(f io.Writer, size int) *BufferedFile {
	return &BufferedFile{
		buffer: bufio.NewWriterSize(f, size),
	}
}

// Reuse of standard library bufio
// https://cs.opensource.google/go/go/+/refs/tags/go1.22.0:src/bufio/bufio.go;l=669
func (b *BufferedFile) write(msg string) int {
	b.mu.Lock()
	result, _ := b.buffer.Write(StringToBytes(msg))
	b.mu.Unlock()
	return result
}

func (b *BufferedFile) flush() {
	b.buffer.Flush()
}

func main() {
	f, err := os.Create("test.data")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	b := NewBufferedFile(f, 1024)

	for i := 0; i < 1000; i++ {
		go b.write(fmt.Sprintf("%d Help me!\n", i))
	}

	time.Sleep(time.Second)
	b.flush()
}
