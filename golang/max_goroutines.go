package main

import "time"

func empty() {
	x := 1 + 1
	x++
	return
}

func main() {
	n_threads := 1_000_000_000

	for i := 0; i < n_threads; i++ {
		go empty()
	}
	time.Sleep(60 * time.Second)
}
