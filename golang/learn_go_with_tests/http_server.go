package main

import (
	"log"
	"net/http"

	"codeberg.org/miry/samples/golang/learn_go_with_tests/http_server"
)

func main() {
	server := &http_server.PlayerServer{http_server.NewInMemoryPlayerStore()}
	log.Fatal(http.ListenAndServe(":5000", server))
}
