package http_server_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"codeberg.org/miry/samples/golang/learn_go_with_tests/http_server"
)

func TestServerStart(t *testing.T) {
	store := StubPlayerStore{
		map[string]int{
			"Pepper": 20,
			"Floyd":  10,
		},
		nil,
	}
	server := &http_server.PlayerServer{&store}

	t.Run("returns Pepper's score", func(t *testing.T) {
		req, _ := http.NewRequest("GET", "/players/Pepper", nil)
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)

		assertStatus(t, http.StatusOK, res.Code)
		assertResponseBody(t, "20", res.Body.String())
	})

	t.Run("returns Floyd's score", func(t *testing.T) {
		req, _ := http.NewRequest(http.MethodGet, "/players/Floyd", nil)
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)

		assertStatus(t, http.StatusOK, res.Code)
		assertResponseBody(t, "10", res.Body.String())
	})

	t.Run("returns 404 on missing players", func(t *testing.T) {
		req, _ := http.NewRequest(http.MethodGet, "/players/Apollo", nil)
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)

		assertStatus(t, http.StatusNotFound, res.Code)
	})
}

func TestStoreWins(t *testing.T) {
	store := StubPlayerStore{
		map[string]int{},
		nil,
	}
	server := &http_server.PlayerServer{&store}

	t.Run("it returns accepted on POST", func(t *testing.T) {
		req, _ := http.NewRequest("POST", "/players/Floyd", nil)
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)

		assertStatus(t, http.StatusAccepted, res.Code)
	})

	t.Run("it records winner", func(t *testing.T) {
		old_len_wins := len(store.winCalls)

		req, _ := http.NewRequest("POST", "/players/Pepper", nil)
		res := httptest.NewRecorder()
		server.ServeHTTP(res, req)

		if len(store.winCalls)-old_len_wins != 1 {
			t.Errorf("got %d calls to RecordWin want %d", len(store.winCalls), old_len_wins+1)
		}

		if store.winCalls[old_len_wins] != "Pepper" {
			t.Errorf("Expected to record player Pepper in winCalls, got %s", store.winCalls[old_len_wins])
		}
	})
}

func assertResponseBody(t testing.TB, expected, actual string) {
	t.Helper()
	if actual != expected {
		t.Errorf("response body is wrong, got %q want %q", actual, expected)
	}
}

func assertStatus(t testing.TB, expected, actual int) {
	t.Helper()
	if actual != expected {
		t.Errorf("response status is wrong, got %d want %d", actual, expected)
	}
}

type StubPlayerStore struct {
	scores   map[string]int
	winCalls []string
}

func (s *StubPlayerStore) GetScore(name string) int {
	score := s.scores[name]
	return score
}

func (s *StubPlayerStore) IncrementScore(name string) {
	s.winCalls = append(s.winCalls, name)
}
