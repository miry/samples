package http_server_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"codeberg.org/miry/samples/golang/learn_go_with_tests/http_server"
)

func TestRecordingWinsAndRetrievingThem(t *testing.T) {
	store := http_server.NewInMemoryPlayerStore()
	server := http_server.PlayerServer{store}
	req, _ := http.NewRequest("POST", "/player/Pfeffer", nil)

	for i := 0; i < 3; i++ {
		res := httptest.NewRecorder()
		server.ServeHTTP(res, req)
		assertStatus(t, 202, res.Code)
	}

	req, _ = http.NewRequest("GET", "/player/Pfeffer", nil)
	res := httptest.NewRecorder()
	server.ServeHTTP(res, req)

	assertStatus(t, http.StatusOK, res.Code)
	assertResponseBody(t, "3", res.Body.String())
}
