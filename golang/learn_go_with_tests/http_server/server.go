package http_server

import (
	"fmt"
	"net/http"
	"strings"
)

type PlayerStore interface {
	GetScore(name string) int
	IncrementScore(name string)
}

type PlayerServer struct {
	Store PlayerStore
}

func (p *PlayerServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	player := strings.TrimPrefix(r.URL.Path, "/players/")

	if r.Method == "POST" {
		p.create(w, player)
	} else if r.Method == "GET" {
		p.show(w, player)
	}
}

func (p *PlayerServer) show(w http.ResponseWriter, player string) {
	score := p.Store.GetScore(player)

	if score == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	fmt.Fprint(w, score)
}

func (p *PlayerServer) create(w http.ResponseWriter, player string) {
	p.Store.IncrementScore(player)
	w.WriteHeader(http.StatusAccepted)
}
