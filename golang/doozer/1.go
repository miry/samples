// DOOZER Application test
// Please adjust the code as required in the task description and share us the link to your solution.
package main

import "fmt"

type Result struct {
	Data *Data
}

type Data struct {
	ID int
}

func BuildResults() []Result {
	list := make([]Result, 10, 10)

	for i := 0; i < 10; i++ {
		list[i] = Result{&Data{i}}
	}

	return list
}

// 1.
// Write a function called BuildResultsFixed based on the BuildResults but resulting in the following output:
// - IDs within the Result.Data are: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
func BuildResultsFixed() []Result {
	return BuildResults()
}

func main() {
	fmt.Println("1. Write a function called BuildResultsFixed")
	results := BuildResultsFixed()

	if len(results) != 10 {
		fmt.Printf("10 Results expected, got %d", len(results))
		return
	}

	for i, r := range results {
		if r.Data == nil || r.Data.ID != i {
			fmt.Printf("invalid ID: %d expected: %d got: %d\n", r.Data.ID)
		}
	}

	item := results[1].Data
	results[1].Data.ID = 50

	if item.ID != 50 {
		fmt.Printf("invalid ID after change: %d expected: %d\n", item.ID, 50)
	}
}
