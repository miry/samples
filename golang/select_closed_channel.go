package main

import (
	"log"
)

// Show how select works with closed channels
// Output:
//
//	2022/08/29 15:50:39 result: 1
func main() {
	pipe := make(chan struct{})
	close(pipe)

	result := -1
	select {
	case <-pipe:
		result = 1
	default:
		result = 0
	}

	log.Printf("result: %d", result)
}
