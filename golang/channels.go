package main

import (
	"log"
	"time"
)

type Buffer struct {
	Id  int
	Job int
}

func client(serverChan, freeList chan *Buffer) {
	i := 0
	job := 0
	for {
		if job > 1000 {
			return
		}

		var b *Buffer
		// Grab a buffer if available; allocate if not.
		select {
		case b = <-freeList:
			// Got one; nothing more to do.
		default:
			// None free, so allocate a new one.
			b = &Buffer{i, 0}
			i++
		}
		load(b, job) // Read next message from the net.
		job++
		select {
		case serverChan <- b: // Send to server.
		default:
			log.Println("Could not send message. Discard job.", b.Id, b.Job, len(serverChan))
			freeList <- b // reuse buffer
		}
	}
}

func server(serverChan, freeList chan *Buffer, finish chan int) {
	// Gives time to add jobs to serverChan from client
	time.Sleep(1 * time.Millisecond)

	for {
		select {
		case b := <-serverChan:
			process(b)
			select {
			case freeList <- b:
				log.Printf("Return worker %d to the pool of workers %d", b.Id, len(freeList))
				// Buffer on free list; nothing more to do.
				// Return worker back to the list for reusage.
			default:
				log.Printf("Freelist is full: %d\n", len(freeList))
				// Free list full, just carry on.
				// Remove extra worker as there is enough
			}
		default:
			log.Println("No Job")
			finish <- 1
			time.Sleep(5 * time.Millisecond)
		}
	}
}

func load(b *Buffer, job int) {
	b.Job = job
	log.Println("Load Buffer :", b.Id, job)
}

func process(b *Buffer) {
	log.Println("Process Buffer :", b.Id, b.Job)
	time.Sleep(10 * time.Millisecond)
}

func main() {
	// Jobs pipeline: What to do
	serverChan := make(chan *Buffer, 10)

	// List of workers to process jobs: Who to do
	freeList := make(chan *Buffer, 100)

	// Signal when everything processed
	finished := make(chan int)

	go server(serverChan, freeList, finished)
	client(serverChan, freeList)

	<-finished
	log.Println("Finished")
}
