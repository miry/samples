package main

// https://golang.org/cmd/objdump/
// go build -o stacktrace stacktrace.go
// go tool objdump -S -s "main." ./stacktrace
//
// 		TEXT main.main(SB) samples/golang/stacktrace.go
// 		func main() {
// 			0x100056880		f9400b90		MOVD 16(R28), R16
// 			0x100056884		eb3063ff		CMP R16, RSP
// 			0x100056888		54000269		BLS 19(PC)
// 			0x10005688c		f8180ffe		MOVD.W R30, -128(RSP)
// 			0x100056890		f81f83fd		MOVD R29, -8(RSP)
// 			0x100056894		d10023fd		SUB $8, RSP, R29
// 			example(make([]string, 2, 4), "hello", 10)
// 			0x100056898		a903ffff		STP (ZR, ZR), 56(RSP)
// 			0x10005689c		a904ffff		STP (ZR, ZR), 72(RSP)
// 			0x1000568a0		a905ffff		STP (ZR, ZR), 88(RSP)
// 			0x1000568a4		a906ffff		STP (ZR, ZR), 104(RSP)
// 			0x1000568a8		9100e3e0		ADD $56, RSP, R0
// 			0x1000568ac		b27f03e1		ORR $2, ZR, R1
// 			0x1000568b0		b27e03e2		ORR $4, ZR, R2
// 			0x1000568b4		90000003		ADRP 0(PC), R3
// 			0x1000568b8		9131bc63		ADD $3183, R3, R3
// 			0x1000568bc		d28000a4		MOVD $5, R4
// 			0x1000568c0		d2800145		MOVD $10, R5
// 			0x1000568c4		94000007		CALL main.example(SB)
// 		}
// 			0x1000568c8		a97ffbfd		LDP -8(RSP), (R29, R30)
// 			0x1000568cc		910203ff		ADD $128, RSP, RSP
// 			0x1000568d0		d65f03c0		RET
// 		func main() {
// 			0x1000568d4		aa1e03e3		MOVD R30, R3
// 			0x1000568d8		97fff60a		CALL runtime.morestack_noctxt.abi0(SB)
// 			0x1000568dc		17ffffe9		JMP main.main(SB)
//
// 		TEXT main.example(SB) samples/golang/stacktrace.go
// 		func example(slice []string, str string, i int) {
// 			0x1000568e0		f9400b90		MOVD 16(R28), R16
// 			0x1000568e4		eb3063ff		CMP R16, RSP
// 			0x1000568e8		54000149		BLS 10(PC)
// 			0x1000568ec		f81e0ffe		MOVD.W R30, -32(RSP)
// 			0x1000568f0		f81f83fd		MOVD R29, -8(RSP)
// 			0x1000568f4		d10023fd		SUB $8, RSP, R29
// 			panic("Want stack trace")
// 			0x1000568f8		d00000e0		ADRP 122880(PC), R0
// 			0x1000568fc		91190000		ADD $1600, R0, R0
// 			0x100056900		90000141		ADRP 163840(PC), R1
// 			0x100056904		91214021		ADD $2128, R1, R1
// 			0x100056908		97ff512a		CALL runtime.gopanic(SB)
// 			0x10005690c		d503201f		NOOP
// 		func example(slice []string, str string, i int) {
// 			0x100056910		aa1e03e3		MOVD R30, R3
// 			0x100056914		97fff5fb		CALL runtime.morestack_noctxt.abi0(SB)
// 			0x100056918		17fffff2		JMP main.example(SB)
// 			0x10005691c		00000000		?

func main() {
	example(make([]string, 2, 4), "hello", 10)
}

// Add next comment to make sure there is no optimisation happen and function was not included in main.
//
//go:noinline
func example(slice []string, str string, i int) {
	panic("Want stack trace")
}
