require "minitest/autorun"

require "./binary_heap"

class BinaryHeapTest < Minitest::Test
  def test_empty
    heap = BinaryHeap(Int32).new
    assert_equal true, heap.empty?

    heap.insert(1)
    assert_equal false, heap.empty?
  end

  def test_insert
    heap = BinaryHeap(Char).new
    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')

    assert_equal false, heap.empty?
  end

  def test_pop
    heap = BinaryHeap(Char).new
    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')

    assert_equal 'Q', heap.pop
  end

  def test_max_operations
    heap = BinaryHeap(Char).new(9) { |x, y| x > y }

    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')
    assert_equal 'Q', heap.pop

    heap.insert('X')
    heap.insert('A')
    heap.insert('M')
    assert_equal 'X', heap.pop

    heap.insert('P')
    heap.insert('L')
    heap.insert('E')
    assert_equal 'P', heap.pop
  end

  def test_min_operations
    heap = BinaryHeap(Char).new(9) { |x, y| x < y }

    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')
    assert_equal 'E', heap.pop

    heap.insert('X')
    heap.insert('A')
    heap.insert('M')
    assert_equal 'A', heap.pop

    heap.insert('P')
    heap.insert('L')
    heap.insert('E')
    assert_equal 'E', heap.pop
  end
end
