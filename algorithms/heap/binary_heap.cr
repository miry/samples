# Course: https://www.coursera.org/learn/algorithms-part1/lecture/Uzwy6/binary-heaps
# References:
#   * https://www.geeksforgeeks.org/binary-heap/
class BinaryHeap(T)
  private INITIAL_CAPACITY = 16u32
  getter size : Int32 = 0

  def initialize(initial_capacity : Int = INITIAL_CAPACITY)
    @compare_fn = ->(x : T, y : T) { x > y }
    initialize(initial_capacity, &@compare_fn)
  end

  def initialize(initial_capacity : Int = INITIAL_CAPACITY, &@compare_fn : Proc(T, T, Bool))
    @size = 0
    @store = Array(T).new(initial_capacity: initial_capacity + 1)
    # Binary heap start indexes from 1
    x = uninitialized T
    @store.push(x)
  end

  def empty?
    @size == 0
  end

  def insert(x : T)
    @size += 1
    if @size < @store.size
      @store[@size] = x
    else
      @store.push(x)
    end
    swim
  end

  # Deletes the Top element from the Heap.
  def pop : T?
    return nil if @size == 0

    result = @store[1]
    @store[1] = @store[@size]
    @size -= 1

    sink(1)

    return result
  end

  private def swap(a, b)
    @store[a], @store[b] = @store[b], @store[a]
  end

  private def swim
    current = @size
    parent = current // 2
    while current > 1 && @compare_fn.call(@store[current], @store[parent])
      swap(current, parent)
      current, parent = parent, parent // 2
    end
  end

  private def sink(start)
    current = start
    left = current * 2
    while left <= @size
      right = left + 1

      left = right if right <= @size && @compare_fn.call(@store[right], @store[left])
      break if @compare_fn.call(@store[current], @store[left])

      swap(current, left)
      current = left
      left = current * 2
    end
  end
end
