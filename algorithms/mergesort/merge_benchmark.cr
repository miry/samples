require "minitest/autorun"
require "benchmark"

require "./merge"
require "./merge_concurrent"

class MergeBenchmarkTest < Minitest::Test
  def test_insert
    size = 10_000_000

    rnd = Random.new
    original = Array(Int32).new(size) { rnd.next_int }

    r = Benchmark.ips(interactive: false) do |x|
      x.report("merge sort") { d = original.dup; Merge.sort(d) }
      x.report("merge concurrent sort") { d = original.dup; MergeConcurrent.sort(d) }
    end

    m1 = r.items.find! &.label.starts_with?("merge concurrent")
    m2 = r.items.find! &.label.starts_with?("merge sort")

    assert m1.slower < m2.slower, "Expected #{m2.label} (#{m2.slower}) be slower than #{m1.label} (#{m1.slower})"
  end
end
