class Merge
  def self.sort(arr)
    aux = arr.dup
    sort_part(arr, aux, 0, arr.size - 1)
    arr
  end

  def self.merge(arr, aux, lo : Int, mid : Int, hi : Int)
    lo.upto(hi) do |i|
      aux[i] = arr[i]
    end

    i = lo
    j = mid + 1
    n = hi - lo + 1
    lo.upto(hi) do |k|
      if i > mid
        arr[k] = aux[j]
        j += 1
      elsif j > hi
        arr[k] = aux[i]
        i += 1
      elsif aux[i] > aux[j]
        arr[k] = aux[j]
        j += 1
      else
        arr[k] = aux[i]
        i += 1
      end
    end
  end

  def self.sort_part(arr, aux, lo : Int, hi : Int)
    if hi - lo > 1
      mid = lo + (hi - lo) // 2
      sort_part(arr, aux, lo, mid)
      sort_part(arr, aux, mid + 1, hi)
      merge(arr, aux, lo, mid, hi)
    else
      if arr[lo] > arr[hi]
        arr[lo], arr[hi] = arr[hi], arr[lo]
      end
    end
  end
end
