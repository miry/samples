require "./merge"

class MergeConcurrent
  def self.sort(arr, deep : Int32 = 4)
    aux = arr.dup
    done = Channel(Nil).new(1)
    sort_part(arr, aux, 0, arr.size - 1, done, deep)

    arr
  end

  def self.sort_part(arr, aux, lo : Int, hi : Int, done : Channel(Nil), deep = 10)
    return Merge.sort_part(arr, aux, lo, hi) if deep < 0

    if hi - lo > 1
      mid = lo + (hi - lo) // 2
      left, right = Channel(Nil).new(1), Channel(Nil).new(1)
      spawn { MergeConcurrent.sort_part(arr, aux, lo, mid, left, deep - 1) }
      spawn { MergeConcurrent.sort_part(arr, aux, mid + 1, hi, right, deep - 1) }
      left.receive
      right.receive
      Merge.merge(arr, aux, lo, mid, hi)
      sleep 0.5
    else
      arr[lo], arr[hi] = arr[hi], arr[lo] if arr[lo] > arr[hi]
    end
  ensure
    done.send(nil)
  end
end
