require "minitest/autorun"

require "./merge_concurrent"

# require "../../crystal/fiber_debug"

class MergeConcurrentTest < Minitest::Test
  def test_small_array_use_default
    subject = Array(Int32).new(1024, 1)
    actual = MergeConcurrent.sort(subject)
    assert_equal subject.sort, actual
  end

  def test_two_batches
    subject = Array(Int32).new(9) { |i| -i }
    actual = MergeConcurrent.sort(subject)
    assert_equal subject.sort, actual
  end

  def test_a
    r = Random.new
    subject = Array(Int32).new(2_000_000) { r.next_int }
    puts "array was genereated"
    # Merge.sort(subject)
    MergeConcurrent.sort(subject, batch_size: 256)
  end

  def test_big_array
    r = Random.new
    subject = Array(Int32).new(10_000_000) { r.next_int }
    MergeConcurrent.sort(subject, 10)
    assert_equal subject.sort, subject
  end
end
