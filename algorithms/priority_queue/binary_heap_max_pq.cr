require "../heap/binary_heap"

class BinaryHeapMaxPQ(T)
  private INITIAL_CAPACITY = 16u32

  def initialize(initial_capacity : Int = INITIAL_CAPACITY)
    @heap = BinaryHeap(T).new(initial_capacity)
  end

  def empty?
    @heap.size == 0
  end

  def insert(x : T)
    @heap.insert(x)
  end

  def pop
    @heap.pop
  end
end
