class OrderedMaxPQ(T)
  private INITIAL_CAPACITY = 16u32

  def initialize(initial_capacity : Int = INITIAL_CAPACITY)
    @store = Array(T).new(initial_capacity)
  end

  def empty?
    @store.size == 0
  end

  def insert(x : T)
    indx = search(x) || @store.size
    @store.insert(indx, x)
  end

  def pop
    @store.pop?
  end

  private def search(val)
    @store.bsearch_index { |x, _| x > val }
  end
end
