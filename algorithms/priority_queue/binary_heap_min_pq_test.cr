require "minitest/autorun"

require "./binary_heap_min_pq"

class BinaryHeapMinPQTest < Minitest::Test
  def test_empty
    heap = BinaryHeapMinPQ(Int32).new
    assert_equal true, heap.empty?
  end

  def test_insert
    heap = BinaryHeapMinPQ(Char).new
    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')

    assert_equal false, heap.empty?
  end

  def test_pop
    heap = BinaryHeapMinPQ(Char).new
    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')

    assert_equal 'E', heap.pop
  end

  def test_pop_empty
    heap = BinaryHeapMinPQ(Char).new
    assert_nil heap.pop
  end
end
