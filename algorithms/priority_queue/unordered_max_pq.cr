class UnorderedMaxPQ(T)
  private INITIAL_CAPACITY = 16u32

  def initialize(initial_capacity : Int = INITIAL_CAPACITY)
    @store = Array(T).new(initial_capacity)
  end

  def empty?
    @store.size == 0
  end

  def insert(x : T)
    @store.push(x)
  end

  def pop
    return nil if @store.size == 0
    max = @store.last
    max_id = @store.size - 1
    @store.each_with_index do |c, i|
      if c > max
        max = c
        max_id = i
      end
    end
    @store.delete_at(max_id)
  end
end
