require "minitest/autorun"

require "./ordered_pq"

class OrderedPQTest < Minitest::Test
  def test_empty
    pq = OrderedPQ(Int32).new
    assert_equal true, pq.empty?
  end

  def test_insert
    pq = OrderedPQ(Char).new
    pq.insert('P')
    pq.insert('Q')
    pq.insert('E')

    assert_equal false, pq.empty?
  end

  def test_max_pop
    max_pq = OrderedPQ(Char).new { |x, y| x > y }
    max_pq.insert('P')
    max_pq.insert('Q')
    max_pq.insert('E')

    assert_equal 'Q', max_pq.pop
  end

  def test_min_pop
    min_pq = OrderedPQ(Char).new { |x, y| x < y }
    min_pq.insert('P')
    min_pq.insert('Q')
    min_pq.insert('E')

    assert_equal 'E', min_pq.pop
  end
end
