require "minitest/autorun"

require "./binary_heap_max_pq"

class BinaryHeapMaxPQTest < Minitest::Test
  def test_empty
    heap = BinaryHeapMaxPQ(Int32).new
    assert_equal true, heap.empty?
  end

  def test_insert
    heap = BinaryHeapMaxPQ(Char).new
    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')

    assert_equal false, heap.empty?
  end

  def test_pop
    heap = BinaryHeapMaxPQ(Char).new
    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')

    assert_equal 'Q', heap.pop
  end

  def test_pop_empty
    heap = BinaryHeapMaxPQ(Char).new
    assert_nil heap.pop
  end
end
