class OrderedPQ(T)
  def initialize(initial_capacity : Int, &@compare_fn : Proc(T, T, Bool))
    @store = Array(T).new(initial_capacity)
  end

  def initialize(initial_capacity : Int)
    @compare_fn = ->(x : T, y : T) { (x <=> y) == 1 }
    @store = Array(T).new(initial_capacity)
  end

  def initialize(&@compare_fn : Proc(T, T, Bool))
    @store = Array(T).new(16)
  end

  def initialize
    @compare_fn = ->(x : T, y : T) { (x <=> y) == 1 }
    @store = Array(T).new(16)
  end

  def empty?
    @store.size == 0
  end

  def insert(x : T)
    indx = search(x) || @store.size
    @store.insert(indx, x)
  end

  def pop
    @store.pop
  end

  def size
    @store.size
  end

  private def search(val)
    @store.bsearch_index { |x, _| @compare_fn.call(x, val) }
  end
end
