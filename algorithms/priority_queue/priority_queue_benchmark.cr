require "minitest/autorun"
require "benchmark"

require "./binary_heap_max_pq"
require "./ordered_max_pq"
require "./unordered_max_pq"

class PriorityQueueBenchmarkTest < Minitest::Test
  def test_insert
    size = 1_000

    unordered = UnorderedMaxPQ(Int32).new(size)
    ordered = OrderedMaxPQ(Int32).new(size)
    heap = BinaryHeapMaxPQ(Int32).new(size)

    rnd = Random.new
    values = Array(Int32).new(size) { rnd.next_int }

    r = Benchmark.ips(interactive: false) do |x|
      x.report("unordered array/insert") { values.each { |i| unordered.insert(i) } }
      x.report("ordered array/insert") { values.each { |i| ordered.insert(i) } }
      x.report("binary heap/insert") { values.each { |i| heap.insert(i) } }
    end

    m1 = r.items.find! &.label.starts_with?("unordered array")
    m2 = r.items.find! &.label.starts_with?("ordered array")
    m3 = r.items.find! &.label.starts_with?("binary heap")

    assert m1.slower < m2.slower, "Expected #{m2.label} (#{m2.slower}) be slower than #{m1.label} (#{m1.slower})"
    assert m3.slower < m2.slower, "Expected #{m2.label} (#{m2.slower}) be slower than #{m3.label} (#{m3.slower})"
  end

  def test_pop
    size = 50_000

    unordered = UnorderedMaxPQ(Int32).new(size)
    ordered = OrderedMaxPQ(Int32).new(size)
    heap = BinaryHeapMaxPQ(Int32).new(size)

    rnd = Random.new
    size.times do
      x = rnd.next_int
      unordered.insert(x)
      ordered.insert(x)
      heap.insert(x)
    end

    m1 = Benchmark.measure("unordered array/pop") { size.times { unordered.pop } }
    m2 = Benchmark.measure("ordered array/pop") { size.times { ordered.pop } }
    m3 = Benchmark.measure("binary heap/pop") { size.times { heap.pop } }

    assert m1.real > m2.real, "Expected #{m1.label} (#{m1.real}) be slower than #{m2.label} (#{m2.real})"
    assert m3.real > m2.real, "Expected #{m3.label} (#{m3.real}) be slower than #{m2.label} (#{m2.real})"
    assert m1.real > m3.real, "Expected #{m1.label} (#{m1.real}) be slower than #{m3.label} (#{m3.real})"
  end
end
