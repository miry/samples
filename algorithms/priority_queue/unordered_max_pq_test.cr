require "minitest/autorun"

require "./unordered_max_pq"

class UnorderedMaxPQTest < Minitest::Test
  def test_empty
    heap = UnorderedMaxPQ(Int32).new
    assert_equal true, heap.empty?
  end

  def test_insert
    heap = UnorderedMaxPQ(Char).new
    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')

    assert_equal false, heap.empty?
  end

  def test_pop
    heap = UnorderedMaxPQ(Char).new
    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')

    assert_equal 'Q', heap.pop
  end

  def test_pop_empty
    heap = UnorderedMaxPQ(Char).new
    assert_nil heap.pop
  end
end
