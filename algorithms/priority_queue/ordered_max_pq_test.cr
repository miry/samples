require "minitest/autorun"

require "./ordered_max_pq"

class OrderedMaxPQTest < Minitest::Test
  def test_empty
    heap = OrderedMaxPQ(Int32).new
    assert_equal true, heap.empty?
  end

  def test_insert
    heap = OrderedMaxPQ(Char).new
    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')

    assert_equal false, heap.empty?
  end

  def test_pop
    heap = OrderedMaxPQ(Char).new
    heap.insert('P')
    heap.insert('Q')
    heap.insert('E')

    assert_equal 'Q', heap.pop
  end

  def test_pop_empty
    heap = OrderedMaxPQ(Char).new
    assert_nil heap.pop
  end
end
