package leetcode

import (
	"container/heap"
)

type MedianFinder interface {
	AddNum(num int)
	FindMedian() float64
}

func NewMedianFinder(solution string) MedianFinder {
	switch solution {
	case "array":
		return &MedianFinderArray{make([]int, 0, 10)}
	case "heap":
		h1 := &intMaxHeap{}
		h2 := &intMinHeap{}
		heap.Init(h1)
		heap.Init(h2)
		return &MedianFinderHeap{
			h1,
			h2,
			0,
		}
	}
	panic("unknown solution")
	return nil
}

type MedianFinderArray struct {
	buffer []int
}

func (this *MedianFinderArray) AddNum(num int) {
	mid := 0
	low, high := 0, len(this.buffer)-1
	for low <= high {
		mid = low + (high-low)/2

		if this.buffer[mid] == num {
			break
		}

		if this.buffer[mid] < num {
			low = mid + 1
		} else {
			high = mid - 1
		}
	}

	if len(this.buffer) == 0 {
		this.buffer = append(this.buffer, num)
	} else {
		if this.buffer[mid] < num {
			this.buffer = append(this.buffer[:mid+1], this.buffer[mid:]...)
			this.buffer[mid+1] = num
		} else {
			if mid == 0 {
				this.buffer = append(this.buffer, 0)
				copy(this.buffer[1:], this.buffer)
				this.buffer[0] = num
			} else {
				this.buffer = append(this.buffer[:mid], this.buffer[mid-1:]...)
				this.buffer[mid] = num
			}
		}
	}
}

func (this *MedianFinderArray) FindMedian() float64 {
	size := len(this.buffer)
	if size%2 == 0 {
		n := size / 2
		n1 := n - 1
		return float64(this.buffer[n]+this.buffer[n1]) / 2.0
	} else {
		return float64(this.buffer[size/2])
	}
}

type MedianFinderHeap struct {
	h1   *intMaxHeap
	h2   *intMinHeap
	size int
}

func (this *MedianFinderHeap) AddNum(num int) {
	if this.h1.Len() == 0 || (*this.h1)[0] > num {
		heap.Push(this.h1, num)
		for this.h1.Len()-this.h2.Len() > 1 {
			heap.Push(this.h2, heap.Pop(this.h1))
		}
	} else {
		heap.Push(this.h2, num)
		for this.h2.Len()-this.h1.Len() > 0 {
			heap.Push(this.h1, heap.Pop(this.h2))
		}
	}
	this.size++
}

func (this *MedianFinderHeap) FindMedian() float64 {
	if this.size == 0 {
		return 0.0
	}

	if this.h1.Len() > this.h2.Len() {
		return float64((*this.h1)[0])
	} else if this.h1.Len() < this.h2.Len() {
		return float64((*this.h2)[0])
	}

	return float64((*this.h2)[0]+(*this.h1)[0]) / 2.0
}

// An intMinHeap is a min-heap of ints.
type intMinHeap []int

func (h intMinHeap) Len() int           { return len(h) }
func (h intMinHeap) Less(i, j int) bool { return h[i] < h[j] }
func (h intMinHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *intMinHeap) Push(x any) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(int))
}

func (h *intMinHeap) Pop() any {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

// An intMaxHeap is a max-heap of ints.
type intMaxHeap []int

func (h intMaxHeap) Len() int           { return len(h) }
func (h intMaxHeap) Less(i, j int) bool { return h[i] > h[j] }
func (h intMaxHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *intMaxHeap) Push(x any) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(int))
}

func (h *intMaxHeap) Pop() any {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}
