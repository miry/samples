# frozen_string_literal: true

require "minitest/autorun"

require_relative "739_daily_temperatures"

class DailyTemperaturesTest < Minitest::Test
  def test_one
    input = [73, 74, 75, 71, 69, 72, 76, 73]
    actual = daily_temperatures(input)
    expected = [1, 1, 4, 2, 1, 1, 0, 0]
    assert_equal expected, actual
  end

  def test_two
    input = [30, 40, 50, 60]
    actual = daily_temperatures(input)
    expected = [1, 1, 1, 0]
    assert_equal expected, actual
  end

  def test_three
    input = [30, 60, 90]
    actual = daily_temperatures(input)
    expected = [1, 1, 0]
    assert_equal expected, actual
  end
end
