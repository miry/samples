# Runtime: 96 ms, faster than 94.22% of Ruby online submissions for Top K Frequent Elements.
# Memory Usage: 211.9 MB, less than 93.64% of Ruby online submissions for Top K Frequent Elements.

# @param {Integer[]} nums
# @param {Integer} k
# @return {Integer[]}
def top_k_frequent(nums, k)
  nums.tally.to_a.sort! { |a, b| b[1] <=> a[1] }[..k - 1].map! { |a| a[0] }
end

# Bucket Sort example
# @param {Integer[]} nums
# @param {Integer} k
# @return {Integer[]}
def top_k_frequent(nums, k)
  f = nums.tally
  sort = []
  f.each do |v, freq|
    sort[freq] ||= []
    sort[freq].push(v)
  end
  result = []
  (sort.size - 1).downto(0) do |i|
    next if sort[i].nil?
    result += sort[i]
    k -= sort[i].size
    break if k <= 0
  end

  result
end
