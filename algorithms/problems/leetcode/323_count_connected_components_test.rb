require "minitest/autorun"
require_relative "323_count_connected_components"

class CountConnectedComponenetsTest < Minitest::Test
  def test_check_bits_for_zero
    assert_equal 1, count_components(3, [[0, 1], [0, 2]])
  end

  def test_sample_two
    assert_equal 2, count_components(6, [[0, 1], [1, 2], [2, 3], [4, 5]])
  end
end
