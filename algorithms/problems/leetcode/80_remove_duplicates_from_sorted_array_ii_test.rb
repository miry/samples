require "minitest/autorun"
require_relative "80_remove_duplicates_from_sorted_array_ii"

class RemoveDuplicatesTest < Minitest::Test
  def test_one
    nums = [1, 1, 1, 2, 2, 3]
    actual = remove_duplicates(nums)
    assert_equal 5, actual
    assert_equal [1, 1, 2, 2, 3], nums[0, 5]
  end

  def test_two
    nums = [0, 0, 1, 1, 1, 1, 2, 3, 3]
    actual = remove_duplicates(nums)
    assert_equal 7, actual
    assert_equal [0, 0, 1, 1, 2, 3, 3], nums[0, 7]
  end

  def test_three
    nums = [1]
    actual = remove_duplicates(nums)
    assert_equal 1, actual
    assert_equal [1], nums
  end
end
