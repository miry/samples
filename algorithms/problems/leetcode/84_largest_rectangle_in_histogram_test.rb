require "minitest/autorun"
require_relative "84_largest_rectangle_in_histogram"

class LargestRectagnleHistogramTest < Minitest::Test
  def test_one
    heights = [2, 1, 5, 6, 2, 3]
    actual = largest_rectangle_area(heights)
    assert_equal 10, actual
  end

  def test_two
    heights = [2, 4]
    actual = largest_rectangle_area(heights)
    assert_equal 4, actual
  end

  def test_three
    heights = [1, 1]
    actual = largest_rectangle_area(heights)
    assert_equal 2, actual
  end

  def test_four
    heights = [2, 1, 2]
    actual = largest_rectangle_area(heights)
    assert_equal 3, actual
  end

  def test_five
    heights = [3, 6, 5, 7, 4, 8, 1, 0]
    actual = largest_rectangle_area(heights)
    assert_equal 20, actual
  end
end
