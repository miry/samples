# NOTE: Uses stupid string reverse method, while ruby doing not good with -1 % 10 or -1 / 10.
#       -1.divmod(10) => [-1, 9]
def reverse(x)
  max_int = 2**31 - 1
  min_int = -2**31

  sign = 1
  if x < 0
    sign = -1
    x = -x
  end
  result = x.to_s.reverse.to_i * sign
  return 0 if result > max_int || result < min_int
  result
end
