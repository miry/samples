# 124. Binary Tree Maximum Path Sum
# https://leetcode.com/problems/binary-tree-maximum-path-sum/description/
#
# A path in a binary tree is a sequence of nodes where each pair of adjacent nodes in the sequence has an edge connecting them.
# A node can only appear in the sequence at most once. Note that the path does not need to pass through the root.
#
# The path sum of a path is the sum of the node's values in the path.
#
# Given the root of a binary tree, return the maximum path sum of any non-empty path.
#
# Example 1:
#
# Input: root = [1,2,3]
# Output: 6
# Explanation: The optimal path is 2 -> 1 -> 3 with a path sum of 2 + 1 + 3 = 6.
#
# Example 2:
#
# Input: root = [-10,9,20,null,null,15,7]
# Output: 42
# Explanation: The optimal path is 15 -> 20 -> 7 with a path sum of 15 + 20 + 7 = 42.
#
class TreeNode
  attr_accessor :val, :left, :right
  def initialize(val = 0, left = nil, right = nil)
    @val = val
    @left = left
    @right = right
  end

  def self.parse(arr)
    return TreeNode.new(nil) if arr.nil? || arr.size == 0
    build_node(arr, arr.size, 0)
  end

  def self.build_node(arr, n, i)
    return nil if i >= n
    shift = (i + 1) * 2 - 1
    TreeNode.new(arr[i], build_node(arr, n, shift), build_node(arr, n, shift + 1))
  end
end

# @param {TreeNode} root
# @return {Integer}
def max_path_sum(root)
  dfs(root).first
end

def dfs(node)
  return [node&.val, 0] if node.nil? || node.val.nil?

  max = node.val

  lmax, lsum = dfs(node.left)
  lsum = 0 if lsum < 0
  max = lmax if lmax && lmax > max

  rmax, rsum = dfs(node.right)
  rsum = 0 if rsum < 0
  max = rmax if rmax && rmax > max

  split = lsum + rsum + node.val
  max = split if split > max

  lsum = rsum if rsum > lsum
  ohne = lsum + node.val
  [max, ohne]
end
