# 27. Remove Element
#
# Given an integer array nums and an integer val,
# remove all occurrences of val in nums in-place.
# The order of the elements may be changed.
# Then return the number of elements in nums which are not equal to val.

def remove_element(nums, val)
  turtle, rabbit = 0, 0
  while rabbit < nums.size
    if nums[rabbit] != val
      nums[turtle] = nums[rabbit]
      turtle += 1
    end
    rabbit += 1
  end
  turtle
end
