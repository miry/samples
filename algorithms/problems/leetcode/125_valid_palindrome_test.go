package leetcode_test

import (
	"crypto/rand"
	"fmt"
	"testing"
	"unsafe"

	"codeberg.org/miry/samples/algorithms/problems/leetcode"
)

func TestIsPalindrome(t *testing.T) {
	tests := []struct {
		input    string
		expected bool
	}{
		{"a", true},
		{"A man, a plan, a canal: Panama", true},
		{"race a car", false},
	}

	t.Run("IsPalindromeWithRE", func(t *testing.T) {
		for _, tt := range tests {
			actual := leetcode.IsPalindromeWithRE(tt.input)
			if tt.expected != actual {
				t.Errorf("for %s expected %v, got %v", tt.input, tt.expected, actual)
			}
		}
	})

	t.Run("IsPalindromeWithCompareStrings", func(t *testing.T) {
		for _, tt := range tests {
			actual := leetcode.IsPalindromeWithCompareStrings(tt.input)
			if tt.expected != actual {
				t.Errorf("for %s expected %v, got %v", tt.input, tt.expected, actual)
			}
		}
	})

	t.Run("IsPalindromeWithExtraSpace", func(t *testing.T) {
		for _, tt := range tests {
			actual := leetcode.IsPalindromeWithExtraSpace(tt.input)
			if tt.expected != actual {
				t.Errorf("for %s expected %v, got %v", tt.input, tt.expected, actual)
			}
		}
	})

	t.Run("IsPalindrome", func(t *testing.T) {
		for _, tt := range tests {
			actual := leetcode.IsPalindromeWithExtraSpace(tt.input)
			if tt.expected != actual {
				t.Errorf("for %s expected %v, got %v", tt.input, tt.expected, actual)
			}
		}
	})
}

func BenchmarkIsPalindromeRandomData(b *testing.B) {
	for _, size := range []int{1, 10, 100, 1000, 10000, 100000, 1000000} {
		benchmarkIsPalindromeRandom(b, size)
	}
}

func benchmarkIsPalindromeRandom(b *testing.B, size int) {
	blk := make([]byte, size)
	_, err := rand.Read(blk)
	if err != nil {
		b.Fatalf("got unexpected error %v", err)
	}
	s := string(blk)

	b.ResetTimer()

	b.Run(fmt.Sprintf("IsPalindromeWithRE_%d", size), func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			leetcode.IsPalindromeWithExtraSpace(s)
		}
	})

	b.Run(fmt.Sprintf("IsPalindromeWithCompareStrings_%d", size), func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			leetcode.IsPalindromeWithCompareStrings(s)
		}
	})

	b.Run(fmt.Sprintf("IsPalindromeWithExtraSpace_%d", size), func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			leetcode.IsPalindromeWithExtraSpace(s)
		}
	})

	b.Run(fmt.Sprintf("IsPalindrome_%d", size), func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			leetcode.IsPalindrome(s)
		}
	})
}

func BenchmarkIsPalindromeValid(b *testing.B) {
	for _, size := range []int{1, 10, 100, 1000, 10000, 100000, 1000000} {
		benchmarkIsPalindromeValid(b, size)
	}
}

func benchmarkIsPalindromeValid(b *testing.B, size int) {
	blk := make([]byte, size)
	for i := 0; i < size; i++ {
		blk[i] = 'a'
	}
	s := unsafe.String(unsafe.SliceData(blk), size)

	b.ResetTimer()

	b.Run(fmt.Sprintf("IsPalindromeWithRE_%d", size), func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			leetcode.IsPalindromeWithExtraSpace(s)
		}
	})

	b.Run(fmt.Sprintf("IsPalindromeWithCompareStrings_%d", size), func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			leetcode.IsPalindromeWithCompareStrings(s)
		}
	})

	b.Run(fmt.Sprintf("IsPalindromeWithExtraSpace_%d", size), func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			leetcode.IsPalindromeWithExtraSpace(s)
		}
	})

	b.Run(fmt.Sprintf("IsPalindrome_%d", size), func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			leetcode.IsPalindrome(s)
		}
	})
}
