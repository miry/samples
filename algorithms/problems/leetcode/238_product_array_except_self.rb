# 238. Product of Array Except Self

def product_except_self(nums)
  accumulate_left = [1]
  accumulate_right = [1]
  start = 1
  i = 0
  n = nums.size
  while i < n
    accumulate_left << accumulate_left.last * nums[i]
    accumulate_right << accumulate_right.last * nums[i]
  end
  accumulate_right.reverse!
  accumulate_right.pop
  accumulate_left.shift

  #                    [ 1,   2,  3,  4]
  # accumulate_left  = [ 1,   2,  6, 24]
  # accumulate_right = [ 24, 24, 12,  4]

  result = []
  i = 0
  while i < n

  end
end
