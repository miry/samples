class Trie
  def initialize
    @data = {}
  end

  #     :type word: String
  #     :rtype: Void
  def insert(word)
    #   puts "> insert(#{word})"
    prefix = ""
    root = @data
    word.chars.each do |c|
      root[c] ||= {}
      root = root[c]
    end
    root[nil] = true
  end

  #     :type word: String
  #     :rtype: Boolean
  def search(word)
    # puts "> search(#{word})"
    root = @data
    word.chars.each do |c|
      return false if !root.key?(c)
      root = root[c]
    end

    root[nil] == true
  end

  #     :type prefix: String
  #     :rtype: Boolean
  def starts_with(prefix, root=@data)
    prefix.chars.each do |c|
      return false if !root.key?(c)
      root = root[c]
    end
    true
  end

  # def match(pattern, root=@data)
  #   puts "> match(#{pattern}, #{root})"
  #   result = []
  #
  #   return result if pattern.empty?
  #
  #   symbol = pattern[0]
  #   puts "  symbol=#{symbol}"
  #
  #   case symbol
  #   when nil
  #   else
  #     root.keys.each do |c|
  #       next if c != symbol
  #       r = match(pattern[1..], root[c])
  #       puts "  r=#{r}"
  #       result += r.map {|word| r.unshift(c) }
  #       result += [c]
  #     end
  #     puts "  result=#{result}"
  #   end
  #
  #   result
  # end
end

# Your Trie object will be instantiated and called as such:
# obj = Trie.new()
# obj.insert(word)
# param_2 = obj.search(word)
# param_3 = obj.starts_with(prefix)
