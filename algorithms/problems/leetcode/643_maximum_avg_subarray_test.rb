require "minitest/autorun"
require_relative "643_maximum_avg_subarray"

class MaximumAvgSubarrayTest < Minitest::Test
  def test_one
    nums = [1, 12, -5, -6, 50, 3]
    k = 4
    actual = find_max_average(nums, k)
    expected = 12.75000
    assert_equal expected, actual
  end

  def test_two
    nums = [5]
    k = 1
    actual = find_max_average(nums, k)
    expected = 5
    assert_equal expected, actual
  end
end
