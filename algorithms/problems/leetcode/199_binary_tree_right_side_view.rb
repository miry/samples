# 199. Binary Tree Right Side View
# https://leetcode.com/problems/binary-tree-right-side-view/description/
#
# Given the root of a binary tree, imagine yourself standing on the right side of it,
# return the values of the nodes you can see ordered from top to bottom.
# Example 1:
#
#     Input: root = [1,2,3,null,5,null,4]
#     Output: [1,3,4]
#
# Example 2:
#
#     Input: root = [1,null,3]
#     Output: [1,3]
#
# Example 3:
#
#     Input: root = []
#     Output: []
#
#

class TreeNode
  attr_accessor :val, :left, :right
  def initialize(val = 0, left = nil, right = nil)
    @val = val
    @left = left
    @right = right
  end

  def self.parse(arr)
    return TreeNode.new(nil) if arr.nil? || arr.size == 0
    build_node(arr, arr.size, 0)
  end

  def self.build_node(arr, n, i)
    return nil if i >= n
    shift = (i + 1) * 2 - 1
    TreeNode.new(arr[i], build_node(arr, n, shift), build_node(arr, n, shift + 1))
  end
end

# Use BFS to traversepath the tree. It still requires to go through all child nodes,
# because left nodes could be longer.
# @param {TreeNode} root
# @return {Integer[]}
def right_side_view(root)
  return [] if root.nil? || root.val.nil?
  result = [root.val]
  queue = [root]
  while queue.size > 0
    new_queue = []
    queue.each do |n|
      new_queue << n.left if n.left&.val
      new_queue << n.right if n.right&.val
    end
    result << new_queue.last.val if new_queue.size > 0
    queue = new_queue
  end
  result
end
