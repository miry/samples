package leetcode

import (
	"fmt"
	"sort"
)

// 703. Kth Largest Element in a Stream
//
// Design a class to find the kth largest element in a stream.
// Note that it is the kth largest element in the sorted order, not the kth distinct element.
//
// Implement KthLargest class:
//
// KthLargest(int k, int[] nums) Initializes the object with the integer k and the stream of integers nums.
// int add(int val) Appends the integer val to the stream and returns the element representing the kth largest element in the stream.
//
// Example 1:
//
// Input
// ["KthLargest", "add", "add", "add", "add", "add"]
// [[3, [4, 5, 8, 2]], [3], [5], [10], [9], [4]]
// Output
// [null, 4, 5, 5, 8, 8]
//
// Explanation
// KthLargest kthLargest = new KthLargest(3, [4, 5, 8, 2]);
// kthLargest.add(3);   // return 4
// kthLargest.add(5);   // return 5
// kthLargest.add(10);  // return 5
// kthLargest.add(9);   // return 8
// kthLargest.add(4);   // return 8
//
//
//
// Constraints:
//
// 1 <= k <= 104
// 0 <= nums.length <= 104
// -104 <= nums[i] <= 104
// -104 <= val <= 104
// At most 104 calls will be made to add.
// It is guaranteed that there will be at least k elements in the array when you search for the kth element.
//

type KthLargest struct {
	idx   int
	store []int
}

func Constructor(k int, nums []int) KthLargest {
	heap := KthLargest{}
	heap.store = nums
	sort.Slice(heap.store, func(i, j int) bool {
		return heap.store[j] < heap.store[i]
	})
	if len(heap.store) > k {
		heap.store = heap.store[:k]
	}
	heap.idx = k - 1
	fmt.Printf("store=%v\n", heap.store)

	return heap
}

func (this *KthLargest) Add(val int) int {
	fmt.Printf("> Add %v\n", val)
	idx, _ := sort.Find(len(this.store), func(i int) int {
		return this.store[i] - val
	})
	fmt.Printf("idx= %d store=%v this.idx=%d\n", idx, this.store, this.idx)

	n := len(this.store)
	if idx < n {
		this.store = append(this.store[:idx+1], this.store[idx:n]...)
		this.store[idx] = val
	} else {
		this.store = append(this.store, val)
	}
	if this.idx > n {
		this.store = this.store[:this.idx]
	}
	fmt.Printf("store=%v\n", this.store)
	return this.store[this.idx]
}
