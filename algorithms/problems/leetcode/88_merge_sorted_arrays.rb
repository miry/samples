# 88. Merge Sorted Array
#
# You are given two integer arrays nums1 and nums2, sorted in non-decreasing order,
# and two integers m and n, representing the number of elements in nums1 and nums2 respectively.
# Merge nums1 and nums2 into a single array sorted in non-decreasing order.
# The final sorted array should not be returned by the function,
# but instead be stored inside the array nums1.
# To accommodate this, nums1 has a length of m + n,
# where the first m elements denote the elements that should be merged,
# and the last n elements are set to 0 and should be ignored. nums2 has a length of n.

def merge(nums1, m, nums2, n)
  return nums1 if n == 0

  t = nums1.size

  j = m - 1
  i = t - 1
  while j >= 0
    nums1[i] = nums1[j]
    i -= 1
    j -= 1
  end

  i += 1
  j = 0
  k = 0

  while i < t && j != n
    if nums1[i] < nums2[j]
      nums1[k] = nums1[i]
      i += 1
    else
      nums1[k] = nums2[j]
      j += 1
    end

    k += 1
  end

  while j < n
    nums1[k] = nums2[j]
    j += 1
    k += 1
  end

  nums1
end
