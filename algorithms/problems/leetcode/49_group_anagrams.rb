# @param {String[]} strs
# @return {String[][]}
BASE = "a".ord
def group_anagrams(strs)
  hash = {}
  i = 0
  n = strs.size
  while i < n
    h = Array.new(26, 0)
    str = strs[i]
    # str.each_byte do |c|
    #   h[c - BASE] += 1
    # end

    # while loop is 3x times faster over each_byte
    # check this repo /ruby/string_loop_benchmark.rb
    j = 0
    b = str.bytes
    while j < b.size
      h[b[j] - ::BASE] += 1
      j += 1
    end

    hash[h] ||= []
    hash[h] << str
    i += 1
  end

  hash.values
end

def group_anagrams_group_by(strs)
  strs.group_by do |str|
    str.chars.sort
  end.values
end
