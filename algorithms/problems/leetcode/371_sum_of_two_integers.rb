# @param {Integer} a
# @param {Integer} b
# @return {Integer}
def get_sum(a, b)
  # 011110   011110  011110      000001
  #                              111111
  #    +       ^       &
  # 010100   010100  010100
  #    =       =       =
  # 110010   001010  010100 <<1  101000

  # Ruby hacks Integer
  return a + b if a < 0 || b < 0

  while b != 0
    check = (a & b) << 1
    a ^= b
    b = check
  end
  a
end
