package leetcode

import (
	"math"
)

// 7. Reverse Integer
//
// Given a signed 32-bit integer x, return x with its digits reversed.
// If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.
// Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
//
// Example 1:
//
//	Input: x = 123
//	Output: 321
//
// Example 2:
//
//	Input: x = -123
//	Output: -321
//
// Example 3:
//
//	Input: x = 120
//	Output: 21
//
// Constraints:
//
//	-2^31 <= x <= 2^31 - 1
func Reverse(x int) int {
	var result, digit int

	max_limit := math.MaxInt32 / 10
	min_limit := math.MinInt32 / 10

	for x != 0 {
		digit = x % 10
		x = x / 10

		if result > max_limit || (result == max_limit && digit > 7) {
			return 0
		}

		if result < min_limit || (result == min_limit && digit < -8) {
			return 0
		}

		result = result*10 + digit
	}

	return result
}
