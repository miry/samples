# 169. Majority Element
# https://leetcode.com/problems/majority-element/description/?envType=study-plan-v2&envId=top-interview-150
def majority_element(nums)
  data = Hash.new(0)
  i = 0
  half = nums.size / 2
  while i < nums.size
    j = nums[i]
    data[j] += 1
    return j if data[j] > half
    i += 1
  end
  raise "Unexpected"
end
