# 36. Valid Sudoku
# https://leetcode.com/problems/valid-sudoku/description/
#
# @param {Character[][]} board
# @return {Boolean}
def is_valid_sudoku(board)
  lines = Array.new(9) { Set.new }
  columns = Array.new(9) { Set.new }
  squares = Array.new(9) { Set.new }

  board.each_with_index do |line, row|
    base_square = (row / 3) * 3
    line.each_with_index do |cell, col|
      next if cell == "." # The validation does not related to position, so can skip empty cells.
      return false if lines[row].add?(cell).nil?
      return false if columns[col].add?(cell).nil?
      return false if squares[base_square + col / 3].add?(cell).nil?
    end
  end

  true
end
