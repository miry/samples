# 261. Graph Valid Tree
#

def dfs(neighbours, id, prev, visited)
  return false if visited.include?(id)
  visited.add(id)

  children = neighbours[id]
  children.each do |child|
    next if child == prev
    return false if !dfs(neighbours, child, id, visited)
  end

  true
end

# I used DFS solution, but it is Possible to use Union Find
def graph_valid_tree(n, edges)
  neighbours = Array.new(n) { [] }
  edges.each do |edge|
    neighbours[edge.first].append(edge.last)
    neighbours[edge.last].append(edge.first)
  end

  visited = Set.new
  dfs(neighbours, 0, -1, visited) && visited.size == n
end
