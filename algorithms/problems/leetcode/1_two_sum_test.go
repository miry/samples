package leetcode_test

import (
	"testing"

	"codeberg.org/miry/samples/algorithms/problems/leetcode"
)

func TestTwoSum(t *testing.T) {
	tt := []struct {
		target   int
		nums     []int
		expected []int
	}{
		{9, []int{2, 7, 11, 15}, []int{0, 1}},
		{6, []int{3, 2, 4}, []int{1, 2}},
		{6, []int{3, 3}, []int{0, 1}},
	}

	for _, tc := range tt {
		actual := leetcode.TwoSum(tc.nums, tc.target)
		if (actual[0] != tc.expected[0]) || (actual[1] != tc.expected[1]) {
			t.Errorf("for nums: %v and target %d\nwe expect %v,\n    got %v", tc.nums, tc.target, tc.expected, actual)
		}
	}
}
