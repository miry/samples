package leetcode

func GetSum(a, b int) int {
	check := 0

	for b != 0 {
		check = (a & b) << 1
		a = a ^ b
		b = check
	}

	return a
}
