require "minitest/autorun"
require_relative "124_binary_tree_maximum_path_sum"

class BinaryTreeMaximumPathSumTest < Minitest::Test
  def test_one
    root = TreeNode.parse [1, 2, 3]
    actual = max_path_sum(root)
    assert_equal 6, actual
  end

  def test_two
    root = TreeNode.parse [-10, 9, 20, nil, nil, 15, 7]
    actual = max_path_sum(root)
    assert_equal 42, actual
  end

  def test_three
    root = TreeNode.parse [-10]
    actual = max_path_sum(root)
    assert_equal(-10, actual)
  end
end
