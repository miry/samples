package leetcode_test

import (
	"testing"

	"codeberg.org/miry/samples/algorithms/problems/leetcode"
	"github.com/stretchr/testify/assert"
)

func TestReverse(t *testing.T) {

	tests := []map[string]int{
		{
			"in":  0,
			"out": 0,
		},
		{
			"in":  -1,
			"out": -1,
		},
		{
			"in":  -123,
			"out": -321,
		},
		{
			"in":  2147483648,
			"out": 0,
		},
		{
			"in":  7463847412,
			"out": 2147483647,
		},
		{
			"in":  8463847412,
			"out": 0,
		},
		{
			"in":  -2147483647,
			"out": 0,
		},
		{
			"in":  -8463847412,
			"out": -2147483648,
		},
		{
			"in":  -9463847412,
			"out": 0,
		},
	}

	for _, test := range tests {
		actual := leetcode.Reverse(test["in"])
		assert.Equal(t, test["out"], actual)
	}
}
