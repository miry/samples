require "minitest/autorun"
require_relative "88_merge_sorted_arrays"

class MergeSortArrayTest < Minitest::Test
  def test_one
    actual = merge([1, 2, 3, 0, 0, 0], 3, [2, 5, 6], 3)
    assert_equal [1, 2, 2, 3, 5, 6], actual
  end

  def test_two
    actual = merge([1], 1, [], 0)
    assert_equal [1], actual
  end

  def test_three
    actual = merge([2, 0], 1, [1], 1)
    assert_equal [1, 2], actual
  end
end
