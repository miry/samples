require "minitest/autorun"
require_relative "261_graph_valid_tree"

class GraphValidTreeTest < Minitest::Test
  def test_one
    assert graph_valid_tree(5, [[0, 1], [0, 2], [0, 3], [1, 4]])
  end

  def test_two
    assert !graph_valid_tree(5, [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]])
  end
end
