require "minitest/autorun"
require_relative "1268_search_suggestions_system"

class SearchSuggestionsSystemTest < Minitest::Test
  def test_one
    products = ["mobile", "mouse", "moneypot", "monitor", "mousepad"]
    keyword = "mouse"
    actual = suggested_products(products, keyword)

    expected = [
      ["mobile", "moneypot", "monitor"], # type: m
      ["mobile", "moneypot", "monitor"], # type: mo
      ["mouse", "mousepad"],             # type: mou
      ["mouse", "mousepad"],             # type: mous
      ["mouse", "mousepad"]              # type: mouse
    ]
    assert_equal expected, actual
  end

  def test_two
    products = ["havana"]
    keyword = "havana"
    actual = suggested_products(products, keyword)
    expected = [
      ["havana"], # type: h
      ["havana"], # type: ha
      ["havana"], # type: hav
      ["havana"], # type: hava
      ["havana"], # type: havan
      ["havana"]  # type: havana
    ]
    assert_equal expected, actual
  end

  def test_missing
    products = ["havana"]
    keyword = "tatiana"
    actual = suggested_products(products, keyword)
    expected = [
      [], # type: t
      [], # type: ta
      [], # type: tat
      [], # type: tati
      [], # type: tatia
      [], # type: tatian
      [] # type: tatiana
    ]
    assert_equal expected, actual
  end
end
