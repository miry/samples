package leetcode

type ListNode struct {
	Val  int
	Next *ListNode
}

func printList(head *ListNode) {
	node := head
	println("[DEBUG] List: ")
	for node != nil {
		println("  * ", node.Val)
		node = node.Next
	}
}

func ReorderList(head *ListNode) {
	if head.Next == nil {
		return
	}

	// Detect the half
	slow, fast := head, head
	for fast != nil && fast.Next != nil {
		fast = fast.Next.Next
		slow = slow.Next
	}

	h2 := slow.Next
	if h2 == nil {
		return
	}
	// Mark the last node of first half as tail
	slow.Next = nil

	// Reverse second half
	slow = h2
	nNode := slow.Next
	var oldNext *ListNode
	for nNode != nil {
		oldNext = nNode.Next
		nNode.Next = slow
		slow = nNode
		nNode = oldNext
	}
	h2.Next = nil // it is tail now
	h2 = slow     // current head

	// Merge
	slow = head
	i := head
	j := h2
	var nexti, nextj *ListNode
	for j != nil {
		nexti = i.Next
		nextj = j.Next

		i.Next = j
		j.Next = nexti

		i = nexti
		j = nextj
	}
}
