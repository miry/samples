package leetcode_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"codeberg.org/miry/samples/algorithms/problems/leetcode"
)

func TestReorderList(t *testing.T) {
	tt := []struct {
		values   []int
		expected []int
	}{
		{[]int{1}, []int{1}},
		{[]int{1, 2}, []int{1, 2}},
		{[]int{1, 2, 3}, []int{1, 3, 2}},
		{[]int{1, 2, 3, 4}, []int{1, 4, 2, 3}},
		{[]int{1, 2, 3, 4, 5}, []int{1, 5, 2, 4, 3}},
	}

	for _, tc := range tt {
		head := &leetcode.ListNode{}
		node := head
		for _, val := range tc.values {
			node.Next = &leetcode.ListNode{val, nil}
			node = node.Next
		}
		head = head.Next

		leetcode.ReorderList(head)
		node = head
		actual := make([]int, len(tc.values), len(tc.values))

		for i := 0; i < len(tc.values); i++ {
			actual[i] = node.Val
			node = node.Next
			if node == nil {
				break
			}
		}

		assert.Equal(t, tc.expected, actual)
	}
}
