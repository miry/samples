require "minitest/autorun"
require_relative "26_remove_duplicates_from_sroted_array"

class RemoveDuplicatesTest < Minitest::Test
  def test_one
    nums = [1, 1, 2]
    actual = remove_duplicates(nums)
    assert_equal 2, actual
    assert_equal [1, 2], nums[0, 2]
  end

  def test_two
    nums = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4]
    actual = remove_duplicates(nums)
    assert_equal 5, actual
    assert_equal [0, 1, 2, 3, 4], nums[0, 5]
  end
end
