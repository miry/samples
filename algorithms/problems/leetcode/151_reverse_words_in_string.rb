# 151. Reverse Words in a String
# https://leetcode.com/problems/reverse-words-in-a-string/description/
#
# Given an input string s, reverse the order of the words.
#
# A word is defined as a sequence of non-space characters. The words in s will be separated by at least one space.
#
# Return a string of the words in reverse order concatenated by a single space.
#
# Note that s may contain leading or trailing spaces or multiple spaces between two words. The returned string should only have a single space separating the words. Do not include any extra spaces.
#
# Example 1:
#
# Input: s = "the sky is blue"
# Output: "blue is sky the"
#
# Example 2:
#
# Input: s = "  hello world  "
# Output: "world hello"
# Explanation: Your reversed string should not contain leading or trailing spaces.
#
# Example 3:
#
# Input: s = "a good   example"
# Output: "example good a"
# Explanation: You need to reduce multiple spaces between two words to a single space in the reversed string.
#

# Reverse the whole string and then reverse each word one by one.
# It implements inplace reverse words.
# Problem it move spaces to the end word by word.
# The better solution to use two pointers and on second step remember the start of string and find the second word ending and write until it reaches the space.
def reverse_words(s)
  reverse(s, 0, s.size - 1)

  i = 0
  l = 0
  n = s.size
  while i < n
    l += 1 while l < n && s[l] != " "
    reverse(s, i, l - 1)
    l -= 1 while s[l] == " "
    i = l + 2
    break if l == n
    l = i
    l += 1 while l < n && s[l] == " "
  end
  s.rstrip!
  s
end

def reverse(s, start, finish)
  i = start
  j = finish
  j -= 1 while s[j] == " "
  result = j

  while i < j
    s[i], s[j] = s[j], s[i]
    i += 1
    j -= 1
  end

  result
end
