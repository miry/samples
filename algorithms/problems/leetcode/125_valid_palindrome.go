// A phrase is a palindrome if, after converting all uppercase letters into lowercase letters
// and removing all non-alphanumeric characters, it reads the same forward and backward.
// Alphanumeric characters include letters and numbers.
//
// Given a string s, return true if it is a palindrome, or false otherwise.

package leetcode

import (
	"regexp"
	"strings"
)

func reverse(s string) string {
	rns := []rune(s)
	for i, j := 0, len(rns)-1; i < j; i, j = i+1, j-1 {
		rns[i], rns[j] = rns[j], rns[i]
	}
	return string(rns)
}

const diff = 'a' - 'A'

// IsPalindromeWithRE checks ASCII string if reverse alphanameruc is equal
func IsPalindromeWithRE(s string) bool {
	// Remove non alphanumeric symbols and convert to lower case
	reg, _ := regexp.Compile("[^a-zA-Z0-9]+")
	str := strings.ToLower(reg.ReplaceAllString(s, ""))

	i := 0
	j := len(str) - 1
	for i < j {
		if str[i] != str[j] {
			return false
		}
		i++
		j--
	}

	return true
}

// IsPalindromeWithCompareStrings checks ASCII string if reverse alphanameruc is equal
func IsPalindromeWithCompareStrings(s string) bool {
	var b strings.Builder
	b.Grow(len(s))
	for _, char := range s {
		if (char >= 'a' && char <= 'z') || (char >= '0' && char <= '9') {
			b.WriteRune(char)
		} else if char >= 'A' && char <= 'Z' {
			b.WriteRune(char + diff)
		}
	}
	normal := b.String()
	return normal == reverse(normal)
}

// IsPalindromeWithExtraSpace checks ASCII string with building chars and comapre the chars from the same string
// It create an extra string to keep valid string
func IsPalindromeWithExtraSpace(s string) bool {
	var b strings.Builder
	b.Grow(len(s))

	for _, char := range s {
		if (char >= 'a' && char <= 'z') || (char >= '0' && char <= '9') {
			b.WriteRune(char)
		} else if char >= 'A' && char <= 'Z' {
			b.WriteRune(char + diff)
		}
	}

	str := b.String()
	i := 0
	j := len(str) - 1

	for i < j {
		if str[i] != str[j] {
			return false
		}
		i++
		j--
	}

	return true
}

// IsPalindrome checks ASCII string without extra space
func IsPalindrome(s string) bool {
	i := 0
	j := len(s) - 1

	left := s[i]
	right := s[j]
	for i < j {
		// Next available left character
		for i < j {
			left = s[i]
			if (left >= 'a' && left <= 'z') || (left >= '0' && left <= '9') {
				break
			} else if left >= 'A' && left <= 'Z' {
				left = left + diff
				break
			}
			i++
		}

		// Next available right character
		for i < j {
			right = s[j]
			if (right >= 'a' && right <= 'z') || (right >= '0' && right <= '9') {
				break
			} else if right >= 'A' && right <= 'Z' {
				right = right + diff
				break
			}
			j--
		}

		if i >= j {
			return true
		}

		if left != right {
			return false
		}
		i++
		j--
	}

	return true
}
