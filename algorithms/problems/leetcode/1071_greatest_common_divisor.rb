# @param {String} str1
# @param {String} str2
# @return {String}
def gcd_of_strings(str1, str2)
  n = str1.size
  m = str2.size

  big = str1
  small = str2
  if n < m
    big = str2
    small = str1
  end

  return "" if !big.start_with?(small)

  while m > 0
    d = small[0, m]
    return d if division_str?(big, d) && division_str?(small, d)
    p m
    m -= 1
  end
  ""
end

def division_str?(str1, str2)
  n = str1.size
  m = str2.size
  return false if n % m != 0
  i = 0
  while i < n
    return false if str1[i, m] != str2
    i += m
  end
  true
end
