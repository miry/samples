require "minitest/autorun"
require_relative "151_reverse_words_in_string"

class ReverseWordsTest < Minitest::Test
  def test_one
    input = "the sky is blue"
    actual = reverse_words(input)
    expected = "blue is sky the"
    assert_equal expected, actual
  end

  def test_two
    input = "  hello world  "
    actual = reverse_words(input)
    expected = "world hello"
    assert_equal expected, actual
  end

  def test_three
    input = "a good   example"
    actual = reverse_words(input)
    expected = "example good a"
    assert_equal expected, actual
  end

  def test_four
    input = " 3c      2zPeO dpIMVv2SG    1AM       o       VnUhxK a5YKNyuG     x9    EQ  ruJO       0Dtb8qG91w 1rT3zH F0m n G wU"
    actual = reverse_words(input)
    expected = "wU G n F0m 1rT3zH 0Dtb8qG91w ruJO EQ x9 a5YKNyuG VnUhxK o 1AM dpIMVv2SG 2zPeO 3c"
    assert_equal expected, actual
  end
end
