package leetcode_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"codeberg.org/miry/samples/algorithms/problems/leetcode"
)

func TestKthLargestSample1(t *testing.T) {
	subject := leetcode.Constructor(3, []int{4, 5, 8, 2})

	assert.Equal(t, 4, subject.Add(3))
	assert.Equal(t, 5, subject.Add(5))
	assert.Equal(t, 5, subject.Add(10))
	assert.Equal(t, 8, subject.Add(9))
	assert.Equal(t, 8, subject.Add(4))
}

// [[1,[]],[-3],[-2],[-4],[0],[4]]
func TestKthLargestSample2(t *testing.T) {
	subject := leetcode.Constructor(1, []int{})

	assert.Equal(t, -3, subject.Add(-3))
	assert.Equal(t, -2, subject.Add(-2))
	assert.Equal(t, -2, subject.Add(-4))
	assert.Equal(t, 0, subject.Add(0))
	assert.Equal(t, 4, subject.Add(4))
}
