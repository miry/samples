package leetcode_test

import (
	"fmt"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"

	"codeberg.org/miry/samples/algorithms/problems/leetcode"
)

func TestMedianFinder(t *testing.T) {
	tt := []struct {
		input    []int
		expected []float64
	}{
		{
			[]int{1, 2, 3},
			[]float64{1.0, 1.5, 2.0},
		},
		{
			[]int{1, 1, 1, 1},
			[]float64{1.0, 1.0, 1.0, 1.0},
		},
		{
			[]int{-1, -2, -3, -4, -5},
			[]float64{-1.0, -1.5, -2.0, -2.5, -3.0},
		},
	}

	for _, solution := range []string{"array", "heap"} {
		t.Run(solution, func(t *testing.T) {
			for _, tc := range tt {
				subject := leetcode.NewMedianFinder(solution)

				actual := []float64{}
				for _, num := range tc.input {
					subject.AddNum(num)
					actual = append(actual, subject.FindMedian())
				}

				assert.Equal(t, tc.expected, actual)
			}
		})
	}
}

func BenchmarkMedian(b *testing.B) {
	for _, size := range []int{1, 10, 100, 1000, 10000, 25000, 50000, 100000, 250000, 500000, 1000000} {
		benchmarkMedian(b, size)
	}
}

func benchmarkMedian(b *testing.B, size int) {

	nums := make([]int, size, size)
	for i := 0; i < size; i++ {
		nums[i] = rand.Int()
	}

	b.Run(fmt.Sprintf("MedianArray_%d", size), func(b *testing.B) {
		subject := leetcode.NewMedianFinder("array")

		b.ResetTimer()

		for i := 0; i < b.N; i++ {
			for j := 0 ; j < size ; j ++ {
				subject.AddNum(nums[j])
				subject.FindMedian()
			}
		}
	})

	b.Run(fmt.Sprintf("MedianHeap_%d", size), func(b *testing.B) {
		subject := leetcode.NewMedianFinder("heap")

		b.ResetTimer()

		for i := 0; i < b.N; i++ {
			for j := 0 ; j < size ; j ++ {
				subject.AddNum(nums[j])
				subject.FindMedian()
			}
		}
	})
}
