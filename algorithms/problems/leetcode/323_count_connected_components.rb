# Count Connected Components
# There is an undirected graph with n nodes. There is also an edges array, where edges[i] = [a, b] means that there is an edge between node a and node b in the graph.
#
# The nodes are numbered from 0 to n - 1.
#
# Return the total number of connected components in that graph.

def union(lid, pid, connections)
  return if connections[lid] == connections[pid]
  old = connections[lid]
  connections[lid] = connections[pid]
  i = 0
  while i < connections.size
    connections[i] = pid if connections[i] == old
    i += 1
  end
  connections
end

def count_components(n, edges)
  state = Array.new(n) { |i| i }

  edges.each do |edge|
    union(edge.first, edge[1], state)
  end

  state.uniq!
  state.size
end
