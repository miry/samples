require "minitest/autorun"
require_relative "371_sum_of_two_integers"

class SumOfTwoIntegers < Minitest::Test
  def test_example_1
    assert_equal 3, get_sum(1, 2)
  end

  def test_example_2
    assert_equal 5, get_sum(2, 3)
  end

  def test_example_3
    assert_equal 4, get_sum(2, 2)
  end

  def test_power_two
    assert_equal 192, get_sum(128, 64)
  end

  def test_fifty
    assert_equal 50, get_sum(20, 30)
  end

  def test_zero
    assert_equal 0, get_sum(-1, 1)
  end

  def test_negative
    assert_equal(-20, get_sum(-10, -10))
  end
end
