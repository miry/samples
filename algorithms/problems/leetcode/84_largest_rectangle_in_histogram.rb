# 7,1,7,2,4,5,4
#
# x x
# x x
# x x  x
# x x xxx
# x x xxx
# x xxxxx
# xxxxxxx
#
# pop: 4 > 0
# 1 - 1
# 2 - 1
# 3 - 1
# 4 - 1
#
# pop: 5 > 4
# 1 - 2
# 2 - 2
# 3 - 2
# 4 - 2
# 5 - 1
#
# pop: 4 < 5
# 1 - 3
# 2 - 3
# 3 - 3
# 4 - 3
# 5 - 0 -> 5 * 1 = 5
#
# pop: 2 < 4
# 1 - 4
# 2 - 4
# 3 - 0: 3 * 3 = 9
# 4 - 0: 3 * 4 = 12
# 5 - 0
#
# pop: 7 > 2
# 1 - 5
# 2 - 5
# 3 - 1
# 4 - 1
# 5 - 1
# 6 - 1
# 7 - 1
#
# pop: 1 < 7
# 1 - 6
# 2 - 0: 5 * 2 = 10
# 3 - 0
# 4 - 0
# 5 - 0
# 6 - 0
# 7 - 0
#
# pop: 7 > 1
# 1 - 7
# 2 - 1
# 3 - 1
# 4 - 1
# 5 - 1
# 6 - 1
# 7 - 1
#
# pop: 0 < 1
# 1 - 7: 7 * 1 = 7
# 2 - 1: 2 * 1 = 2
# 3 - 1: 3 * 1 = 3
# 4 - 1: 4 * 1 = 4
# 5 - 1: 5 * 1 = 5
# 6 - 1: 6 * 1 = 6
# 7 - 1: 7 * 1 = 7
#
def largest_rectangle_area(heights)
  stack = []
  max = 0
  li, lh, s, start = 0, 0, 0, 0
  i = 0
  n = heights.size
  while i < n
    height = heights[i]
    start = i
    while stack.size != 0
      lh, li = stack.last
      break if lh <= height
      s = (i - li) * lh
      max = s if s > max
      start = li
      stack.pop
    end
    stack << [height, start]
    i += 1
  end
  i = 0
  n = stack.size
  while i < n
    lh, li = stack[i]
    s = (heights.size - li) * lh
    max = s if s > max
    i += 1
  end
  max
end
