require "minitest/autorun"
require_relative "199_binary_tree_right_side_view"

class BinaryTreeRightSideViewTest < Minitest::Test
  def test_parse_empty
    actual = TreeNode.parse([])
    assert_nil actual.val
    assert_nil actual.left
    assert_nil actual.right
  end

  def test_parse_single
    actual = TreeNode.parse([1])
    assert_equal 1, actual.val
    assert_nil actual.left
    assert_nil actual.right
  end

  def test_parse_double
    actual = TreeNode.parse([1, 2, 3])
    assert_equal 1, actual.val
    assert_equal 2, actual.left.val
    assert_equal 3, actual.right.val
  end

  def test_parse_sample_one
    actual = TreeNode.parse([1, 2, 3, nil, 5, nil, 4])
    assert_equal 1, actual.val
    assert_equal 2, actual.left.val
    assert_nil actual.left.left.val
    assert_equal 5, actual.left.right.val
    assert_nil actual.right.left.val
    assert_equal 4, actual.right.right.val
  end

  def test_one
    root = TreeNode.parse [1, 2, 3, nil, 5, nil, 4]
    expected = [1, 3, 4]
    actual = right_side_view(root)
    assert_equal expected, actual
  end

  def test_two
    root = TreeNode.parse [1, nil, 3]
    expected = [1, 3]
    actual = right_side_view(root)
    assert_equal expected, actual
  end

  def test_three
    root = TreeNode.parse []
    expected = []
    actual = right_side_view(root)
    assert_equal expected, actual
  end

  def test_half_empty
    root = TreeNode.parse [1, 2]
    expected = [1, 2]
    actual = right_side_view(root)
    assert_equal expected, actual
  end

  def test_five
    root = TreeNode.parse [1, 2, 3, 4]
    expected = [1, 3, 4]
    actual = right_side_view(root)
    assert_equal expected, actual
  end
end
