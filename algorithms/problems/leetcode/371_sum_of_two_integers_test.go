package leetcode_test

import (
	"testing"

	"codeberg.org/miry/samples/algorithms/problems/leetcode"
	"github.com/stretchr/testify/assert"
)

func TestGetSum(t *testing.T) {
	tests := []map[string][2]int{
		{
			"in":  [2]int{1, 2},
			"out": [2]int{3, 0},
		},
		{
			"in":  [2]int{-1, 1},
			"out": [2]int{0, 0},
		},
		{
			"in":  [2]int{-10, -1},
			"out": [2]int{-11, 0},
		},
	}

	for _, test := range tests {
		actual := leetcode.GetSum(test["in"][0], test["in"][1])
		assert.Equal(t, test["out"][0], actual, "Expected %v for %v", test["out"][0], test["in"])
	}
}
