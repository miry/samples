require "minitest/autorun"

require_relative "338_counting_bits"

class CountingBits < Minitest::Test
  def test_check_bits_for_zero
    assert_equal [0], count_bits(0)
  end

  def test_check_bits_for_one
    assert_equal [0, 1], count_bits(1)
  end

  def test_check_bits_for_two
    assert_equal [0, 1, 1], count_bits(2)
  end

  def test_check_bits_for_five
    assert_equal [0, 1, 1, 2, 1, 2], count_bits(5)
  end

  def test_check_bits_for_big
    assert_equal 11, count_bits(85723)[-1]
  end
end
