# frozen_string_literal: true

require "minitest/autorun"

require_relative "136_single_number"

class SingleNumber < Minitest::Test
  def test_example_1
    assert_equal 1, single_number([2, 2, 1])
  end

  def test_example_2
    assert_equal 4, single_number([4, 1, 2, 1, 2])
  end

  def test_example_3
    assert_equal 1, single_number([1])
  end

  def test_in_the_middle
    assert_equal 2, single_number([3, 2, 3])
  end
end
