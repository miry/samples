require "minitest/autorun"
require_relative "7_reverse_integer"

class ReverseIntegerTest < Minitest::Test
  def test_null
    actual = reverse(0)
    assert_equal 0, actual
  end

  def test_one
    actual = reverse(1)
    assert_equal 1, actual
  end

  def test_negative
    actual = reverse(-1)
    assert_equal(-1, actual)
  end

  def test_negative_ten
    actual = reverse(-10)
    assert_equal(-1, actual)
  end

  def test_one_two_three
    actual = reverse(123)
    assert_equal 321, actual
  end

  def test_big_int
    actual = reverse(2147483648)
    assert_equal 0, actual
  end
end
