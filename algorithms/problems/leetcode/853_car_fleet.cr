# https://leetcode.com/problems/car-fleet/
#
# 853. Car Fleet
#
# There are n cars going to the same destination along a one-lane road.
# The destination is target miles away.
#
# You are given two integer array position and speed, both of length n,
# where position[i] is the position of the ith car and speed[i] is the speed
# of the ith car (in miles per hour).
#
# A car can never pass another car ahead of it,
# but it can catch up to it and drive bumper to bumper at the same speed.
# The faster car will slow down to match the slower car's speed.
# The distance between these two cars is ignored
# (i.e., they are assumed to have the same position).
#
# A car fleet is some non-empty set of cars driving at the same position and same speed.
# Note that a single car is also a car fleet.
#
# If a car catches up to a car fleet right at the destination point,
# it will still be considered as one car fleet.
#
# Return the number of car fleets that will arrive at the destination.
#
#
# Example 1:
#
# Input: target = 12, position = [10,8,0,5,3], speed = [2,4,1,1,3]
# Output: 3
# Explanation:
# The cars starting at 10 (speed 2) and 8 (speed 4) become a fleet, meeting each other at 12.
# The car starting at 0 does not catch up to any other car, so it is a fleet by itself.
# The cars starting at 5 (speed 1) and 3 (speed 3) become a fleet, meeting each other at 6.
# The fleet moves at speed 1 until it reaches target.
# Note that no other cars meet these fleets before the destination, so the answer is 3.
#
# Example 2:
#
# Input: target = 10, position = [3], speed = [3]
# Output: 1
# Explanation: There is only one car, hence there is only one fleet.
#
# Example 3:
#
# Input: target = 100, position = [0,2,4], speed = [4,2,1]
# Output: 1
# Explanation:
# The cars starting at 0 (speed 4) and 2 (speed 2) become a fleet,
# meeting each other at 4. The fleet moves at speed 2.
# Then, the fleet (speed 2) and the car starting at 4 (speed 1) become one fleet,
# meeting each other at 6. The fleet moves at speed 1 until it reaches target.
#
# Constraints:
#
#     n == position.length == speed.length
#     1 <= n <= 105
#     0 < target <= 106
#     0 <= position[i] < target
#     All the values of position are unique.
#     0 < speed[i] <= 106
#

def car_fleet(target, position, speed)
  n = position.size
  return n if n <= 1

  road = [] of Array(Float64)

  position.each_with_index do |v, i|
    road << [v.to_f, speed[i].to_f, (target - v) * 1.0 / speed[i]]
  end
  road.sort! { |a, b| b[0] <=> a[0] }

  result = 0
  r1 = [] of Float64
  road.each do |car|
    if r1.size == 0 || r1[2] < car[2]
      r1 = car
      result += 1
    end
  end
  result
end

puts "\n853. Car Fleet"
[
  {12, [10, 8, 0, 5, 3], [2, 4, 1, 1, 3], 3},
  {10, [3], [3], 1},
  {100, [0, 2, 4], [4, 2, 1], 1},
  {31, [5, 26, 18, 25, 29, 21, 22, 12, 19, 6], [7, 6, 6, 4, 3, 4, 9, 7, 6, 4], 6},
].each do |i|
  r = car_fleet(i[0], i[1], i[2])
  if r != i[3]
    raise "Wrong value for case #{i}. Expected #{i[3]}, got `#{r}`"
  else
    puts "Success case #{i}"
  end
end
