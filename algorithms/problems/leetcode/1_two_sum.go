package leetcode

func TwoSum(nums []int, target int) []int {
	store := make(map[int]int, len(nums))

	var a, b int
	for i := 0; i < len(nums); i++ {
		a = nums[i]
		b = target - nums[i]
		if indx, ok := store[b]; ok {
			return []int{indx, i}
		}
		if _, ok := store[a]; !ok {
			store[a] = i
		}
	}
	panic("should not happen")
}
