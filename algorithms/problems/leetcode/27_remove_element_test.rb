require "minitest/autorun"
require_relative "27_remove_element"

class RemoveElementTest < Minitest::Test
  def test_one
    nums = [3, 2, 2, 3]
    actual = remove_element(nums, 3)
    assert_equal 2, actual
    assert_equal [2, 2], nums[0, 2]
  end

  def test_two
    nums = [0, 1, 2, 2, 3, 0, 4, 2]
    actual = remove_element(nums, 2)
    assert_equal 5, actual
    assert_equal [0, 1, 3, 0, 4], nums[0, 5]
  end
end
