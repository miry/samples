# https://leetcode.com/problems/largest-rectangle-in-histogram/
#
# 84. Largest Rectangle in Histogram
#
# Given an array of integers heights representing the histogram's bar height
# where the width of each bar is 1, return the area of the largest rectangle in the histogram.
#
# Example 1:
#
# Input: heights = [2,1,5,6,2,3]
# Output: 10
# Explanation: The above is a histogram where width of each bar is 1.
# The largest rectangle is shown in the red area, which has an area = 10 units.
#
# Example 2:
#
# Input: heights = [2,4]
# Output: 4
#
# Constraints:
#
#     1 <= heights.length <= 105
#     0 <= heights[i] <= 104
#

def largest_rectangle_area_bruet_force(heights)
  max = 0
  heights.each_with_index do |height, i|
    (1..height).each do |row|
      width = 1
      while i + width < heights.size && heights[i + width] >= row
        width += 1
      end
      square = row * width
      max = square if max < square
    end
  end
  max
end

def largest_rectangle_area(heights)
  review = [0]
  rectangles = [] of Int64
  n = heights.size
  max = 0
  while review.size > 0
    i = review.pop
    height = heights[i]
    pp! i
    pp! height
    (1..height).each do |row|
      # pp! row
      width = 1
      while i + width < n && heights[i + width] >= row
        width += 1
      end
      col = i + width + 1
      # pp! col
      square = row * width
      rectangles << square
      max = square if max < square
      if col < n
        review.push(col) if review.size == 0 || review.last != col
      end
    end
    if i + 1 < n && heights[i + 1] > height
      review.push i + 1
    end
    pp! rectangles
    pp! review
  end
  max
end

puts "\n84. Largest Rectangle in Histogram"
[
  {[2, 1, 5, 6, 2, 3], 10},
  {[2, 4], 4},
  {[0, 9], 9},
  {[0, 9, 0], 9},
  {[0, 9, 0, 8], 9},
  {[3, 6, 5, 7, 4, 8, 1, 0], 20},
  {[2, 2, 6, 6, 5, 5, 5, 0, 9, 3, 6, 3, 8, 6, 6], 25},
].each do |i|
  result = largest_rectangle_area(i[0])
  expected = i.last
  if result != expected
    raise "Wrong value for case #{i}. Expected #{expected}, got `#{result}`"
  else
    puts "Success case #{i}"
  end
end
