require "date"
require "fileutils"

# Create a dummy `day*.cr`` and `day*_spec.cr` files.
# Print the basic switch case for the `cli.cr`.
def generate(year, day)
  filename_base = "day%02d" % day
  dd = "%02d" % day
  File.open("#{year}/#{filename_base}.cr", "w") do |f|
    f.puts(<<~EOS)
      # https://adventofcode.com/#{year}/day/#{day}
      #
      # --- Day #{day}: ... ---
      #

      require "../utils.cr"

      module Day#{day}
        extend self

        # --- Part One ---
        def problem1(records : Array(String))
          records.each do |instruction|
            next if instruction == ""
          end

          raise "Day #{day}.1 is not implemented"
        end

        # --- Part Two ---
        def problem2(records : Array(String))
          records.each do |instruction|
            next if instruction == ""
          end

          raise "Day #{day}.2 is not implemented"
        end
      end

      # vim: filetype=crystal syntax=crystal
    EOS
  end

  File.open("#{year}/#{filename_base}_spec.cr", "w") do |f|
    f.puts(<<~EOS)
      require "spec"
      require "./day#{dd}"

      describe Day#{day} do
        describe "problem1" do
          pending "sample" do
            input = <<-EOF
            <sample>
            EOF
            actual = Day#{day}.problem1(input.split("\\n"))
            actual.should eq(12)
          end
        end

        describe "problem2" do
          pending "sample" do
            input = <<-EOF
            <sample>
            EOF
            actual = Day#{day}.problem2(input.split("\\n"))
            actual.should eq(12)
          end
        end
      end

      # vim: filetype=crystal syntax=crystal
    EOS
  end
end

year = ARGV.first || Date.today.year
puts "1. Generate #{year} year folder"
FileUtils.mkdir_p "#{year}/input"

puts "2. Create a symlink to cli.cr"
File.symlink("../cli.cr", "#{year}/cli.cr")

puts "3. Generate problems templates and tests"
1.upto(25) do |day|
  generate(year, day)
end
