struct Range(B, E)
  def overlap?(other : Range(B, E))
    includes?(other.begin) || includes?(other.end) ||
      other.includes?(self.begin) || other.includes?(self.end)
  end

  def contains?(other : Range(B, E))
    includes?(other.begin) && (
      includes?(other.end) || (@exclusive && other.exclusive? && other.end == @end)
    )
  end

  def +(other : Range(B, E)) : Array(Range(B, E))
    result = Array(Range(B, E)).new

    if !@exclusive && @end == (other.begin - 1)
      result = [Range.new(@begin, other.end)]
    elsif !other.excludes_end? && other.end == (@begin - 1)
      result = [Range.new(other.begin, @end)]
    elsif !includes?(other.begin) && !includes?(other.end) && !other.includes?(@begin) && !other.includes?(@end)
      result = [self, other]
    elsif includes?(other.begin) && includes?(other.end)
      result = [self]
    elsif other.includes?(@begin) && other.includes?(@end)
      result = [other]
    elsif includes?(other.begin)
      result = [Range.new(@begin, other.end)]
    elsif includes?(other.end)
      result = [Range.new(other.begin, @end)]
    elsif other.includes?(@begin)
      result = [Range.new(other.begin, @end)]
    elsif other.includes?(@end)
      result = [Range.new(@begin, other.end)]
    else
      raise "not implemented !!! #{self} #{other}"
    end

    return result
  end

  def -(other : Range(B, E)) : Array(Range(B, E))
    result = Array(Range(B, E)).new
    if !includes?(other.begin) && !includes?(other.end) && !other.includes?(@begin) && !other.includes?(@end)
      result = [self]
    elsif includes?(other.begin) && includes?(other.end)
      result << Range.new(@begin, other.begin - 1) if @begin != other.begin
      result << Range.new(other.end + 1, @end) if @end != other.end
    elsif other.includes?(@begin) && other.includes?(@end)
    elsif includes?(other.begin)
      result << Range.new(@begin, other.begin - 1) if @begin != other.begin
    elsif includes?(other.end)
      result << Range.new(other.end + 1, @end) if @end != other.end
    else
      raise "not implemented !!! #{self} #{other}"
    end

    return result
  end

  def &(other : Range(B, E)) : Range(B, E)
    return other if contains?(other)
    return self if other.contains?(self)

    return Range(B, E).new(other.begin, @end, @exclusive) if includes?(other.begin)
    return Range(B, E).new(@begin, other.end, other.exclusive?) if includes?(other.end)

    raise "not implemented"
  end

  def split(other : Range) : Array(Range(B, E))
    return [self] if self == other
    return [self] if !overlap?(other)

    raise ArgumentError.new("Ranges should have same exclusivity") if @exclusive != other.exclusive?

    result = Array(Range(B, E)).new

    b = @begin
    if other.begin > @begin && includes?(other.begin)
      result << Range(B, E).new(@begin, other.begin, exclusive: true)
      b = other.begin
    end

    if other.end < @end && includes?(other.end)
      result << Range(B, E).new(b, other.end, exclusive: other.exclusive?)
      b = other.end
      b += 1 if !other.exclusive?
    end
    result << Range(B, E).new(b, @end, exclusive: @exclusive)

    result
  end
end

# Ported version from algorithms/priority_queue/ordered_pq.cr
class PriorityQueue(T)
  def initialize(initial_capacity : UInt32, &@compare_fn : Proc(T, T, Bool))
    @store = Array(T).new(initial_capacity)
  end

  def initialize(&@compare_fn : Proc(T, T, Bool))
    @store = Array(T).new(16)
  end

  def initialize
    @compare_fn = ->(x : T, y : T) { x < y }
    @store = Array(T).new(16)
  end

  def empty?
    @store.size == 0
  end

  def insert(x : T)
    indx = search(x) || @store.size
    @store.insert(indx, x)
  end

  def pop
    @store.pop
  end

  def size
    @store.size
  end

  private def search(val)
    @store.bsearch_index { |x, _| @compare_fn.call(x, val) }
  end
end

def print_matrix(arr, format_cell = "%-3s", sep = "", output = STDOUT)
  print_matrix(arr, format_cell, sep, output) { |c| c }
end

def print_matrix(arr, format_cell = "%-3s", sep = "", output = STDOUT)
  arr.each_with_index do |row, ri|
    r = [] of String
    row.each_with_index do |cell, ci|
      cell_mod = yield cell, ri, ci
      r << (format_cell % [cell_mod])
    end
    output.puts r.join(sep)
  end

  return NoReturn
end

def print_plan(plan, height, width, empty = ' ', output = STDOUT, indent = "", start_x = 0, start_y = 0)
  # Clear the screen
  # puts "\e[H\e[2J"
  output.print "#{indent}     "
  start_x.upto(width - 1) do |i|
    output.print i.to_s.chars[-1]
  end
  output.puts
  start_y.upto(height - 1) do |y|
    x = start_x
    output.print "%s%03d  " % {indent, y}
    while x < width
      output.print plan.fetch({x: x, y: y}, empty).to_s
      x += 1
    end
    output.print "\n"
  end
  output.puts "\n\n"
end

def print_plan_portion(plan, low, left, right)
  # Clear the screen
  puts "\e[H\e[2J"
  low.times do |y|
    x = left
    print "%03d  " % y
    while x < right
      print plan.fetch({x, y}, '.')
      x += 1
    end
    print "\n"
  end
  puts "\n\n"
end

alias Coord = NamedTuple(x: Int32, y: Int32)
alias Coord3D = NamedTuple(x: Int32, y: Int32, z: Int32)

# insert_sorted is a primitive function to keep array sorted
def insert_sorted(arr : Array(_), item : _) : Int
  if arr.size == 0
    arr.insert(0, item)
    return 0
  end

  # Primitive method
  # indx = arr.index { |i| i >= item }

  # Bisect method
  indx = bisect(arr, item)

  if indx
    arr.insert(indx, item)
  else
    arr << item
  end
  return indx
end

def bisect(arr, item)
  lo = 0
  hi = arr.size - 1
  indx = nil
  mid = 0

  while lo < hi
    return hi + 1 if item >= arr[hi]
    return lo if item <= arr[lo]

    mid = lo + (hi - lo).tdiv(2)
    if arr[mid] == item
      return mid
    end

    if arr[mid] > item
      break if hi == mid
      hi = mid
    else
      break if lo == mid
      lo = mid
    end
  end
  mid + 1
end

# vim: filetype=crystal syntax=crystal
