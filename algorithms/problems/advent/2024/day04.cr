# https://adventofcode.com/2024/day/4
#
# --- Day 4: Ceres Search ---
#
# "Looks like the Chief's not here. Next!" One of The Historians pulls out a device and pushes the only button on it. After a brief flash, you recognize the interior of the Ceres monitoring station!
#
# As the search for the Chief continues, a small Elf who lives on the station tugs on your shirt; she'd like to know if you could help her with her word search (your puzzle input). She only has to find one word: XMAS.
#
# This word search allows words to be horizontal, vertical, diagonal, written backwards, or even overlapping other words. It's a little unusual, though, as you don't merely need to find one instance of XMAS - you need to find all of them. Here are a few ways XMAS might appear, where irrelevant characters have been replaced with .:
#
# ..X...
# .SAMX.
# .A..A.
# XMAS.S
# .X....
#
# The actual word search will be full of letters instead. For example:
#
# MMMSXXMASM
# MSAMXMSMSA
# AMXSXMAAMM
# MSAMASMSMX
# XMASAMXAMM
# XXAMMXXAMA
# SMSMSASXSS
# SAXAMASAAA
# MAMMMXMMMM
# MXMXAXMASX
#
# In this word search, XMAS occurs a total of 18 times; here's the same word search again, but where letters not involved in any XMAS have been replaced with .:
#
# ....XXMAS.
# .SAMXMS...
# ...S..A...
# ..A.A.MS.X
# XMASAMX.MM
# X.....XA.A
# S.S.S.S.SS
# .A.A.A.A.A
# ..M.M.M.MM
# .X.X.XMASX
#
# Take a look at the little Elf's word search. How many times does XMAS appear?
#
# Your puzzle answer was 2718.
#
# Answer: 2718
# (execute time: 5.043375 ms, memory: 5694288 bytes)
#
# --- Part Two ---
#
# The Elf looks quizzically at you. Did you misunderstand the assignment?
#
# Looking for the instructions, you flip over the word search to find that this isn't actually an XMAS puzzle; it's an X-MAS puzzle in which you're supposed to find two MAS in the shape of an X. One way to achieve that is like this:
#
# M.S
# .A.
# M.S
#
# Irrelevant characters have again been replaced with . in the above diagram. Within the X, each MAS can be written forwards or backwards.
#
# Here's the same example from before, but this time all of the X-MASes have been kept instead:
#
# .M.S......
# ..A..MSMS.
# .M.S.MAA..
# ..A.ASMSM.
# .M.S.M....
# ..........
# S.S.S.S.S.
# .A.A.A.A..
# M.M.M.M.M.
# ..........
#
# In this example, an X-MAS appears 9 times.
#
# Flip the word search from the instructions back over to the word search side and try again. How many times does an X-MAS appear?
#
# Your puzzle answer was 2046.
#
# Answer: 2046
# (execute time: 2.830584 ms, memory: 1905264 bytes)

require "../utils.cr"

module Day4
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    plan = Hash(Coord, Char).new
    y = 0
    records.each do |instruction|
      next if instruction == ""
      instruction.chars.each_with_index do |c, x|
        plan[{x: x, y: y}] = c
      end
      y += 1
    end
    result = 0
    plan.each_key do |point|
      result += detect_word_at(plan, point)
    end
    result
  end

  def detect_word_at(plan, coord)
    return 0 if 'X' != plan[coord]
    neighbours_coord_diffs = StaticArray[
      StaticArray[-1, -1], StaticArray[-1, 0], StaticArray[-1, 1],
      StaticArray[0, -1], StaticArray[0, 1],
      StaticArray[1, -1], StaticArray[1, 0], StaticArray[1, 1],
    ]
    ncoord = coord
    neighbours_coords = neighbours_coord_diffs.map do |diff_coord|
      r = [] of Coord
      3.times do
        ncoord = {x: ncoord[:x] + diff_coord[0], y: ncoord[:y] + diff_coord[1]}
        r << ncoord
      end
      ncoord = coord
      r
    end

    result = 0
    neighbours_coords.each do |coords|
      if plan[coords[0]]? == 'M' && plan[coords[1]]? == 'A' && plan[coords[2]]? == 'S'
        result += 1
      end
    end

    result
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    plan = Hash(Coord, Char).new
    y = 0
    records.each do |instruction|
      next if instruction == ""
      instruction.chars.each_with_index do |c, x|
        plan[{x: x, y: y}] = c
      end
      y += 1
    end
    result = 0
    plan.each_key do |point|
      result += detect_xmas_at(plan, point)
    end
    result
  end

  def detect_xmas_at(plan, coord)
    return 0 if 'A' != plan[coord]
    neighbours_coords = [
      {x: coord[:x] - 1, y: coord[:y] - 1},
      {x: coord[:x] + 1, y: coord[:y] + 1},
      {x: coord[:x] + 1, y: coord[:y] - 1},
      {x: coord[:x] - 1, y: coord[:y] + 1},
    ]

    neighbours_coords.each do |c|
      return 0 if !plan.has_key?(c)
    end

    if (
         (plan[neighbours_coords[0]] == 'M' && plan[neighbours_coords[1]] == 'S') ||
         (plan[neighbours_coords[0]] == 'S' && plan[neighbours_coords[1]] == 'M')
       ) &&
       (
         (plan[neighbours_coords[2]] == 'M' && plan[neighbours_coords[3]] == 'S') ||
         (plan[neighbours_coords[2]] == 'S' && plan[neighbours_coords[3]] == 'M')
       )
      return 1
    end
    return 0
  end
end

# vim: filetype=crystal syntax=crystal
