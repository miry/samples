require "spec"
require "./day06"

describe Day6 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      ....#.....
      .........#
      ..........
      ..#.......
      .......#..
      ..........
      .#..^.....
      ........#.
      #.........
      ......#...
      EOF
      actual = Day6.problem1(input.split("\n"))
      actual.should eq(41)
    end

    it "test example" do
      input = <<-EOF
      ....
      #...
      .^#.
      .#..
      EOF
      actual = Day6.problem1(input.split("\n"))
      actual.should eq(3)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      ....#.....
      .........#
      ..........
      ..#.......
      .......#..
      ..........
      .#..^.....
      ........#.
      #.........
      ......#...
      EOF
      actual = Day6.problem2(input.split("\n"))
      actual.should eq(6)
    end

    it "small loop" do
      input = <<-EOF
      .#...#.
      ......#
      .^.....
      .....#.
      EOF
      actual = Day6.problem2(input.split("\n"))
      actual.should eq(2)
    end

    it "ignore start loop" do
      input = <<-EOF
      .#...#.
      ......#
      .^.....
      .......
      .....#.
      EOF
      actual = Day6.problem2(input.split("\n"))
      actual.should eq(2)
    end

    it "create obsticle in existed route" do
      input = <<-EOF
      ....#.....
      ....>>>>v#
      ....^...v.
      ..#.^...v.
      ..>>>>v#v.
      ..^.^.v.v.
      .#^<<<v<<.
      ......#.#.
      #.........
      ......#...
      EOF
      actual = Day6.problem2(input.split("\n"))
      actual.should eq(false)
    end

    it "handle loops single line" do
      input = <<-EOF
      .#...
      .....
      #^#..
      .#.#.
      EOF
      actual = Day6.problem2(input.split("\n"))
      actual.should eq(2)
    end

    it "primitive most popular case that covers most of the problems" do
      input = <<-EOF
      ....
      #...
      .^#.
      .#..
      EOF
      actual = Day6.problem2(input.split("\n"))
      actual.should eq(0)
    end

    # This test example open my eye on how I could make loops
    it "obsticles are placed before the guard starts walking" do
      input = <<-EOF
      ###
      #.#
      #.#
      #^#
      EOF
      actual = Day6.problem2(input.split("\n"))
      actual.should eq(0)
    end
  end
end

# vim: filetype=crystal syntax=crystal
