require "spec"
require "./day03"

describe Day3 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))
      EOF
      actual = Day3.problem1(input.split("\n"))
      actual.should eq(161)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      xmul(2,4)&mul[3,7]!^don't()
      _mul(5,5)+mul(32,64](mul(11,8)
      undo()?mul(8,5))
      EOF
      actual = Day3.problem2(input.split("\n"))
      actual.should eq(48)
    end
  end
end
