# https://adventofcode.com/2024/day/7
#
# --- Day 7: Bridge Repair ---
#
# The Historians take you to a familiar rope bridge over a river in the middle of a jungle. The Chief isn't on this side of the bridge, though; maybe he's on the other side?
#
# When you go to cross the bridge, you notice a group of engineers trying to repair it. (Apparently, it breaks pretty frequently.) You won't be able to cross until it's fixed.
#
# You ask how long it'll take; the engineers tell you that it only needs final calibrations, but some young elephants were playing nearby and stole all the operators from their calibration equations! They could finish the calibrations if only someone could determine which test values could possibly be produced by placing any combination of operators into their calibration equations (your puzzle input).
#
# For example:
#
# 190: 10 19
# 3267: 81 40 27
# 83: 17 5
# 156: 15 6
# 7290: 6 8 6 15
# 161011: 16 10 13
# 192: 17 8 14
# 21037: 9 7 18 13
# 292: 11 6 16 20
#
# Each line represents a single equation. The test value appears before the colon on each line; it is your job to determine whether the remaining numbers can be combined with operators to produce the test value.
#
# Operators are always evaluated left-to-right, not according to precedence rules. Furthermore, numbers in the equations cannot be rearranged. Glancing into the jungle, you can see elephants holding two different types of operators: add (+) and multiply (*).
#
# Only three of the above equations can be made true by inserting operators:
#
#     190: 10 19 has only one position that accepts an operator: between 10 and 19. Choosing + would give 29, but choosing * would give the test value (10 * 19 = 190).
#     3267: 81 40 27 has two positions for operators. Of the four possible configurations of the operators, two cause the right side to match the test value: 81 + 40 * 27 and 81 * 40 + 27 both equal 3267 (when evaluated left-to-right)!
#     292: 11 6 16 20 can be solved in exactly one way: 11 + 6 * 16 + 20.
#
# The engineers just need the total calibration result, which is the sum of the test values from just the equations that could possibly be true. In the above example, the sum of the test values for the three equations listed above is 3749.
#
# Determine which equations could possibly be true. What is their total calibration result?
#
# Your puzzle answer was 850435817339.
#
# Output:
#   Answer: 850435817339
#   (execute time: 1.900875 ms, memory: 1892448 bytes)
#
# --- Part Two ---
#
# The engineers seem concerned; the total calibration result you gave them is nowhere close to being within safety tolerances. Just then, you spot your mistake: some well-hidden elephants are holding a third type of operator.
#
# The concatenation operator (||) combines the digits from its left and right inputs into a single number. For example, 12 || 345 would become 12345. All operators are still evaluated left-to-right.
#
# Now, apart from the three equations that could be made true using only addition and multiplication, the above example has three more equations that can be made true by inserting operators:
#
#     156: 15 6 can be made true through a single concatenation: 15 || 6 = 156.
#     7290: 6 8 6 15 can be made true using 6 * 8 || 6 * 15.
#     192: 17 8 14 can be made true using 17 || 8 + 14.
#
# Adding up all six test values (the three that could be made before using only + and * plus the new three that can now be made by also using ||) produces the new total calibration result of 11387.
#
# Using your new knowledge of elephant hiding spots, determine which equations could possibly be true. What is their total calibration result?
#
# Your puzzle answer was 104824810233437.
#
# Output:
#   Answer: 104824810233437
#   (execute time: 3.333709 ms, memory: 3086000 bytes)

require "../utils.cr"

module Day7
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    result = 0_i64
    records.each do |instruction|
      next if instruction == ""

      equation_result, operands_raw = instruction.split(": ", 2)
      equation = equation_result.to_i64
      operands = operands_raw.split(" ").map(&.to_i64)
      if valid_equation?(equation, operands)
        result += equation
      end
    end

    result
  end

  def valid_equation?(equation : Int64, operands)
    return true if equation == 0 && operands.size == 0
    return false if equation < 0 || (operands.empty? && equation > 0)
    a = operands.last
    b = operands[..-2]
    valid_equation?(equation - a, b) || ((equation % a == 0) && valid_equation?((equation / a).to_i64, b))
  end

  def valid_equation_with_combine?(equation : Int64, operands)
    return true if equation == 0 && operands.size == 0
    return false if equation < 0 || (operands.empty? && equation > 0)

    a = operands.last
    return (a == equation) if operands.size == 1
    b = operands[..-2]

    result = valid_equation_with_combine?(equation - a, b) || \
       ((equation % a == 0) && valid_equation_with_combine?((equation / a).to_i64, b))

    if !result
      eq = equation.to_s
      ass = a.to_s
      if eq.ends_with?(ass) && eq != ass
        result = valid_equation_with_combine?(eq[...-ass.size].to_i64, b)
      end
    end

    result
  end

  # The help tool to validate the expression in tests
  def make_expression(equation : Int64, operands) : Bool | Array(Int64 | Char)
    return Array(Int64 | Char).new if equation == 0 && operands.size == 0
    return false if equation < 0 || (operands.empty? && equation > 0)

    a = operands.last
    if operands.size == 1
      if a == equation
        return [a] of (Int64 | Char)
      else
        return false
      end
    end

    a = operands.last
    b = operands[..-2]

    # Sum
    result = make_expression(equation - a, b)
    return result.as(Array(Int64 | Char)) + ['+', a] if result

    # Multiplication
    if equation % a == 0
      result = make_expression((equation / a).to_i64, b)
      return result.as(Array(Int64 | Char)) + ['*', a] if result
    end

    # Concat
    eq = equation.to_s
    ass = a.to_s
    if eq.ends_with?(ass) && eq != ass
      result = make_expression(eq[...-ass.size].to_i64, b)
      return result.as(Array(Int64 | Char)) + ['|', a] if result
    end

    return false
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    result = 0_i64
    records.each do |instruction|
      next if instruction == ""

      equation_result, operands_raw = instruction.split(": ", 2)
      equation = equation_result.to_i64
      operands = operands_raw.split(" ").map(&.to_i64)
      v = valid_equation_with_combine?(equation, operands)
      result += equation if v

      # Debug the expressions
      # crystal run -Dxdebug cli.cr -- -d 7 -p 2 -i input/day07.txt > out.log
      {% if flag?(:xdebug) %}
        puts "#{instruction} => #{v}: #{make_expression(equation, operands)}"
      {% end %}
    end

    result
  end
end
# vim: filetype=crystal syntax=crystal
