# https://adventofcode.com/2024/day/9
#
# --- Day 9: Disk Fragmenter ---
#
# Another push of the button leaves you in the familiar hallways of some friendly amphipods! Good thing you each somehow got your own personal mini submarine. The Historians jet away in search of the Chief, mostly by driving directly into walls.
#
# While The Historians quickly figure out how to pilot these things, you notice an amphipod in the corner struggling with his computer. He's trying to make more contiguous free space by compacting all of the files, but his program isn't working; you offer to help.
#
# He shows you the disk map (your puzzle input) he's already generated. For example:
#
# 2333133121414131402
#
# The disk map uses a dense format to represent the layout of files and free space on the disk. The digits alternate between indicating the length of a file and the length of free space.
#
# So, a disk map like 12345 would represent a one-block file, two blocks of free space, a three-block file, four blocks of free space, and then a five-block file. A disk map like 90909 would represent three nine-block files in a row (with no free space between them).
#
# Each file on disk also has an ID number based on the order of the files as they appear before they are rearranged, starting with ID 0. So, the disk map 12345 has three files: a one-block file with ID 0, a three-block file with ID 1, and a five-block file with ID 2. Using one character for each block where digits are the file ID and . is free space, the disk map 12345 represents these individual blocks:
#
# 0..111....22222
#
# The first example above, 2333133121414131402, represents these individual blocks:
#
# 00...111...2...333.44.5555.6666.777.888899
#
# The amphipod would like to move file blocks one at a time from the end of the disk to the leftmost free space block (until there are no gaps remaining between file blocks). For the disk map 12345, the process looks like this:
#
# 0..111....22222
# 02.111....2222.
# 022111....222..
# 0221112...22...
# 02211122..2....
# 022111222......
#
# The first example requires a few more steps:
#
# 00...111...2...333.44.5555.6666.777.888899
# 009..111...2...333.44.5555.6666.777.88889.
# 0099.111...2...333.44.5555.6666.777.8888..
# 00998111...2...333.44.5555.6666.777.888...
# 009981118..2...333.44.5555.6666.777.88....
# 0099811188.2...333.44.5555.6666.777.8.....
# 009981118882...333.44.5555.6666.777.......
# 0099811188827..333.44.5555.6666.77........
# 00998111888277.333.44.5555.6666.7.........
# 009981118882777333.44.5555.6666...........
# 009981118882777333644.5555.666............
# 00998111888277733364465555.66.............
# 0099811188827773336446555566..............
#
# The final step of this file-compacting process is to update the filesystem checksum. To calculate the checksum, add up the result of multiplying each of these blocks' position with the file ID number it contains. The leftmost block is in position 0. If a block contains free space, skip it instead.
#
# Continuing the first example, the first few blocks' position multiplied by its file ID number are 0 * 0 = 0, 1 * 0 = 0, 2 * 9 = 18, 3 * 9 = 27, 4 * 8 = 32, and so on. In this example, the checksum is the sum of these, 1928.
#
# Compact the amphipod's hard drive using the process he requested. What is the resulting filesystem checksum? (Be careful copy/pasting the input for this puzzle; it is a single, very long line.)
#
# Your puzzle answer was 6201130364722.
# --- Part Two ---
#
# Upon completion, two things immediately become clear. First, the disk definitely has a lot more contiguous free space, just like the amphipod hoped. Second, the computer is running much more slowly! Maybe introducing all of that file system fragmentation was a bad idea?
#
# The eager amphipod already has a new plan: rather than move individual blocks, he'd like to try compacting the files on his disk by moving whole files instead.
#
# This time, attempt to move whole files to the leftmost span of free space blocks that could fit the file. Attempt to move each file exactly once in order of decreasing file ID number starting with the file with the highest file ID number. If there is no span of free space to the left of a file that is large enough to fit the file, the file does not move.
#
# The first example from above now proceeds differently:
#
# 00...111...2...333.44.5555.6666.777.888899
# 0099.111...2...333.44.5555.6666.777.8888..
# 0099.1117772...333.44.5555.6666.....8888..
# 0099.111777244.333....5555.6666.....8888..
# 00992111777.44.333....5555.6666.....8888..
#
# The process of updating the filesystem checksum is the same; now, this example's checksum would be 2858.
#
# Start over, now compacting the amphipod's hard drive using this new method instead. What is the resulting filesystem checksum?
#
# Your puzzle answer was 6221662795602.
#
# Output:
#   Answer: 6221662795602
#   (execute time: 66.621375 ms, memory: 1233600 bytes)

require "../utils.cr"

module Day9
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    disk = [] of Int32
    free = [] of Int32
    indx = 0
    records.each do |instruction|
      next if instruction == ""
      file = true
      id = 0
      instruction.each_char do |c|
        size = c.to_i32

        f = file ? id : -1
        size.times do
          disk << f
          free << indx if !file
          indx += 1
        end
        file = !file
        id += 1 if file
      end
    end

    last = disk.size - 1
    while disk[last] < 0
      last -= 1
    end
    while !free.empty?
      indx = free.shift
      break if last < indx

      disk[indx] = disk[last]
      disk[last] = -1
      last -= 1
      while disk[last] < 0
        last -= 1
      end
    end

    # checksum
    result = 0_i64
    disk.each_with_index do |v, i|
      break if v == -1
      result += v * i
    end
    result
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    free = [] of Tuple(Int32, Int32)
    files = [] of Tuple(Int32, Int32, Int32)
    indx = 0
    records.each do |instruction|
      next if instruction == ""
      file = true
      id = 0
      instruction.each_char do |c|
        size = c.to_i32

        f = file ? id : -1
        if file
          files << {indx, size, id} if size > 0
          id += 1
        else
          free << {indx, size} if size > 0
        end
        indx += size
        file = !file
      end
    end

    block_indx = files.size - 1
    indx = 0
    while block_indx >= 0
      block = files[block_indx]
      free_block_indx = 0
      while free_block_indx < free.size
        new_max = 0
        free_block = free[free_block_indx]
        break if free_block[0] > block[0]
        if free_block[1] >= block[1]
          files[block_indx] = {free_block[0], block[1], block[2]}
          if free_block[1] == block[1]
            free.delete_at(free_block_indx)
          else
            free[free_block_indx] = {free_block[0] + block[1], free_block[1] - block[1]}
          end
          break
        end
        free_block_indx += 1
      end

      block_indx -= 1
      # gets
    end

    # checksum
    result = 0_i64
    files.each do |b|
      indx = b[0]
      b[1].times do |i|
        result += b[2] * (indx + i)
      end
    end
    result
  end
end
# vim: filetype=crystal syntax=crystal
