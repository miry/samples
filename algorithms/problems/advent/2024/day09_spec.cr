require "spec"
require "./day09"

describe Day9 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      2333133121414131402
      EOF
      actual = Day9.problem1(input.split("\n"))
      actual.should eq(1928)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      2333133121414131402
      EOF
      actual = Day9.problem2(input.split("\n"))
      actual.should eq(2858)
    end
  end
end

# vim: filetype=crystal syntax=crystal
