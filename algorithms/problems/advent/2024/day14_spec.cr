require "spec"
require "./day14"

describe Day14 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      p=0,4 v=3,-3
      p=6,3 v=-1,-3
      p=10,3 v=-1,2
      p=2,0 v=2,-1
      p=0,0 v=1,3
      p=3,0 v=-2,-2
      p=7,6 v=-1,-3
      p=3,0 v=-1,-2
      p=9,3 v=2,3
      p=7,3 v=-1,2
      p=2,4 v=2,-3
      p=9,5 v=-3,-3
      EOF
      actual = Day14.problem1(input.split("\n"), {x: 11, y: 7})
      actual.should eq(12)
    end
  end

  describe "problem2" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = Day14.problem2(input.split("\n"))
      actual.should eq(12)
    end
  end
end
# vim: filetype=crystal syntax=crystal
