require "spec"
require "./day23"

describe Day23 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      kh-tc
      qp-kh
      de-cg
      ka-co
      yn-aq
      qp-ub
      cg-tb
      vc-aq
      tb-ka
      wh-tc
      yn-cg
      kh-ub
      ta-co
      de-co
      tc-td
      tb-wq
      wh-td
      ta-ka
      td-qp
      aq-cg
      wq-ub
      ub-vc
      de-ta
      wq-aq
      wq-vc
      wh-yn
      ka-de
      kh-ta
      co-tc
      wh-qp
      tb-vc
      td-yn
      EOF
      actual = Day23.problem1(input.split("\n"))
      actual.should eq(7)
    end
  end

  describe "problem2" do
    it "single" do
      input = <<-EOF
      ka-co
      ta-co 
      de-co
      co-qq
      ta-ka
      de-ta
      ka-de
      qq-de
      EOF
      actual = Day23.problem2(input.split("\n"))
      actual.should eq("co,de,ka,ta")
    end

    it "sample" do
      input = <<-EOF
      kh-tc
      qp-kh
      de-cg
      ka-co
      yn-aq
      qp-ub
      cg-tb
      vc-aq
      tb-ka
      wh-tc
      yn-cg
      kh-ub
      ta-co
      de-co
      tc-td
      tb-wq
      wh-td
      ta-ka
      td-qp
      aq-cg
      wq-ub
      ub-vc
      de-ta
      wq-aq
      wq-vc
      wh-yn
      ka-de
      kh-ta
      co-tc
      wh-qp
      tb-vc
      td-yn
      EOF
      actual = Day23.problem2(input.split("\n"))
      actual.should eq("co,de,ka,ta")
    end
  end
end

# vim: filetype=crystal syntax=crystal
