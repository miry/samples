# https://adventofcode.com/2024/day/21
#
# --- Day 21: ... ---
#

require "../utils.cr"

module Day21
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    codes = [] of Array(Char)
    records.each do |instruction|
      next if instruction == ""
      codes << instruction.chars
    end
    pp! codes

    codes.each do |code|
      pp! code

      code1 = code_directions(code)
      pp! code1.size
      code2 = code_directions(code1, table: ROUTES_DIRECTIONS)
      pp! code2.size
      code3 = code_directions(code2, table: ROUTES_DIRECTIONS)
      pp! code3.size
      break
    end
  end

  DIRECTIONS     = ['^', '>', 'v', '<']
  ROUTES_NUMERIC = {
    'A' => ['3', nil, nil, '0'],
    '0' => ['2', 'A', nil, nil],
    '1' => ['4', '2', nil, nil],
    '2' => ['5', '3', '0', '1'],
    '3' => ['6', nil, 'A', '2'],
    '4' => ['7', '5', '1', nil],
    '5' => ['8', '6', '2', '4'],
    '6' => ['9', nil, '3', '5'],
    '7' => [nil, '8', '4', nil],
    '8' => [nil, '9', '5', '7'],
    '9' => [nil, nil, '6', '9'],
  }

  ROUTES_DIRECTIONS = {
    'A' => [nil, nil, '>', '^'],
    '^' => [nil, 'A', 'v', nil],
    '<' => [nil, 'v', nil, nil],
    'v' => ['^', '>', nil, '<'],
    '>' => ['A', nil, nil, 'v'],
  }

  def directional_keypad(start, finish, table) : Array(Array(Char))
    # puts "> directional_keypad(#{start}, #{finish}, #{depth})"
    dest = table[start]
    direction_index = dest.index(finish)
    if direction_index
      return [[DIRECTIONS[direction_index]]]
    end

    visited = Set(Char).new
    result = [] of Array(Char)

    queue = dest.map { |c| [c] }
    found = false
    # while !found
    #   new_queue = [] of Char
    #
    #   queue.each do |neighbour|
    #     next if visited.includes?(neighbour)
    #     if neighbour.last == finish
    #       found
    #     end
    #   end
    #
    #   queue = new_queue
    # end

    result
  end

  def code_directions(code, table = ROUTES_NUMERIC)
    start = 'A'
    i = 0
    n = code.size
    result = [] of Char
    while i < n
      cur = code[i]
      directions = directional_keypad(start, cur, table: table)
      result += directions
      result << 'A'
      start = cur
      i += 1
    end
    result
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    records.each do |instruction|
      next if instruction == ""
    end

    raise "Day 21.2 is not implemented"
  end
end

# vim: filetype=crystal syntax=crystal
