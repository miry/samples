# https://adventofcode.com/2024/day/6
#
# --- Day 6: Guard Gallivant ---
#
# The Historians use their fancy device again, this time to whisk you all away to the North Pole prototype suit manufacturing lab... in the year 1518! It turns out that having direct access to history is very convenient for a group of historians.
#
# You still have to be careful of time paradoxes, and so it will be important to avoid anyone from 1518 while The Historians search for the Chief. Unfortunately, a single guard is patrolling this part of the lab.
#
# Maybe you can work out where the guard will go ahead of time so that The Historians can search safely?
#
# You start by making a map (your puzzle input) of the situation. For example:
#
# ....#.....
# .........#
# ..........
# ..#.......
# .......#..
# ..........
# .#..^.....
# ........#.
# #.........
# ......#...
#
# The map shows the current position of the guard with ^ (to indicate the guard is currently facing up from the perspective of the map). Any obstructions - crates, desks, alchemical reactors, etc. - are shown as #.
#
# Lab guards in 1518 follow a very strict patrol protocol which involves repeatedly following these steps:
#
#     If there is something directly in front of you, turn right 90 degrees.
#     Otherwise, take a step forward.
#
# Following the above protocol, the guard moves up several times until she reaches an obstacle (in this case, a pile of failed suit prototypes):
#
# ....#.....
# ....^....#
# ..........
# ..#.......
# .......#..
# ..........
# .#........
# ........#.
# #.........
# ......#...
#
# Because there is now an obstacle in front of the guard, she turns right before continuing straight in her new facing direction:
#
# ....#.....
# ........>#
# ..........
# ..#.......
# .......#..
# ..........
# .#........
# ........#.
# #.........
# ......#...
#
# Reaching another obstacle (a spool of several very long polymers), she turns right again and continues downward:
#
# ....#.....
# .........#
# ..........
# ..#.......
# .......#..
# ..........
# .#......v.
# ........#.
# #.........
# ......#...
#
# This process continues for a while, but the guard eventually leaves the mapped area (after walking past a tank of universal solvent):
#
# ....#.....
# .........#
# ..........
# ..#.......
# .......#..
# ..........
# .#........
# ........#.
# #.........
# ......#v..
#
# By predicting the guard's route, you can determine which specific positions in the lab will be in the patrol path. Including the guard's starting position, the positions visited by the guard before leaving the area are marked with an X:
#
# ....#.....
# ....XXXXX#
# ....X...X.
# ..#.X...X.
# ..XXXXX#X.
# ..X.X.X.X.
# .#XXXXXXX.
# .XXXXXXX#.
# #XXXXXXX..
# ......#X..
#
# In this example, the guard will visit 41 distinct positions on your map.
#
# Predict the path of the guard. How many distinct positions will the guard visit before leaving the mapped area?
#
# Your puzzle answer was 4982.
#
# Output:
#   Answer: 4982
#   (execute time: 1.551 ms, memory: 1641776 bytes)
#
# --- Part Two ---
#
# While The Historians begin working around the guard's patrol route, you borrow their fancy device and step outside the lab. From the safety of a supply closet, you time travel through the last few months and record the nightly status of the lab's guard post on the walls of the closet.
#
# Returning after what seems like only a few seconds to The Historians, they explain that the guard's patrol area is simply too large for them to safely search the lab without getting caught.
#
# Fortunately, they are pretty sure that adding a single new obstruction won't cause a time paradox. They'd like to place the new obstruction in such a way that the guard will get stuck in a loop, making the rest of the lab safe to search.
#
# To have the lowest chance of creating a time paradox, The Historians would like to know all of the possible positions for such an obstruction. The new obstruction can't be placed at the guard's starting position - the guard is there right now and would notice.
#
# In the above example, there are only 6 different positions where a new obstruction would cause the guard to get stuck in a loop. The diagrams of these six situations use O to mark the new obstruction, | to show a position where the guard moves up/down, - to show a position where the guard moves left/right, and + to show a position where the guard moves both up/down and left/right.
#
# Option one, put a printing press next to the guard's starting position:
#
# ....#.....
# ....+---+#
# ....|...|.
# ..#.|...|.
# ....|..#|.
# ....|...|.
# .#.O^---+.
# ........#.
# #.........
# ......#...
#
# Option two, put a stack of failed suit prototypes in the bottom right quadrant of the mapped area:
#
# ....#.....
# ....+---+#
# ....|...|.
# ..#.|...|.
# ..+-+-+#|.
# ..|.|.|.|.
# .#+-^-+-+.
# ......O.#.
# #.........
# ......#...
#
# Option three, put a crate of chimney-squeeze prototype fabric next to the standing desk in the bottom right quadrant:
#
# ....#.....
# ....+---+#
# ....|...|.
# ..#.|...|.
# ..+-+-+#|.
# ..|.|.|.|.
# .#+-^-+-+.
# .+----+O#.
# #+----+...
# ......#...
#
# Option four, put an alchemical retroencabulator near the bottom left corner:
#
# ....#.....
# ....+---+#
# ....|...|.
# ..#.|...|.
# ..+-+-+#|.
# ..|.|.|.|.
# .#+-^-+-+.
# ..|...|.#.
# #O+---+...
# ......#...
#
# Option five, put the alchemical retroencabulator a bit to the right instead:
#
# ....#.....
# ....+---+#
# ....|...|.
# ..#.|...|.
# ..+-+-+#|.
# ..|.|.|.|.
# .#+-^-+-+.
# ....|.|.#.
# #..O+-+...
# ......#...
#
# Option six, put a tank of sovereign glue right next to the tank of universal solvent:
#
# ....#.....
# ....+---+#
# ....|...|.
# ..#.|...|.
# ..+-+-+#|.
# ..|.|.|.|.
# .#+-^-+-+.
# .+----++#.
# #+----++..
# ......#O..
#
# It doesn't really matter what you choose to use as an obstacle so long as you and The Historians can put it into position without the guard noticing. The important thing is having enough options that you can find one that minimizes time paradoxes, and in this example, there are 6 different positions you could choose.
#
# You need to get the guard stuck in a loop by adding a single new obstruction. How many different positions could you choose for this obstruction?
#
# Your puzzle answer was 1663.
#
# Output:
#   Answer: 1663
#   (execute time: 1376.944375 ms, memory: 6117443632 bytes)
#

require "../utils.cr"

module Day6
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    y = 0
    width = records.first.size
    map = Hash(Coord, Char).new
    guard = Coord.new(x: 0, y: 0)
    records.each do |instruction|
      next if instruction == ""
      instruction.chars.each_with_index do |col, x|
        c = Coord.new(x: x, y: y)
        map[c] = col
        if col != '.' && col != '#'
          guard = c
        end
      end
      y += 1
    end

    {% if flag?(:xdebug) %}
      print_plan map, y, width, '.'
    {% end %}

    directions = [
      Coord.new(x: 0, y: -1),
      Coord.new(x: 1, y: 0),
      Coord.new(x: 0, y: 1),
      Coord.new(x: -1, y: 0),
    ]
    direction_indx = {
      '^' => 0,
      '>' => 1,
      'v' => 2,
      '<' => 3,
    }[map[guard]]
    direction = directions[direction_indx]

    map[guard] = 'G'

    loop do
      next_step = Coord.new(x: guard[:x] + direction[:x], y: guard[:y] + direction[:y])
      break if !map.has_key?(next_step)
      if map[next_step] == '#'
        direction_indx += 1
        direction_indx %= 4
        direction = directions[direction_indx]
        next
      end
      guard = next_step
      map[guard] = 'G'
    end

    map.values.count { |i| i == 'G' }
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    y = 0
    width = records.first.size
    map = Hash(Coord, Char).new
    start = Coord.new(x: 0, y: 0)
    direction_symbol = '*'
    records.each do |instruction|
      next if instruction == ""
      instruction.chars.each_with_index do |col, x|
        c = Coord.new(x: x, y: y)
        map[c] = col
        if col != '.' && col != '#'
          start = c
          direction_symbol = col
        end
      end
      y += 1
    end
    height = y
    guard = start

    travel(map, height, width, start, guard, direction_symbol, loops: true, visited: nil)
  end

  def travel(map, height, width, start, guard, direction_symbol, visited = nil, loops = true)
    visited ||= Hash(Coord, Set(Char)).new

    directions = StaticArray[
      Coord.new(x: 0, y: -1),
      Coord.new(x: 1, y: 0),
      Coord.new(x: 0, y: 1),
      Coord.new(x: -1, y: 0),
    ]
    direction_symbols = StaticArray['^', '>', 'v', '<']
    direction_map = {
      '^' => 0,
      '>' => 1,
      'v' => 2,
      '<' => 3,
    }
    direction_indx = direction_map[direction_symbol]
    direction = directions[direction_indx]

    obsticles = Set(Coord).new
    loop do
      next_step = Coord.new(x: guard[:x] + direction[:x], y: guard[:y] + direction[:y])

      # Check if guard reached the border
      break if !map.has_key?(next_step)

      # Verify it the guard in the loop
      return false if visited.has_key?(next_step) && visited[next_step].includes?(direction_symbol)

      if map[next_step] == '#'
        direction_indx = (direction_indx + 1) % 4
        direction_symbol = direction_symbols[direction_indx]
        direction = directions[direction_indx]

        return false if visited.has_key?(guard) && visited[guard].includes?(direction_symbol)
        map[guard] = direction_symbol
        visited[guard] ||= Set(Char).new
        visited[guard].add direction_symbol

        next
      end

      if loops && next_step != start && !visited.has_key?(next_step)
        modified = map.dup
        modified[next_step] = '#'
        if !travel(modified, height, width, start, guard, direction_symbol, loops: false, visited: visited.clone)
          obsticles.add(next_step)
        end
      end

      guard = next_step
      map[guard] = direction_symbol
      visited[guard] ||= Set(Char).new
      visited[guard].add direction_symbol

      {% if flag?(:xdebug) %}
        if loops
          # clear the page
          puts "\e[H\e[2J"
          print_plan map, height, width, '.'
          sleep 100.milliseconds
        end
      {% end %}
    end
    return obsticles.size
  end
end

# vim: filetype=crystal syntax=crystal
