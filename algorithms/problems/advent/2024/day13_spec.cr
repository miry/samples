require "spec"
require "./day13"

describe Day13 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      Button A: X+94, Y+34
      Button B: X+22, Y+67
      Prize: X=8400, Y=5400

      Button A: X+26, Y+66
      Button B: X+67, Y+21
      Prize: X=12748, Y=12176

      Button A: X+17, Y+86
      Button B: X+84, Y+37
      Prize: X=7870, Y=6450

      Button A: X+69, Y+23
      Button B: X+27, Y+71
      Prize: X=18641, Y=10279
      EOF
      actual = Day13.problem1(input.split("\n"))
      actual.should eq(480)
    end

    it "solution" do
      actual = Day13.problem1(File.read_lines("./input/day13.txt"))
      actual.should eq(29877)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      Button A: X+94, Y+34
      Button B: X+22, Y+67
      Prize: X=8400, Y=5400

      Button A: X+26, Y+66
      Button B: X+67, Y+21
      Prize: X=12748, Y=12176

      Button A: X+17, Y+86
      Button B: X+84, Y+37
      Prize: X=7870, Y=6450

      Button A: X+69, Y+23
      Button B: X+27, Y+71
      Prize: X=18641, Y=10279
      EOF
      actual = Day13.problem2(input.split("\n"))
      actual.should eq(875318608908)
    end

    it "solution" do
      actual = Day13.problem2(File.read_lines("./input/day13.txt"))
      actual.should eq(99423413811305)
    end
  end
end
# vim: filetype=crystal syntax=crystal
