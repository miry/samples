require "spec"
require "./day18"

describe Day18 do
  describe "problem1" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = Day18.problem1(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem2" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = Day18.problem2(input.split("\n"))
      actual.should eq(12)
    end
  end
end
# vim: filetype=crystal syntax=crystal
