require "spec"
require "./day08"

describe Day8 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      ............
      ........0...
      .....0......
      .......0....
      ....0.......
      ......A.....
      ............
      ............
      ........A...
      .........A..
      ............
      ............
      EOF
      actual = Day8.problem1(input.split("\n"))
      actual.should eq(14)
    end
  end

  describe "problem2" do
    it "small" do
      input = <<-EOF
      T.........
      ...T......
      .T........
      ..........
      ..........
      ..........
      ..........
      ..........
      ..........
      ..........
      EOF
      actual = Day8.problem2(input.split("\n"))
      actual.should eq(9)
    end
    it "sample" do
      input = <<-EOF
      ............
      ........0...
      .....0......
      .......0....
      ....0.......
      ......A.....
      ............
      ............
      ........A...
      .........A..
      ............
      ............
      EOF
      actual = Day8.problem2(input.split("\n"))
      actual.should eq(34)
    end
  end
end

# vim: filetype=crystal syntax=crystal
