require "spec"
require "./day19"

describe Day19 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      r, wr, b, g, bwu, rb, gb, br

      brwrr
      bggr
      gbbr
      rrbgbr
      ubwu
      bwurrg
      brgr
      bbrgwb
      EOF
      actual = Day19.problem1(input.split("\n"))
      actual.should eq(6)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      r, wr, b, g, bwu, rb, gb, br

      brwrr
      bggr
      gbbr
      rrbgbr
      ubwu
      bwurrg
      brgr
      bbrgwb
      EOF
      actual = Day19.problem2(input.split("\n"))
      actual.should eq(16)
    end
  end
end
# vim: filetype=crystal syntax=crystal
