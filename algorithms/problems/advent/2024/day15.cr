# https://adventofcode.com/2024/day/15
#
# --- Day 15: Warehouse Woes ---
#
# You appear back inside your own mini submarine! Each Historian drives their mini submarine in a different direction; maybe the Chief has his own submarine down here somewhere as well?
#
# You look up to see a vast school of lanternfish swimming past you. On closer inspection, they seem quite anxious, so you drive your mini submarine over to see if you can help.
#
# Because lanternfish populations grow rapidly, they need a lot of food, and that food needs to be stored somewhere. That's why these lanternfish have built elaborate warehouse complexes operated by robots!
#
# These lanternfish seem so anxious because they have lost control of the robot that operates one of their most important warehouses! It is currently running amok, pushing around boxes in the warehouse with no regard for lanternfish logistics or lanternfish inventory management strategies.
#
# Right now, none of the lanternfish are brave enough to swim up to an unpredictable robot so they could shut it off. However, if you could anticipate the robot's movements, maybe they could find a safe option.
#
# The lanternfish already have a map of the warehouse and a list of movements the robot will attempt to make (your puzzle input). The problem is that the movements will sometimes fail as boxes are shifted around, making the actual movements of the robot difficult to predict.
#
# For example:
#
# ##########
# #..O..O.O#
# #......O.#
# #.OO..O.O#
# #..O@..O.#
# #O#..O...#
# #O..O..O.#
# #.OO.O.OO#
# #....O...#
# ##########
#
# <vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^
# vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v
# ><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<
# <<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^
# ^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><
# ^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^
# >^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^
# <><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>
# ^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>
# v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^
#
# As the robot (@) attempts to move, if there are any boxes (O) in the way, the robot will also attempt to push those boxes. However, if this action would cause the robot or a box to move into a wall (#), nothing moves instead, including the robot. The initial positions of these are shown on the map at the top of the document the lanternfish gave you.
#
# The rest of the document describes the moves (^ for up, v for down, < for left, > for right) that the robot will attempt to make, in order. (The moves form a single giant sequence; they are broken into multiple lines just to make copy-pasting easier. Newlines within the move sequence should be ignored.)
#
# Here is a smaller example to get started:
#
# ########
# #..O.O.#
# ##@.O..#
# #...O..#
# #.#.O..#
# #...O..#
# #......#
# ########
#
# <^^>>>vv<v>>v<<
#
# Were the robot to attempt the given sequence of moves, it would push around the boxes as follows:
#
# Initial state:
# ########
# #..O.O.#
# ##@.O..#
# #...O..#
# #.#.O..#
# #...O..#
# #......#
# ########
#
# Move <:
# ########
# #..O.O.#
# ##@.O..#
# #...O..#
# #.#.O..#
# #...O..#
# #......#
# ########
#
# Move ^:
# ########
# #.@O.O.#
# ##..O..#
# #...O..#
# #.#.O..#
# #...O..#
# #......#
# ########
#
# Move ^:
# ########
# #.@O.O.#
# ##..O..#
# #...O..#
# #.#.O..#
# #...O..#
# #......#
# ########
#
# Move >:
# ########
# #..@OO.#
# ##..O..#
# #...O..#
# #.#.O..#
# #...O..#
# #......#
# ########
#
# Move >:
# ########
# #...@OO#
# ##..O..#
# #...O..#
# #.#.O..#
# #...O..#
# #......#
# ########
#
# Move >:
# ########
# #...@OO#
# ##..O..#
# #...O..#
# #.#.O..#
# #...O..#
# #......#
# ########
#
# Move v:
# ########
# #....OO#
# ##..@..#
# #...O..#
# #.#.O..#
# #...O..#
# #...O..#
# ########
#
# Move v:
# ########
# #....OO#
# ##..@..#
# #...O..#
# #.#.O..#
# #...O..#
# #...O..#
# ########
#
# Move <:
# ########
# #....OO#
# ##.@...#
# #...O..#
# #.#.O..#
# #...O..#
# #...O..#
# ########
#
# Move v:
# ########
# #....OO#
# ##.....#
# #..@O..#
# #.#.O..#
# #...O..#
# #...O..#
# ########
#
# Move >:
# ########
# #....OO#
# ##.....#
# #...@O.#
# #.#.O..#
# #...O..#
# #...O..#
# ########
#
# Move >:
# ########
# #....OO#
# ##.....#
# #....@O#
# #.#.O..#
# #...O..#
# #...O..#
# ########
#
# Move v:
# ########
# #....OO#
# ##.....#
# #.....O#
# #.#.O@.#
# #...O..#
# #...O..#
# ########
#
# Move <:
# ########
# #....OO#
# ##.....#
# #.....O#
# #.#O@..#
# #...O..#
# #...O..#
# ########
#
# Move <:
# ########
# #....OO#
# ##.....#
# #.....O#
# #.#O@..#
# #...O..#
# #...O..#
# ########
#
# The larger example has many more moves; after the robot has finished those moves, the warehouse would look like this:
#
# ##########
# #.O.O.OOO#
# #........#
# #OO......#
# #OO@.....#
# #O#.....O#
# #O.....OO#
# #O.....OO#
# #OO....OO#
# ##########
#
# The lanternfish use their own custom Goods Positioning System (GPS for short) to track the locations of the boxes. The GPS coordinate of a box is equal to 100 times its distance from the top edge of the map plus its distance from the left edge of the map. (This process does not stop at wall tiles; measure all the way to the edges of the map.)
#
# So, the box shown below has a distance of 1 from the top edge of the map and 4 from the left edge of the map, resulting in a GPS coordinate of 100 * 1 + 4 = 104.
#
# #######
# #...O..
# #......
#
# The lanternfish would like to know the sum of all boxes' GPS coordinates after the robot finishes moving. In the larger example, the sum of all boxes' GPS coordinates is 10092. In the smaller example, the sum is 2028.
#
# Predict the motion of the robot and boxes in the warehouse. After the robot is finished moving, what is the sum of all boxes' GPS coordinates?
#
# Your puzzle answer was 1505963.
#
# Output:
#
#   Answer: 1505963
#   (execute time: 2.614125 ms, memory: 1140784 bytes)
#
# --- Part Two ---
#
# The lanternfish use your information to find a safe moment to swim in and turn off the malfunctioning robot! Just as they start preparing a festival in your honor, reports start coming in that a second warehouse's robot is also malfunctioning.
#
# This warehouse's layout is surprisingly similar to the one you just helped. There is one key difference: everything except the robot is twice as wide! The robot's list of movements doesn't change.
#
# To get the wider warehouse's map, start with your original map and, for each tile, make the following changes:
#
#     If the tile is #, the new map contains ## instead.
#     If the tile is O, the new map contains [] instead.
#     If the tile is ., the new map contains .. instead.
#     If the tile is @, the new map contains @. instead.
#
# This will produce a new warehouse map which is twice as wide and with wide boxes that are represented by []. (The robot does not change size.)
#
# The larger example from before would now look like this:
#
# ####################
# ##....[]....[]..[]##
# ##............[]..##
# ##..[][]....[]..[]##
# ##....[]@.....[]..##
# ##[]##....[]......##
# ##[]....[]....[]..##
# ##..[][]..[]..[][]##
# ##........[]......##
# ####################
#
# Because boxes are now twice as wide but the robot is still the same size and speed, boxes can be aligned such that they directly push two other boxes at once. For example, consider this situation:
#
# #######
# #...#.#
# #.....#
# #..OO@#
# #..O..#
# #.....#
# #######
#
# <vv<<^^<<^^
#
# After appropriately resizing this map, the robot would push around these boxes as follows:
#
# Initial state:
# ##############
# ##......##..##
# ##..........##
# ##....[][]@.##
# ##....[]....##
# ##..........##
# ##############
#
# Move <:
# ##############
# ##......##..##
# ##..........##
# ##...[][]@..##
# ##....[]....##
# ##..........##
# ##############
#
# Move v:
# ##############
# ##......##..##
# ##..........##
# ##...[][]...##
# ##....[].@..##
# ##..........##
# ##############
#
# Move v:
# ##############
# ##......##..##
# ##..........##
# ##...[][]...##
# ##....[]....##
# ##.......@..##
# ##############
#
# Move <:
# ##############
# ##......##..##
# ##..........##
# ##...[][]...##
# ##....[]....##
# ##......@...##
# ##############
#
# Move <:
# ##############
# ##......##..##
# ##..........##
# ##...[][]...##
# ##....[]....##
# ##.....@....##
# ##############
#
# Move ^:
# ##############
# ##......##..##
# ##...[][]...##
# ##....[]....##
# ##.....@....##
# ##..........##
# ##############
#
# Move ^:
# ##############
# ##......##..##
# ##...[][]...##
# ##....[]....##
# ##.....@....##
# ##..........##
# ##############
#
# Move <:
# ##############
# ##......##..##
# ##...[][]...##
# ##....[]....##
# ##....@.....##
# ##..........##
# ##############
#
# Move <:
# ##############
# ##......##..##
# ##...[][]...##
# ##....[]....##
# ##...@......##
# ##..........##
# ##############
#
# Move ^:
# ##############
# ##......##..##
# ##...[][]...##
# ##...@[]....##
# ##..........##
# ##..........##
# ##############
#
# Move ^:
# ##############
# ##...[].##..##
# ##...@.[]...##
# ##....[]....##
# ##..........##
# ##..........##
# ##############
#
# This warehouse also uses GPS to locate the boxes. For these larger boxes, distances are measured from the edge of the map to the closest edge of the box in question. So, the box shown below has a distance of 1 from the top edge of the map and 5 from the left edge of the map, resulting in a GPS coordinate of 100 * 1 + 5 = 105.
#
# ##########
# ##...[]...
# ##........
#
# In the scaled-up version of the larger example from above, after the robot has finished all of its moves, the warehouse would look like this:
#
# ####################
# ##[].......[].[][]##
# ##[]...........[].##
# ##[]........[][][]##
# ##[]......[]....[]##
# ##..##......[]....##
# ##..[]............##
# ##..@......[].[][]##
# ##......[][]..[]..##
# ####################
#
# The sum of these boxes' GPS coordinates is 9021.
#
# Predict the motion of the robot and boxes in this new, scaled-up warehouse. What is the sum of all boxes' final GPS coordinates?
#
# Your puzzle answer was 1543141.
#
# Output:
#
#   Answer: 1543141
#   (execute time: 84.167667 ms, memory: 459947264 bytes)

require "../utils.cr"

module Day15
  extend self

  # Use the simple state and calculate base on single block
  # --- Part One ---
  def problem1(records : Array(String))
    i = 0
    n = records.size
    width = records.first.size
    map = Hash(Coord, Char).new
    robot = Coord.new(x: 0, y: 0)
    walls = Set(Coord).new
    while i < n
      line = records[i]
      break if line == ""
      line.chars.each_with_index do |col, x|
        next if col == '.'
        c = Coord.new(x: x, y: i)
        if col == '@'
          robot = c
        elsif col == '#'
          walls.add?(c)
        else
          map[c] = col
        end
      end
      i += 1
    end
    height = i

    i += 1
    moves = Array(Char).new
    while i < n
      line = records[i]
      break if line == ""
      moves += line.chars
      i += 1
    end

    {% if flag?(:xdebug) %}
      print_plan map, height, width, '.'
      pp! moves
      pp! robot
    {% end %}

    directions = {
      '>' => {x: 1, y: 0},
      'v' => {x: 0, y: 1},
      '<' => {x: -1, y: 0},
      '^' => {x: 0, y: -1},
    }

    # Moves
    moves.each_with_index do |move, step|
      {% if flag?(:xdebug) %}
        puts "\nstep: #{step} #{move}"
      {% end %}
      direction = directions[move]
      next_step = Coord.new(x: robot[:x] + direction[:x], y: robot[:y] + direction[:y])

      next if walls.includes?(next_step)

      if map.has_key?(next_step)
        n = next_step
        steps = 0
        while map.has_key?(n)
          n = Coord.new(x: n[:x] + direction[:x], y: n[:y] + direction[:y])
          steps += 1
        end
        next if walls.includes?(n)

        n = next_step
        steps.times do
          n = Coord.new(x: n[:x] + direction[:x], y: n[:y] + direction[:y])
          map[n] = 'O'
        end
        map.delete(next_step)
      end

      robot = next_step
      {% if flag?(:xdebug) %}
        m = map.clone
        m[robot] = '@'
        print_plan m, height, width, '.'
        gets
      {% end %}
    end

    result = 0
    map.each_key do |coord|
      result += coord[:y]*100 + coord[:x]
    end
    result
  end

  class Item
    getter coord_begin : Coord
    getter walls : Set(Coord)

    def initialize(@coord_begin, @walls)
    end

    def move(direction, map)
      false
    end

    def clone
      Item.new(@coord_begin, @walls)
    end
  end

  class Box < Item
    getter coord_end : Coord

    def initialize(coord, @walls, map = nil)
      @coord_begin = coord
      @coord_end = Coord.new(x: coord[:x] + 1, y: coord[:y])
      if map
        map[@coord_begin] = self
        map[@coord_end] = self
      end
    end

    def move(direction : Coord, map)
      # puts "  > move box #{@coord_begin} by #{direction}"
      next_coord = Coord.new(x: @coord_begin[:x] + direction[:x], y: @coord_begin[:y] + direction[:y])
      next_coord_end = Coord.new(x: next_coord[:x] + 1, y: next_coord[:y])

      return false if @walls.includes?(next_coord) || @walls.includes?(next_coord_end)

      if next_coord == @coord_end
        if map.has_key?(next_coord_end)
          return false if !map[next_coord_end].move(direction, map)
        end
      else
        if map.has_key?(next_coord)
          return false if !map[next_coord].move(direction, map)
        end
        if next_coord_end != @coord_begin &&
           map.has_key?(next_coord_end)
          return false if !map[next_coord_end].move(direction, map)
        end
      end

      map.delete(@coord_begin)
      map.delete(@coord_end)

      @coord_begin = next_coord
      @coord_end = next_coord_end

      map[@coord_begin] = self
      map[@coord_end] = self

      true
    end

    def to_s
      object_id.to_s.chars[-1]
    end

    def clone
      Box.new(@coord_begin, @walls)
    end
  end

  class Robot < Item
    getter map : Hash(Coord, Item)

    def initialize(coord, @walls, @map)
      @coord_begin = coord
      @map[@coord_begin] = self
    end

    def move(direction : Coord)
      next_coord = Coord.new(x: @coord_begin[:x] + direction[:x], y: @coord_begin[:y] + direction[:y])

      return false if @walls.includes?(next_coord)

      if @map.has_key?(next_coord)
        box = @map[next_coord]
        # handle transactions model. if first box could be moved, but the second was not moved, all boxes should be stay on the same places.
        # I used full state cloning, but it should be track what objects were changed and revert those changes only.
        map = @map.clone
        return false if !box.move(direction, map)
        @map = map
      end

      @map.delete(@coord_begin)
      @coord_begin = next_coord
      @map[@coord_begin] = self

      true
    end

    def to_s
      '@'
    end

    # allow to run clone method when duplicate the Hash
    def clone
      Robot.new(coord: @coord_begin, walls: @walls, map: @map)
    end
  end

  # Use objects to calculate the double space objects
  # --- Part Two ---
  def problem2(records : Array(String))
    i = 0
    n = records.size
    width = records.first.size*2
    map = Hash(Coord, Item).new
    walls = Set(Coord).new
    robot = Robot.new(coord: Coord.new(x: 0, y: 0), walls: walls, map: map)
    while i < n
      line = records[i]
      break if line == ""
      line.chars.each_with_index do |col, x|
        next if col == '.'
        c1 = Coord.new(x: x*2, y: i)
        if col == '@'
          map.delete(robot.coord_begin)
          robot = Robot.new(c1, walls: walls, map: map)
        elsif col == '#'
          # TODO: Convert the walls to Item subclass
          c2 = Coord.new(x: c1[:x] + 1, y: i)
          walls.add?(c1)
          walls.add?(c2)
        else
          Box.new(c1, walls: walls, map: map)
        end
      end
      i += 1
    end
    height = i

    i += 1
    moves = Array(Char).new
    while i < n
      line = records[i]
      break if line == ""
      moves += line.chars
      i += 1
    end

    {% if flag?(:xdebug) %}
      print_plan map, height, width, '.'
      pp! moves
    {% end %}

    directions = {
      '>' => {x: 1, y: 0},
      'v' => {x: 0, y: 1},
      '<' => {x: -1, y: 0},
      '^' => {x: 0, y: -1},
    }

    # Moves
    moves.each_with_index do |move, step|
      {% if flag?(:xdebug) %}
        puts "\nstep: #{step} #{move}"
      {% end %}

      direction = directions[move]
      robot.move(direction)

      {% if flag?(:xdebug) %}
        print_plan robot.map, height, width, '.'
        # gets
      {% end %}
    end

    # Calculate result
    result = 0
    {% if flag?(:xdebug) %}
      print_plan robot.map, height, width, '.'
    {% end %}
    map = robot.map
    map.delete(robot.coord_begin)
    map.values.map(&.coord_begin).uniq.each do |coord|
      result += coord[:y]*100 + coord[:x]
    end
    result
  end
end

# vim: filetype=crystal syntax=crystal
