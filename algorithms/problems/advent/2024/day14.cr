# https://adventofcode.com/2024/day/14
#
# --- Day 14: Restroom Redoubt ---
#
# One of The Historians needs to use the bathroom; fortunately, you know there's a bathroom near an unvisited location on their list, and so you're all quickly teleported directly to the lobby of Easter Bunny Headquarters.
#
# Unfortunately, EBHQ seems to have "improved" bathroom security again after your last visit. The area outside the bathroom is swarming with robots!
#
# To get The Historian safely to the bathroom, you'll need a way to predict where the robots will be in the future. Fortunately, they all seem to be moving on the tile floor in predictable straight lines.
#
# You make a list (your puzzle input) of all of the robots' current positions (p) and velocities (v), one robot per line. For example:
#
# p=0,4 v=3,-3
# p=6,3 v=-1,-3
# p=10,3 v=-1,2
# p=2,0 v=2,-1
# p=0,0 v=1,3
# p=3,0 v=-2,-2
# p=7,6 v=-1,-3
# p=3,0 v=-1,-2
# p=9,3 v=2,3
# p=7,3 v=-1,2
# p=2,4 v=2,-3
# p=9,5 v=-3,-3
#
# Each robot's position is given as p=x,y where x represents the number of tiles the robot is from the left wall and y represents the number of tiles from the top wall (when viewed from above). So, a position of p=0,0 means the robot is all the way in the top-left corner.
#
# Each robot's velocity is given as v=x,y where x and y are given in tiles per second. Positive x means the robot is moving to the right, and positive y means the robot is moving down. So, a velocity of v=1,-2 means that each second, the robot moves 1 tile to the right and 2 tiles up.
#
# The robots outside the actual bathroom are in a space which is 101 tiles wide and 103 tiles tall (when viewed from above). However, in this example, the robots are in a space which is only 11 tiles wide and 7 tiles tall.
#
# The robots are good at navigating over/under each other (due to a combination of springs, extendable legs, and quadcopters), so they can share the same tile and don't interact with each other. Visually, the number of robots on each tile in this example looks like this:
#
# 1.12.......
# ...........
# ...........
# ......11.11
# 1.1........
# .........1.
# .......1...
#
# These robots have a unique feature for maximum bathroom security: they can teleport. When a robot would run into an edge of the space they're in, they instead teleport to the other side, effectively wrapping around the edges. Here is what robot p=2,4 v=2,-3 does for the first few seconds:
#
# Initial state:
# ...........
# ...........
# ...........
# ...........
# ..1........
# ...........
# ...........
#
# After 1 second:
# ...........
# ....1......
# ...........
# ...........
# ...........
# ...........
# ...........
#
# After 2 seconds:
# ...........
# ...........
# ...........
# ...........
# ...........
# ......1....
# ...........
#
# After 3 seconds:
# ...........
# ...........
# ........1..
# ...........
# ...........
# ...........
# ...........
#
# After 4 seconds:
# ...........
# ...........
# ...........
# ...........
# ...........
# ...........
# ..........1
#
# After 5 seconds:
# ...........
# ...........
# ...........
# .1.........
# ...........
# ...........
# ...........
#
# The Historian can't wait much longer, so you don't have to simulate the robots for very long. Where will the robots be after 100 seconds?
#
# In the above example, the number of robots on each tile after 100 seconds has elapsed looks like this:
#
# ......2..1.
# ...........
# 1..........
# .11........
# .....1.....
# ...12......
# .1....1....
#
# To determine the safest area, count the number of robots in each quadrant after 100 seconds. Robots that are exactly in the middle (horizontally or vertically) don't count as being in any quadrant, so the only relevant robots are:
#
# ..... 2..1.
# ..... .....
# 1.... .....
#
# ..... .....
# ...12 .....
# .1... 1....
#
# In this example, the quadrants contain 1, 3, 4, and 1 robot. Multiplying these together gives a total safety factor of 12.
#
# Predict the motion of the robots in your list within a space which is 101 tiles wide and 103 tiles tall. What will the safety factor be after exactly 100 seconds have elapsed?
#
# Your puzzle answer was 219512160.
#
# Output:
#
#   Answer: 219512160
#   (execute time: 1.626709 ms, memory: 419696 bytes)
#
# --- Part Two ---
#
# During the bathroom break, someone notices that these robots seem awfully similar to ones built and used at the North Pole. If they're the same type of robots, they should have a hard-coded Easter egg: very rarely, most of the robots should arrange themselves into a picture of a Christmas tree.
#
# What is the fewest number of seconds that must elapse for the robots to display the Easter egg?
#
# Your puzzle answer was 6398.
#
# Output:
#
#   Answer: 6398
#   (execute time: 115.979958 ms, memory: 139727728 bytes)

require "../utils.cr"

module Day14
  extend self

  class Robot
    getter pos : Coord = {x: 0, y: 0}
    getter vel : Coord = {x: 0, y: 0}
    @size : Coord = {x: 101, y: 103}

    def initialize(@pos, @vel, @size)
    end

    def steps(n = 1)
      x = (@pos[:x] + n*@vel[:x]) % @size[:x]
      y = (@pos[:y] + n*@vel[:y]) % @size[:y]

      x = x + @size[:x] if x < 0
      y = y + @size[:y] if y < 0

      @pos = {x: x, y: y}
    end
  end

  # --- Part One ---
  def problem1(records : Array(String), size = {x: 101, y: 103})
    robots = Set(Robot).new
    records.each do |instruction|
      next if instruction == ""
      pos_raw, vel_raw = instruction.split(" ", 2)
      pos_x, pos_y = pos_raw[2..].split(",", 2).map(&.to_i32)
      pos = {x: pos_x, y: pos_y}
      vel_x, vel_y = vel_raw[2..].split(",", 2).map(&.to_i32)
      vel = {x: vel_x, y: vel_y}
      robot = Robot.new(pos, vel, size)
      robots.add? robot
    end

    halfx = size[:x] // 2
    halfy = size[:y] // 2
    map = Hash(Coord, Int32).new
    results = StaticArray(Int32, 4).new(0)
    robots.each do |r|
      r.steps(100)
      map[r.pos] ||= 0
      map[r.pos] += 1
      next if r.pos[:x] == halfx || r.pos[:y] == halfy
      i = 0
      i = 1 if r.pos[:x] > halfx
      i += 2 if r.pos[:y] > halfy
      results[i] += 1
    end

    {% if flag?(:xdebug) %}
      print_plan map, size[:y], size[:x], '.'
    {% end %}

    results.reduce(1) { |acc, i| acc * i }
  end

  # --- Part Two ---
  def problem2(records : Array(String), size = {x: 101, y: 103})
    robots = Set(Robot).new
    records.each do |instruction|
      next if instruction == ""
      pos_raw, vel_raw = instruction.split(" ", 2)
      pos_x, pos_y = pos_raw[2..].split(",", 2).map(&.to_i32)
      pos = {x: pos_x, y: pos_y}
      vel_x, vel_y = vel_raw[2..].split(",", 2).map(&.to_i32)
      vel = {x: vel_x, y: vel_y}
      robot = Robot.new(pos, vel, size)
      robots.add? robot
    end

    i = 1
    halfx = size[:x] // 2
    halfy = size[:y] // 2
    while i < 6399
      map = Hash(Coord, Char).new
      robots.each do |r|
        r.steps(1)
        map[r.pos] = '*'
      end

      if max_line(map, size)
        {% if flag?(:xdebug) %}
          puts "step: #{i}"
          print_plan map, size[:y], size[:x], '.'
          sleep 100.milliseconds
        {% end %}
        return i
      end
      i += 1
    end

    0
  end

  def max_line(map, size)
    lines = Array(Int32).new(size[:y], 0)
    map.each_key do |coord|
      lines[coord[:x]] += 1
    end

    # Just guessed the number base on picture
    # TODO: Detect the straight lines
    estimated = 30
    lines.max(2).all? { |i| i > estimated }
  end
end

# vim: filetype=crystal syntax=crystal
