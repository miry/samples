require "spec"
require "./day07"

describe Day7 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      190: 10 19
      3267: 81 40 27
      83: 17 5
      156: 15 6
      7290: 6 8 6 15
      161011: 16 10 13
      192: 17 8 14
      21037: 9 7 18 13
      292: 11 6 16 20
      EOF
      actual = Day7.problem1(input.split("\n"))
      actual.should eq(3749)
    end

    it "validate equastion" do
      actual = Day7.valid_equation?(3267, [81, 40, 27])
      actual.should eq(true)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      190: 10 19
      3267: 81 40 27
      83: 17 5
      156: 15 6
      7290: 6 8 6 15
      161011: 16 10 13
      192: 17 8 14
      21037: 9 7 18 13
      292: 11 6 16 20
      EOF
      actual = Day7.problem2(input.split("\n"))
      actual.should eq(11387)
    end

    it "validate equastion" do
      ts = [
        {in: {eq: 156_i64, operands: [15, 6] of Int64}, expect: true},
        {in: {eq: 192_i64, operands: [15, 6] of Int64}, expect: false},
        {in: {eq: 192_i64, operands: [17, 8, 14] of Int64}, expect: true},
        {in: {eq: 193_i64, operands: [1, 17, 8, 14] of Int64}, expect: false},
        {in: {eq: 486_i64, operands: [6, 8, 6] of Int64}, expect: true},
        {in: {eq: 408_i64, operands: [6, 8, 6] of Int64}, expect: true},
        {in: {eq: 7290_i64, operands: [6, 8, 6, 15] of Int64}, expect: true},
        {in: {eq: 123456789_i64, operands: [1, 2, 3, 4, 5, 6, 7, 8, 9] of Int64}, expect: true},
        {in: {eq: 12427056279_i64, operands: [3, 5, 3, 8, 3, 22, 1, 651, 5, 3, 1, 6] of Int64}, expect: false},
        {in: {eq: 885_i64, operands: [2, 6, 5, 68, 1] of Int64}, expect: true},
        {in: {eq: 326690_i64, operands: [8, 10, 67, 2, 6, 1, 3, 49, 996] of Int64}, expect: false},
      ]
      ts.each do |tc|
        actual = Day7.valid_equation_with_combine?(tc[:in][:eq], tc[:in][:operands])
        actual.should eq(tc[:expect])
      end
    end

    it "make expressions" do
      ts = [
        {in: {eq: 1_i64, operands: [1] of Int64}, expect: [1] of Int64 | Char},
        {in: {eq: 156_i64, operands: [150, 6] of Int64}, expect: [150, '+', 6] of Int64 | Char},
        {in: {eq: 190_i64, operands: [10, 19] of Int64}, expect: [10, '*', 19] of Int64 | Char},
        {in: {eq: 3267_i64, operands: [81, 40, 27] of Int64}, expect: [81, '*', 40, '+', 27] of Int64 | Char},
        {in: {eq: 156_i64, operands: [15, 6] of Int64}, expect: [15, '|', 6] of Int64 | Char},
        {in: {eq: 7290_i64, operands: [6, 8, 6, 15] of Int64}, expect: [6, '*', 8, '|', 6, '*', 15] of Int64 | Char},
        {in: {eq: 192_i64, operands: [17, 8, 14] of Int64}, expect: [17, '|', 8, '+', 14] of Int64 | Char},
        {in: {eq: 326690_i64, operands: [8, 10, 67, 2, 6, 1, 3, 49, 996] of Int64}, expect: false},
        {in: {eq: 885_i64, operands: [2, 6, 5, 68, 1] of Int64}, expect: [2, '+', 6, '+', 5, '*', 68, '+', 1] of Int64 | Char},
      ]
      ts.each do |tc|
        actual = Day7.make_expression(tc[:in][:eq], tc[:in][:operands])
        actual.should eq(tc[:expect])
      end
    end
  end
end
# vim: filetype=crystal syntax=crystal
