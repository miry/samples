# https://adventofcode.com/2024/day/18
#
# --- Day 18: ... ---
#

require "../utils.cr"

module Day18
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    records.each do |instruction|
      next if instruction == ""
    end

    raise "Day 18.1 is not implemented"
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    records.each do |instruction|
      next if instruction == ""
    end

    raise "Day 18.2 is not implemented"
  end
end
# vim: filetype=crystal syntax=crystal
