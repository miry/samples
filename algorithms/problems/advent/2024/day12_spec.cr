require "spec"
require "./day12"

describe Day12 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      AAAA
      BBCD
      BBCC
      EEEC
      EOF
      actual = Day12.problem1(input.split("\n"))
      actual.should eq(140)
    end

    it "sample two" do
      input = <<-EOF
      OOOOO
      OXOXO
      OOOOO
      OXOXO
      OOOOO
      EOF
      actual = Day12.problem1(input.split("\n"))
      actual.should eq(772)
    end

    it "sample three" do
      input = <<-EOF
      RRRRIICCFF
      RRRRIICCCF
      VVRRRCCFFF
      VVRCCCJFFF
      VVVVCJJCFE
      VVIVCCJJEE
      VVIIICJJEE
      MIIIIIJJEE
      MIIISIJEEE
      MMMISSJEEE
      EOF
      actual = Day12.problem1(input.split("\n"))
      actual.should eq(1930)
    end
  end

  describe "problem2" do
    it "single" do
      input = <<-EOF
      A
      EOF
      actual = Day12.problem2(input.split("\n"))
      actual.should eq(4)
    end

    it "line" do
      input = <<-EOF
      AA
      EOF
      actual = Day12.problem2(input.split("\n"))
      actual.should eq(8)
    end

    it "col" do
      input = <<-EOF
      A
      A
      EOF
      actual = Day12.problem2(input.split("\n"))
      actual.should eq(8)
    end
    it "sample" do
      input = <<-EOF
      AAAA
      BBCD
      BBCC
      EEEC
      EOF
      actual = Day12.problem2(input.split("\n"))
      actual.should eq(80)
    end
  end
end
# vim: filetype=crystal syntax=crystal
