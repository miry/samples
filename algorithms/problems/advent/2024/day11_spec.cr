require "spec"
require "./day11"

describe Day11 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      125 17
      EOF
      actual = Day11.problem1(input.split("\n"))
      actual.should eq(55312)
    end
  end

  describe "blinking" do
    it "sample" do
      actual = Day11.blinking([125_i64, 17_i64], 1)
      actual.should eq([253000, 1, 7])
      actual = Day11.blinking([125_i64, 17_i64], 2)
      actual.should eq([253, 0, 2024, 14168])
      actual = Day11.blinking([125_i64, 17_i64], 3)
      actual.should eq([512072, 1, 20, 24, 28676032])
      actual = Day11.blinking([125_i64, 17_i64], 6)
      actual.should eq([2097446912, 14168, 4048, 2, 0, 2, 4, 40, 48, 2024, 40, 48, 80, 96, 2, 8, 6, 7, 6, 0, 3, 2])
    end

    it "sample 2" do
      actual = Day11.blinking([0, 1, 10, 99, 999] of Int64, 1)
      actual.should eq([1, 2024, 1, 0, 9, 9, 2021976])
    end

    it "0" do
      actual = Day11.blinking([0] of Int64, 25)
      actual.size.should eq(19778)
    end
  end

  describe "blinking_recursive" do
    it "sample" do
      actual = Day11.blinking_recursive([125_i64, 17_i64], 1)
      actual.should eq(3)
      actual = Day11.blinking_recursive([125_i64, 17_i64], 2)
      actual.should eq(4)
      actual = Day11.blinking_recursive([125_i64, 17_i64], 3)
      actual.should eq(5)
      actual = Day11.blinking_recursive([125_i64, 17_i64], 6)
      actual.should eq(22)
    end

    it "sample 2" do
      actual = Day11.blinking_recursive([0, 1, 10, 99, 999] of Int64, 1)
      actual.should eq(7)
    end

    it "0" do
      actual = Day11.blinking_recursive([0] of Int64, 25)
      actual.should eq(19778)
    end
  end
end
# vim: filetype=crystal syntax=crystal
