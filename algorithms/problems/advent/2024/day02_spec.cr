require "spec"
require "./day02"

describe Day2 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      7 6 4 2 1
      1 2 7 8 9
      9 7 6 2 1
      1 3 2 4 5
      8 6 4 4 1
      1 3 6 7 9
      EOF
      actual = Day2.problem1(input.split("\n"))
      actual.should eq(2)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      7 6 4 2 1
      1 2 7 8 9
      9 7 6 2 1
      1 3 2 4 5
      8 6 4 4 1
      1 3 6 7 9
      EOF
      actual = Day2.problem2(input.split("\n"))
      actual.should eq(4)
    end
  end

  describe "increasing?" do
    it "increase" do
      actual = Day2.increasing?([1, 2, 3, 4, 5, 6, 7])
      actual.should eq(true)
    end

    it "increase level false" do
      actual = Day2.increasing?([10, 1, 2, 3, 4, 5, 6, 7])
      actual.should eq(false)
    end

    it "increase level 1" do
      actual = Day2.increasing?([10, 1, 2, 3, 4, 5, 6, 7], 1)
      actual.should eq(true)
    end

    it "case" do
      actual = Day2.increasing?([19, 20, 21, 23, 24, 25, 28, 26], 1)
      actual.should eq(true)
    end

    pending "case 2" do
      actual = Day2.increasing?([77, 74, 78, 80, 83, 84, 87], 1)
      actual.should eq(true)
    end
  end

  describe "decreasing?" do
    it "sample 1" do
      actual = Day2.decreasing?([7, 6, 5, 4, 3, 2])
      actual.should eq(true)
    end

    it "sample 2" do
      actual = Day2.decreasing?([77, 74, 78, 80, 83, 84, 87])
      actual.should eq(false)
    end

    pending "sample 3" do
      actual = Day2.decreasing?([59, 57, 56, 53, 54, 53, 52])
      actual.should eq(false)
      actual = Day2.decreasing?([59, 57, 56, 53, 54, 53, 52], 1)
      actual.should eq(true)
    end

    pending "sample 4" do
      actual = Day2.decreasing?([38, 33, 36, 34, 33])
      actual.should eq(false)
      actual = Day2.decreasing?([38, 33, 36, 34, 33], 1)
      actual.should eq(true)
    end
  end
end
