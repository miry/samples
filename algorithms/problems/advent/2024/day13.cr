# https://adventofcode.com/2024/day/13
#
# --- Day 13: Claw Contraption ---
#
# Next up: the lobby of a resort on a tropical island. The Historians take a moment to admire the hexagonal floor tiles before spreading out.
#
# Fortunately, it looks like the resort has a new arcade! Maybe you can win some prizes from the claw machines?
#
# The claw machines here are a little unusual. Instead of a joystick or directional buttons to control the claw, these machines have two buttons labeled A and B. Worse, you can't just put in a token and play; it costs 3 tokens to push the A button and 1 token to push the B button.
#
# With a little experimentation, you figure out that each machine's buttons are configured to move the claw a specific amount to the right (along the X axis) and a specific amount forward (along the Y axis) each time that button is pressed.
#
# Each machine contains one prize; to win the prize, the claw must be positioned exactly above the prize on both the X and Y axes.
#
# You wonder: what is the smallest number of tokens you would have to spend to win as many prizes as possible? You assemble a list of every machine's button behavior and prize location (your puzzle input). For example:
#
# Button A: X+94, Y+34
# Button B: X+22, Y+67
# Prize: X=8400, Y=5400
#
# Button A: X+26, Y+66
# Button B: X+67, Y+21
# Prize: X=12748, Y=12176
#
# Button A: X+17, Y+86
# Button B: X+84, Y+37
# Prize: X=7870, Y=6450
#
# Button A: X+69, Y+23
# Button B: X+27, Y+71
# Prize: X=18641, Y=10279
#
# This list describes the button configuration and prize location of four different claw machines.
#
# For now, consider just the first claw machine in the list:
#
#     Pushing the machine's A button would move the claw 94 units along the X axis and 34 units along the Y axis.
#     Pushing the B button would move the claw 22 units along the X axis and 67 units along the Y axis.
#     The prize is located at X=8400, Y=5400; this means that from the claw's initial position, it would need to move exactly 8400 units along the X axis and exactly 5400 units along the Y axis to be perfectly aligned with the prize in this machine.
#
# The cheapest way to win the prize is by pushing the A button 80 times and the B button 40 times. This would line up the claw along the X axis (because 80*94 + 40*22 = 8400) and along the Y axis (because 80*34 + 40*67 = 5400). Doing this would cost 80*3 tokens for the A presses and 40*1 for the B presses, a total of 280 tokens.
#
# For the second and fourth claw machines, there is no combination of A and B presses that will ever win a prize.
#
# For the third claw machine, the cheapest way to win the prize is by pushing the A button 38 times and the B button 86 times. Doing this would cost a total of 200 tokens.
#
# So, the most prizes you could possibly win is two; the minimum tokens you would have to spend to win all (two) prizes is 480.
#
# You estimate that each button would need to be pressed no more than 100 times to win a prize. How else would someone be expected to play?
#
# Figure out how to win as many prizes as possible. What is the fewest tokens you would have to spend to win all possible prizes?
#
# Your puzzle answer was 29877.
#
# The first half of this puzzle is complete! It provides one gold star: *
#
# Output:
#
#   Answer: 29877
#   (execute time: 0.897417 ms, memory: 366416 bytes)
#
# --- Part Two ---
#
# As you go to win the first prize, you discover that the claw is nowhere near where you expected it would be. Due to a unit conversion error in your measurements, the position of every prize is actually 10000000000000 higher on both the X and Y axis!
#
# Add 10000000000000 to the X and Y position of every prize. After making this change, the example above would now look like this:
#
# Button A: X+94, Y+34
# Button B: X+22, Y+67
# Prize: X=10000000008400, Y=10000000005400
#
# Button A: X+26, Y+66
# Button B: X+67, Y+21
# Prize: X=10000000012748, Y=10000000012176
#
# Button A: X+17, Y+86
# Button B: X+84, Y+37
# Prize: X=10000000007870, Y=10000000006450
#
# Button A: X+69, Y+23
# Button B: X+27, Y+71
# Prize: X=10000000018641, Y=10000000010279
#
# Now, it is only possible to win a prize on the second and fourth claw machines. Unfortunately, it will take many more than 100 presses to do so.
#
# Using the corrected prize coordinates, figure out how to win as many prizes as possible. What is the fewest tokens you would have to spend to win all possible prizes?
#
# Output:
#
#   Answer: 99423413811305
#   (execute time: 0.872916 ms, memory: 381008 bytes)

require "../utils.cr"

module Day13
  extend self

  # X1*a + X2*b = X
  # Y1*a + Y2*b = Y
  #
  # X1 * a = X - X2*b
  # a = (X - X2*b) / X1
  #
  # Y1 * (X - X2*b) / X1 + Y2 * b = Y
  # Y1 * X / X1 - Y1*X2*b / X1 + Y2 * b = Y
  # Y2 * b - Y1*X2*b / X1 = Y - Y1 * X / X1
  #
  # (Y2 - Y1*X2/X1) * b = (Y*X1 - Y1 * X) / X1
  # (Y2*X1 - Y1*X2) * b = (Y*X1 - Y1 * X)
  # b = (Y*X1 - Y1 * X) / (Y2*X1 - Y1*X2)
  # a = (X - X2*b) / X1
  # --- Part One ---
  def problem1(records : Array(String))
    result = 0
    records.in_groups_of(4).each do |instruction|
      next if instruction.nil?
      a = instruction[0].not_nil!
      x1, y1 = a[12..].split(", Y+").map(&.to_i32)
      b = instruction[1].not_nil!
      x2, y2 = b[12..].split(", Y+").map(&.to_i32)

      c = instruction[2].not_nil!
      x, y = c[9..].split(", Y=").map(&.to_i32)

      btoken = ((y * x1 - y1 * x) / (y2*x1 - y1*x2)).to_i32
      atoken = (x - x2*btoken)//x1

      if (x1*atoken + x2*btoken == x) && (y1*atoken + y2*btoken == y)
        result += btoken + 3*atoken
      end
    end
    result
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    result = 0_i64
    records.in_groups_of(4).each do |instruction|
      next if instruction.nil?
      a = instruction[0].not_nil!
      x1, y1 = a[12..].split(", Y+").map(&.to_i64)
      b = instruction[1].not_nil!
      x2, y2 = b[12..].split(", Y+").map(&.to_i64)

      c = instruction[2].not_nil!
      x, y = c[9..].split(", Y=").map { |i| i.to_i64 + 10000000000000 }

      btoken = ((y * x1 - y1 * x) / (y2*x1 - y1*x2)).to_i64
      atoken = (x - x2*btoken)//x1

      if (x1*atoken + x2*btoken == x) && (y1*atoken + y2*btoken == y)
        result += btoken + 3_i64 * atoken
      end
    end
    result
  end
end
# vim: filetype=crystal syntax=crystal
