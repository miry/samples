require "spec"
require "./day20"

describe Day20 do
  describe "problem1" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = Day20.problem1(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem2" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = Day20.problem2(input.split("\n"))
      actual.should eq(12)
    end
  end
end

# vim: filetype=crystal syntax=crystal
