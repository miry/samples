# https://adventofcode.com/2024/day/20
#
# --- Day 20: ... ---
#

require "../utils.cr"

module Day20
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    records.each do |instruction|
      next if instruction == ""
    end

    raise "Day 20.1 is not implemented"
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    records.each do |instruction|
      next if instruction == ""
    end

    raise "Day 20.2 is not implemented"
  end
end

# vim: filetype=crystal syntax=crystal
