require "spec"
require "./day17"

describe Day17 do
  describe "problem1" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = Day17.problem1(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem2" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = Day17.problem2(input.split("\n"))
      actual.should eq(12)
    end
  end
end
# vim: filetype=crystal syntax=crystal
