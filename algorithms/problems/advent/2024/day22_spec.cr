require "spec"
require "./day22"

describe Day22 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      1
      10
      100
      2024
      EOF
      actual = Day22.problem1(input.split("\n"))
      actual.should eq(37327623)
    end
  end

  describe "mix" do
    it "test 42 and 15" do
      actual = Day22.mix(42, 15)
      actual.should eq(37)
    end
  end

  describe "prune" do
    it "the secret number 100000000" do
      actual = Day22.prune(100000000)
      actual.should eq(16113920)
    end
  end

  describe "evolve" do
    it "modifies 123" do
      actual = Day22.evolve(123_i128)
      actual.should eq(15887950)
    end
    it "modifies 15887950" do
      actual = Day22.evolve(15887950_i128)
      actual.should eq(16495136)
    end
  end

  describe "problem2" do
    it "small" do
      input = <<-EOF
      123
      EOF
      actual = Day22.problem2(input.split("\n"))
      actual.should eq(23)
    end

    it "sample" do
      input = <<-EOF
      1
      2
      3
      2024
      EOF
      actual = Day22.problem2(input.split("\n"))
      actual.should eq(23)
    end
  end
end

# vim: filetype=crystal syntax=crystal
