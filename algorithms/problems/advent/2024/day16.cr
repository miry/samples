# https://adventofcode.com/2024/day/16
#
# --- Day 16: ... ---
#

require "../utils.cr"

module Day16
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    # i = 0
    # n = records.size
    # width = records.first.size
    # map = Hash(Coord, Char).new
    # walker = Coord.new(x: 0, y: 0)
    # finish = Coord.new(x: 0, y: 0)
    # while i < n
    #   line = records[i]
    #   break if line == ""
    #   line.chars.each_with_index do |col, x|
    #     next if col == '.'
    #     c = Coord.new(x: x, y: i)
    #     map[c] = col
    #     if col == 'S'
    #       walker = c
    #     end
    #
    #     if col == 'E'
    #       finish = c
    #     end
    #   end
    #   i += 1
    # end
    # height = i
    # {% if flag?(:xdebug) %}
    #   print_plan map, height, width, '.'
    # {% end %}
    #
    # direction = Coord.new(x: 1, y: 0)
    # visited : Hash(Tuple(Coord, Coord), Int32) = {{walker, direction} => 0}
    # a_star(map, walker, direction, finish, Coord.new(x: 0, y: 0), 0, visited, nil, height, width)
  end

  DIRECTIONS = [
    Coord.new(x: 1, y: 0),
    Coord.new(x: 0, y: 1),
    Coord.new(x: -1, y: 0),
    Coord.new(x: 0, y: -1),
  ]

  OPPOSITE_DIRECTIONS = {
    Coord.new(x: 1, y: 0)  => Coord.new(x: -1, y: 0),
    Coord.new(x: 0, y: 1)  => Coord.new(x: 0, y: -1),
    Coord.new(x: -1, y: 0) => Coord.new(x: 1, y: 0),
    Coord.new(x: 0, y: -1) => Coord.new(x: 0, y: 1),
  }

  def a_star(map, walker, direction, finish, last, score : Int32, visited, max_distance, height, width, cache = Hash(Tuple(Coord, Coord), Int32?).new) : Int32?
    {% if flag?(:xdebug) %}
      puts "> #{walker} #{direction}"
      print_plan map, height, width, '.'
      gets
    {% end %}
    return score if walker == finish
    cache_key = {walker, direction}
    return cache[cache_key] if cache.has_key?(cache_key)
    return nil if map[walker]? == '#'
    return nil if max_distance && score > max_distance

    opposite_direction = OPPOSITE_DIRECTIONS[direction]
    visited.add? walker
    map[walker] = 'S'
    result = max_distance
    dmap = map.dup

    next_coords = DIRECTIONS.map do |dir|
      next if dir == opposite_direction
      next_coord = Coord.new(x: walker[:x] + dir[:x], y: walker[:y] + dir[:y])
      next if visited.includes?(next_coord) || map[next_coord]? == '#'
      {next_coord, dir}
    end.compact.sort { |a, b|
      ((finish[:x] - a[0][:x]).abs + (finish[:y] - a[0][:y]).abs) <=> ((finish[:x] - b[0][:x]).abs + (finish[:y] - b[0][:y]).abs)
    }
    {% if flag?(:xdebug) %}
      pp! next_coords
    {% end %}

    next_coords.each do |next_coord, dir|
      s = dir == direction ? 1 : 1001
      distance = a_star(dmap, next_coord, dir, finish, walker, score + s, visited.dup, result, height, width)
      if !distance.nil?
        result ||= distance
        if result > distance
          result = distance
        end
      end
    end
    cache[cache_key] = result
    result
  end

  def next_steps(map, walker, direction, finish, score, visited)
    # opposite_direction = OPPOSITE_DIRECTIONS[direction]
    # result = []
    # DIRECTIONS.map do |dir|
    #   next if dir == opposite_direction
    #   next_coord = Coord.new(x: walker[:x] + dir[:x], y: walker[:y] + dir[:y])
    #   r = {next_coord, dir}
    #   next if map[next_coord]? == '#' || visited[r]? <= score
    #   x_distance = (finish[:x] - a[0][:x]).abs
    #   y_distance = (finish[:y] - a[0][:y]).abs
    #   estimate = (x_distance - y_distance).abs * 1000 + (x_distance + y_distance)
    #   result << {next_coord, dir, estimate}
    # end
    # result
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    records.each do |instruction|
      next if instruction == ""
    end

    raise "Day 16.2 is not implemented"
  end
end

# vim: filetype=crystal syntax=crystal
