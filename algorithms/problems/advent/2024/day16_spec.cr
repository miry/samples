require "spec"
require "./day16"

describe Day16 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      ###############
      #.......#....E#
      #.#.###.#.###.#
      #.....#.#...#.#
      #.###.#####.#.#
      #.#.#.......#.#
      #.#.#####.###.#
      #...........#.#
      ###.#.#####.#.#
      #...#.....#.#.#
      #.#.#.###.#.#.#
      #.....#...#.#.#
      #.###.#.#.#.#.#
      #S..#.....#...#
      ###############
      EOF
      actual = Day16.problem1(input.split("\n"))
      actual.should eq(7036)
    end
  end

  describe "problem2" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = Day16.problem2(input.split("\n"))
      actual.should eq(12)
    end
  end
end

# vim: filetype=crystal syntax=crystal
