require "spec"
require "./day21"

describe Day21 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      029A
      980A
      179A
      456A
      379A
      EOF
      actual = Day21.problem1(input.split("\n"))
      actual.should eq(126384)
    end
  end

  describe "directional_keypad" do
    it "from A to 3" do
      actual = Day21.directional_keypad('A', '3', table: Day21::ROUTES_NUMERIC)
      actual.should eq([['^']])
    end

    it "from A to 0" do
      actual = Day21.directional_keypad('A', '0', table: Day21::ROUTES_NUMERIC)
      actual.should eq([['<']])
    end

    it "from A to 2" do
      actual = Day21.directional_keypad('A', '2', table: Day21::ROUTES_NUMERIC)
      actual.should eq([['^', '<'], ['<', '^']])
    end

    it "from A to 7" do
      actual = Day21.directional_keypad('A', '7', table: Day21::ROUTES_NUMERIC)
      actual.should eq(['^', '^', '^', '<', '<'])
    end

    it "from A to <" do
      actual = Day21.directional_keypad('A', '<', table: Day21::ROUTES_DIRECTIONS)
      actual.should eq(['<', 'v', '<'])
    end
  end

  describe "code_directions" do
    it "input 029A" do
      actual = Day21.code_directions(['0', '2', '9', 'A'])
      actual.should eq(['<', 'A', '^', 'A', '^', '^', '>', 'A', 'v', 'v', 'v', 'A'])
    end
    it "input 029A second layer" do
      actual = Day21.code_directions(['<', 'A', '^', 'A', '^', '^', '>', 'A', 'v', 'v', 'v', 'A'], table: Day21::ROUTES_DIRECTIONS)
      actual.should eq(['<', 'v', '<', 'A', '>', '^', '>', 'A', '<', 'A', '>', 'A', '<', 'A', 'v', '^', 'A', 'v', '>', 'A', '^', 'A', '<', 'v', 'A', '^', 'v', 'A', '^', 'v', 'A', '^', '>', 'A'])
    end
  end

  describe "problem2" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = Day21.problem2(input.split("\n"))
      actual.should eq(12)
    end
  end
end

# vim: filetype=crystal syntax=crystal
