require "spec"
require "./day04"

describe Day4 do
  describe "problem1" do
    it "simple" do
      input = <<-EOF
      XMAS
      EOF
      actual = Day4.problem1(input.split("\n"))
      actual.should eq(1)
    end
    it "second" do
      input = <<-EOF
      XMAS
      XMAS
      EOF
      actual = Day4.problem1(input.split("\n"))
      actual.should eq(2)
    end
    it "without other letters" do
      input = <<-EOF
      ....XXMAS.
      .SAMXMS...
      ...S..A...
      ..A.A.MS.X
      XMASAMX.MM
      X.....XA.A
      S.S.S.S.SS
      .A.A.A.A.A
      ..M.M.M.MM
      .X.X.XMASX
      EOF
      actual = Day4.problem1(input.split("\n"))
      actual.should eq(18)
    end

    it "sample" do
      input = <<-EOF
      MMMSXXMASM
      MSAMXMSMSA
      AMXSXMAAMM
      MSAMASMSMX
      XMASAMXAMM
      XXAMMXXAMA
      SMSMSASXSS
      SAXAMASAAA
      MAMMMXMMMM
      MXMXAXMASX
      EOF
      actual = Day4.problem1(input.split("\n"))
      actual.should eq(18)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      .M.S......
      ..A..MSMS.
      .M.S.MAA..
      ..A.ASMSM.
      .M.S.M....
      ..........
      S.S.S.S.S.
      .A.A.A.A..
      M.M.M.M.M.
      ..........
      EOF
      actual = Day4.problem2(input.split("\n"))
      actual.should eq(9)
    end

    it "full sample" do
      input = <<-EOF
      MMMSXXMASM
      MSAMXMSMSA
      AMXSXMAAMM
      MSAMASMSMX
      XMASAMXAMM
      XXAMMXXAMA
      SMSMSASXSS
      SAXAMASAAA
      MAMMMXMMMM
      MXMXAXMASX
      EOF
      actual = Day4.problem2(input.split("\n"))
      actual.should eq(9)
    end
  end
end
