require "spec"
require "./day25"

describe Day25 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      #####
      .####
      .####
      .####
      .#.#.
      .#...
      .....

      #####
      ##.##
      .#.##
      ...##
      ...#.
      ...#.
      .....

      .....
      #....
      #....
      #...#
      #.#.#
      #.###
      #####

      .....
      .....
      #.#..
      ###..
      ###.#
      ###.#
      #####

      .....
      .....
      .....
      #....
      #.#..
      #.#.#
      #####
      EOF
      actual = Day25.problem1(input.split("\n"))
      actual.should eq(3)
    end
  end

  describe "problem2" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = Day25.problem2(input.split("\n"))
      actual.should eq(12)
    end
  end
end

# vim: filetype=crystal syntax=crystal
