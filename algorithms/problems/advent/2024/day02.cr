# https://adventofcode.com/2024/day/2
#
# --- Day 2: Red-Nosed Reports ---
#
# Fortunately, the first location The Historians want to search isn't a long walk from the Chief Historian's office.
#
# While the Red-Nosed Reindeer nuclear fusion/fission plant appears to contain no sign of the Chief Historian, the engineers there run up to you as soon as they see you. Apparently, they still talk about the time Rudolph was saved through molecular synthesis from a single electron.
#
# They're quick to add that - since you're already here - they'd really appreciate your help analyzing some unusual data from the Red-Nosed reactor. You turn to check if The Historians are waiting for you, but they seem to have already divided into groups that are currently searching every corner of the facility. You offer to help with the unusual data.
#
# The unusual data (your puzzle input) consists of many reports, one report per line. Each report is a list of numbers called levels that are separated by spaces. For example:
#
# 7 6 4 2 1
# 1 2 7 8 9
# 9 7 6 2 1
# 1 3 2 4 5
# 8 6 4 4 1
# 1 3 6 7 9
#
# This example data contains six reports each containing five levels.
#
# The engineers are trying to figure out which reports are safe. The Red-Nosed reactor safety systems can only tolerate levels that are either gradually increasing or gradually decreasing. So, a report only counts as safe if both of the following are true:
#
#     The levels are either all increasing or all decreasing.
#     Any two adjacent levels differ by at least one and at most three.
#
# In the example above, the reports can be found safe or unsafe by checking those rules:
#
#     7 6 4 2 1: Safe because the levels are all decreasing by 1 or 2.
#     1 2 7 8 9: Unsafe because 2 7 is an increase of 5.
#     9 7 6 2 1: Unsafe because 6 2 is a decrease of 4.
#     1 3 2 4 5: Unsafe because 1 3 is increasing but 3 2 is decreasing.
#     8 6 4 4 1: Unsafe because 4 4 is neither an increase or a decrease.
#     1 3 6 7 9: Safe because the levels are all increasing by 1, 2, or 3.
#
# So, in this example, 2 reports are safe.
#
# Analyze the unusual data from the engineers. How many reports are safe?
#
# Your puzzle answer was 326.
#
# Answer: 326
# (execute time: 2.949583 ms, memory: 505168 bytes)
#
# --- Part Two ---
#
# The engineers are surprised by the low number of safe reports until they realize they forgot to tell you about the Problem Dampener.
#
# The Problem Dampener is a reactor-mounted module that lets the reactor safety systems tolerate a single bad level in what would otherwise be a safe report. It's like the bad level never happened!
#
# Now, the same rules apply as before, except if removing a single level from an unsafe report would make it safe, the report instead counts as safe.
#
# More of the above example's reports are now safe:
#
#     7 6 4 2 1: Safe without removing any level.
#     1 2 7 8 9: Unsafe regardless of which level is removed.
#     9 7 6 2 1: Unsafe regardless of which level is removed.
#     1 3 2 4 5: Safe by removing the second level, 3.
#     8 6 4 4 1: Safe by removing the third level, 4.
#     1 3 6 7 9: Safe without removing any level.
#
# Thanks to the Problem Dampener, 4 reports are actually safe!
#
# Update your analysis by handling situations where the Problem Dampener can remove a single level from unsafe reports. How many reports are now safe?
#
# Your puzzle answer was 381.
#
# Answer: 381
# (execute time: 7.30725 ms, memory: 2118400 bytes)

require "../utils.cr"

module Day2
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    result = 0
    records.each do |instruction|
      next if instruction == ""
      items = instruction.split(" ").map { |i| i.to_i32 }
      result += 1 if items[0] > items[1] ? decreasing?(items) : increasing?(items)
    end

    result
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    result = 0
    records.each do |instruction|
      next if instruction == ""
      items = instruction.split(" ").map { |i| i.to_i32 }
      if bruet_force_decreasing?(items) || bruet_force_increasing?(items)
        # if decreasing?(items, 1) || increasing?(items, 1)
        # puts "#{items.inspect} =>  true"
        result += 1
      else
        # puts "#{items.inspect} =>  false"
      end
    end

    result
  end

  def increasing?(items : Array(Int32), level : Int32 = 0) : Bool
    # puts "> increasing?(#{items}, #{level})"
    prev = items[0]
    i = 1
    size = items.size
    while i < size
      if items[i] <= prev || (items[i] - prev).abs > 3
        level -= 1
        # puts "  #{items[i]} <> #{prev} #{level}"
        return false if level < 0
        return increasing?(items[1..]) if i == 1
      else
        prev = items[i]
      end
      i += 1
    end
    true
  end

  def decreasing?(items : Array(Int32), level : Int32 = 0) : Bool
    prev = items[0]
    i = 1
    size = items.size
    while i < size
      if items[i] >= prev || (prev - items[i]) > 3
        level -= 1
        return false if level < 0
        return decreasing?(items[i..]) if i == 1
      else
        prev = items[i]
      end
      i += 1
    end
    true
  end

  # It is not elegant solution, but it covers all strange cases
  def bruet_force_increasing?(items) : Bool
    return true if increasing?(items)

    i = 0
    n = items.size
    while i < n
      return true if increasing?(items[...i] + items[i + 1..])
      i += 1
    end
    false
  end

  def bruet_force_decreasing?(items) : Bool
    return true if decreasing?(items)

    i = 0
    n = items.size
    while i < n
      return true if decreasing?(items[...i] + items[i + 1..])
      i += 1
    end
    false
  end
end

# vim: filetype=crystal syntax=crystal
