require "spec"
require "./day05"

describe Day5 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      47|53
      97|13
      97|61
      97|47
      75|29
      61|13
      75|53
      29|13
      97|29
      53|29
      61|53
      97|53
      61|29
      47|13
      75|47
      97|75
      47|61
      75|61
      47|29
      75|13
      53|13

      75,47,61,53,29
      97,61,53,29,13
      75,29,13
      75,97,47,61,53
      61,13,29
      97,13,75,29,47
      EOF
      actual = Day5.problem1(input.split("\n"))
      actual.should eq(143)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      47|53
      97|13
      97|61
      97|47
      75|29
      61|13
      75|53
      29|13
      97|29
      53|29
      61|53
      97|53
      61|29
      47|13
      75|47
      97|75
      47|61
      75|61
      47|29
      75|13
      53|13

      75,47,61,53,29
      97,61,53,29,13
      75,29,13
      75,97,47,61,53
      61,13,29
      97,13,75,29,47
      EOF
      actual = Day5.problem2(input.split("\n"))
      actual.should eq(123)
    end
  end
end
