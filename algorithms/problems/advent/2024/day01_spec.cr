require "spec"
require "./day01"

describe Day1 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      3   4
      4   3
      2   5
      1   3
      3   9
      3   3
      EOF
      actual = Day1.problem1(input.split("\n"))
      actual.should eq(11)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      3   4
      4   3
      2   5
      1   3
      3   9
      3   3
      EOF
      actual = Day1.problem2(input.split("\n"))
      actual.should eq(31)
    end
  end
end
