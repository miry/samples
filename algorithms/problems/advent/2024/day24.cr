# https://adventofcode.com/2024/day/24
#
# --- Day 24: ... ---
#

require "../utils.cr"

module Day24
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    i = 0
    n = records.size
    registers = Hash(String, Int32).new
    zregisters = Set(String).new
    while records[i] != ""
      register, val = records[i].split(": ", 2)
      registers[register] = val.to_i
      zregisters.add?(register) if register[0] == 'z'
      i += 1
    end

    i += 1
    instructions = Array(Tuple(String, String, String, String)).new
    while i < n
      expr, result_register = records[i].split(" -> ", 2)
      op1, operation, op2 = expr.split(" ")
      instructions << {operation, op1, op2, result_register}
      zregisters.add?(result_register) if result_register[0] == 'z'
      i += 1
    end

    process(registers, zregisters, instructions)
  end

  def process(registers, zregisters, instructions)
    result_registers = zregisters.dup
    i = 0
    while !instructions.empty?
      i = 0 if i >= instructions.size
      instruction = instructions[i]

      operation, op1, op2, result_register = instruction
      if !registers.has_key?(op1) || !registers.has_key?(op2)
        i += 1
        next
      end

      case operation
      when "AND"
        registers[result_register] = registers[op1] & registers[op2]
      when "OR"
        registers[result_register] = registers[op1] | registers[op2]
      when "XOR"
        registers[result_register] = registers[op1] ^ registers[op2]
      end
      instructions.delete_at(i)

      if result_register[0] == 'z'
        result_registers.delete(result_register)
        break if result_registers.empty?
      end
    end
    bits_to_number(zregisters, registers)
  end

  # --- Part Two ---
  def problem2(records : Array(String), size = 8)
    i = 0
    n = records.size
    registers = Hash(String, Int64).new
    zregisters = Set(String).new
    xregisters = Set(String).new
    yregisters = Set(String).new
    while records[i] != ""
      register, val = records[i].split(": ", 2)
      registers[register] = val.to_i64
      zregisters.add?(register) if register[0] == 'z'
      xregisters.add?(register) if register[0] == 'x'
      yregisters.add?(register) if register[0] == 'y'
      i += 1
    end

    i += 1
    wires = Hash(String, Tuple(String, String, String, String)).new
    instructions = Array(Tuple(String, String, String, String)).new
    while i < n
      expr, result_register = records[i].split(" -> ", 2)
      op1, operation, op2 = expr.split(" ")
      instructions << {operation, op1, op2, result_register}
      wires[result_register] = {operation, op1, op2, result_register}
      zregisters.add?(result_register) if result_register[0] == 'z'
      i += 1
    end

    max_bit = zregisters.max[1..].to_i
    pp! max_bit

    x = bits_to_number(xregisters, registers)
    pp! x.to_s(base: 2, precision: max_bit)
    y = bits_to_number(yregisters, registers)
    pp! y.to_s(base: 2, precision: max_bit)
    checksum = x + y
    pp! checksum.to_s(base: 2, precision: max_bit)
    checksum_bin = checksum.to_s(base: 2, precision: max_bit).reverse

    z = process_hash(registers, zregisters, wires.dup)
    pp! z.to_s(base: 2, precision: max_bit).reverse

    visited_pairs = Set(Array(Set(String))).new
    wires.keys.sort.each_permutation(size, reuse: true) do |cons|
      modified = wires.dup
      pairs = Array(Set(String)).new
      cons.each_slice(2, reuse: true) do |pair|
        modified[pair[0]], modified[pair[1]] = wires[pair[1]], wires[pair[0]]
        pairs << pair.to_set
      end

      pp! pairs
      next if visited_pairs.includes?(pairs)

      if checksum == process_hash(registers.dup, zregisters, modified, checksum_bin)
        return cons.sort.join(",")
      end

      visited_pairs.add pairs
    end
  end

  # def compress(instructions, zregisters)
  #   used_registers = Set(String).new
  #
  #   queue = zregisters
  #   while !queue.empty?
  #     new_queue = Set(String).new
  #
  #     pp! queue
  #     queue.each do |register|
  #       next if used_registers.includes?(register) || !instructions.has_key?(register)
  #       used_registers.add?(register)
  #       new_queue.add? instructions[register][1]
  #       new_queue.add? instructions[register][2]
  #     end
  #
  #     pp! used_registers
  #     queue = new_queue
  #   end
  #   used_registers.to_a
  # end

  def process_hash(registers, zregisters, instructions, checksum = nil)
    result_registers = zregisters.dup
    i = 0

    while !result_registers.empty?
      instructions.each do |result_register, instruction|
        operation, op1, op2, _old = instruction
        if !registers.has_key?(op1) || !registers.has_key?(op2)
          i += 1
          next
        end

        case operation
        when "AND"
          registers[result_register] = registers[op1] & registers[op2]
        when "OR"
          registers[result_register] = registers[op1] | registers[op2]
        when "XOR"
          registers[result_register] = registers[op1] ^ registers[op2]
        end
        instructions.delete(result_register)
        if result_register[0] == 'z'
          result_registers.delete(result_register)
          if checksum
            i = result_register[1..].to_i
            puts "checksum[#{i}] = #{checksum[i]} register = #{result_register} val = #{registers[result_register]}"
            if checksum[i] != registers[result_register].to_s
              puts "wrong sum"
              return 0
            end
          end
          break if result_registers.empty?
        end
      end
    end
    bits_to_number(zregisters, registers)
  end

  def bits_to_number(bits, registers)
    result = 0_u64
    bits.to_a.sort.each do |register|
      result |= 1_u64 << register[1..].to_i if registers[register] == 1
    end
    result
  end
end

# vim: filetype=crystal syntax=crystal
