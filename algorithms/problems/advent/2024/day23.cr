# https://adventofcode.com/2024/day/23
#
# --- Day 23: LAN Party ---
#
# As The Historians wander around a secure area at Easter Bunny HQ, you come across posters for a LAN party scheduled for today! Maybe you can find it; you connect to a nearby datalink port and download a map of the local network (your puzzle input).
#
# The network map provides a list of every connection between two computers. For example:
#
# kh-tc
# qp-kh
# de-cg
# ka-co
# yn-aq
# qp-ub
# cg-tb
# vc-aq
# tb-ka
# wh-tc
# yn-cg
# kh-ub
# ta-co
# de-co
# tc-td
# tb-wq
# wh-td
# ta-ka
# td-qp
# aq-cg
# wq-ub
# ub-vc
# de-ta
# wq-aq
# wq-vc
# wh-yn
# ka-de
# kh-ta
# co-tc
# wh-qp
# tb-vc
# td-yn
#
# Each line of text in the network map represents a single connection; the line kh-tc represents a connection between the computer named kh and the computer named tc. Connections aren't directional; tc-kh would mean exactly the same thing.
#
# LAN parties typically involve multiplayer games, so maybe you can locate it by finding groups of connected computers. Start by looking for sets of three computers where each computer in the set is connected to the other two computers.
#
# In this example, there are 12 such sets of three inter-connected computers:
#
# aq,cg,yn
# aq,vc,wq
# co,de,ka
# co,de,ta
# co,ka,ta
# de,ka,ta
# kh,qp,ub
# qp,td,wh
# tb,vc,wq
# tc,td,wh
# td,wh,yn
# ub,vc,wq
#
# If the Chief Historian is here, and he's at the LAN party, it would be best to know that right away. You're pretty sure his computer's name starts with t, so consider only sets of three computers where at least one computer's name starts with t. That narrows the list down to 7 sets of three inter-connected computers:
#
# co,de,ta
# co,ka,ta
# de,ka,ta
# qp,td,wh
# tb,vc,wq
# tc,td,wh
# td,wh,yn
#
# Find all the sets of three inter-connected computers. How many contain at least one computer with a name that starts with t?
#
# Your puzzle answer was 1218.
#
# Output:
#
#   Answer: 1218
#   (execute time: 3.793875 ms, memory: 2553776 bytes)
#
# --- Part Two ---
#
# There are still way too many results to go through them all. You'll have to find the LAN party another way and go there yourself.
#
# Since it doesn't seem like any employees are around, you figure they must all be at the LAN party. If that's true, the LAN party will be the largest set of computers that are all connected to each other. That is, for each computer at the LAN party, that computer will have a connection to every other computer at the LAN party.
#
# In the above example, the largest set of computers that are all connected to each other is made up of co, de, ka, and ta. Each computer in this set has a connection to every other computer in the set:
#
# ka-co
# ta-co
# de-co
# ta-ka
# de-ta
# ka-de
#
# The LAN party posters say that the password to get into the LAN party is the name of every computer at the LAN party, sorted alphabetically, then joined together with commas. (The people running the LAN party are clearly a bunch of nerds.) In this example, the password would be co,de,ka,ta.
#
# What is the password to get into the LAN party?
#
# Your puzzle answer was ah,ap,ek,fj,fr,jt,ka,ln,me,mp,qa,ql,zg.
#
# Output:
#
#   Answer: ah,ap,ek,fj,fr,jt,ka,ln,me,mp,qa,ql,zg
#   (execute time: 9.128833 ms, memory: 7481056 bytes)

require "../utils.cr"

module Day23
  extend self

  # --- Part One ---
  # 2482 too high
  def problem1(records : Array(String))
    graf = Hash(String, Set(String)).new
    records.each do |instruction|
      next if instruction == ""

      com1, com2 = instruction.split("-", 2)
      graf[com1] ||= Set(String).new
      graf[com1].add?(com2)
      graf[com2] ||= Set(String).new
      graf[com2].add?(com1)
    end

    # visited = Set(String).new

    result = 0
    queue = graf.keys
    queue.sort!
    visited = Set(String).new
    queue.each do |key|
      neighbours = graf[key].to_a.sort.select { |a| a > key }
      neighbours.each_combination(2) do |cons|
        next if !graf[cons[0]].includes?(cons[1]) || cons[0] > cons[1]

        if key[0] == 't' || cons[0][0] == 't' || cons[1][0] == 't'
          # cache_key = (key + cons[0] + cons[1]).chars.sort.join
          cache_key = [key, cons[0], cons[1]].sort.join
          pp! cache_key if visited.includes?(cache_key) || !graf[cons[1]].includes?(cons[0]) || !graf[cons[0]].includes?(cons[1]) || !graf[key].includes?(cons[0]) || !graf[key].includes?(cons[1])
          visited.add?(cache_key)
          result += 1
        end
      end
    end
    result
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    graf = Hash(String, Set(String)).new
    records.each do |instruction|
      next if instruction == ""

      com1, com2 = instruction.split("-", 2)
      graf[com1] ||= Set{com1}
      graf[com1].add?(com2)
      graf[com2] ||= Set{com2}
      graf[com2].add?(com1)
    end

    # This approach works for the current input but may not be robust for all cases.
    # For each pair of connected nodes, identify the largest common keys.
    # If a larger common subset is found, update the maximum and refine the nodes
    # by checking if each node in the subset is present in all lists using set intersections.
    max = Set(String).new
    graf.each do |key, neighbours|
      neighbours.each do |right|
        next if right == key
        common = neighbours & graf[right]
        if common.size > max.size
          result = common.dup
          common.each do |node|
            result &= graf[node]
          end
          max = result if result.size > max.size
        end
      end
    end
    max.to_a.sort.join(",")
  end
end

# vim: filetype=crystal syntax=crystal
