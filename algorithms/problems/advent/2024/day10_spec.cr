require "spec"
require "./day10"

describe Day10 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      0123
      1234
      8765
      9876
      EOF
      actual = Day10.problem1(input.split("\n"))
      actual.should eq(1)
    end
    it "sample 2" do
      input = <<-EOF
      ...0...
      ...1...
      ...2...
      6543456
      7.....7
      8.....8
      9.....9
      EOF
      actual = Day10.problem1(input.split("\n"))
      actual.should eq(2)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      .....0.
      ..4321.
      ..5..2.
      ..6543.
      ..7..4.
      ..8765.
      ..9....
      EOF
      actual = Day10.problem2(input.split("\n"))
      actual.should eq(3)
    end
  end
end
# vim: filetype=crystal syntax=crystal
