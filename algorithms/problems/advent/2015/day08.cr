# https://adventofcode.com/2015/day/8
#
# --- Day 8: ... ---
#

def problem8(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 8 is not implemented"
end

# --- Part Two ---

def problem8_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 8.2 is not implemented"
end
