require "spec"
require "./day01"

describe "Day 1" do
  describe "problem1" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem1(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem1_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem1_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
