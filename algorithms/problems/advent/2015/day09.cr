# https://adventofcode.com/2015/day/9
#
# --- Day 9: ... ---
#

def problem9(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 9 is not implemented"
end

# --- Part Two ---

def problem9_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 9.2 is not implemented"
end
