# https://adventofcode.com/2015/day/10
#
# --- Day 10: ... ---
#

def problem10(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 10 is not implemented"
end

# --- Part Two ---

def problem10_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 10.2 is not implemented"
end
