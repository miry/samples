# https://adventofcode.com/2015/day/21
#
# --- Day 21: ... ---
#

def problem21(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 21 is not implemented"
end

# --- Part Two ---

def problem21_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 21.2 is not implemented"
end
