require "spec"
require "./day08"

describe "Day 8" do
  describe "problem8" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem8(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem8_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem8_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
