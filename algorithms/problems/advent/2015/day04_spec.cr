require "spec"
require "./day04"

describe "Day 4" do
  describe "problem4" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem4(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem4_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem4_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
