# https://adventofcode.com/2015/day/18
#
# --- Day 18: ... ---
#

def problem18(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 18 is not implemented"
end

# --- Part Two ---

def problem18_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 18.2 is not implemented"
end
