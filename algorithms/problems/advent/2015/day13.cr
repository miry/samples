# https://adventofcode.com/2015/day/13
#
# --- Day 13: ... ---
#

def problem13(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 13 is not implemented"
end

# --- Part Two ---

def problem13_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 13.2 is not implemented"
end
