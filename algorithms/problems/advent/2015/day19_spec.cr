require "spec"
require "./day19"

describe "Day 19" do
  describe "problem19" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem19(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem19_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem19_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
