require "spec"
require "./day14"

describe "Day 14" do
  describe "problem14" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem14(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem14_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem14_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
