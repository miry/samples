# https://adventofcode.com/2015/day/7
#
# --- Day 7: ... ---
#

def problem7(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 7 is not implemented"
end

# --- Part Two ---

def problem7_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 7.2 is not implemented"
end
