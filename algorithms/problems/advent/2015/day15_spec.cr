require "spec"
require "./day15"

describe "Day 15" do
  describe "problem15" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem15(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem15_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem15_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
