require "spec"
require "./day22"

describe "Day 22" do
  describe "problem22" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem22(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem22_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem22_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
