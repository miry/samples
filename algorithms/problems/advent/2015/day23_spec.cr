require "spec"
require "./day23"

describe "Day 23" do
  describe "problem23" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem23(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem23_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem23_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
