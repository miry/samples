# https://adventofcode.com/2015/day/17
#
# --- Day 17: ... ---
#

def problem17(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 17 is not implemented"
end

# --- Part Two ---

def problem17_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 17.2 is not implemented"
end
