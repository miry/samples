require "spec"
require "./day13"

describe "Day 13" do
  describe "problem13" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem13(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem13_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem13_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
