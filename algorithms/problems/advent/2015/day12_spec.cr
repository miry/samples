require "spec"
require "./day12"

describe "Day 12" do
  describe "problem12" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem12(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem12_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem12_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
