require "spec"
require "./day18"

describe "Day 18" do
  describe "problem18" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem18(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem18_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem18_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
