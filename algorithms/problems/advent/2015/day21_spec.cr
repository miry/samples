require "spec"
require "./day21"

describe "Day 21" do
  describe "problem21" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem21(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem21_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem21_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
