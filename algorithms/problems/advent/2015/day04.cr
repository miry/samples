# https://adventofcode.com/2015/day/4
#
# --- Day 4: ... ---
#

def problem4(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 4 is not implemented"
end

# --- Part Two ---

def problem4_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 4.2 is not implemented"
end
