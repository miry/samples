require "spec"
require "./day20"

describe "Day 20" do
  describe "problem20" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem20(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem20_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem20_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
