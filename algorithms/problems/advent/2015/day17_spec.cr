require "spec"
require "./day17"

describe "Day 17" do
  describe "problem17" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem17(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem17_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem17_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
