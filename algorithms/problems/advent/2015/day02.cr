# https://adventofcode.com/2015/day/2
#
# --- Day 2: ... ---
#

def problem2(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 2 is not implemented"
end

# --- Part Two ---

def problem2_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 2.2 is not implemented"
end
