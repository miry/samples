# https://adventofcode.com/2015/day/3
#
# --- Day 3: ... ---
#

def problem3(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 3 is not implemented"
end

# --- Part Two ---

def problem3_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 3.2 is not implemented"
end
