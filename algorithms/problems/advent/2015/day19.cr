# https://adventofcode.com/2015/day/19
#
# --- Day 19: ... ---
#

def problem19(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 19 is not implemented"
end

# --- Part Two ---

def problem19_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 19.2 is not implemented"
end
