# https://adventofcode.com/2015/day/12
#
# --- Day 12: ... ---
#

def problem12(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 12 is not implemented"
end

# --- Part Two ---

def problem12_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 12.2 is not implemented"
end
