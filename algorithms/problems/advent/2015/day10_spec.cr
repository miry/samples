require "spec"
require "./day10"

describe "Day 10" do
  describe "problem10" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem10(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem10_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem10_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
