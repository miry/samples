require "spec"
require "./day16"

describe "Day 16" do
  describe "problem16" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem16(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem16_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem16_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
