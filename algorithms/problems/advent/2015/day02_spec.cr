require "spec"
require "./day02"

describe "Day 2" do
  describe "problem2" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem2(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem2_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem2_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
