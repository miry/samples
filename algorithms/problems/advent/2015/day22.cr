# https://adventofcode.com/2015/day/22
#
# --- Day 22: ... ---
#

def problem22(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 22 is not implemented"
end

# --- Part Two ---

def problem22_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 22.2 is not implemented"
end
