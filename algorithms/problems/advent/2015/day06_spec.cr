require "spec"
require "./day06"

describe "Day 6" do
  describe "problem6" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem6(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem6_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem6_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
