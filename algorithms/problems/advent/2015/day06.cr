# https://adventofcode.com/2015/day/6
#
# --- Day 6: ... ---
#

def problem6(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 6 is not implemented"
end

# --- Part Two ---

def problem6_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 6.2 is not implemented"
end
