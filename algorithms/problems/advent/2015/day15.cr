# https://adventofcode.com/2015/day/15
#
# --- Day 15: ... ---
#

def problem15(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 15 is not implemented"
end

# --- Part Two ---

def problem15_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 15.2 is not implemented"
end
