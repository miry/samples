require "spec"
require "./day05"

describe "Day 5" do
  describe "problem5" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem5(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem5_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem5_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
