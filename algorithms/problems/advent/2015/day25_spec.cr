require "spec"
require "./day25"

describe "Day 25" do
  describe "problem25" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem25(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem25_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem25_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
