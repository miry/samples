# https://adventofcode.com/2015/day/14
#
# --- Day 14: ... ---
#

def problem14(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 14 is not implemented"
end

# --- Part Two ---

def problem14_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 14.2 is not implemented"
end
