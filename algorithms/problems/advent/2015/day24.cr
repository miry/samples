# https://adventofcode.com/2015/day/24
#
# --- Day 24: ... ---
#

def problem24(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 24 is not implemented"
end

# --- Part Two ---

def problem24_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 24.2 is not implemented"
end
