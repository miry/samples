require "spec"
require "./day03"

describe "Day 3" do
  describe "problem3" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem3(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem3_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem3_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
