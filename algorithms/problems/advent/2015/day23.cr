# https://adventofcode.com/2015/day/23
#
# --- Day 23: ... ---
#

def problem23(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 23 is not implemented"
end

# --- Part Two ---

def problem23_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 23.2 is not implemented"
end
