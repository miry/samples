require "spec"
require "./day11"

describe "Day 11" do
  describe "problem11" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem11(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem11_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem11_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
