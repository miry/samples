require "spec"
require "./day09"

describe "Day 9" do
  describe "problem9" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem9(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem9_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem9_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
