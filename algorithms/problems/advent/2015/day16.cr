# https://adventofcode.com/2015/day/16
#
# --- Day 16: ... ---
#

def problem16(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 16 is not implemented"
end

# --- Part Two ---

def problem16_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 16.2 is not implemented"
end
