require "spec"
require "./day07"

describe "Day 7" do
  describe "problem7" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem7(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem7_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem7_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
