# https://adventofcode.com/2015/day/5
#
# --- Day 5: ... ---
#

def problem5(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 5 is not implemented"
end

# --- Part Two ---

def problem5_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 5.2 is not implemented"
end
