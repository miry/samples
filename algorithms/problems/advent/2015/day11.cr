# https://adventofcode.com/2015/day/11
#
# --- Day 11: ... ---
#

def problem11(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 11 is not implemented"
end

# --- Part Two ---

def problem11_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 11.2 is not implemented"
end
