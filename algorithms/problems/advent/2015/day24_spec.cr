require "spec"
require "./day24"

describe "Day 24" do
  describe "problem24" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem24(input.split("\n"))
      actual.should eq(12)
    end
  end

  describe "problem24_part_two" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = problem24_part_two(input.split("\n"))
      actual.should eq(12)
    end
  end
end
