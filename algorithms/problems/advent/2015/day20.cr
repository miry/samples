# https://adventofcode.com/2015/day/20
#
# --- Day 20: ... ---
#

def problem20(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 20 is not implemented"
end

# --- Part Two ---

def problem20_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 20.2 is not implemented"
end
