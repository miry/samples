# https://adventofcode.com/2015/day/25
#
# --- Day 25: ... ---
#

def problem25(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 25 is not implemented"
end

# --- Part Two ---

def problem25_part_two(records : Array(String))
  records.each do |instruction|
    next if instruction == ""
  end

  raise "Day 25.2 is not implemented"
end
