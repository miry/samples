require "spec"
require "./utils"

describe "Utils" do
  describe "#print_plan" do
    it "print table with columns" do
      coords = {
        {x: 0, y: 0} => '#',
        {x: 0, y: 1} => '#',
        {x: 1, y: 1} => '#',
        {x: 2, y: 1} => '#',
      }
      io = IO::Memory.new
      print_plan(coords, 4, 10, empty = '.', output: io)
      io.to_s.rchop.should eq(<<-EOS)
           0123456789
      000  #.........
      001  ###.......
      002  ..........
      003  ..........

      EOS
    end
  end

  describe "matrix" do
    it "print matrix with columns" do
      arr = [
        "1.1....11.",
        ".111...2..",
        "..2.1.111.",
        "...1.2.2..",
        ".112313211",
        "...1.2....",
        "..1...1...",
        ".1.....1..",
        "1.......1.",
        "222111....",
      ]
      io = IO::Memory.new
      print_matrix(arr.map(&.chars), output: io)
      io.to_s.rchop.should eq(<<-EOS)
      1  .  1  .  .  .  .  1  1  .
      .  1  1  1  .  .  .  2  .  .
      .  .  2  .  1  .  1  1  1  .
      .  .  .  1  .  2  .  2  .  .
      .  1  1  2  3  1  3  2  1  1
      .  .  .  1  .  2  .  .  .  .
      .  .  1  .  .  .  1  .  .  .
      .  1  .  .  .  .  .  1  .  .
      1  .  .  .  .  .  .  .  1  .
      2  2  2  1  1  1  .  .  .  .
      EOS
    end

    it "print matrix with custom format" do
      arr = [
        "22 13 17 11  0".strip.split(/\s+/),
        " 8  2 23  4 24".strip.split(/\s+/),
        "21  9 14 16  7".strip.split(/\s+/),
        " 6 10  3 18  5".strip.split(/\s+/),
        " 1 12 20 15 19".strip.split(/\s+/),
      ]
      io = IO::Memory.new
      print_matrix(arr, format_cell: "%2s", sep: " ", output: io)
      io.to_s.should eq("22 13 17 11  0\n 8  2 23  4 24\n21  9 14 16  7\n 6 10  3 18  5\n 1 12 20 15 19\n")
    end

    it "print matrix modify cell content from callback" do
      arr = [
        "22 13 17 11  0".strip.split(/\s+/).map(&.to_i32),
        " 8  2 23  4 24".strip.split(/\s+/).map(&.to_i32),
        "21  9 14 16  7".strip.split(/\s+/).map(&.to_i32),
        " 6 10  3 18  5".strip.split(/\s+/).map(&.to_i32),
        " 1 12 20 15 19".strip.split(/\s+/).map(&.to_i32),
      ]
      played = [7, 4, 9, 5, 11, 17]
      io = IO::Memory.new
      print_matrix(arr, format_cell: "%3s", sep: " ", output: io) do |cell|
        played.includes?(cell) ? "*#{cell}" : cell.to_s
      end
      io.to_s.rchop.should eq(<<-EOS)
       22  13 *17 *11   0
        8   2  23  *4  24
       21  *9  14  16  *7
        6  10   3  18  *5
        1  12  20  15  19
      EOS
    end
  end

  describe "Range#+" do
    it "includes second" do
      actual = (10..12) + (10..11)
      actual.should eq([10..12])
    end

    it "merge in one range" do
      actual = (10..12) + (11..13)
      actual.should eq([10..13])
    end

    it "merge two neighbours in one range" do
      actual = (10..12) + (13..14)
      actual.should eq([10..14])
    end

    it "skip merge two neighbours in one range if exclude end" do
      actual = (10...12) + (13..14)
      actual.should eq([10...12, 13..14])
    end

    it "merge two neighbours in one range if exclude end" do
      actual = (10...12) + (12..14)
      actual.should eq([10..14])
    end

    it "merge two neighbours reverse in one range" do
      actual = (13..14) + (10..12)
      actual.should eq([10..14])
    end

    it "merge same" do
      actual = (-120100..-32970) + (-120100..-32970)
      actual.should eq([-120100..-32970])
    end
  end

  describe "Range#-" do
    it "uncommon" do
      actual = (10..12) - (1..2)
      actual.should eq([10..12])
    end

    it "common cut" do
      actual = (10..13) - (10..11)
      actual.should eq([12..13])
    end

    it "common small cut" do
      actual = (10..13) - (11..14)
      actual.should eq([10..10])
    end

    it "subtract single" do
      actual = (10..13) - (10..10)
      actual.should eq([11..13])
    end
  end

  describe "Range#split" do
    it "returns themself if contains the same" do
      actual = (10..11).split(10..11)
      actual.should eq([10..11])
    end

    it "returns themself if contains the same and exclusive" do
      actual = (10...11).split(10...11)
      actual.should eq([10...11])
    end

    it "returns 2 elements if end is different" do
      actual = (10..11).split(10..12)
      actual.should eq([10..11])
    end

    it "covers the case other is bigger" do
      actual = (15..18).split(10..20)
      actual.should eq([15..18])
    end

    it "covers the case other is bigger" do
      actual = (15..20).split(10..18)
      actual.should eq([15..18, 19..20])
    end

    it "returns 2 elements if end is different and exclusive" do
      actual = (10...11).split(10...12)
      actual.should eq([10...11])
    end

    it "returns same objects if not common points" do
      actual = (10...11).split(11...12)
      actual.should eq([10...11])
    end

    it "returns same objects if not common points" do
      actual = (10...11).split(12...13)
      actual.should eq([10...11])
    end

    it "checks for inner split exclusive" do
      actual = (10...20).split(15...16)
      actual.should eq([10...15, 15...16, 16...20])
    end

    it "checks for inner split" do
      actual = (10..20).split(15..16)
      actual.should eq([10...15, 15..16, 17..20])
    end

    it "more samples" do
      (79...93).split(50...98).should eq([79...93])
    end
  end

  describe "Range#contains" do
    it "covers exclusive case" do
      (10...11).contains?(10...11).should be_true
      (10...20).contains?(15...20).should be_true
      (10...20).contains?(15..20).should be_false
      (50...98).contains?(79...93).should be_true
    end
  end

  describe "Range#common" do
    it "returns the smallest" do
      actual = (10...20) & (11...15)
      actual.should eq(11...15)
    end
  end

  describe "insert_sorted" do
    it "insert in the middle sorted place" do
      arr = [1, 2, 3, 4, 5, 6, 7, 8, 10]
      insert_sorted(arr, 9).should eq(8)
      arr.should eq([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    end

    it "insert in begin sorted place" do
      arr = [1, 2]
      insert_sorted(arr, 0)
      arr.should eq([0, 1, 2])
    end

    it "insert in begin sorted place" do
      arr = [0, 1, 2]
      insert_sorted(arr, 0).should eq(0)
      arr.should eq([0, 0, 1, 2])
    end

    it "insert in end sorted place" do
      arr = [1, 2]
      insert_sorted(arr, 3).should eq(2)
      arr.should eq([1, 2, 3])
    end

    it "insert similar in end sorted place" do
      arr = [1, 2, 3]
      insert_sorted(arr, 3)
      arr.should eq([1, 2, 3, 3])
    end

    it "insert in empty array" do
      arr = [] of Int32
      insert_sorted(arr, 3)
      arr.should eq([3])
    end
  end
end

# vim: filetype=crystal syntax=crystal
