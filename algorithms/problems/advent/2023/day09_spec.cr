require "spec"
require "./day09"

describe Day9 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      0 3 6 9 12 15
      1 3 6 10 15 21
      10 13 16 21 30 45
      EOF
      actual = Day9.problem1(input.split("\n"))
      actual.should eq(114)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      0 3 6 9 12 15
      1 3 6 10 15 21
      10 13 16 21 30 45
      EOF
      actual = Day9.problem2(input.split("\n"))
      actual.should eq(2)
    end
  end
end
