# https://adventofcode.com/2023/day/1
#
# --- Day 1: Trebuchet?! ---
#
# Something is wrong with global snow production, and you've been selected to take a look. The Elves have even given you a map; on it, they've used stars to mark the top fifty locations that are likely to be having problems.
#
# You've been doing this long enough to know that to restore snow operations, you need to check all fifty stars by December 25th.
#
# Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!
#
# You try to ask why they can't just use a weather machine ("not powerful enough") and where they're even sending you ("the sky") and why your map looks mostly blank ("you sure ask a lot of questions") and hang on did you just say the sky ("of course, where do you think snow comes from") when you realize that the Elves are already loading you into a trebuchet ("please hold still, we need to strap you in").
#
# As they're making the final adjustments, they discover that their calibration document (your puzzle input) has been amended by a very young Elf who was apparently just excited to show off her art skills. Consequently, the Elves are having trouble reading the values on the document.
#
# The newly-improved calibration document consists of lines of text; each line originally contained a specific calibration value that the Elves now need to recover. On each line, the calibration value can be found by combining the first digit and the last digit (in that order) to form a single two-digit number.
#
# For example:
#
# 1abc2
# pqr3stu8vwx
# a1b2c3d4e5f
# treb7uchet
#
# In this example, the calibration values of these four lines are 12, 38, 15, and 77. Adding these together produces 142.
#
# Consider your entire calibration document. What is the sum of all of the calibration values?
#
# Your puzzle answer was 55029.
#
# Output:
#
# Answer: 2344935
# (execute time: 1.081459 ms, memory: 319552 bytes)
#
# --- Part Two ---
#
# Your calculation isn't quite right. It looks like some of the digits are actually spelled out with letters: one, two, three, four, five, six, seven, eight, and nine also count as valid "digits".
#
# Equipped with this new information, you now need to find the real first and last digit on each line. For example:
#
# two1nine
# eightwothree
# abcone2threexyz
# xtwone3four
# 4nineeightseven2
# zoneight234
# 7pqrstsixteen
#
# In this example, the calibration values are 29, 83, 13, 24, 42, 14, and 76. Adding these together produces 281.
#
# What is the sum of all of the calibration values?
#
# Your puzzle answer was 55686.
#
# Output:
#
# Answer: 55686
# (execute time: 00:00:00.002696750 , memory: 301168 bytes)

require "../utils.cr"

module Day1
  extend self

  FIRST_DIGIT_PATTERN = /([1-9]|one|two|three|four|five|six|seven|eight|nine)/
  # Regexp uses gridy algorithms and match the longest line as possible.
  # That plays the role to get the last element with matching all whole characters before with .*.
  LAST_DIGIT_PATTERN = /.*([1-9]|one|two|three|four|five|six|seven|eight|nine)/
  WORD_TO_DIGIT      = {
    "1"     => 1,
    "2"     => 2,
    "3"     => 3,
    "4"     => 4,
    "5"     => 5,
    "6"     => 6,
    "7"     => 7,
    "8"     => 8,
    "9"     => 9,
    "one"   => 1,
    "two"   => 2,
    "three" => 3,
    "four"  => 4,
    "five"  => 5,
    "six"   => 6,
    "seven" => 7,
    "eight" => 8,
    "nine"  => 9,
  }

  # Fast solution to get the first|last digit without Regexp
  def first_digit(str, last = false) : UInt8
    slice = str.to_slice
    n = slice.size
    start = '0'.ord
    ending = '9'.ord
    if last
      (slice.size - 1).downto(0) do |i|
        c = slice[i]
        return c - '0'.ord if c <= ending && c > start
      end
    else
      n.times do |i|
        c = slice[i]
        return c - '0'.ord if c <= ending && c > start
      end
    end
    raise "could not find a digit"
  end

  # Alternative solution to #first_digit, but with Iterators
  def first_digit_iterator(str, last = false) : UInt8
    start = '0'.ord
    ending = '9'.ord
    itr = last ? str.to_slice.reverse_each : str.to_slice.each

    result = itr.find do |c|
      c <= ending && c > start
    end

    return result - '0'.ord if result

    raise "could not find a digit"
  end

  # Fast solution to get the first|last digit and word representation with Regexp
  # https://www.pcre.org/current/doc/html/pcre2syntax.html
  def get_digit_from_mix(str, last = false) : Int32
    pattern = last ? LAST_DIGIT_PATTERN : FIRST_DIGIT_PATTERN
    m = str.match!(pattern)
    WORD_TO_DIGIT[m[1]]
  end

  # --- Part One ---
  # Optimised version of solution
  def problem1(records : Array(String))
    result, v1, v2 = 0, 0, 0
    records.each do |instruction|
      next if instruction == ""
      v1 = first_digit(instruction)
      v2 = first_digit(instruction, last: true)
      result += (v1 * 10 + v2)
    end

    result
  end

  # Short version of solution
  def problem1_short(records)
    records.reject(&.blank?).sum do |str|
      d1 = str.match!(/([0-9])/)[1]
      d2 = str.match!(/.*([0-9])/)[1]
      d1.to_i * 10 + d2.to_i
    end
  end

  # --- Part Two ---
  # Optimised version of solution
  def problem2(records : Array(String))
    result, v1, v2 = 0, 0, 0
    records.map do |instruction|
      next if instruction == ""
      v1 = get_digit_from_mix(instruction)
      v2 = get_digit_from_mix(instruction, last: true)
      result += (v1 * 10 + v2)
    end

    result
  end

  # Short version of solution
  def problem2_short(records)
    records.reject(&.blank?).sum do |str|
      d1 = str.match!(FIRST_DIGIT_PATTERN)[1]
      d2 = str.match!(LAST_DIGIT_PATTERN)[1]
      WORD_TO_DIGIT[d1] * 10 + WORD_TO_DIGIT[d2]
    end
  end
end
