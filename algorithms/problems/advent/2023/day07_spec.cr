require "spec"
require "./day07"

describe Day7 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      32T3K 765
      T55J5 684
      KK677 28
      KTJJT 220
      QQQJA 483
      EOF
      actual = Day7.problem1(input.split("\n"))
      actual.should eq(6440)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      32T3K 765
      T55J5 684
      KK677 28
      KTJJT 220
      QQQJA 483
      EOF
      actual = Day7.problem2(input.split("\n"))
      actual.should eq(5905)
    end
  end
end
