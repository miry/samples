require "spec"
require "./day20"

describe Day20 do
  sample1 = <<-EOF.split("\n")
  broadcaster -> a, b, c
  %a -> b
  %b -> c
  %c -> inv
  &inv -> a
  EOF

  sample2 = <<-EOF.split("\n")
  broadcaster -> a
  %a -> inv, con
  &inv -> b
  %b -> con
  &con -> output
  EOF

  describe "problem1" do
    it "sample1" do
      actual = Day20.problem1(sample1)
      actual.should eq(32000000)
    end
    it "sample2" do
      actual = Day20.problem1(sample2)
      actual.should eq(11687500)
    end
  end
end
