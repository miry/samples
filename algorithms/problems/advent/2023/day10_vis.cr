require "../utils.cr"
require "./day10"

input_file_path = "input/day10.txt"
if !File.exists?(input_file_path)
  raise "Could not open #{input_file_path}"
end
input = File.open(input_file_path)
entries = [] of String
input.each_line do |line|
  entries << line
end

Day10.print_route(entries, 0)
