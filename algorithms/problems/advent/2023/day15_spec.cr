require "spec"
require "./day15"

describe Day15 do
  sample = <<-EOF.split("\n")
  rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7
  EOF

  describe "problem1" do
    it "sample" do
      actual = Day15.problem1(sample)
      actual.should eq(1320)
    end
  end

  describe "problem2" do
    it "sample" do
      actual = Day15.problem2(sample)
      actual.should eq(145)
    end
  end
end
