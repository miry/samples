require "spec"
require "./day06"

describe Day6 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      Time:      7  15   30
      Distance:  9  40  200
      EOF
      actual = Day6.problem1(input.split("\n"))
      actual.should eq(288)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      Time:      7  15   30
      Distance:  9  40  200
      EOF
      actual = Day6.problem2(input.split("\n"))
      actual.should eq(71503)
    end
  end
end
