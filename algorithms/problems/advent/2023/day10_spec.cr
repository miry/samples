require "spec"
require "./day10"

describe Day10 do
  describe "problem1" do
    it "sample simple" do
      input = <<-EOF
      .....
      .S-7.
      .|.|.
      .L-J.
      .....
      EOF
      actual = Day10.problem1(input.split("\n"))
      actual.should eq(4)
    end

    it "sample complex" do
      input = <<-EOF
      ..F7.
      .FJ|.
      SJ.L7
      |F--J
      LJ...
      EOF
      actual = Day10.problem1(input.split("\n"))
      actual.should eq(8)
    end

    it "sample with non main pipe showm" do
      input = <<-EOF
      7-F7-
      .FJ|7
      SJLL7
      |F--J
      LJ.LJ
      EOF
      actual = Day10.problem1(input.split("\n"))
      actual.should eq(8)
    end
  end

  describe "problem2" do
    it "sample one" do
      input = <<-EOF
      ...........
      .S-------7.
      .|F-----7|.
      .||.....||.
      .||.....||.
      .|L-7.F-J|.
      .|..|.|..|.
      .L--J.L--J.
      ...........
      EOF
      actual = Day10.problem2(input.split("\n"))
      actual.should eq(4)
    end

    it "sample without connection" do
      input = <<-EOF
      ..........
      .S------7.
      .|F----7|.
      .||....||.
      .||....||.
      .|L-7F-J|.
      .|..||..|.
      .L--JL--J.
      ..........
      EOF
      actual = Day10.problem2(input.split("\n"))
      actual.should eq(4)
    end

    it "sample 1" do
      input = <<-EOF
      ..........
      .S7F----7.
      .||L---7|.
      .||....||.
      .||....||.
      .|L----J|.
      .|..F7..|.
      .L--JL--J.
      ..........
      EOF
      actual = Day10.problem2(input.split("\n"))
      actual.should eq(4)
    end

    it "fill all down from L--7" do
      input = <<-EOF
      ....F--7
      .S7.L-7|
      .|L-7.||
      .|..L-J|
      .L-----J
      EOF
      actual = Day10.problem2(input.split("\n"))
      actual.should eq(2)
    end

    it "fill all up from F--" do
      input = <<-EOF
      ....F---7
      ....L--7|
      .S7F-7.||
      .|LJ.L-J|
      .L------J
      EOF
      actual = Day10.problem2(input.split("\n"))
      actual.should eq(1)
    end

    it "big example" do
      input = <<-EOF
      .F----7F7F7F7F-7....
      .|F--7||||||||FJ....
      .||.FJ||||||||L7....
      FJL7L7LJLJ||LJ.L-7..
      L--J.L7...LJS7F-7L7.
      ....F-J..F7FJ|L7L7L7
      ....L7.F7||L7|.L7L7|
      .....|FJLJ|FJ|F7|.LJ
      ....FJL-7.||.||||...
      ....L---J.LJ.LJLJ...
      EOF
      actual = Day10.problem2(input.split("\n"))
      actual.should eq(8)
    end

    it "big example with non main loop pipes" do
      input = <<-EOF
      FF7FSF7F7F7F7F7F---7
      L|LJ||||||||||||F--J
      FL-7LJLJ||||||LJL-77
      F--JF--7||LJLJ7F7FJ-
      L---JF-JLJ.||-FJLJJ7
      |F|F-JF---7F7-L7L|7|
      |FFJF7L7F-JF7|JL---7
      7-L-JL7||F7|L7F-7F7|
      L.L7LFJ|||||FJL7||LJ
      L7JLJL-JLJLJL--JLJ.L
      EOF
      actual = Day10.problem2(input.split("\n"))
      actual.should eq(10)
    end
  end

  describe "build routes" do
    pending "should read build route for example" do
      input = <<-EOF
      ..........
      .S------7.
      .|F----7|.
      .||....||.
      .||....||.
      .|L-7F-J|.
      .|..||..|.
      .L--JL--J.
      ..........
      EOF
      Day10.print_route(input.split("\n"), 0.5)
    end

    pending "should read build route for example" do
      input = <<-EOF
      FF7FSF7F7F7F7F7F---7
      L|LJ||||||||||||F--J
      FL-7LJLJ||||||LJL-77
      F--JF--7||LJLJ7F7FJ-
      L---JF-JLJ.||-FJLJJ7
      |F|F-JF---7F7-L7L|7|
      |FFJF7L7F-JF7|JL---7
      7-L-JL7||F7|L7F-7F7|
      L.L7LFJ|||||FJL7||LJ
      L7JLJL-JLJLJL--JLJ.L
      EOF
      Day10.print_route(input.split("\n"), 0.5)
    end
  end
end
