# https://adventofcode.com/2023/day/24
#
# Explanation: https://www.youtube.com/watch?v=jXjc2jEbRZg
# https://developer.apple.com/documentation/accelerate/solving_systems_of_linear_equations_with_lapack
# Solution: https://www.reddit.com/r/adventofcode/comments/18pnycy/comment/ket7ajw
#
# --- Day 24: Never Tell Me The Odds ---
#
# It seems like something is going wrong with the snow-making process. Instead of forming snow, the water that's been absorbed into the air seems to be forming hail!
#
# Maybe there's something you can do to break up the hailstones?
#
# Due to strong, probably-magical winds, the hailstones are all flying through the air in perfectly linear trajectories. You make a note of each hailstone's position and velocity (your puzzle input). For example:
#
# 19, 13, 30 @ -2,  1, -2
# 18, 19, 22 @ -1, -1, -2
# 20, 25, 34 @ -2, -2, -4
# 12, 31, 28 @ -1, -2, -1
# 20, 19, 15 @  1, -5, -3
#
# Each line of text corresponds to the position and velocity of a single hailstone. The positions indicate where the hailstones are right now (at time 0). The velocities are constant and indicate exactly how far each hailstone will move in one nanosecond.
#
# Each line of text uses the format px py pz @ vx vy vz. For instance, the hailstone specified by 20, 19, 15 @ 1, -5, -3 has initial X position 20, Y position 19, Z position 15, X velocity 1, Y velocity -5, and Z velocity -3. After one nanosecond, the hailstone would be at 21, 14, 12.
#
# Perhaps you won't have to do anything. How likely are the hailstones to collide with each other and smash into tiny ice crystals?
#
# To estimate this, consider only the X and Y axes; ignore the Z axis. Looking forward in time, how many of the hailstones' paths will intersect within a test area? (The hailstones themselves don't have to collide, just test for intersections between the paths they will trace.)
#
# In this example, look for intersections that happen with an X and Y position each at least 7 and at most 27; in your actual data, you'll need to check a much larger test area. Comparing all pairs of hailstones' future paths produces the following results:
#
# Hailstone A: 19, 13, 30 @ -2, 1, -2
# Hailstone B: 18, 19, 22 @ -1, -1, -2
# Hailstones' paths will cross inside the test area (at x=14.333, y=15.333).
#
# Hailstone A: 19, 13, 30 @ -2, 1, -2
# Hailstone B: 20, 25, 34 @ -2, -2, -4
# Hailstones' paths will cross inside the test area (at x=11.667, y=16.667).
#
# Hailstone A: 19, 13, 30 @ -2, 1, -2
# Hailstone B: 12, 31, 28 @ -1, -2, -1
# Hailstones' paths will cross outside the test area (at x=6.2, y=19.4).
#
# Hailstone A: 19, 13, 30 @ -2, 1, -2
# Hailstone B: 20, 19, 15 @ 1, -5, -3
# Hailstones' paths crossed in the past for hailstone A.
#
# Hailstone A: 18, 19, 22 @ -1, -1, -2
# Hailstone B: 20, 25, 34 @ -2, -2, -4
# Hailstones' paths are parallel; they never intersect.
#
# Hailstone A: 18, 19, 22 @ -1, -1, -2
# Hailstone B: 12, 31, 28 @ -1, -2, -1
# Hailstones' paths will cross outside the test area (at x=-6, y=-5).
#
# Hailstone A: 18, 19, 22 @ -1, -1, -2
# Hailstone B: 20, 19, 15 @ 1, -5, -3
# Hailstones' paths crossed in the past for both hailstones.
#
# Hailstone A: 20, 25, 34 @ -2, -2, -4
# Hailstone B: 12, 31, 28 @ -1, -2, -1
# Hailstones' paths will cross outside the test area (at x=-2, y=3).
#
# Hailstone A: 20, 25, 34 @ -2, -2, -4
# Hailstone B: 20, 19, 15 @ 1, -5, -3
# Hailstones' paths crossed in the past for hailstone B.
#
# Hailstone A: 12, 31, 28 @ -1, -2, -1
# Hailstone B: 20, 19, 15 @ 1, -5, -3
# Hailstones' paths crossed in the past for both hailstones.
#
# So, in this example, 2 hailstones' future paths cross inside the boundaries of the test area.
#
# However, you'll need to search a much larger test area if you want to see if any hailstones might collide. Look for intersections that happen with an X and Y position each at least 200000000000000 and at most 400000000000000. Disregard the Z axis entirely.
#
# Considering only the X and Y axes, check all pairs of hailstones' future paths for intersections. How many of these intersections occur within the test area?
#
# Your puzzle answer was 20963.
# --- Part Two ---
#
# Upon further analysis, it doesn't seem like any hailstones will naturally collide. It's up to you to fix that!
#
# You find a rock on the ground nearby. While it seems extremely unlikely, if you throw it just right, you should be able to hit every hailstone in a single throw!
#
# You can use the probably-magical winds to reach any integer position you like and to propel the rock at any integer velocity. Now including the Z axis in your calculations, if you throw the rock at time 0, where do you need to be so that the rock perfectly collides with every hailstone? Due to probably-magical inertia, the rock won't slow down or change direction when it collides with a hailstone.
#
# In the example above, you can achieve this by moving to position 24, 13, 10 and throwing the rock at velocity -3, 1, 2. If you do this, you will hit every hailstone as follows:
#
# Hailstone: 19, 13, 30 @ -2, 1, -2
# Collision time: 5
# Collision position: 9, 18, 20
#
# Hailstone: 18, 19, 22 @ -1, -1, -2
# Collision time: 3
# Collision position: 15, 16, 16
#
# Hailstone: 20, 25, 34 @ -2, -2, -4
# Collision time: 4
# Collision position: 12, 17, 18
#
# Hailstone: 12, 31, 28 @ -1, -2, -1
# Collision time: 6
# Collision position: 6, 19, 22
#
# Hailstone: 20, 19, 15 @ 1, -5, -3
# Collision time: 1
# Collision position: 21, 14, 12
#
# Above, each hailstone is identified by its initial position and its velocity. Then, the time and position of that hailstone's collision with your rock are given.
#
# After 1 nanosecond, the rock has exactly the same position as one of the hailstones, obliterating it into ice dust! Another hailstone is smashed to bits two nanoseconds after that. After a total of 6 nanoseconds, all of the hailstones have been destroyed.
#
# So, at time 0, the rock needs to be at X position 24, Y position 13, and Z position 10. Adding these three coordinates together produces 47. (Don't add any coordinates from the rock's velocity.)
#
# Determine the exact position and velocity the rock needs to have at time 0 so that it perfectly collides with every hailstone. What do you get if you add up the X, Y, and Z coordinates of that initial position?
#
# Your puzzle answer was 999782576459892.
#
# Output:
#
# crystal run cli.cr -- -d 24 -i input/day24.txt
# --- Day 24 ---
# --- Part One ---
# Answer: 20963
# (execute time: 36.822709 ms, memory: 24518192 bytes)
# crystal run cli.cr -- -d 24 -p 2 -i input/day24.txt
# --- Day 24: ---
# --- Part Two ---
# Answer: 999782576459892.0
# (execute time: 3.191875 ms, memory: 499360 bytes)

require "../utils.cr"

module Day24
  extend self

  class Vector
    property x : Float64
    property y : Float64
    property z : Float64

    def initialize(x, y, z)
      @x = x
      @y = y
      @z = z
    end

    # Vector addition
    def +(other : Vector) : Vector
      Vector.new(@x + other.x, @y + other.y, @z + other.z)
    end

    # Vector subtraction
    def -(other : Vector) : Vector
      Vector.new(@x - other.x, @y - other.y, @z - other.z)
    end

    # Vector dot product
    def dot(other : Vector) : Float64
      @x * other.x + @y * other.y + @z * other.z
    end

    # Vector cross product
    def cross(other : Vector) : Vector
      Vector.new(@y * other.z - @z * other.y, @z * other.x - @x * other.z, @x * other.y - @y * other.x)
    end

    # Vector magnitude
    def magnitude : Float64
      Math.sqrt(@x**2 + @y**2 + @z**2)
    end

    # Scalar multiplication
    def *(scalar : Float64) : Vector
      Vector.new(@x * scalar, @y * scalar, @z * scalar)
    end

    # Scalar division
    def /(scalar : Float64) : Vector
      Vector.new(@x / scalar, @y / scalar, @z / scalar)
    end

    def to_s
      "{x: #{x}, y: #{y}, z: #{z}}"
    end
  end

  class Ray
    property origin : Vector
    property direction : Vector

    def initialize(origin, direction)
      @origin = origin
      @direction = direction
    end
  end

  def find_intersection(ray1, ray2)
    # Vector between the origins of the two rays
    delta_origin = ray2.origin - ray1.origin

    # Calculate the cross product of the direction vectors of the two rays
    cross_product = ray1.direction.cross(ray2.direction)

    # Check if the cross product is zero, which means the rays are parallel
    return nil if cross_product.magnitude.abs < 1e-6

    # Calculate the parameters for the parametric equations of the rays
    t1 = delta_origin.cross(ray2.direction).dot(cross_product) / cross_product.magnitude**2
    t2 = delta_origin.cross(ray1.direction).dot(cross_product) / cross_product.magnitude**2

    # Calculate the intersection points
    intersection_point1 = ray1.origin + (ray1.direction * t1)
    intersection_point2 = ray2.origin + (ray2.direction * t2)

    # Check if the intersection points are the same (within a small tolerance)
    return intersection_point1 if (intersection_point1 - intersection_point2).magnitude < 1

    nil # No intersection
  end

  def parse(records, include_z = false)
    hails = Array(Ray).new(records.size)
    records.each_with_index do |instruction, _|
      position_raw, velocity_raw = instruction.split(" @ ").map { |sides| sides.split(",").map(&.to_f64) }
      # hails << { {x: position_raw[0], y: position_raw[1], z: position_raw[2]}, {x: velocity_raw[0], y: velocity_raw[1], z: velocity_raw[2]} }
      # hails << Ray.new(Vector.new(position_raw[0], position_raw[1], position_raw[2]), Vector.new(velocity_raw[0], velocity_raw[1], velocity_raw[2]))
      hails << Ray.new(Vector.new(position_raw[0], position_raw[1], include_z ? position_raw[2] : 0_f64), Vector.new(velocity_raw[0], velocity_raw[1], include_z ? velocity_raw[2] : 0_f64))
    end
    return hails
  end

  # --- Part One ---
  def problem1(records : Array(String), area = 200000000000000..400000000000000)
    hails = parse(records)

    result = 0
    hails.combinations(2).each do |pair|
      point = find_intersection(pair[0], pair[1])
      if point
        t0 = ((point.x - pair[0].origin.x) / pair[0].direction.x).round(3)
        t1 = ((point.x - pair[1].origin.x) / pair[1].direction.x).round(3)
        result += 1 if area.includes?(point.x) && area.includes?(point.y) && t0 >= 0 && t1 >= 0
      end
    end
    result
  end

  # --- Part Two ---
  # Solution: https://www.reddit.com/r/adventofcode/comments/18pnycy/comment/ket7ajw
  # Ported: https://topaz.github.io/paste/#XQAAAQAiAwAAAAAAAAAyGEruliPhOB7yEkDWpQGEhIKTgPt1VhI3lpjTdpJW+YZBOCt8Cg9+mD/1RcO1pETBAV3UPQvf9lTSSyLhWAIrcVAe74HQwGTELmgeQk/VwN+z6sL+7g/+siZXui+gFb3sOpG+jUPfvJf4BPLRqVREogM0jh5T/VdKNqq4DZpvrpRsC8HZh4MyHHnYQIHlNTl1jb7qEPvd/LA4UYwIkXWyi6s+vR1az/6Q8Qa283cF9Bf1EAHya2lfONQPr5hzT67G7c56dkd/GtqtpWMh6zgdgm+iq7xcilf63bQ0Rfd4KiHC2tY9yvDdq2tdKb9beOQOT12vDa+aAH9RBjsrhAlHBO8HW0FjdEHv9IQ1U/bjWys3ZqRj65fYZlow/0VKdzzvvVmTDNExDsH8SHX/6igy/0W9ZFzLmaxJBZeVZq3SNE1WmEsIT62B8dUksvNso+xjAr4F0GUgYk0kjT//pteowA==
  # NOTE: It works if there is no parallel rays.
  #       There is a problem how calculated division with big float and has epsilon of result +/- 1
  def problem2(records : Array(String), area = 200000000000000..400000000000000)
    data = Array(Array(Float64)).new
    records.each_with_index do |instruction, _|
      data << instruction.split(" @ ").flat_map { |sides| sides.split(",").map(&.to_f64) }
    end
    x, y = solve(*cols(data, 0, 1, 3, 4))
    z, *_ = solve(*cols(data, 1, 2, 4, 5))
    (x + y + z).round + 1
  end

  def cols(data, a, b, c, d)
    matrix_a = Array(Array(Float64)).new(data.size)
    vector_b = Array(Float64).new(data.size)
    data.each do |r|
      matrix_a << [r[c], -r[d], r[a], r[b]]
      vector_b << r[b] * r[c] - r[a] * r[d]
    end
    return matrix_a, vector_b
  end

  def solve(matrix_a, vector_b)
    m = matrix_a.zip(vector_b).map { |a, b| a + [b] }
    m = m[...4].map { |lane_a| lane_a.zip(m[4]).map { |a, b| a - b } }

    m.size.times do |i|
      m[i] = (0...m[i].size).map { |k| m[i][k]/m[i][i] }
      (i + 1).upto(m.size - 1) do |j|
        m[j] = (0...m[i].size).map { |k| m[j][k] - m[i][k]*m[j][i] }
      end
    end

    (m.size - 1).downto(0) do |i|
      i.times do |j|
        m[j] = (0...m[i].size).map { |k| m[j][k] - m[i][k]*m[j][i] }
      end
    end

    return m.map(&.last)
  end
end
