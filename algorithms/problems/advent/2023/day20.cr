# https://adventofcode.com/2023/day/20
#
# NOTE: For part1 find pattern when states repeats and part2 repeats of conj state to find with LCM
#
# --- Day 20: Pulse Propagation ---
#
# With your help, the Elves manage to find the right parts and fix all of the machines. Now, they just need to send the command to boot up the machines and get the sand flowing again.
#
# The machines are far apart and wired together with long cables. The cables don't connect to the machines directly, but rather to communication modules attached to the machines that perform various initialization tasks and also act as communication relays.
#
# Modules communicate using pulses. Each pulse is either a high pulse or a low pulse. When a module sends a pulse, it sends that type of pulse to each module in its list of destination modules.
#
# There are several different types of modules:
#
# Flip-flop modules (prefix %) are either on or off; they are initially off. If a flip-flop module receives a high pulse, it is ignored and nothing happens. However, if a flip-flop module receives a low pulse, it flips between on and off. If it was off, it turns on and sends a high pulse. If it was on, it turns off and sends a low pulse.
#
# Conjunction modules (prefix &) remember the type of the most recent pulse received from each of their connected input modules; they initially default to remembering a low pulse for each input. When a pulse is received, the conjunction module first updates its memory for that input. Then, if it remembers high pulses for all inputs, it sends a low pulse; otherwise, it sends a high pulse.
#
# There is a single broadcast module (named broadcaster). When it receives a pulse, it sends the same pulse to all of its destination modules.
#
# Here at Desert Machine Headquarters, there is a module with a single button on it called, aptly, the button module. When you push the button, a single low pulse is sent directly to the broadcaster module.
#
# After pushing the button, you must wait until all pulses have been delivered and fully handled before pushing it again. Never push the button if modules are still processing pulses.
#
# Pulses are always processed in the order they are sent. So, if a pulse is sent to modules a, b, and c, and then module a processes its pulse and sends more pulses, the pulses sent to modules b and c would have to be handled first.
#
# The module configuration (your puzzle input) lists each module. The name of the module is preceded by a symbol identifying its type, if any. The name is then followed by an arrow and a list of its destination modules. For example:
#
# broadcaster -> a, b, c
# %a -> b
# %b -> c
# %c -> inv
# &inv -> a
#
# In this module configuration, the broadcaster has three destination modules named a, b, and c. Each of these modules is a flip-flop module (as indicated by the % prefix). a outputs to b which outputs to c which outputs to another module named inv. inv is a conjunction module (as indicated by the & prefix) which, because it has only one input, acts like an inverter (it sends the opposite of the pulse type it receives); it outputs to a.
#
# By pushing the button once, the following pulses are sent:
#
# button -low-> broadcaster
# broadcaster -low-> a
# broadcaster -low-> b
# broadcaster -low-> c
# a -high-> b
# b -high-> c
# c -high-> inv
# inv -low-> a
# a -low-> b
# b -low-> c
# c -low-> inv
# inv -high-> a
#
# After this sequence, the flip-flop modules all end up off, so pushing the button again repeats the same sequence.
#
# Here's a more interesting example:
#
# broadcaster -> a
# %a -> inv, con
# &inv -> b
# %b -> con
# &con -> output
#
# This module configuration includes the broadcaster, two flip-flops (named a and b), a single-input conjunction module (inv), a multi-input conjunction module (con), and an untyped module named output (for testing purposes). The multi-input conjunction module con watches the two flip-flop modules and, if they're both on, sends a low pulse to the output module.
#
# Here's what happens if you push the button once:
#
# button -low-> broadcaster
# broadcaster -low-> a
# a -high-> inv
# a -high-> con
# inv -low-> b
# con -high-> output
# b -high-> con
# con -low-> output
#
# Both flip-flops turn on and a low pulse is sent to output! However, now that both flip-flops are on and con remembers a high pulse from each of its two inputs, pushing the button a second time does something different:
#
# button -low-> broadcaster
# broadcaster -low-> a
# a -low-> inv
# a -low-> con
# inv -high-> b
# con -high-> output
#
# Flip-flop a turns off! Now, con remembers a low pulse from module a, and so it sends only a high pulse to output.
#
# Push the button a third time:
#
# button -low-> broadcaster
# broadcaster -low-> a
# a -high-> inv
# a -high-> con
# inv -low-> b
# con -low-> output
# b -low-> con
# con -high-> output
#
# This time, flip-flop a turns on, then flip-flop b turns off. However, before b can turn off, the pulse sent to con is handled first, so it briefly remembers all high pulses for its inputs and sends a low pulse to output. After that, flip-flop b turns off, which causes con to update its state and send a high pulse to output.
#
# Finally, with a on and b off, push the button a fourth time:
#
# button -low-> broadcaster
# broadcaster -low-> a
# a -low-> inv
# a -low-> con
# inv -high-> b
# con -high-> output
#
# This completes the cycle: a turns off, causing con to remember only low pulses and restoring all modules to their original states.
#
# To get the cables warmed up, the Elves have pushed the button 1000 times. How many pulses got sent as a result (including the pulses sent by the button itself)?
#
# In the first example, the same thing happens every time the button is pushed: 8 low pulses and 4 high pulses are sent. So, after pushing the button 1000 times, 8000 low pulses and 4000 high pulses are sent. Multiplying these together gives 32000000.
#
# In the second example, after pushing the button 1000 times, 4250 low pulses and 2750 high pulses are sent. Multiplying these together gives 11687500.
#
# Consult your module configuration; determine the number of low pulses and high pulses that would be sent after pushing the button 1000 times, waiting for all pulses to be fully handled after each push of the button. What do you get if you multiply the total number of low pulses sent by the total number of high pulses sent?
#
# Your puzzle answer was 886347020.
#
# Output:
# Answer: 886347020
# (execute time: 76.636416 ms, memory: 13336560 bytes)
#
# --- Part Two ---
#
# The final machine responsible for moving the sand down to Island Island has a module attached named rx. The machine turns on when a single low pulse is sent to rx.
#
# Reset all modules to their default states. Waiting for all pulses to be fully handled after each button press, what is the fewest number of button presses required to deliver a single low pulse to the module named rx?
#
# Your puzzle answer was 233283622908263.
#
# Output:
# Answer: 233283622908263
# (execute time: 273.327541 ms, memory: 51844704 bytes)

require "../utils.cr"

module Day20
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    curcuit = parse(records)
    push_button(curcuit)
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    curcuit = parse(records)
    push_button2(curcuit)
  end

  def push_button(curcuit, times = 1000)
    pulses = [0, 0]

    # Initialize Flip Flop states
    flip_flops_state = Hash(String, Int32).new
    curcuit.each do |node, c|
      flip_flops_state[node] = 0 if c[0] == '%'
    end

    # Remember the type of the most recent pulse received from each of their connected input modules
    # they initially default to remembering a low pulse for each input.
    conjunctions = Set(String).new
    curcuit.each do |node, c|
      conjunctions.add(node) if c[0] == '&'
    end

    conjunction_state = Hash(String, Hash(String, Int32)).new
    curcuit.each do |node, c|
      common = c[1] & conjunctions
      common.each do |conj|
        conjunction_state[conj] ||= Hash(String, Int32).new
        conjunction_state[conj][node] = 0
      end
    end

    queue : Array(Tuple(String, Int32, String)) = Array(Tuple(String, Int32, String)).new

    times.times do |i|
      # Recipient and signal - "a single low pulse is sent directly to the broadcaster module"
      queue = [{"button", 0, "broadcaster"}]
      while queue.size > 0
        new_queue = typeof(queue).new
        queue.each do |(src, signal, dst)|
          # puts "#{src} -#{signal == 0 ? "low" : "high"}-> #{dst}"
          pulses[signal] += 1
          next if !curcuit.has_key?(dst)
          type, dsts = curcuit[dst]
          case type
          when '%' # FlipFlop
            if signal == 0
              flip_flops_state[dst] = flip_flops_state[dst] ^ 1
              new_queue += dsts.map { |next_dst| {dst, flip_flops_state[dst], next_dst} }
            end
          when '&' # Conjunction
            conjunction_state[dst][src] = signal
            new_signal = conjunction_state[dst].all? { |_, s| s == 1 } ? 0 : 1
            new_queue += dsts.map { |next_dst| {dst, new_signal, next_dst} }
          else # Broadcast
            new_queue += dsts.map { |next_dst| {dst, signal, next_dst} }
          end
        end
        queue = new_queue
      end

      if flip_flops_state.values.uniq! == [0] && conjunction_state.values.flat_map(&.values).uniq! == [0]
        r = times / (i + 1)
        pulses = pulses.map { |s| s * r }
        break
      end
    end

    pulses.product
  end

  def push_button2(curcuit, times = 1000)
    # Initialize Flip Flop states
    flip_flops_state = Hash(String, Int32).new
    curcuit.each do |node, c|
      flip_flops_state[node] = 0 if c[0] == '%'
    end

    # Remember the type of the most recent pulse received from each of their connected input modules
    # they initially default to remembering a low pulse for each input.
    conjunctions = Set(String).new
    curcuit.each do |node, c|
      conjunctions.add(node) if c[0] == '&'
    end

    conjunction_state = Hash(String, Hash(String, Int32)).new
    curcuit.each do |node, c|
      common = c[1] & conjunctions
      common.each do |conj|
        conjunction_state[conj] ||= Hash(String, Int32).new
        conjunction_state[conj][node] = 0
      end
    end

    # Identified conjunction from my input
    conjunction_state_lcm = conjunction_state["gf"].dup

    queue : Array(Tuple(String, Int32, String)) = Array(Tuple(String, Int32, String)).new

    (1..).each do |i|
      # Recipient and signal - "a single low pulse is sent directly to the broadcaster module"
      queue = [{"button", 0, "broadcaster"}]
      while queue.size > 0
        new_queue = typeof(queue).new
        queue.each do |(src, signal, dst)|
          # puts "#{src} -#{signal == 0 ? "low" : "high"}-> #{dst}"
          return i if signal == 0 if dst == "rx"
          next if !curcuit.has_key?(dst)
          type, dsts = curcuit[dst]
          case type
          when '%' # FlipFlop
            if signal == 0
              flip_flops_state[dst] = flip_flops_state[dst] ^ 1
              new_queue += dsts.map { |next_dst| {dst, flip_flops_state[dst], next_dst} }
            end
          when '&' # Conjunction
            conjunction_state[dst][src] = signal
            new_signal = conjunction_state[dst].all? { |_, s| s == 1 } ? 0 : 1
            new_queue += dsts.map { |next_dst| {dst, new_signal, next_dst} }

            if dst == "gf" && signal == 1
              conjunction_state_lcm[src] = i if conjunction_state_lcm[src] == 0
              if !conjunction_state_lcm.values.includes?(0)
                return conjunction_state_lcm.values.reduce(1_i64) { |x, y| x.lcm(y) }
              end
            end
          else # Broadcast
            new_queue += dsts.map { |next_dst| {dst, signal, next_dst} }
          end
        end
        queue = new_queue
      end
    end
    -1
  end

  def parse(records)
    curcuit = Hash(String, Tuple(Char, Set(String))).new
    curcuit["button"] = {'.', Set{"broadcaster"}}
    records.each do |instruction|
      src_raw, dsts_raw = instruction.split(" -> ")
      type = src_raw[0]
      src = type.in?(['%', '&']) ? src_raw[1..] : src_raw
      dsts = Set.new dsts_raw.split(", ")
      curcuit[src] = {type, dsts}
    end
    curcuit
  end
end
