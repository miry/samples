# https://adventofcode.com/2023/day/14
#
# --- Day 14: Parabolic Reflector Dish ---
#
# You reach the place where all of the mirrors were pointing: a massive parabolic reflector dish attached to the side of another large mountain.
#
# The dish is made up of many small mirrors, but while the mirrors themselves are roughly in the shape of a parabolic reflector dish, each individual mirror seems to be pointing in slightly the wrong direction. If the dish is meant to focus light, all it's doing right now is sending it in a vague direction.
#
# This system must be what provides the energy for the lava! If you focus the reflector dish, maybe you can go where it's pointing and use the light to fix the lava production.
#
# Upon closer inspection, the individual mirrors each appear to be connected via an elaborate system of ropes and pulleys to a large metal platform below the dish. The platform is covered in large rocks of various shapes. Depending on their position, the weight of the rocks deforms the platform, and the shape of the platform controls which ropes move and ultimately the focus of the dish.
#
# In short: if you move the rocks, you can focus the dish. The platform even has a control panel on the side that lets you tilt it in one of four directions! The rounded rocks (O) will roll when the platform is tilted, while the cube-shaped rocks (#) will stay in place. You note the positions of all of the empty spaces (.) and rocks (your puzzle input). For example:
#
# O....#....
# O.OO#....#
# .....##...
# OO.#O....O
# .O.....O#.
# O.#..O.#.#
# ..O..#O..O
# .......O..
# #....###..
# #OO..#....
#
# Start by tilting the lever so all of the rocks will slide north as far as they will go:
#
# OOOO.#.O..
# OO..#....#
# OO..O##..O
# O..#.OO...
# ........#.
# ..#....#.#
# ..O..#.O.O
# ..O.......
# #....###..
# #....#....
#
# You notice that the support beams along the north side of the platform are damaged; to ensure the platform doesn't collapse, you should calculate the total load on the north support beams.
#
# The amount of load caused by a single rounded rock (O) is equal to the number of rows from the rock to the south edge of the platform, including the row the rock is on. (Cube-shaped rocks (#) don't contribute to load.) So, the amount of load caused by each rock in each row is as follows:
#
# OOOO.#.O.. 10
# OO..#....#  9
# OO..O##..O  8
# O..#.OO...  7
# ........#.  6
# ..#....#.#  5
# ..O..#.O.O  4
# ..O.......  3
# #....###..  2
# #....#....  1
#
# The total load is the sum of the load caused by all of the rounded rocks. In this example, the total load is 136.
#
# Tilt the platform so that the rounded rocks all roll north. Afterward, what is the total load on the north support beams?
#
# Your puzzle answer was 107430.
#
# Output:
# Answer: 107430
# (execute time: 41.565292 ms, memory: 2292256 bytes)
#
# --- Part Two ---
#
# The parabolic reflector dish deforms, but not in a way that focuses the beam. To do that, you'll need to move the rocks to the edges of the platform. Fortunately, a button on the side of the control panel labeled "spin cycle" attempts to do just that!
#
# Each cycle tilts the platform four times so that the rounded rocks roll north, then west, then south, then east. After each tilt, the rounded rocks roll as far as they can before the platform tilts in the next direction. After one cycle, the platform will have finished rolling the rounded rocks in those four directions in that order.
#
# Here's what happens in the example above after each of the first few cycles:
#
# After 1 cycle:
# .....#....
# ....#...O#
# ...OO##...
# .OO#......
# .....OOO#.
# .O#...O#.#
# ....O#....
# ......OOOO
# #...O###..
# #..OO#....
#
# After 2 cycles:
# .....#....
# ....#...O#
# .....##...
# ..O#......
# .....OOO#.
# .O#...O#.#
# ....O#...O
# .......OOO
# #..OO###..
# #.OOO#...O
#
# After 3 cycles:
# .....#....
# ....#...O#
# .....##...
# ..O#......
# .....OOO#.
# .O#...O#.#
# ....O#...O
# .......OOO
# #...O###.O
# #.OOO#...O
#
# This process should work if you leave it running long enough, but you're still worried about the north support beams. To make sure they'll survive for a while, you need to calculate the total load on the north support beams after 1000000000 cycles.
#
# In the above example, after 1000000000 cycles, the total load on the north support beams is 64.
#
# Run the spin cycle for 1000000000 cycles. Afterward, what is the total load on the north support beams?
#
# Your puzzle answer was 96317.
#
# Output:
# Answer: 96317
# (execute time: 24365.116417 ms, memory: 1030717520 bytes)

require "../utils.cr"

module Day14
  extend self

  def parse(records)
    plan = Hash(Coord, Char).new
    records.each_with_index do |instruction, y|
      instruction.chars.each_with_index do |c, x|
        plan[{x: x, y: y}] = c if c != '.'
      end
    end
    return {plan, records.size, records.first.size}
  end

  # Move rocks to North
  def tilt_rocks!(plan, height, width, direction, cache = Hash({Hash(Coord, Char), Tuple(Int32, Int32)}, Hash(Coord, Char)).new)
    cache_key = {plan, direction}
    return cache[cache_key] if cache.has_key?(cache_key)

    new_plan = Hash(Coord, Char).new
    tilted = false

    loop do
      tilted = false
      new_plan = Hash(Coord, Char).new
      plan.each do |pos, c|
        if c == 'O'
          npos = {x: pos[:x] + direction[0], y: pos[:y] + direction[1]}
          cur = npos
          while !plan.has_key?(npos) && (0...height).includes?(npos[:y]) && (0...width).includes?(npos[:x])
            cur = npos
            npos = {x: npos[:x] + direction[0], y: npos[:y] + direction[1]}
          end

          npos = cur
          if !plan.has_key?(npos) && (0...height).includes?(npos[:y]) && (0...width).includes?(npos[:x])
            new_plan[npos] = 'O'
            tilted = true
          else
            new_plan[pos] = 'O'
          end
        else
          new_plan[pos] = c
        end
      end
      plan = new_plan
      break if !tilted
    end

    cache[cache_key] = plan
    plan
  end

  # --- Part One ---
  def problem1(records : Array(String))
    plan, height, width = parse(records)
    plan = tilt_rocks!(plan, height, width, {0, -1})
    score(plan, height)
  end

  def score(plan, height)
    result = 0
    plan.each do |pos, c|
      next if c != 'O'
      result += height - pos[:y]
    end
    result
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    plan, height, width = parse(records)
    known_patterns = Hash(Array(Coord), Int32).new
    cache = Hash({Hash(Coord, Char), Tuple(Int32, Int32)}, Hash(Coord, Char)).new
    i = 0
    loop_identified = false

    directions = StaticArray[
      {0, -1},
      {-1, 0},
      {0, 1},
      {1, 0},
    ]

    while i < 1000000000
      directions.each do |d|
        plan = tilt_rocks!(plan, height, width, d, cache)
      end

      if !loop_identified
        k = plan.keys
        if known_patterns.has_key?(k)
          loop_identified = true
          diff = i - known_patterns[k]
          while i < 1000000000
            i += diff
          end
          i -= diff
        else
          known_patterns[k] = i
        end
      end

      i += 1
    end

    score(plan, height)
  end
end
