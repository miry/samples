require "spec"
require "./day14"

describe Day14 do
  sample = <<-EOF.split("\n")
  O....#....
  O.OO#....#
  .....##...
  OO.#O....O
  .O.....O#.
  O.#..O.#.#
  ..O..#O..O
  .......O..
  #....###..
  #OO..#....
  EOF

  describe "problem1" do
    it "sample" do
      actual = Day14.problem1(sample)
      actual.should eq(136)
    end
  end

  describe "problem2" do
    it "sample" do
      actual = Day14.problem2(sample)
      actual.should eq(64)
    end
  end
end
