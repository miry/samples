require "spec"
require "./day03"

describe Day3 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      467..114..
      ...*......
      ..35..633.
      ......#...
      617*......
      .....+.58.
      ..592.....
      ......755.
      ...$.*....
      .664.598..
      EOF
      actual = Day3.problem1(input.split("\n"))
      actual.should eq(4361)
    end

    it "covers case with last digits" do
      input = <<-EOF
      467..114.
      ...*.....
      ..35..633
      ......#..
      617*.....
      .....+.58
      ..592....
      ......755
      ...$.*...
      .664.598.
      EOF
      actual = Day3.problem1(input.split("\n"))
      actual.should eq(4361)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      467..114..
      ...*......
      ..35..633.
      ......#...
      617*......
      .....+.58.
      ..592.....
      ......755.
      ...$.*....
      .664.598..
      EOF
      actual = Day3.problem2(input.split("\n"))
      actual.should eq(467835)
    end
  end
end
