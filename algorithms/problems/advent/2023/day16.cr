# https://adventofcode.com/2023/day/16
#
# --- Day 16: The Floor Will Be Lava ---
#
# With the beam of light completely focused somewhere, the reindeer leads you deeper still into the Lava Production Facility. At some point, you realize that the steel facility walls have been replaced with cave, and the doorways are just cave, and the floor is cave, and you're pretty sure this is actually just a giant cave.
#
# Finally, as you approach what must be the heart of the mountain, you see a bright light in a cavern up ahead. There, you discover that the beam of light you so carefully focused is emerging from the cavern wall closest to the facility and pouring all of its energy into a contraption on the opposite side.
#
# Upon closer inspection, the contraption appears to be a flat, two-dimensional square grid containing empty space (.), mirrors (/ and \), and splitters (| and -).
#
# The contraption is aligned so that most of the beam bounces around the grid, but each tile on the grid converts some of the beam's light into heat to melt the rock in the cavern.
#
# You note the layout of the contraption (your puzzle input). For example:
#
# .|...\....
# |.-.\.....
# .....|-...
# ........|.
# ..........
# .........\
# ..../.\\..
# .-.-/..|..
# .|....-|.\
# ..//.|....
#
# The beam enters in the top-left corner from the left and heading to the right. Then, its behavior depends on what it encounters as it moves:
#
#     If the beam encounters empty space (.), it continues in the same direction.
#     If the beam encounters a mirror (/ or \), the beam is reflected 90 degrees depending on the angle of the mirror. For instance, a rightward-moving beam that encounters a / mirror would continue upward in the mirror's column, while a rightward-moving beam that encounters a \ mirror would continue downward from the mirror's column.
#     If the beam encounters the pointy end of a splitter (| or -), the beam passes through the splitter as if the splitter were empty space. For instance, a rightward-moving beam that encounters a - splitter would continue in the same direction.
#     If the beam encounters the flat side of a splitter (| or -), the beam is split into two beams going in each of the two directions the splitter's pointy ends are pointing. For instance, a rightward-moving beam that encounters a | splitter would split into two beams: one that continues upward from the splitter's column and one that continues downward from the splitter's column.
#
# Beams do not interact with other beams; a tile can have many beams passing through it at the same time. A tile is energized if that tile has at least one beam pass through it, reflect in it, or split in it.
#
# In the above example, here is how the beam of light bounces around the contraption:
#
# >|<<<\....
# |v-.\^....
# .v...|->>>
# .v...v^.|.
# .v...v^...
# .v...v^..\
# .v../2\\..
# <->-/vv|..
# .|<<<2-|.\
# .v//.|.v..
#
# Beams are only shown on empty tiles; arrows indicate the direction of the beams. If a tile contains beams moving in multiple directions, the number of distinct directions is shown instead. Here is the same diagram but instead only showing whether a tile is energized (#) or not (.):
#
# ######....
# .#...#....
# .#...#####
# .#...##...
# .#...##...
# .#...##...
# .#..####..
# ########..
# .#######..
# .#...#.#..
#
# Ultimately, in this example, 46 tiles become energized.
#
# The light isn't energizing enough tiles to produce lava; to debug the contraption, you need to start by analyzing the current situation. With the beam starting in the top-left heading right, how many tiles end up being energized?
#
# Your puzzle answer was 8551.
#
# Output:
# Answer: 8551
# (execute time: 39.769708 ms, memory: 2880256 bytes)
#
# --- Part Two ---
#
# As you try to work out what might be wrong, the reindeer tugs on your shirt and leads you to a nearby control panel. There, a collection of buttons lets you align the contraption so that the beam enters from any edge tile and heading away from that edge. (You can choose either of two directions for the beam if it starts on a corner; for instance, if the beam starts in the bottom-right corner, it can start heading either left or upward.)
#
# So, the beam could start on any tile in the top row (heading downward), any tile in the bottom row (heading upward), any tile in the leftmost column (heading right), or any tile in the rightmost column (heading left). To produce lava, you need to find the configuration that energizes as many tiles as possible.
#
# In the above example, this can be achieved by starting the beam in the fourth tile from the left in the top row:
#
# .|<2<\....
# |v-v\^....
# .v.v.|->>>
# .v.v.v^.|.
# .v.v.v^...
# .v.v.v^..\
# .v.v/2\\..
# <-2-/vv|..
# .|<<<2-|.\
# .v//.|.v..
#
# Using this configuration, 51 tiles are energized:
#
# .#####....
# .#.#.#....
# .#.#.#####
# .#.#.##...
# .#.#.##...
# .#.#.##...
# .#.#####..
# ########..
# .#######..
# .#...#.#..
#
# Find the initial beam configuration that energizes the largest number of tiles; how many tiles are energized in that configuration?
#
# Your puzzle answer was 8754.
#
# Output:
# Answer: 8754
# (execute time: 9788.436375 ms, memory: 577674176 bytes)

require "../utils.cr"

module Day16
  extend self

  DIRECTIONS = {
    '>' => {1, 0},
    'v' => {0, 1},
    '^' => {0, -1},
    '<' => {-1, 0},
  }
  CHANGE_DIRECTIONS_SLASH     = {'>' => 'v', '<' => '^', '^' => '<', 'v' => '>'}
  CHANGE_DIRECTIONS_BACKSLASH = {'>' => '^', '<' => 'v', '^' => '>', 'v' => '<'}
  LEFT_RIGTH                  = ['<', '>']

  # --- Part One ---
  def problem1(records : Array(String))
    plan, height, width = parse(records)
    # print_plan(plan, height, width)

    start = {pos: {x: -1, y: 0}, dir: '>'}
    route(plan, start).size
  end

  def route(plan, start)
    plan[start[:pos]] = '>'

    cur : Set(typeof(start)) = Set(typeof(start)).new
    cur.add(start)

    # visited required to avoid loops in case beam is going return to the same location
    visited = Set(typeof(start)).new

    # store energy nodes
    result = Set(Coord).new

    # Pre Initialize memory with only single object for loop variables
    new_beams : Set(typeof(start)) = Set(typeof(start)).new
    dir_diff = {0, 0}
    next_position = {x: 0, y: 0}

    while cur.size > 0
      new_beams = Set(typeof(start)).new
      cur.each do |beam|
        # Avoid loops
        next if visited.includes?(beam)
        visited.add(beam)
        result.add(beam[:pos])

        dir_diff = DIRECTIONS[beam[:dir]]
        next_position = {x: beam[:pos][:x] + dir_diff[0], y: beam[:pos][:y] + dir_diff[1]}
        next if !plan.has_key?(next_position)

        case plan[next_position]
        when '.'
          new_beams.add({pos: next_position, dir: beam[:dir]})
        when '|'
          if beam[:dir].in?(LEFT_RIGTH)
            new_beams.add({pos: next_position, dir: '^'})
            new_beams.add({pos: next_position, dir: 'v'})
          else
            new_beams.add({pos: next_position, dir: beam[:dir]})
          end
        when '-'
          if beam[:dir].in?(LEFT_RIGTH)
            new_beams.add({pos: next_position, dir: beam[:dir]})
          else
            new_beams.add({pos: next_position, dir: '<'})
            new_beams.add({pos: next_position, dir: '>'})
          end
        when '/'
          next_dir = CHANGE_DIRECTIONS_BACKSLASH[beam[:dir]]
          new_beams.add({pos: next_position, dir: next_dir})
        when '\\'
          next_dir = CHANGE_DIRECTIONS_SLASH[beam[:dir]]
          new_beams.add({pos: next_position, dir: next_dir})
        end
      end
      cur = new_beams
    end

    # Start point is not part of the map and should be removed
    result.delete(start[:pos])
    plan.delete(start[:pos])
    result
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    plan, height, width = parse(records)
    # print_plan(plan, height, width)

    max = 0
    height.times do |y|
      start = {pos: {x: -1, y: y}, dir: '>'}
      max = [max, route(plan, start).size].max
      start = {pos: {x: width, y: y}, dir: '<'}
      max = [max, route(plan, start).size].max
    end

    width.times do |x|
      start = {pos: {x: x, y: -1}, dir: 'v'}
      max = [max, route(plan, start).size].max
      start = {pos: {x: x, y: height}, dir: '^'}
      max = [max, route(plan, start).size].max
    end

    max
  end

  def parse(records)
    plan = Hash(Coord, Char).new
    records.each_with_index do |instruction, y|
      instruction.chars.each_with_index do |c, x|
        plan[{x: x, y: y}] = c
      end
    end
    return {plan, records.size, records.first.size}
  end
end
