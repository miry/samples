# https://adventofcode.com/2023/day/17
#
# https://advent-of-code.xavd.id/writeups/2023/day/17/
# Dijkstra vs A*
#
# --- Day 17: Clumsy Crucible ---
#
# The lava starts flowing rapidly once the Lava Production Facility is operational. As you leave, the reindeer offers you a parachute, allowing you to quickly reach Gear Island.
#
# As you descend, your bird's-eye view of Gear Island reveals why you had trouble finding anyone on your way up: half of Gear Island is empty, but the half below you is a giant factory city!
#
# You land near the gradually-filling pool of lava at the base of your new lavafall. Lavaducts will eventually carry the lava throughout the city, but to make use of it immediately, Elves are loading it into large crucibles on wheels.
#
# The crucibles are top-heavy and pushed by hand. Unfortunately, the crucibles become very difficult to steer at high speeds, and so it can be hard to go in a straight line for very long.
#
# To get Desert Island the machine parts it needs as soon as possible, you'll need to find the best way to get the crucible from the lava pool to the machine parts factory. To do this, you need to minimize heat loss while choosing a route that doesn't require the crucible to go in a straight line for too long.
#
# Fortunately, the Elves here have a map (your puzzle input) that uses traffic patterns, ambient temperature, and hundreds of other parameters to calculate exactly how much heat loss can be expected for a crucible entering any particular city block.
#
# For example:
#
# 2413432311323
# 3215453535623
# 3255245654254
# 3446585845452
# 4546657867536
# 1438598798454
# 4457876987766
# 3637877979653
# 4654967986887
# 4564679986453
# 1224686865563
# 2546548887735
# 4322674655533
#
# Each city block is marked by a single digit that represents the amount of heat loss if the crucible enters that block. The starting point, the lava pool, is the top-left city block; the destination, the machine parts factory, is the bottom-right city block. (Because you already start in the top-left block, you don't incur that block's heat loss unless you leave that block and then return to it.)
#
# Because it is difficult to keep the top-heavy crucible going in a straight line for very long, it can move at most three blocks in a single direction before it must turn 90 degrees left or right. The crucible also can't reverse direction; after entering each city block, it may only turn left, continue straight, or turn right.
#
# One way to minimize heat loss is this path:
#
# 2>>34^>>>1323
# 32v>>>35v5623
# 32552456v>>54
# 3446585845v52
# 4546657867v>6
# 14385987984v4
# 44578769877v6
# 36378779796v>
# 465496798688v
# 456467998645v
# 12246868655<v
# 25465488877v5
# 43226746555v>
#
# This path never moves more than three consecutive blocks in the same direction and incurs a heat loss of only 102.
#
# Directing the crucible from the lava pool to the machine parts factory, but not moving more than three consecutive blocks in the same direction, what is the least heat loss it can incur?
#
# Your puzzle answer was 686.
# --- Part Two ---
#
# The crucibles of lava simply aren't large enough to provide an adequate supply of lava to the machine parts factory. Instead, the Elves are going to upgrade to ultra crucibles.
#
# Ultra crucibles are even more difficult to steer than normal crucibles. Not only do they have trouble going in a straight line, but they also have trouble turning!
#
# Once an ultra crucible starts moving in a direction, it needs to move a minimum of four blocks in that direction before it can turn (or even before it can stop at the end). However, it will eventually start to get wobbly: an ultra crucible can move a maximum of ten consecutive blocks without turning.
#
# In the above example, an ultra crucible could follow this path to minimize heat loss:
#
# 2>>>>>>>>1323
# 32154535v5623
# 32552456v4254
# 34465858v5452
# 45466578v>>>>
# 143859879845v
# 445787698776v
# 363787797965v
# 465496798688v
# 456467998645v
# 122468686556v
# 254654888773v
# 432267465553v
#
# In the above example, an ultra crucible would incur the minimum possible heat loss of 94.
#
# Here's another example:
#
# 111111111111
# 999999999991
# 999999999991
# 999999999991
# 999999999991
#
# Sadly, an ultra crucible would need to take an unfortunate path like this one:
#
# 1>>>>>>>1111
# 9999999v9991
# 9999999v9991
# 9999999v9991
# 9999999v>>>>
#
# This route causes the ultra crucible to incur the minimum possible heat loss of 71.
#
# Directing the ultra crucible from the lava pool to the machine parts factory, what is the least heat loss it can incur?
#
# Your puzzle answer was 801.
#
# Output:
#
# --- Day 17 ---
# --- Part One ---
# Answer: 686
# (execute time: 720.000292 ms, memory: 58379792 bytes)
# --- Day 17: ---
# --- Part Two ---
# Answer: 801
# (execute time: 1742.934917 ms, memory: 67986720 bytes)

require "../utils.cr"

module Day17
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    plan, height, width = parse(records)
    # print_plan(plan, height, width)

    travel(plan, height, width, 1, 3)
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    plan, height, width = parse(records)
    # print_plan(plan, height, width)

    travel(plan, height, width, 4, 10)
  end

  def parse(records) : Tuple(Hash(Coord, Char), Int32, Int32)
    plan = Hash(Coord, Char).new
    records.each_with_index do |instruction, y|
      instruction.chars.each_with_index do |c, x|
        plan[{x: x, y: y}] = c
      end
    end
    return {plan, records.size, records.first.size}
  end

  def travel(plan : Hash(Coord, Char), height, width, min_size, max_size)
    neighbours = {
      'v' => {1, 0},
      '>' => {0, 1},
      '^' => {-1, 0},
      '<' => {0, -1},
    }

    start = {x: 0, y: 0}
    target_pos = {x: width - 1, y: height - 1}
    min_distance_to = Hash(Tuple(Coord, Char), Int32).new(Int32::MAX)

    # queue contains current processing node, came direction, so far the distance and estimated distance to target (priority).
    queue = Day17::PriorityQueue(NamedTuple(node: Coord, direction: Char, distance: Int32, priority: Int32)).new(initial_capacity: 9000) { |a, b| a[:priority] < b[:priority] }
    queue.insert({node: start, direction: '.', distance: 0, priority: target_pos[:x] + target_pos[:y]})

    result = Hash(String, Int32).new
    while queue.size > 0
      route = queue.pop
      node_pos, dir, distance = route[:node], route[:direction], route[:distance]

      return distance if node_pos == target_pos
      next if distance > min_distance_to[{node_pos, dir}]

      neighbours.each do |next_dir, delta|
        next if next_dir == dir
        directions = [dir, next_dir].sort!
        next if directions.in?([['<', '>'], ['^', 'v']])

        local_distance = distance
        (1..max_size).map do |i|
          pos = {x: node_pos[:x] + i * delta[1], y: node_pos[:y] + i * delta[0]}
          break if !plan.has_key?(pos)
          local_distance += plan[pos].to_i
          if i >= min_size && local_distance < min_distance_to[{pos, next_dir}]
            min_distance_to[{pos, next_dir}] = local_distance
            queue.insert({
              node:      pos,
              direction: next_dir,
              distance:  local_distance,
              priority:  local_distance + target_pos[:x] - pos[:x] + target_pos[:y] - pos[:y] + 2,
            })
          end
        end
      end
    end
  end

  # PriorityQueue implementation with ordered array.
  class PriorityQueue(T)
    def initialize(initial_capacity : UInt32, &@compare_fn : Proc(T, T, Bool))
      @store = Array(T).new(initial_capacity)
    end

    def initialize(&@compare_fn : Proc(T, T, Bool))
      @store = Array(T).new(16)
    end

    def initialize
      @compare_fn = ->(x : T, y : T) { x < y }
      @store = Array(T).new(16)
    end

    def empty?
      @store.size == 0
    end

    def insert(x : T)
      indx = search(x) || @store.size
      @store.insert(indx, x)
    end

    def pop
      @store.pop
    end

    def size
      @store.size
    end

    private def search(val)
      @store.bsearch_index { |x, _| @compare_fn.call(x, val) }
    end
  end
end
