require "spec"
require "benchmark"

require "./day01"

describe Day1 do
  describe ".first_digit" do
    it "gets nothing" do
      expect_raises(Exception) { Day1.first_digit("oops") }
    end

    it "gets digit from first position" do
      actual = Day1.first_digit("123")
      actual.should eq(1)
    end

    it "gets digit from middle position" do
      actual = Day1.first_digit("foo323")
      actual.should eq(3)
    end

    it "gets digit from last position" do
      actual = Day1.first_digit("foo9")
      actual.should eq(9)
    end

    it "sample" do
      cases = [
        {in: "1abc2", out: 1},
        {in: "pqr3stu8vwx", out: 3},
        {in: "a1b2c3d4e5f", out: 1},
        {in: "treb7uchet", out: 7},
      ]
      cases.each do |example|
        actual = Day1.first_digit(example[:in])
        actual.should eq(example[:out])
      end
    end

    it "search from end of string" do
      cases = [
        {in: "1abc2", out: 2},
        {in: "pqr3stu8vwx", out: 8},
        {in: "a1b2c3d4e5f", out: 5},
        {in: "treb7uchet", out: 7},
      ]
      cases.each do |example|
        actual = Day1.first_digit(example[:in], last: true)
        actual.should eq(example[:out])
      end
    end
  end

  describe ".get_digit_from_mix" do
    it "raises exception" do
      expect_raises(Exception) { Day1.get_digit_from_mix("foo") }
    end

    it "identifies first word from left" do
      actual = Day1.get_digit_from_mix("onetwothree")
      actual.should eq(1)
    end

    it "identifies last word" do
      actual = Day1.get_digit_from_mix("onetwothree", last: true)
      actual.should eq(3)
    end

    it "eighthree" do
      Day1.get_digit_from_mix("eighthree").should eq(8)
      Day1.get_digit_from_mix("eighthree", last: true).should eq(3)
    end
  end

  describe "problem1" do
    it "sample" do
      input = <<-EOF
      1abc2
      pqr3stu8vwx
      a1b2c3d4e5f
      treb7uchet
      EOF
      actual = Day1.problem1(input.split("\n"))
      actual.should eq(142)
    end

    it "short" do
      input = <<-EOF
      1abc2
      pqr3stu8vwx
      a1b2c3d4e5f
      treb7uchet
      EOF
      actual = Day1.problem1_short(input.split("\n"))
      actual.should eq(142)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      two1nine
      eightwothree
      abcone2threexyz
      xtwone3four
      4nineeightseven2
      zoneight234
      7pqrstsixteen
      EOF
      actual = Day1.problem2(input.split("\n"))
      actual.should eq(281)
    end

    it "sample short" do
      input = <<-EOF
      two1nine
      eightwothree
      abcone2threexyz
      xtwone3four
      4nineeightseven2
      zoneight234
      7pqrstsixteen
      EOF
      actual = Day1.problem2_short(input.split("\n"))
      actual.should eq(281)
    end

    it "double" do
      input = <<-EOF
      8kqzkcb
      EOF
      actual = Day1.problem2(input.split("\n"))
      actual.should eq(88)
    end

    it "eighthree" do
      input = <<-EOF
      eighthree
      eightwo
      EOF
      actual = Day1.problem2(input.split("\n"))
      actual.should eq(83 + 82)
    end
  end
end
