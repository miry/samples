# https://adventofcode.com/2023/day/22
#
# --- Day 22: Sand Slabs ---
#
# Enough sand has fallen; it can finally filter water for Snow Island.
#
# Well, almost.
#
# The sand has been falling as large compacted bricks of sand, piling up to form an impressive stack here near the edge of Island Island. In order to make use of the sand to filter water, some of the bricks will need to be broken apart - nay, disintegrated - back into freely flowing sand.
#
# The stack is tall enough that you'll have to be careful about choosing which bricks to disintegrate; if you disintegrate the wrong brick, large portions of the stack could topple, which sounds pretty dangerous.
#
# The Elves responsible for water filtering operations took a snapshot of the bricks while they were still falling (your puzzle input) which should let you work out which bricks are safe to disintegrate. For example:
#
# 1,0,1~1,2,1
# 0,0,2~2,0,2
# 0,2,3~2,2,3
# 0,0,4~0,2,4
# 2,0,5~2,2,5
# 0,1,6~2,1,6
# 1,1,8~1,1,9
#
# Each line of text in the snapshot represents the position of a single brick at the time the snapshot was taken. The position is given as two x,y,z coordinates - one for each end of the brick - separated by a tilde (~). Each brick is made up of a single straight line of cubes, and the Elves were even careful to choose a time for the snapshot that had all of the free-falling bricks at integer positions above the ground, so the whole snapshot is aligned to a three-dimensional cube grid.
#
# A line like 2,2,2~2,2,2 means that both ends of the brick are at the same coordinate - in other words, that the brick is a single cube.
#
# Lines like 0,0,10~1,0,10 or 0,0,10~0,1,10 both represent bricks that are two cubes in volume, both oriented horizontally. The first brick extends in the x direction, while the second brick extends in the y direction.
#
# A line like 0,0,1~0,0,10 represents a ten-cube brick which is oriented vertically. One end of the brick is the cube located at 0,0,1, while the other end of the brick is located directly above it at 0,0,10.
#
# The ground is at z=0 and is perfectly flat; the lowest z value a brick can have is therefore 1. So, 5,5,1~5,6,1 and 0,2,1~0,2,5 are both resting on the ground, but 3,3,2~3,3,3 was above the ground at the time of the snapshot.
#
# Because the snapshot was taken while the bricks were still falling, some bricks will still be in the air; you'll need to start by figuring out where they will end up. Bricks are magically stabilized, so they never rotate, even in weird situations like where a long horizontal brick is only supported on one end. Two bricks cannot occupy the same position, so a falling brick will come to rest upon the first other brick it encounters.
#
# Here is the same example again, this time with each brick given a letter so it can be marked in diagrams:
#
# 1,0,1~1,2,1   <- A
# 0,0,2~2,0,2   <- B
# 0,2,3~2,2,3   <- C
# 0,0,4~0,2,4   <- D
# 2,0,5~2,2,5   <- E
# 0,1,6~2,1,6   <- F
# 1,1,8~1,1,9   <- G
#
# At the time of the snapshot, from the side so the x axis goes left to right, these bricks are arranged like this:
#
#  x
# 012
# .G. 9
# .G. 8
# ... 7
# FFF 6
# ..E 5 z
# D.. 4
# CCC 3
# BBB 2
# .A. 1
# --- 0
#
# Rotating the perspective 90 degrees so the y axis now goes left to right, the same bricks are arranged like this:
#
#  y
# 012
# .G. 9
# .G. 8
# ... 7
# .F. 6
# EEE 5 z
# DDD 4
# ..C 3
# B.. 2
# AAA 1
# --- 0
#
# Once all of the bricks fall downward as far as they can go, the stack looks like this, where ? means bricks are hidden behind other bricks at that location:
#
#  x
# 012
# .G. 6
# .G. 5
# FFF 4
# D.E 3 z
# ??? 2
# .A. 1
# --- 0
#
# Again from the side:
#
#  y
# 012
# .G. 6
# .G. 5
# .F. 4
# ??? 3 z
# B.C 2
# AAA 1
# --- 0
#
# Now that all of the bricks have settled, it becomes easier to tell which bricks are supporting which other bricks:
#
#     Brick A is the only brick supporting bricks B and C.
#     Brick B is one of two bricks supporting brick D and brick E.
#     Brick C is the other brick supporting brick D and brick E.
#     Brick D supports brick F.
#     Brick E also supports brick F.
#     Brick F supports brick G.
#     Brick G isn't supporting any bricks.
#
# Your first task is to figure out which bricks are safe to disintegrate. A brick can be safely disintegrated if, after removing it, no other bricks would fall further directly downward. Don't actually disintegrate any bricks - just determine what would happen if, for each brick, only that brick were disintegrated. Bricks can be disintegrated even if they're completely surrounded by other bricks; you can squeeze between bricks if you need to.
#
# In this example, the bricks can be disintegrated as follows:
#
#     Brick A cannot be disintegrated safely; if it were disintegrated, bricks B and C would both fall.
#     Brick B can be disintegrated; the bricks above it (D and E) would still be supported by brick C.
#     Brick C can be disintegrated; the bricks above it (D and E) would still be supported by brick B.
#     Brick D can be disintegrated; the brick above it (F) would still be supported by brick E.
#     Brick E can be disintegrated; the brick above it (F) would still be supported by brick D.
#     Brick F cannot be disintegrated; the brick above it (G) would fall.
#     Brick G can be disintegrated; it does not support any other bricks.
#
# So, in this example, 5 bricks can be safely disintegrated.
#
# Figure how the blocks will settle based on the snapshot. Once they've settled, consider disintegrating a single brick; how many bricks could be safely chosen as the one to get disintegrated?
#
# Your puzzle answer was 430.
#
# --- Part Two ---
#
# Disintegrating bricks one at a time isn't going to be fast enough. While it might sound dangerous, what you really need is a chain reaction.
#
# You'll need to figure out the best brick to disintegrate. For each brick, determine how many other bricks would fall if that brick were disintegrated.
#
# Using the same example as above:
#
#     Disintegrating brick A would cause all 6 other bricks to fall.
#     Disintegrating brick F would cause only 1 other brick, G, to fall.
#
# Disintegrating any other brick would cause no other bricks to fall. So, in this example, the sum of the number of other bricks that would fall as a result of disintegrating each brick is 7.
#
# For each brick, determine how many other bricks would fall if that brick were disintegrated. What is the sum of the number of other bricks that would fall?
#
# Your puzzle answer was 60558.
#
# Output:
#
#     $ crystal run cli.cr -- -d 22 -i input/day22.txt
#     --- Day 22 ---
#     --- Part One ---
#     Answer: 430
#     (execute time: 8746.982042 ms, memory: 1105332752 bytes)
#
#     $ crystal run cli.cr -- -d 22 -p 2 -i input/day22.txt
#     --- Day 22: ---
#     --- Part Two ---
#     Answer: 60558
#     (execute time: 18014.489625 ms, memory: 2362591024 bytes)

require "../utils.cr"

module Day22
  extend self

  struct Cube
    getter cubes : Array(Coord3D)

    def initialize(start : Coord3D, edge : Coord3D)
      richtung = StaticArray(Int32, 3).new(0)
      size = 0
      [:x, :y, :z].each_with_index do |os, i|
        # It works for samples that coordinates sorted by default
        size += edge[os] - start[os]
        richtung[i] = edge[os] - start[os] != 0 ? 1 : 0
      end

      # Identify all cubes of the brick
      @cubes = Array(Coord3D).new(size)
      pos = start
      while pos != edge
        @cubes << pos
        pos = {x: pos[:x] + richtung[0], y: pos[:y] + richtung[1], z: pos[:z] + richtung[2]}
      end
      @cubes << edge
    end

    def initialize(cubes : Array(Coord3D))
      @cubes = cubes.dup
    end

    # fall! returns true if brick was moved
    # The fall means only z attributes changes for each cube in brick.
    # The change fall all cubes the same.
    def fall!(plan) : Bool
      height = height_to_fall(plan)

      # No space to fall
      return false if height == 0

      id = plan[@cubes.first]
      # First cleanup, to make sure it is not rely on sorted cubes
      @cubes.each { |c| plan.delete(c) }

      # Update new postions of each cube and sync with plan
      @cubes.each_with_index do |c, i|
        pos = {x: c[:x], y: c[:y], z: c[:z] - height}
        plan[pos] = id
        @cubes[i] = pos
      end

      true
    end

    # fall? returns true if brick could fall
    def fall?(plan)
      height_to_fall(plan) > 0
    end

    # height_to_fall returns how deep the brick could fall
    def height_to_fall(plan)
      bottoms = @cubes.map(&.[:z]).uniq!
      z = bottoms.min
      empty = 0
      return empty if z <= 1

      height = 0
      if bottoms.size == 1
        z -= 1
        positions = @cubes.map { |c| {x: c[:x], y: c[:y], z: z} }
        while z > 0 && positions.all? { |pos| !plan.has_key?(pos) }
          height += 1
          z -= 1
          positions.map! { |c| {x: c[:x], y: c[:y], z: z} }
        end
        return height
      end

      # Check if there is space under the brick
      c = @cubes.first
      pos = {x: c[:x], y: c[:y], z: z - 1}
      while !plan.has_key?(pos) && pos[:z] > 0
        height += 1
        pos = {x: c[:x], y: c[:y], z: pos[:z] - 1}
      end
      return height
    end

    def clone
      Cube.new(@cubes)
    end
  end

  # --- Part One ---
  def problem1(records : Array(String))
    plan, bricks = parse(records)

    # First step make the fall happen and stabilize the system
    moved = true
    while moved
      moved = false
      bricks.each do |brick|
        moved ||= brick.fall!(plan)
      end
    end

    # Identify number of bricks that could be removed without collapsing the system
    result = 0
    bricks.size.times do |id|
      probe = plan.reject { |_, brick_id| brick_id == id }
      moved = false
      bricks.each_with_index do |brick, brick_id|
        next if brick_id == id
        if brick.fall?(probe)
          moved = true
          break
        end
      end
      result += 1 if !moved
    end
    result
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    plan, bricks = parse(records)

    # First step make the fall happen and stabilize the system
    moved = true
    while moved
      moved = false
      bricks.map! do |brick|
        moved ||= brick.fall!(plan)
        brick
      end
    end

    # Identify the bricks that would make a collapse of the system
    # fragile_brick_ids keeps the ids of the bricks that would make at least one difference in the system
    fragile_brick_ids = Array(Int32).new(bricks.size)
    bricks.size.times do |id|
      probe = plan.reject { |_, brick_id| brick_id == id }
      moved = false
      bricks.each_with_index do |brick, brick_id|
        next if brick_id == id
        if brick.fall?(probe)
          moved = true
          break
        end
      end
      fragile_brick_ids << id if moved
    end

    # Now count the number of each movements, if remove brick from the system
    result = 0
    fragile_brick_ids.each do |id|
      # Duplicate the map, to keep for each brick own map
      probe = plan.reject { |_, brick_id| brick_id == id }
      # Keep the state of each brick for each case, as it is related to the system
      probe_bricks = bricks.clone

      # Store id of bricks that would move at least once
      fallen = Set(Int32).new

      # Repeat the process of falling and store moved bricks in Set
      moved = true
      while moved
        moved = false
        probe_bricks.each_with_index do |brick, brick_id|
          next if brick_id == id
          if brick.fall!(probe)
            moved = true
            fallen.add(brick_id)
          end
        end
      end
      # Add to global counter the number of moved bricks for case when X brick removed
      result += fallen.size
    end
    result
  end

  # parse returns states of bricks and plan
  def parse(records)
    bricks = Array(Cube).new(records.size)
    plan = Hash(Coord3D, Int32).new
    records.each_with_index do |brick_raw, id|
      s, e = brick_raw.split("~").map(&.split(",").map(&.to_i))
      brick = Cube.new({x: s[0], y: s[1], z: s[2]}, {x: e[0], y: e[1], z: e[2]})
      # Sync that state of all bricks with a global plan/map
      brick.cubes.each { |coord| plan[coord] = id }
      bricks << brick
    end
    return {plan, bricks}
  end
end
