# https://adventofcode.com/2023/day/23
#
# NOTE: Use Graph and algorithm to find the max path
#
# --- Day 23: A Long Walk ---
#
# The Elves resume water filtering operations! Clean water starts flowing over the edge of Island Island.
#
# They offer to help you go over the edge of Island Island, too! Just hold on tight to one end of this impossibly long rope and they'll lower you down a safe distance from the massive waterfall you just created.
#
# As you finally reach Snow Island, you see that the water isn't really reaching the ground: it's being absorbed by the air itself. It looks like you'll finally have a little downtime while the moisture builds up to snow-producing levels. Snow Island is pretty scenic, even without any snow; why not take a walk?
#
# There's a map of nearby hiking trails (your puzzle input) that indicates paths (.), forest (#), and steep slopes (^, >, v, and <).
#
# For example:
#
# #.#####################
# #.......#########...###
# #######.#########.#.###
# ###.....#.>.>.###.#.###
# ###v#####.#v#.###.#.###
# ###.>...#.#.#.....#...#
# ###v###.#.#.#########.#
# ###...#.#.#.......#...#
# #####.#.#.#######.#.###
# #.....#.#.#.......#...#
# #.#####.#.#.#########v#
# #.#...#...#...###...>.#
# #.#.#v#######v###.###v#
# #...#.>.#...>.>.#.###.#
# #####v#.#.###v#.#.###.#
# #.....#...#...#.#.#...#
# #.#########.###.#.#.###
# #...###...#...#...#.###
# ###.###.#.###v#####v###
# #...#...#.#.>.>.#.>.###
# #.###.###.#.###.#.#v###
# #.....###...###...#...#
# #####################.#
#
# You're currently on the single path tile in the top row; your goal is to reach the single path tile in the bottom row. Because of all the mist from the waterfall, the slopes are probably quite icy; if you step onto a slope tile, your next step must be downhill (in the direction the arrow is pointing). To make sure you have the most scenic hike possible, never step onto the same tile twice. What is the longest hike you can take?
#
# In the example above, the longest hike you can take is marked with O, and your starting position is marked S:
#
# #S#####################
# #OOOOOOO#########...###
# #######O#########.#.###
# ###OOOOO#OOO>.###.#.###
# ###O#####O#O#.###.#.###
# ###OOOOO#O#O#.....#...#
# ###v###O#O#O#########.#
# ###...#O#O#OOOOOOO#...#
# #####.#O#O#######O#.###
# #.....#O#O#OOOOOOO#...#
# #.#####O#O#O#########v#
# #.#...#OOO#OOO###OOOOO#
# #.#.#v#######O###O###O#
# #...#.>.#...>OOO#O###O#
# #####v#.#.###v#O#O###O#
# #.....#...#...#O#O#OOO#
# #.#########.###O#O#O###
# #...###...#...#OOO#O###
# ###.###.#.###v#####O###
# #...#...#.#.>.>.#.>O###
# #.###.###.#.###.#.#O###
# #.....###...###...#OOO#
# #####################O#
#
# This hike contains 94 steps. (The other possible hikes you could have taken were 90, 86, 82, 82, and 74 steps long.)
#
# Find the longest hike you can take through the hiking trails listed on your map. How many steps long is the longest hike?
#
# Your puzzle answer was 2310.
# --- Part Two ---
#
# As you reach the trailhead, you realize that the ground isn't as slippery as you expected; you'll have no problem climbing up the steep slopes.
#
# Now, treat all slopes as if they were normal paths (.). You still want to make sure you have the most scenic hike possible, so continue to ensure that you never step onto the same tile twice. What is the longest hike you can take?
#
# In the example above, this increases the longest hike to 154 steps:
#
# #S#####################
# #OOOOOOO#########OOO###
# #######O#########O#O###
# ###OOOOO#.>OOO###O#O###
# ###O#####.#O#O###O#O###
# ###O>...#.#O#OOOOO#OOO#
# ###O###.#.#O#########O#
# ###OOO#.#.#OOOOOOO#OOO#
# #####O#.#.#######O#O###
# #OOOOO#.#.#OOOOOOO#OOO#
# #O#####.#.#O#########O#
# #O#OOO#...#OOO###...>O#
# #O#O#O#######O###.###O#
# #OOO#O>.#...>O>.#.###O#
# #####O#.#.###O#.#.###O#
# #OOOOO#...#OOO#.#.#OOO#
# #O#########O###.#.#O###
# #OOO###OOO#OOO#...#O###
# ###O###O#O###O#####O###
# #OOO#OOO#O#OOO>.#.>O###
# #O###O###O#O###.#.#O###
# #OOOOO###OOO###...#OOO#
# #####################O#
#
# Find the longest hike you can take through the surprisingly dry hiking trails listed on your map. How many steps long is the longest hike?
#
# Your puzzle answer was 6738.
#
# Output:
#
# crystal run cli.cr -- -d 23 -i input/day23.txt
# --- Day 23 ---
# --- Part One ---
# Answer: 2310
# (execute time: 1225.564583 ms, memory: 5132555344 bytes)
# crystal run cli.cr -- -d 23 -p 2 -i input/day23.txt
# --- Day 23: ---
# --- Part Two ---
# Answer: 6738
# (execute time: 66007.1945 ms, memory: 20440081904 bytes)

require "../utils.cr"

module Day23
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    plan, field_size = parse(records)
    # print_plan plan, field_size[:y], field_size[:x]

    start = {x: 1, y: 0}
    target = {x: field_size[:x] - 2, y: field_size[:y] - 2}
    route(start, target, Set{start}, plan, field_size)
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    plan, field_size = parse(records)
    plan.each do |k, v|
      plan[k] = '.' if v.in? ['>', 'v']
    end
    # print_plan plan, field_size[:y], field_size[:x]

    # Optimize number of vertexes/nodes to minimum.
    # There are only important nodes with more than 2 connections.
    knots = build_knots(plan, field_size)
    graph = build_graph(knots, plan)
    # puts graph

    start = {x: 1, y: 0}
    target = {x: field_size[:x] - 2, y: field_size[:y] - 1}

    max = 0
    graph.dfs(start, target) do |route|
      sum = route.sum { |node| node[1] }
      max = [sum, max].max
    end
    max
  end

  NEIGHBOURS = {
    '>' => {x: 1, y: 0},
    'v' => {x: 0, y: 1},
    '<' => {x: -1, y: 0},
    '^' => {x: 0, y: -1},
  }

  def route(position, goal, visited, plan, field_size)
    return visited.size if position == goal
    result = 0
    NEIGHBOURS.each do |direction, delta|
      next_step = {x: position[:x] + delta[:x], y: position[:y] + delta[:y]}
      next if !plan.has_key?(next_step) || plan[next_step] == '#' || visited.includes?(next_step)
      v = visited.dup
      case plan[next_step]
      when '.'
        v << next_step
      when 'v', '>'
        next if [direction, plan[next_step]].sort!.in?([['<', '>'], ['^', 'v']])
        v << next_step
        slope_delta = NEIGHBOURS[plan[next_step]]
        prev = next_step
        next_step = {x: next_step[:x] + slope_delta[:x], y: next_step[:y] + slope_delta[:y]}
        while plan[next_step] == '.' && !v.includes?(next_step)
          prev = next_step
          v.add(prev)
          next_step = {x: next_step[:x] + slope_delta[:x], y: next_step[:y] + slope_delta[:y]}
        end
        next_step = prev
      end
      result = [route(next_step, goal, v, plan, field_size), result].max
    end

    return result
  end

  def parse(records)
    plan = Hash(Coord, Char).new
    records.each_with_index do |instruction, y|
      instruction.chars.each_with_index do |c, x|
        plan[{x: x, y: y}] = c
      end
    end
    return {plan, {y: records.size, x: records.first.size}}
  end

  class UndirectedGraph(C)
    def initialize
      # adjacency-list implementation
      @graph = Hash(C, Hash(C, Int32)).new
    end

    def add(src : C, target : C, weight : Int32)
      @graph[src] ||= Hash(C, Int32).new
      @graph[src][target] = weight

      @graph[target] ||= Hash(C, Int32).new
      @graph[target][src] = weight
    end

    def neighbours(node : C)
      @graph[node]
    end

    def empty?
      @graph.empty?
    end

    def bfs(start : C, & : C -> Nil)
      return if !@graph.has_key?(start)

      queue = [start]
      visited = Set(C).new
      visiting = Set(C).new
      while queue.size > 0
        visiting = Set(C).new
        queue.each do |node|
          yield node

          visited.add(node)
          @graph[node].each do |cell|
            next if visited.includes?(cell) || queue.includes?(cell) || !@graph.has_key?(cell)
            visiting.add(cell)
          end
        end
        queue = visiting
      end
    end

    # Traverse the graph from start to bottom and find all possible paths.
    # NOTE: DFS is not optimised.
    #
    #   A - B -C
    #     \ |/
    #       D
    # Produces paths:
    #   A-B-C
    #   A-B-D-C
    #   A-D-B-C
    #   A-D-C
    def dfs(start : C, goal : C, & : Array(Tuple(C, Int32)) -> Nil)
      return if !@graph.has_key?(start) || !@graph.has_key?(goal)

      max_path = 0
      queue : Array(Array(C)) = [[start]]
      weights = [[0]]
      while queue.size > 0
        branch = queue.pop
        branch_weights = weights.pop
        node_id = branch.last

        neighbours = neighbours(node_id).reject { |neighbour_id| branch.includes?(neighbour_id) }
        neighbours.each do |neighbour_id, neighbour_weight|
          if neighbour_id == goal
            route = Array(Tuple(C, Int32)).new
            branch.each_with_index do |n, i|
              route << {n, branch_weights[i]}
            end
            route += [{neighbour_id, neighbour_weight}]
            yield route
          else
            queue << (branch + [neighbour_id])
            weights << (branch_weights + [neighbour_weight])
          end
        end
      end
    end

    # DOT format. References https://graphviz.org/doc/info/lang.html
    def to_s(io : IO) : Nil
      io << "strict graph {\n"
      @graph.each do |s, dsts|
        dsts.each do |dst, weight|
          io << "  \"#{s}\" -> \"#{dst}\" [ label = \"#{weight}\" ]\n"
        end
      end
      io << "}\n"
    end
  end

  def build_knots(plan, field_size) : Set(Coord)
    result = Set(Coord).new
    field_size[:y].times do |y|
      field_size[:x].times do |x|
        pos = {x: x, y: y}
        next if plan[pos] == '#'

        count_of_neighbours = NEIGHBOURS.values.map { |delta| {x: pos[:x] + delta[:x], y: pos[:y] + delta[:y]} }.count { |coord| !plan.has_key?(coord) || plan[coord] == '#' }
        next if count_of_neighbours == 2
        result << pos
      end
    end
    result
  end

  def build_graph(knots : Set(Coord), plan)
    graph = UndirectedGraph(Coord).new
    knots.each do |knot|
      distance = 1
      moves = Set{knot}
      visited = Set{knot}
      while moves.size > 0
        next_moves = typeof(moves).new
        moves.each do |pos|
          visited << pos
          NEIGHBOURS.values.each do |delta|
            next_pos = {x: pos[:x] + delta[:x], y: pos[:y] + delta[:y]}
            next if !plan.has_key?(next_pos) || plan[next_pos] == '#' || visited.includes?(next_pos)
            if knots.includes?(next_pos)
              graph.add(knot, next_pos, distance)
            else
              next_moves << next_pos
            end
          end
        end
        moves = next_moves
        distance += 1
      end
    end
    graph
  end
end
