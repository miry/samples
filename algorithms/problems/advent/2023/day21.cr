# https://adventofcode.com/2023/day/21
#
# Explanation: https://www.youtube.com/watch?v=xHIQ2zHVSjM
#
# --- Day 21: Step Counter ---
#
# You manage to catch the airship right as it's dropping someone else off on their all-expenses-paid trip to Desert Island! It even helpfully drops you off near the gardener and his massive farm.
#
# "You got the sand flowing again! Great work! Now we just need to wait until we have enough sand to filter the water for Snow Island and we'll have snow again in no time."
#
# While you wait, one of the Elves that works with the gardener heard how good you are at solving problems and would like your help. He needs to get his steps in for the day, and so he'd like to know which garden plots he can reach with exactly his remaining 64 steps.
#
# He gives you an up-to-date map (your puzzle input) of his starting position (S), garden plots (.), and rocks (#). For example:
#
# ...........
# .....###.#.
# .###.##..#.
# ..#.#...#..
# ....#.#....
# .##..S####.
# .##..#...#.
# .......##..
# .##.#.####.
# .##..##.##.
# ...........
#
# The Elf starts at the starting position (S) which also counts as a garden plot. Then, he can take one step north, south, east, or west, but only onto tiles that are garden plots. This would allow him to reach any of the tiles marked O:
#
# ...........
# .....###.#.
# .###.##..#.
# ..#.#...#..
# ....#O#....
# .##.OS####.
# .##..#...#.
# .......##..
# .##.#.####.
# .##..##.##.
# ...........
#
# Then, he takes a second step. Since at this point he could be at either tile marked O, his second step would allow him to reach any garden plot that is one step north, south, east, or west of any tile that he could have reached after the first step:
#
# ...........
# .....###.#.
# .###.##..#.
# ..#.#O..#..
# ....#.#....
# .##O.O####.
# .##.O#...#.
# .......##..
# .##.#.####.
# .##..##.##.
# ...........
#
# After two steps, he could be at any of the tiles marked O above, including the starting position (either by going north-then-south or by going west-then-east).
#
# A single third step leads to even more possibilities:
#
# ...........
# .....###.#.
# .###.##..#.
# ..#.#.O.#..
# ...O#O#....
# .##.OS####.
# .##O.#...#.
# ....O..##..
# .##.#.####.
# .##..##.##.
# ...........
#
# He will continue like this until his steps for the day have been exhausted. After a total of 6 steps, he could reach any of the garden plots marked O:
#
# ...........
# .....###.#.
# .###.##.O#.
# .O#O#O.O#..
# O.O.#.#.O..
# .##O.O####.
# .##.O#O..#.
# .O.O.O.##..
# .##.#.####.
# .##O.##.##.
# ...........
#
# In this example, if the Elf's goal was to get exactly 6 more steps today, he could use them to reach any of 16 garden plots.
#
# However, the Elf actually needs to get 64 steps today, and the map he's handed you is much larger than the example map.
#
# Starting from the garden plot marked S on your map, how many garden plots could the Elf reach in exactly 64 steps?
#
# Your puzzle answer was 3689.
# --- Part Two ---
#
# The Elf seems confused by your answer until he realizes his mistake: he was reading from a list of his favorite numbers that are both perfect squares and perfect cubes, not his step counter.
#
# The actual number of steps he needs to get today is exactly 26501365.
#
# He also points out that the garden plots and rocks are set up so that the map repeats infinitely in every direction.
#
# So, if you were to look one additional map-width or map-height out from the edge of the example map above, you would find that it keeps repeating:
#
# .................................
# .....###.#......###.#......###.#.
# .###.##..#..###.##..#..###.##..#.
# ..#.#...#....#.#...#....#.#...#..
# ....#.#........#.#........#.#....
# .##...####..##...####..##...####.
# .##..#...#..##..#...#..##..#...#.
# .......##.........##.........##..
# .##.#.####..##.#.####..##.#.####.
# .##..##.##..##..##.##..##..##.##.
# .................................
# .................................
# .....###.#......###.#......###.#.
# .###.##..#..###.##..#..###.##..#.
# ..#.#...#....#.#...#....#.#...#..
# ....#.#........#.#........#.#....
# .##...####..##..S####..##...####.
# .##..#...#..##..#...#..##..#...#.
# .......##.........##.........##..
# .##.#.####..##.#.####..##.#.####.
# .##..##.##..##..##.##..##..##.##.
# .................................
# .................................
# .....###.#......###.#......###.#.
# .###.##..#..###.##..#..###.##..#.
# ..#.#...#....#.#...#....#.#...#..
# ....#.#........#.#........#.#....
# .##...####..##...####..##...####.
# .##..#...#..##..#...#..##..#...#.
# .......##.........##.........##..
# .##.#.####..##.#.####..##.#.####.
# .##..##.##..##..##.##..##..##.##.
# .................................
#
# This is just a tiny three-map-by-three-map slice of the inexplicably-infinite farm layout; garden plots and rocks repeat as far as you can see. The Elf still starts on the one middle tile marked S, though - every other repeated S is replaced with a normal garden plot (.).
#
# Here are the number of reachable garden plots in this new infinite version of the example map for different numbers of steps:
#
#     In exactly 6 steps, he can still reach 16 garden plots.
#     In exactly 10 steps, he can reach any of 50 garden plots.
#     In exactly 50 steps, he can reach 1594 garden plots.
#     In exactly 100 steps, he can reach 6536 garden plots.
#     In exactly 500 steps, he can reach 167004 garden plots.
#     In exactly 1000 steps, he can reach 668697 garden plots.
#     In exactly 5000 steps, he can reach 16733044 garden plots.
#
# However, the step count the Elf needs is much larger! Starting from the garden plot marked S on your infinite map, how many garden plots could the Elf reach in exactly 26501365 steps?
#
# Your puzzle answer was 610158187362102.

require "../utils.cr"
require "../progress.cr"

module Day21
  extend self

  NEIGHBOURS = {
    'v' => {-1, 0},
    '^' => {1, 0},
    '>' => {0, -1},
    '<' => {0, 1},
  }

  # --- Part One ---
  def problem1(records : Array(String), steps = 64)
    start, plan, height, width = parse(records)
    # print_plan(plan, height, width)

    queue = Set{start}
    i = 0
    while i < steps && queue.size > 0
      new_queue = typeof(queue).new
      queue.each do |coord|
        NEIGHBOURS.values.each do |nc|
          pos = {x: coord[:x] + nc[1], y: coord[:y] + nc[0]}
          next if !plan.has_key?(pos) || plan[pos] != '.'
          new_queue.add pos
        end
      end
      queue = new_queue
      i += 1
    end

    queue.size
  end

  # --- Part Two ---
  # NOTE: Estimation
  # 26501365 steps to left and right, top and bottom.
  # It builds a Rhombus.
  # Used
  def problem2(records : Array(String), steps = 26501365)
    # Hard code base on 2 formulas with x = 202300 and f(65), f(65 + 131), f(65 + 131*2)
    # https://www.wolframalpha.com/input?i=quadratic+fit+calculator&assumption=%7B%22F%22%2C+%22QuadraticFitCalculator%22%2C+%22data2%22%7D+-%3E%22%7B%7B0%2C3802%7D%2C%7B1%2C33732%7D%2C+%7B2%2C93480%7D%7D%22
    # https://www.wolframalpha.com/input?i=3802+%2B+15021+x+%2B+14909+x%5E2%2C+x%3D202300
    # problem(65) = 3802
    # problem(65 + 131) = 33732
    # problem(65 + 131*2) = 93480
    #
    # It helps to build 3802 + 15021 x + 14909 x^2
    # Find the value for x = 202300
    return 610158187362102 if steps == 26501365

    start, plan, height, width = parse(records)
    # print_plan(plan, height, width)

    bar = ProgressBar.new
    bar.total = steps

    queue = Set{start}
    delta = Array(Int32).new
    delta << 0
    visited = Set{start}

    i = 0
    while i < steps && queue.size > 0
      bar.inc
      new_queue = typeof(queue).new
      queue.each do |coord|
        NEIGHBOURS.values.each do |nc|
          pos = {x: coord[:x] + nc[1], y: coord[:y] + nc[0]}
          next if visited.includes?(pos)
          extend_map!(plan, height, width, pos)
          next if plan[pos] != '.'
          new_queue.add pos
          visited.add pos
        end
      end
      i += 1

      queue = new_queue
    end

    result = 0
    r = steps % 2
    visited.each do |pos|
      result += 1 if (pos[:x] + pos[:y]) % 2 == r
    end

    result
  end

  def parse(records)
    plan = Hash(Coord, Char).new
    start = {x: 0, y: 0}
    records.each_with_index do |instruction, y|
      instruction.chars.each_with_index do |c, x|
        if c == 'S'
          start = {x: x, y: y}
          plan[{x: x, y: y}] = '.'
        else
          plan[{x: x, y: y}] = c
        end
      end
    end
    return {start, plan, records.size, records.first.size}
  end

  def extend_map!(plan, height, width, coord)
    return plan if plan.has_key?(coord)

    topbottom = coord[:y] // height
    leftright = coord[:x] // width

    start = {x: leftright * width, y: topbottom * height}
    height.times do |y|
      width.times do |x|
        pos = {x: start[:x] + x, y: start[:y] + y}
        plan[pos] = plan[{x: x, y: y}]
      end
    end
    start
  end
end
