# https://adventofcode.com/2023/day/25
#
# --- Day 25: Snowverload ---
#
# Still somehow without snow, you go to the last place you haven't checked: the center of Snow Island, directly below the waterfall.
#
# Here, someone has clearly been trying to fix the problem. Scattered everywhere are hundreds of weather machines, almanacs, communication modules, hoof prints, machine parts, mirrors, lenses, and so on.
#
# Somehow, everything has been wired together into a massive snow-producing apparatus, but nothing seems to be running. You check a tiny screen on one of the communication modules: Error 2023. It doesn't say what Error 2023 means, but it does have the phone number for a support line printed on it.
#
# "Hi, you've reached Weather Machines And So On, Inc. How can I help you?" You explain the situation.
#
# "Error 2023, you say? Why, that's a power overload error, of course! It means you have too many components plugged in. Try unplugging some components and--" You explain that there are hundreds of components here and you're in a bit of a hurry.
#
# "Well, let's see how bad it is; do you see a big red reset button somewhere? It should be on its own module. If you push it, it probably won't fix anything, but it'll report how overloaded things are." After a minute or two, you find the reset button; it's so big that it takes two hands just to get enough leverage to push it. Its screen then displays:
#
# SYSTEM OVERLOAD!
#
# Connected components would require
# power equal to at least 100 stars!
#
# "Wait, how many components did you say are plugged in? With that much equipment, you could produce snow for an entire--" You disconnect the call.
#
# You have nowhere near that many stars - you need to find a way to disconnect at least half of the equipment here, but it's already Christmas! You only have time to disconnect three wires.
#
# Fortunately, someone left a wiring diagram (your puzzle input) that shows how the components are connected. For example:
#
# jqt: rhn xhk nvd
# rsh: frs pzl lsr
# xhk: hfx
# cmg: qnr nvd lhk bvb
# rhn: xhk bvb hfx
# bvb: xhk hfx
# pzl: lsr hfx nvd
# qnr: nvd
# ntq: jqt hfx bvb xhk
# nvd: lhk
# lsr: lhk
# rzs: qnr cmg lsr rsh
# frs: qnr lhk lsr
#
# Each line shows the name of a component, a colon, and then a list of other components to which that component is connected. Connections aren't directional; abc: xyz and xyz: abc both represent the same configuration. Each connection between two components is represented only once, so some components might only ever appear on the left or right side of a colon.
#
# In this example, if you disconnect the wire between hfx/pzl, the wire between bvb/cmg, and the wire between nvd/jqt, you will divide the components into two separate, disconnected groups:
#
#     9 components: cmg, frs, lhk, lsr, nvd, pzl, qnr, rsh, and rzs.
#     6 components: bvb, hfx, jqt, ntq, rhn, and xhk.
#
# Multiplying the sizes of these groups together produces 54.
#
# Find the three wires you need to disconnect in order to divide the components into two separate groups. What do you get if you multiply the sizes of these two groups together?
#
# Your puzzle answer was 514794.

require "../utils.cr"

module Day25
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    connections = parse(records)

    # Count number of usage of same node
    counts = Hash(String, Int32).new
    connections.each do |k, _nodes|
      s = 0
      connections.each do |_, nodes|
        s += nodes.count(k)
      end
      counts[k] = s
    end

    if connections.size == 15
      brute_force(connections)
    else
      visualized_method(connections)
    end
  end

  def delete_conn(connections, pair)
    connections[pair[0]].delete(pair[1])
    connections[pair[1]].delete(pair[0])
  end

  def build_kettens(connections, deep = 1)
    # puts "> build_kettens(connections, #{deep})"
    return nil if deep > 2

    start = connections.keys.first
    queue = Set{start}
    ketten = Set(String).new

    while queue.size > 0
      new_queue = typeof(queue).new
      queue.each do |parent|
        ketten << parent
        connections[parent].each do |child_node|
          next if ketten.includes?(child_node)
          new_queue << child_node
        end
      end
      queue = new_queue
    end

    if ketten.size == connections.size
      return [ketten]
    end

    r = build_kettens(connections.reject(ketten), deep + 1)
    r.nil? ? r : [ketten] + r
  end

  def build_pairs
  end

  def parse(records)
    connections = Hash(String, Array(String)).new
    records.map do |instructions|
      left_node, *nodes = instructions.split(": ").flat_map &.split(/ +/).reject(&.blank?)
      connections[left_node] ||= Array(String).new
      nodes.each do |right_node|
        connections[right_node] ||= Array(String).new
        connections[left_node] << right_node
        connections[right_node] << left_node
      end
    end
    connections
  end

  def visualized_method(connections)
    draw_strict_graph(connections)
    puts "(copy the graph and generate image with `pbpaste | dot -Tpng -Kneato > day25.png`)"

    # Extract from picture `pbpaste | dot -Tpng -Kneato > day25.png`
    remove_connections = if connections.size == 15
                           [{"hfx", "pzl"}, {"bvb", "cmg"}, {"nvd", "jqt"}]
                         else
                           [{"kgl", "xzz"}, {"qfb", "vkd"}, {"xxq", "hqq"}]
                         end

    target = connections.clone
    delete_conn(target, remove_connections[0])
    delete_conn(target, remove_connections[1])
    delete_conn(target, remove_connections[2])

    r = build_kettens(target)
    if !r.nil? && r.size == 2
      return r[0].size * r[1].size
    end
    nil
  end

  # Draw dot format of graph and generate image with:
  # pbpaste | dot -Tsvg -Kcirco > output.svg
  def draw_strict_graph(graph)
    # Draw dot graph
    puts "strict graph {"
    graph.each do |src, dsts|
      puts "  #{src} -- {#{dsts.join(" ")}}"
    end
    puts "}"
  end

  def brute_force(connections)
    counts = Hash(String, Int32).new
    connections.each do |k, _nodes|
      s = 0
      connections.each do |_, nodes|
        s += nodes.count(k)
      end
      counts[k] = s
    end

    connections.keys.each do |node|
      connections[node].sort! { |a, b| counts[b] <=> counts[a] }
    end

    breakings_order = connections.keys.sort! { |a, b| counts[b] <=> counts[a] }

    throughput = breakings_order[0..1].sum { |i| counts[i] } - 2
    pairs = Set(StaticArray(String, 2)).new
    sums = Hash(StaticArray(String, 2), Int32).new
    breakings_order.each do |src|
      connections[src].each do |dst|
        s = counts[src] + counts[dst]
        pair = StaticArray[src, dst].sort
        pairs << pair
        sums[pair] = counts[pair[0]] + counts[pair[1]]
      end
    end
    pairs = pairs.to_a.sort! { |a, b| sums[b] <=> sums[a] }

    channels = Array(Channel(Int32)).new
    throughput = breakings_order[0..5].sum { |i| counts[i] }
    pairs_count = pairs.size
    while throughput > 0
      proc = ->(target_sum : Int32, the_connections : Hash(String, Array(String)), channel : Channel(Int32)) do
        spawn do
          exiting = false
          puts "[throughput: #{target_sum}] started"
          i = 0
          while !exiting && i < pairs_count - 2
            j = i + 1
            k = j + 1

            sum = sums[pairs[i]] + sums[pairs[j]] + sums[pairs[k]]
            # Next wil be less than current
            break if sum < target_sum

            while !exiting && j < pairs_count - 1
              k = j + 1
              while !exiting && k < pairs_count
                sum = sums[pairs[i]] + sums[pairs[j]] + sums[pairs[k]]
                break if sum < target_sum
                if sum > target_sum
                  k += 1
                  next
                end

                # puts "trio: #{pairs[i]} #{pairs[j]} #{pairs[k]} s: #{sum}"

                target = the_connections.clone

                delete_conn(target, pairs[i])
                delete_conn(target, pairs[j])
                delete_conn(target, pairs[k])

                if channel.closed?
                  Fiber.yield
                  exiting = true
                  break
                end

                r = build_kettens(target)
                if r && r.size == 2
                  result = r[0].size * r[1].size
                  puts "[throughput: #{target_sum}] result: #{result}"
                  channel.send(result)
                  Fiber.yield
                  exiting = true
                  break
                end
                k += 1
              end
              j += 1
            end
            i += 1
          end
          puts "[throughput: #{target_sum}] finished"
        end
      end
      channel = Channel(Int32).new
      channels << channel
      proc.call throughput, connections, channel
      throughput -= 1
    end

    return Channel.receive_first(channels)
  end
end
