require "spec"
require "./day22"

describe Day22 do
  sample = <<-EOF.split("\n")
  1,0,1~1,2,1
  0,0,2~2,0,2
  0,2,3~2,2,3
  0,0,4~0,2,4
  2,0,5~2,2,5
  0,1,6~2,1,6
  1,1,8~1,1,9
  EOF

  describe "problem1" do
    it "sample" do
      actual = Day22.problem1(sample)
      actual.should eq(5)
    end
  end

  describe "problem2" do
    it "sample" do
      actual = Day22.problem2(sample)
      actual.should eq(7)
    end
  end

  describe Day22::Cube do
    describe "#new" do
      it "single point" do
        Day22::Cube.new({x: 0, y: 0, z: 0}, {x: 0, y: 0, z: 0})
      end

      it "multiple point" do
        Day22::Cube.new({x: 0, y: 0, z: 0}, {x: 0, y: 10, z: 0})
      end
    end

    describe "#cubes" do
      it "returns single cube" do
        subject = Day22::Cube.new({x: 0, y: 0, z: 0}, {x: 0, y: 0, z: 0})
        subject.cubes.should eq([{x: 0, y: 0, z: 0}])
      end

      it "returns 2 cube line" do
        subject = Day22::Cube.new({x: 0, y: 0, z: 0}, {x: 0, y: 1, z: 0})
        subject.cubes.should eq([{x: 0, y: 0, z: 0}, {x: 0, y: 1, z: 0}])
      end

      it "returns 10 cube line" do
        subject = Day22::Cube.new({x: 0, y: 0, z: 0}, {x: 0, y: 10, z: 0})
        expected = (0..10).to_a.map { |y| {x: 0, y: y, z: 0} }
        subject.cubes.should eq(expected)
      end
    end
  end
end
