require "spec"
require "./day16"

describe Day16 do
  sample = <<-'EOF'.split("\n")
  .|...\....
  |.-.\.....
  .....|-...
  ........|.
  ..........
  .........\
  ..../.\\..
  .-.-/..|..
  .|....-|.\
  ..//.|....
  EOF

  describe "problem1" do
    it "sample" do
      actual = Day16.problem1(sample)
      actual.should eq(46)
    end
  end

  describe "problem2" do
    it "sample" do
      actual = Day16.problem2(sample)
      actual.should eq(51)
    end
  end
end
