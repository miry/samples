# https://adventofcode.com/2023/day/18
#
# NOTE: Use Math to find the polygon area like https://en.wikipedia.org/wiki/Shoelace_formula
#       and https://en.m.wikipedia.org/wiki/Pick's_theorem
#
# --- Day 18: Lavaduct Lagoon ---
#
# Thanks to your efforts, the machine parts factory is one of the first factories up and running since the lavafall came back. However, to catch up with the large backlog of parts requests, the factory will also need a large supply of lava for a while; the Elves have already started creating a large lagoon nearby for this purpose.
#
# However, they aren't sure the lagoon will be big enough; they've asked you to take a look at the dig plan (your puzzle input). For example:
#
# R 6 (#70c710)
# D 5 (#0dc571)
# L 2 (#5713f0)
# D 2 (#d2c081)
# R 2 (#59c680)
# D 2 (#411b91)
# L 5 (#8ceee2)
# U 2 (#caa173)
# L 1 (#1b58a2)
# U 2 (#caa171)
# R 2 (#7807d2)
# U 3 (#a77fa3)
# L 2 (#015232)
# U 2 (#7a21e3)
#
# The digger starts in a 1 meter cube hole in the ground. They then dig the specified number of meters up (U), down (D), left (L), or right (R), clearing full 1 meter cubes as they go. The directions are given as seen from above, so if "up" were north, then "right" would be east, and so on. Each trench is also listed with the color that the edge of the trench should be painted as an RGB hexadecimal color code.
#
# When viewed from above, the above example dig plan would result in the following loop of trench (#) having been dug out from otherwise ground-level terrain (.):
#
# #######
# #.....#
# ###...#
# ..#...#
# ..#...#
# ###.###
# #...#..
# ##..###
# .#....#
# .######
#
# At this point, the trench could contain 38 cubic meters of lava. However, this is just the edge of the lagoon; the next step is to dig out the interior so that it is one meter deep as well:
#
# #######
# #######
# #######
# ..#####
# ..#####
# #######
# #####..
# #######
# .######
# .######
#
# Now, the lagoon can contain a much more respectable 62 cubic meters of lava. While the interior is dug out, the edges are also painted according to the color codes in the dig plan.
#
# The Elves are concerned the lagoon won't be large enough; if they follow their dig plan, how many cubic meters of lava could it hold?
#
# Your puzzle answer was 52231.
# --- Part Two ---
#
# The Elves were right to be concerned; the planned lagoon would be much too small.
#
# After a few minutes, someone realizes what happened; someone swapped the color and instruction parameters when producing the dig plan. They don't have time to fix the bug; one of them asks if you can extract the correct instructions from the hexadecimal codes.
#
# Each hexadecimal code is six hexadecimal digits long. The first five hexadecimal digits encode the distance in meters as a five-digit hexadecimal number. The last hexadecimal digit encodes the direction to dig: 0 means R, 1 means D, 2 means L, and 3 means U.
#
# So, in the above example, the hexadecimal codes can be converted into the true instructions:
#
#     #70c710 = R 461937
#     #0dc571 = D 56407
#     #5713f0 = R 356671
#     #d2c081 = D 863240
#     #59c680 = R 367720
#     #411b91 = D 266681
#     #8ceee2 = L 577262
#     #caa173 = U 829975
#     #1b58a2 = L 112010
#     #caa171 = D 829975
#     #7807d2 = L 491645
#     #a77fa3 = U 686074
#     #015232 = L 5411
#     #7a21e3 = U 500254
#
# Digging out this loop and its interior produces a lagoon that can hold an impressive 952408144115 cubic meters of lava.
#
# Convert the hexadecimal color codes into the correct instructions; if the Elves follow this new dig plan, how many cubic meters of lava could the lagoon hold?
#
# Your puzzle answer was 57196493937398.

require "colorize"

require "../utils.cr"

module Day18
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    dig_plan = Array(Tuple(Char, Int32)).new(records.size)
    records.each do |instruction|
      direction, meters = instruction.split(" ")
      dig_plan << {direction[0], meters.to_i}
    end

    polygon_area(dig_plan)
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    dig_plan = Array(Tuple(Char, Int32)).new
    records.each do |instruction|
      _direction, _meters, color = instruction.split(" ")
      direction, meters = hex_to_instruction color[1..7]
      dig_plan << {direction, meters}
    end

    polygon_area(dig_plan)
  end

  def hex_to_instruction(hex)
    dir = {
      '0' => 'R',
      '1' => 'D',
      '2' => 'L',
      '3' => 'U',
    }[hex[6]]
    return {dir, hex[1..5].to_i(16)}
  end

  # Find area by Shoelace formula: A = 1 / 2 * {sum from i=0 to n of ( xi * yi+1 - xi+1 * yi )}
  # https://en.m.wikipedia.org/wiki/Pick's_theorem#Formula
  # https://en.wikipedia.org/wiki/Shoelace_formula#Example
  def polygon_area(dig_plan)
    coords = Array(NamedTuple(x: Int64, y: Int64)).new(dig_plan.size)
    directions = {
      'R' => {1_i64, 0_i64},
      'D' => {0_i64, 1_i64},
      'L' => {-1_i64, 0_i64},
      'U' => {0_i64, -1_i64},
    }
    pos = {x: 0_i64, y: 0_i64}
    perimeter = 0_i64
    dig_plan.each do |(direction, meters)|
      diff = directions[direction]
      pos = {x: pos[:x] + diff[0] * meters, y: pos[:y] + diff[1] * meters}
      coords.push(pos)
      perimeter += meters
    end

    sum = 0_i64
    coords.push(coords[0]) # Repeat first element to cycle the math
    n = coords.size - 1
    i = 0
    while i < n
      sum += coords[i][:x] * coords[i + 1][:y] - coords[i + 1][:x] * coords[i][:y]
      i += 1
    end
    sum // 2 + perimeter // 2 + 1
  end
end
