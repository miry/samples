# https://adventofcode.com/2023/day/19
#
# --- Day 19: Aplenty ---
#
# The Elves of Gear Island are thankful for your help and send you on your way. They even have a hang glider that someone stole from Desert Island; since you're already going that direction, it would help them a lot if you would use it to get down there and return it to them.
#
# As you reach the bottom of the relentless avalanche of machine parts, you discover that they're already forming a formidable heap. Don't worry, though - a group of Elves is already here organizing the parts, and they have a system.
#
# To start, each part is rated in each of four categories:
#
#     x: Extremely cool looking
#     m: Musical (it makes a noise when you hit it)
#     a: Aerodynamic
#     s: Shiny
#
# Then, each part is sent through a series of workflows that will ultimately accept or reject the part. Each workflow has a name and contains a list of rules; each rule specifies a condition and where to send the part if the condition is true. The first rule that matches the part being considered is applied immediately, and the part moves on to the destination described by the rule. (The last rule in each workflow has no condition and always applies if reached.)
#
# Consider the workflow ex{x>10:one,m<20:two,a>30:R,A}. This workflow is named ex and contains four rules. If workflow ex were considering a specific part, it would perform the following steps in order:
#
#     Rule "x>10:one": If the part's x is more than 10, send the part to the workflow named one.
#     Rule "m<20:two": Otherwise, if the part's m is less than 20, send the part to the workflow named two.
#     Rule "a>30:R": Otherwise, if the part's a is more than 30, the part is immediately rejected (R).
#     Rule "A": Otherwise, because no other rules matched the part, the part is immediately accepted (A).
#
# If a part is sent to another workflow, it immediately switches to the start of that workflow instead and never returns. If a part is accepted (sent to A) or rejected (sent to R), the part immediately stops any further processing.
#
# The system works, but it's not keeping up with the torrent of weird metal shapes. The Elves ask if you can help sort a few parts and give you the list of workflows and some part ratings (your puzzle input). For example:
#
# px{a<2006:qkq,m>2090:A,rfg}
# pv{a>1716:R,A}
# lnx{m>1548:A,A}
# rfg{s<537:gd,x>2440:R,A}
# qs{s>3448:A,lnx}
# qkq{x<1416:A,crn}
# crn{x>2662:A,R}
# in{s<1351:px,qqz}
# qqz{s>2770:qs,m<1801:hdj,R}
# gd{a>3333:R,R}
# hdj{m>838:A,pv}
#
# {x=787,m=2655,a=1222,s=2876}
# {x=1679,m=44,a=2067,s=496}
# {x=2036,m=264,a=79,s=2244}
# {x=2461,m=1339,a=466,s=291}
# {x=2127,m=1623,a=2188,s=1013}
#
# The workflows are listed first, followed by a blank line, then the ratings of the parts the Elves would like you to sort. All parts begin in the workflow named in. In this example, the five listed parts go through the following workflows:
#
#     {x=787,m=2655,a=1222,s=2876}: in -> qqz -> qs -> lnx -> A
#     {x=1679,m=44,a=2067,s=496}: in -> px -> rfg -> gd -> R
#     {x=2036,m=264,a=79,s=2244}: in -> qqz -> hdj -> pv -> A
#     {x=2461,m=1339,a=466,s=291}: in -> px -> qkq -> crn -> R
#     {x=2127,m=1623,a=2188,s=1013}: in -> px -> rfg -> A
#
# Ultimately, three parts are accepted. Adding up the x, m, a, and s rating for each of the accepted parts gives 7540 for the part with x=787, 4623 for the part with x=2036, and 6951 for the part with x=2127. Adding all of the ratings for all of the accepted parts gives the sum total of 19114.
#
# Sort through all of the parts you've been given; what do you get if you add together all of the rating numbers for all of the parts that ultimately get accepted?
#
# Your puzzle answer was 333263.
#
# Output:
# Answer: 333263
# (execute time: 5.528667 ms, memory: 919760 bytes)
#
# --- Part Two ---
#
# Even with your help, the sorting process still isn't fast enough.
#
# One of the Elves comes up with a new plan: rather than sort parts individually through all of these workflows, maybe you can figure out in advance which combinations of ratings will be accepted or rejected.
#
# Each of the four ratings (x, m, a, s) can have an integer value ranging from a minimum of 1 to a maximum of 4000. Of all possible distinct combinations of ratings, your job is to figure out which ones will be accepted.
#
# In the above example, there are 167409079868000 distinct combinations of ratings that will be accepted.
#
# Consider only your list of workflows; the list of part ratings that the Elves wanted you to sort is no longer relevant. How many distinct combinations of ratings will be accepted by the Elves' workflows?
#
# Your puzzle answer was 130745440937650.
#
# Output:
# Answer: 130745440937650
# (execute time: 27.465834 ms, memory: 2984240 bytes)

require "../utils.cr"

module Day19
  extend self

  record Rule, a : Char, op : Char, val : Int32, dst : String do
    setter dst

    def rate(rating)
      return dst if a == '.'

      cond = op == '<' ? rating[a] < val : rating[a] > val
      cond && dst
    end
  end

  # --- Part One ---
  def problem1(records : Array(String))
    workflows, ratings = parse(records)

    ratings.reject! do |rating|
      r = workflows["in"].find &.rate(rating)
      while r && !r.dst.in?(["A", "R"])
        r = workflows[r.dst].find &.rate(rating)
      end
      r.nil? || r.dst == "R"
    end

    ratings.sum do |rating|
      rating.values.sum
    end
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    workflows, ratings = parse(records)
    graph = destinations(workflows, true)

    # draw_digraph(graph)

    paths = routes(graph, workflows)
    paths.sum(0_i128) { |l| count(l, workflows) }
  end

  # Depth First Search or DFS for a Graph
  def routes(graph, workflows, visited = Array(Tuple(Int32, String)).new, node = "in", idx = 0)
    v = visited + [{idx, node}]
    return [v] if node == "A"

    result = Array(Array(Tuple(Int32, String))).new
    return result if node == "R"

    workflows[node].map(&.dst).each_with_index do |dst, i|
      r = routes(graph, workflows, v, dst, i)
      result += r if !r.empty?
    end
    result
  end

  # Build a simplified graph base on workflows.
  def destinations(workflows, compcating = true, start_node = "in")
    result = Hash(String, Set(String)).new
    queue = Set{start_node}
    while queue.size > 0
      new_queue = Set(String).new
      queue.each do |src|
        workflows[src].each do |rule|
          d = rule.dst
          result[src] ||= Set(String).new
          result[src].add d
          next if d.in?(["A", "R"])
          new_queue.add d
        end
      end
      queue = new_queue
    end

    if compcating
      prev = result.size
      r = compcat!(result, workflows).size
      while r != prev
        prev = r
        r = compcat!(result, workflows).size
      end
    end

    result
  end

  # Compact graph and workflows to replace nodes with A or R
  def compcat!(graph, workflows)
    replaces = Hash(String, String).new
    graph.each do |src, dsts|
      next if dsts.size != 1
      dst = dsts.to_a.first
      replaces[src] = dst
    end

    replaces.keys.each do |src|
      graph.delete(src)
      workflows.delete(src)
    end

    graph.each do |src, dsts|
      replaces.each do |old_val, new_val|
        while dsts.delete(old_val)
          dsts.add new_val
          workflows[src].map! do |rule|
            rule.dst = new_val if rule.dst == old_val
            rule
          end
        end
      end
    end

    graph
  end

  def count(route : Array(Tuple(Int32, String)), workflows) : Int64
    src = route[0][1]           # => "in"
    negatives = Array(Rule).new # Accumulate rules that should be inversed.
    positives = Array(Rule).new # Accumulate rules that should be executed to make sure pass to the dst node.
    dsts = workflows[src].map(&.dst)
    route[1..].each do |(idx, dst)|
      negatives += workflows[src][0...idx.to_i] if idx.to_i > 0
      positives << workflows[src][idx]
      src = dst
    end

    # Start with default range of values.
    rating = {
      'x' => 1..4000,
      'm' => 1..4000,
      'a' => 1..4000,
      's' => 1..4000,
    }

    negatives.each do |rule|
      case rule.op
      when '<'
        rating[rule.a] = ([rating[rule.a].begin, rule.val].max)..(rating[rule.a].end)
      when '>'
        rating[rule.a] = (rating[rule.a].begin)..([rating[rule.a].end, rule.val].min)
      else
        raise "is not expected"
      end
    end

    positives.each do |rule|
      case rule.op
      when '>'
        rating[rule.a] = Range.new([rating[rule.a].begin, rule.val + 1].max, rating[rule.a].end, rating[rule.a].exclusive?)
      when '<'
        rating[rule.a] = (rating[rule.a].begin)..([rating[rule.a].end, rule.val - 1].min)
        # else
        # It reaches the end of rules and applied all negatives
      end
    end

    rating.values.product { |i| i.end.to_i64 - i.begin.to_i64 + 1_i64 }
  end

  def parse(records)
    workflows = Hash(String, Array(Rule)).new
    ratings = Array(Hash(Char, Int32)).new
    i = 0
    while i < records.size
      instruction = records[i]
      break if instruction == ""

      name, rules = instruction.split("{")
      workflows[name] = rules[0..-2].split(",").map do |r|
        if r.includes?(":")
          formula, dst = r.split(":")
          a = formula[0]
          op = formula[1]
          val = formula[2..].to_i
          Rule.new(a: a, op: op, val: val, dst: dst)
        else
          Rule.new(a: '.', op: '.', val: -1, dst: r)
        end
      end
      i += 1
    end

    i += 1
    while i < records.size
      instruction = records[i]
      h = Hash(Char, Int32).new
      instruction[1..-2].split(",").each do |inst|
        a, b = inst.split("=")
        h[a[0]] = b.to_i
      end
      ratings << h
      i += 1
    end

    {workflows, ratings}
  end

  # Draw dot format of graph and generate image with:
  # pbpaste | dot -Tsvg -Kcirco > output.svg
  def draw_digraph(graph)
    # Draw dot graph
    puts "digraph {"
    graph.each do |src, dsts|
      puts "#{src} -> {#{dsts.reject { |d| d == "R" }.join(" ")}}"
    end
    puts "}"
  end
end
