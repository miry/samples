# https://adventofcode.com/2023/day/3
#
# --- Day 3: Gear Ratios ---
#
# You and the Elf eventually reach a gondola lift station; he says the gondola lift will take you up to the water source, but this is as far as he can bring you. You go inside.
#
# It doesn't take long to find the gondolas, but there seems to be a problem: they're not moving.
#
# "Aaah!"
#
# You turn around to see a slightly-greasy Elf with a wrench and a look of surprise. "Sorry, I wasn't expecting anyone! The gondola lift isn't working right now; it'll still be a while before I can fix it." You offer to help.
#
# The engineer explains that an engine part seems to be missing from the engine, but nobody can figure out which one. If you can add up all the part numbers in the engine schematic, it should be easy to work out which part is missing.
#
# The engine schematic (your puzzle input) consists of a visual representation of the engine. There are lots of numbers and symbols you don't really understand, but apparently any number adjacent to a symbol, even diagonally, is a "part number" and should be included in your sum. (Periods (.) do not count as a symbol.)
#
# Here is an example engine schematic:
#
# 467..114..
# ...*......
# ..35..633.
# ......#...
# 617*......
# .....+.58.
# ..592.....
# ......755.
# ...$.*....
# .664.598..
#
# In this schematic, two numbers are not part numbers because they are not adjacent to a symbol: 114 (top right) and 58 (middle right). Every other number is adjacent to a symbol and so is a part number; their sum is 4361.
#
# Of course, the actual engine schematic is much larger. What is the sum of all of the part numbers in the engine schematic?
#
# Your puzzle answer was 520019.
#
# Output:
# (execute time: 00:00:00.011946458 , memory: 623008 bytes)
#
# --- Part Two ---
#
# The engineer finds the missing part and installs it in the engine! As the engine springs to life, you jump in the closest gondola, finally ready to ascend to the water source.
#
# You don't seem to be going very fast, though. Maybe something is still wrong? Fortunately, the gondola has a phone labeled "help", so you pick it up and the engineer answers.
#
# Before you can explain the situation, she suggests that you look out the window. There stands the engineer, holding a phone in one hand and waving with the other. You're going so slowly that you haven't even left the station. You exit the gondola.
#
# The missing part wasn't the only issue - one of the gears in the engine is wrong. A gear is any * symbol that is adjacent to exactly two part numbers. Its gear ratio is the result of multiplying those two numbers together.
#
# This time, you need to find the gear ratio of every gear and add them all up so that the engineer can figure out which gear needs to be replaced.
#
# Consider the same engine schematic again:
#
# 467..114..
# ...*......
# ..35..633.
# ......#...
# 617*......
# .....+.58.
# ..592.....
# ......755.
# ...$.*....
# .664.598..
#
# In this schematic, there are two gears. The first is in the top left; it has part numbers 467 and 35, so its gear ratio is 16345. The second gear is in the lower right; its gear ratio is 451490. (The * adjacent to 617 is not a gear because it is only adjacent to one part number.) Adding up all of the gear ratios produces 467835.
#
# What is the sum of all of the gear ratios in your engine schematic?
#
# Your puzzle answer was 75519888.
#
# Output:
# (execute time: 00:00:00.013524833 , memory: 1567280 bytes)

require "../utils.cr"

module Day3
  extend self

  # --- Part One ---
  def problem1(records : Array(String))
    map = Hash(Coord, Char).new
    digits = Set(Coord).new

    neighbours = {
      "ne": {-1, -1},
      "n":  {-1, 0},
      "nw": {-1, 1},

      "se": {1, -1},
      "s":  {1, 0},
      "sw": {1, 1},

      "w": {0, -1},
      "e": {0, 1},
    }

    max_x = 0
    lines = records.size
    y = 0
    # Identify digits connected to symbols and build a map
    records.each do |instruction|
      max_x = instruction.size
      instruction.chars.each_with_index do |c, x|
        next if c == '.'

        coord = {x: x, y: y}

        # Process digits
        if c.in?('0'..'9')
          map[coord] = c
          next
        end

        # Process other symbols
        neighbours.values.map { |nc| {x: coord[:x] + nc[1], y: coord[:y] + nc[0]} }.each do |dc|
          digits.add(dc)
        end
      end
      y += 1
    end

    number = 0
    valid = false
    result = 0
    lines.times do |line|
      result += number if number > 0 && valid
      number = 0
      valid = false
      max_x.times do |x|
        coord = {x: x, y: line}
        c = map[coord]?
        if c
          number = number * 10 + (c.ord - '0'.ord)
          valid ||= digits.includes?(coord)
        else
          result += number if number > 0 && valid
          valid = false
          number = 0
        end
      end
    end
    result
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    map = Hash(Coord, Char).new
    symbols = Set(Coord).new

    max_x = 0
    y = 0
    records.each do |instruction|
      max_x = instruction.size
      instruction.chars.each_with_index do |c, x|
        coord = {x: x, y: y}
        map[coord] = c
        symbols.add(coord) if c == '*'
      end
      y += 1
    end

    result = 0

    digit_symbols = '0'..'9'
    symbols.each do |s|
      numbers = [] of Int32
      [
        {x: s[:x] - 1, y: s[:y] - 1},
        {x: s[:x] - 1, y: s[:y]},
        {x: s[:x] - 1, y: s[:y] + 1},
      ].each do |line|
        x, y = line[:x], line[:y]
        while x >= 0 && digit_symbols.includes?(map[{x: x, y: y}])
          x -= 1
        end
        x += 1 if !map.has_key?({x: x, y: y})

        number = 0
        x_max = s[:x] + 1
        last_digit = false
        while map.has_key?({x: x, y: y}) && (x <= x_max || last_digit)
          last_digit = false
          c = map[{x: x, y: y}]
          case c
          when '0'..'9'
            number = number * 10 + (c.ord - '0'.ord)
            last_digit = true
          else
            if number > 0
              numbers << number
              number = 0
            end
          end
          x += 1
        end

        numbers << number if number > 0
      end

      # Calculate the gear number
      if numbers.size > 1
        result += numbers.product
      end
    end

    result
  end
end
