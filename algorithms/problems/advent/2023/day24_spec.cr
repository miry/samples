require "spec"
require "./day24"

describe Day24 do
  sample = <<-EOF.split("\n")
  19, 13, 30 @ -2,  1, -2
  18, 19, 22 @ -1, -1, -2
  20, 25, 34 @ -2, -2, -4
  12, 31, 28 @ -1, -2, -1
  20, 19, 15 @  1, -5, -3
  EOF

  describe "problem1" do
    it "sample" do
      actual = Day24.problem1(sample, 7..27)
      actual.should eq(2)
    end
  end

  describe "problem2" do
    pending "sample" do
      actual = Day24.problem2(sample, 7..27)
      actual.should eq(12)
    end
  end
end
