require "spec"
require "./day19"

describe Day19 do
  sample = <<-EOF.split("\n")
  px{a<2006:qkq,m>2090:A,rfg}
  pv{a>1716:R,A}
  lnx{m>1548:A,A}
  rfg{s<537:gd,x>2440:R,A}
  qs{s>3448:A,lnx}
  qkq{x<1416:A,crn}
  crn{x>2662:A,R}
  in{s<1351:px,qqz}
  qqz{s>2770:qs,m<1801:hdj,R}
  gd{a>3333:R,R}
  hdj{m>838:A,pv}

  {x=787,m=2655,a=1222,s=2876}
  {x=1679,m=44,a=2067,s=496}
  {x=2036,m=264,a=79,s=2244}
  {x=2461,m=1339,a=466,s=291}
  {x=2127,m=1623,a=2188,s=1013}
  EOF

  describe "problem1" do
    it "sample" do
      actual = Day19.problem1(sample)
      actual.should eq(19114)
    end
  end

  describe "problem2" do
    it "sample" do
      actual = Day19.problem2(sample)
      actual.should eq(167409079868000)
    end
  end

  describe "#destinations" do
    it "sample with compacting" do
      workflows, _ratings = Day19.parse(sample)
      graph = Day19.destinations(workflows)
      expected = {
        "in"  => Set{"px", "qqz"},
        "px"  => Set{"qkq", "A", "rfg"},
        "qqz" => Set{"hdj", "R", "A"},
        "qkq" => Set{"A", "crn"},
        "rfg" => Set{"R", "A"},
        "hdj" => Set{"A", "pv"},
        "crn" => Set{"A", "R"},
        "pv"  => Set{"R", "A"},
      }
      graph.should eq(expected)
      graph.each do |src, dsts|
        workflows[src].map(&.dst).sort!.uniq!.should eq(dsts.to_a.sort)
      end
    end

    it "sample without compacting" do
      workflows, _ratings = Day19.parse(sample)
      graph = Day19.destinations(workflows, false)
      expected = {
        "in"  => Set{"px", "qqz"},
        "px"  => Set{"qkq", "A", "rfg"},
        "qqz" => Set{"hdj", "qs", "R"},
        "qkq" => Set{"A", "crn"},
        "rfg" => Set{"gd", "R", "A"},
        "qs"  => Set{"lnx", "A"},
        "hdj" => Set{"A", "pv"},
        "crn" => Set{"A", "R"},
        "lnx" => Set{"A"},
        "pv"  => Set{"R", "A"},
        "gd"  => Set{"R"},
      }
      graph.should eq(expected)
      graph.each do |src, dsts|
        workflows[src].map(&.dst).sort!.uniq!.should eq(dsts.to_a.sort)
      end
    end
  end

  describe "#routes" do
    it "for compacted" do
      workflows, _ratings = Day19.parse(sample)
      graph = Day19.destinations(workflows, true)

      expected = [
        [{0, "in"}, {0, "px"}, {0, "qkq"}, {0, "A"}],
        [{0, "in"}, {0, "px"}, {0, "qkq"}, {1, "crn"}, {0, "A"}],
        [{0, "in"}, {0, "px"}, {1, "A"}],
        [{0, "in"}, {0, "px"}, {2, "rfg"}, {2, "A"}],
        [{0, "in"}, {1, "qqz"}, {0, "A"}],
        [{0, "in"}, {1, "qqz"}, {1, "hdj"}, {0, "A"}],
        [{0, "in"}, {1, "qqz"}, {1, "hdj"}, {1, "pv"}, {1, "A"}],
      ].sort
      actual = Day19.routes(graph, workflows).sort
      actual.should eq(expected)
    end

    it "without compacting" do
      workflows, _ratings = Day19.parse(sample)
      graph = {
        "in"  => Set{"px", "qqz"},
        "px"  => Set{"qkq", "A", "rfg"},
        "qqz" => Set{"hdj", "qs", "R"},
        "qkq" => Set{"A", "crn"},
        "rfg" => Set{"gd", "R", "A"},
        "qs"  => Set{"lnx", "A"},
        "hdj" => Set{"A", "pv"},
        "crn" => Set{"A", "R"},
        "lnx" => Set{"A"},
        "pv"  => Set{"R", "A"},
        "gd"  => Set{"R"},
      }

      expected = [
        [{0, "in"}, {0, "px"}, {0, "qkq"}, {0, "A"}],
        [{0, "in"}, {0, "px"}, {0, "qkq"}, {1, "crn"}, {0, "A"}],
        [{0, "in"}, {0, "px"}, {1, "A"}],
        [{0, "in"}, {0, "px"}, {2, "rfg"}, {2, "A"}],
        [{0, "in"}, {1, "qqz"}, {0, "qs"}, {0, "A"}],
        [{0, "in"}, {1, "qqz"}, {0, "qs"}, {1, "lnx"}, {0, "A"}],
        [{0, "in"}, {1, "qqz"}, {0, "qs"}, {1, "lnx"}, {1, "A"}],
        [{0, "in"}, {1, "qqz"}, {1, "hdj"}, {0, "A"}],
        [{0, "in"}, {1, "qqz"}, {1, "hdj"}, {1, "pv"}, {1, "A"}],
      ].sort
      actual = Day19.routes(graph, workflows).sort
      actual.should eq(expected)
    end
  end

  describe "#count" do
    it "without compacting" do
      workflows, _ratings = Day19.parse(sample)
      {
        [{0, "in"}, {0, "px"}, {1, "A"}]                         => 4000_i64 * 1910_i64 * 1995_i64 * 1350_i64,
        [{0, "in"}, {0, "px"}, {2, "rfg"}, {2, "A"}]             => 2440_i64 * 2090_i64 * 1995_i64 * 814_i64,
        [{0, "in"}, {0, "px"}, {0, "qkq"}, {0, "A"}]             => 1415_i64 * 4000_i64 * 2005_i64 * 1350_i64,
        [{0, "in"}, {0, "px"}, {0, "qkq"}, {1, "crn"}, {0, "A"}] => 14486526000000,
        [{0, "in"}, {1, "qqz"}, {1, "hdj"}, {0, "A"}]            => 4000_i64 * 962_i64 * 4000_i64 * 1420_i64,
        [{0, "in"}, {1, "qqz"}, {1, "hdj"}, {1, "pv"}, {1, "A"}] => 4000_i64 * 838_i64 * 1716_i64 * 1420_i64,
      }.each do |route, expected|
        actual = Day19.count(route, workflows)
        actual.should eq(expected)
      end
    end

    it "with compacting" do
      workflows, _ratings = Day19.parse(sample)
      Day19.destinations(workflows)

      {
        [{0, "in"}, {0, "px"}, {0, "qkq"}, {0, "A"}]             => 1415_i64 * 4000_i64 * 2005_i64 * 1350_i64,
        [{0, "in"}, {0, "px"}, {0, "qkq"}, {1, "crn"}, {0, "A"}] => 14486526000000,
        [{0, "in"}, {0, "px"}, {1, "A"}]                         => 4000_i64 * 1910_i64 * 1995_i64 * 1350_i64,
        [{0, "in"}, {0, "px"}, {2, "rfg"}, {2, "A"}]             => 2440_i64 * 2090_i64 * 1995_i64 * 814_i64,
        [{0, "in"}, {1, "qqz"}, {0, "A"}]                        => 78720000000000,
        [{0, "in"}, {1, "qqz"}, {1, "hdj"}, {0, "A"}]            => 21856640000000,
        [{0, "in"}, {1, "qqz"}, {1, "hdj"}, {1, "pv"}, {1, "A"}] => 8167885440000,
      }.each do |route, expected|
        actual = Day19.count(route, workflows)
        actual.should eq(expected)
      end
    end
  end
end
