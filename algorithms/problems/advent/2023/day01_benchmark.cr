require "spec"
require "benchmark"

require "./day01"

describe Day1 do
  describe ".first_digit" do
    it "faster than regexp to get first" do
      r = Benchmark.ips do |x|
        regexp = /([0-9])/
        x.report("first_digit") { Day1.first_digit("pljnzhchrrqvkncfnfive6four7dzqkfslm9") }
        x.report("regexp") { regexp.match!("pljnzhchrrqvkncfnfive6four7dzqkfslm9")[1] }
        x.report("first_digit_iterator") { Day1.first_digit_iterator("pljnzhchrrqvkncfnfive6four7dzqkfslm9") }
      end
      m1 = r.items.find! { |i| i.label == "first_digit" }
      m2 = r.items.find! { |i| i.label == "regexp" }
      m3 = r.items.find! { |i| i.label == "first_digit_iterator" }
      m1.slower.should be < m2.slower # 76.68× slower
      m1.slower.should be < m3.slower # 15.62× slower
    end

    it "faster than regexp to get last" do
      r = Benchmark.ips do |x|
        regexp = /.*([0-9])/
        x.report("first_digit") { Day1.first_digit("pljnzhchrrqvkncfnfive6four7dzqkfslm9", last: true) }
        x.report("regexp") { regexp.match!("pljnzhchrrqvkncfnfive6four7dzqkfslm9")[1] }
        x.report("first_digit_iterator") { Day1.first_digit_iterator("pljnzhchrrqvkncfnfive6four7dzqkfslm9", last: true) }
      end
      m1 = r.items.find! { |i| i.label == "first_digit" }
      m2 = r.items.find! { |i| i.label == "regexp" }
      m3 = r.items.find! { |i| i.label == "first_digit_iterator" }
      m1.slower.should be < m2.slower # 76.68× slower
      m1.slower.should be < m3.slower # 15.62× slower
    end
  end
end
