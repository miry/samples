require "spec"
require "./day08"

describe Day8 do
  describe "problem1" do
    it "sample one" do
      input = <<-EOF
      RL

      AAA = (BBB, CCC)
      BBB = (DDD, EEE)
      CCC = (ZZZ, GGG)
      DDD = (DDD, DDD)
      EEE = (EEE, EEE)
      GGG = (GGG, GGG)
      ZZZ = (ZZZ, ZZZ)
      EOF
      actual = Day8.problem1(input.split("\n"))
      actual.should eq(2)
    end

    it "sample two" do
      input = <<-EOF
      LLR

      AAA = (BBB, BBB)
      BBB = (AAA, ZZZ)
      ZZZ = (ZZZ, ZZZ)
      EOF
      actual = Day8.problem1(input.split("\n"))
      actual.should eq(6)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      LR

      11A = (11B, XXX)
      11B = (XXX, 11Z)
      11Z = (11B, XXX)
      22A = (22B, XXX)
      22B = (22C, 22C)
      22C = (22Z, 22Z)
      22Z = (22B, 22B)
      XXX = (XXX, XXX)
      EOF
      actual = Day8.problem2(input.split("\n"))
      actual.should eq(6)
    end
  end
end
