# https://adventofcode.com/2023/day/12
#
# NOTE: The problem similar to previous year. Use Dynmic programming to make a suggestion.
#       Started doing optimised version and then build bruetforce solution for verification.
#       The each sample is independent, used parallisation.
#
# --- Day 12: Hot Springs ---
#
# You finally reach the hot springs! You can see steam rising from secluded areas attached to the primary, ornate building.
#
# As you turn to enter, the researcher stops you. "Wait - I thought you were looking for the hot springs, weren't you?" You indicate that this definitely looks like hot springs to you.
#
# "Oh, sorry, common mistake! This is actually the onsen! The hot springs are next door."
#
# You look in the direction the researcher is pointing and suddenly notice the massive metal helixes towering overhead. "This way!"
#
# It only takes you a few more steps to reach the main gate of the massive fenced-off area containing the springs. You go through the gate and into a small administrative building.
#
# "Hello! What brings you to the hot springs today? Sorry they're not very hot right now; we're having a lava shortage at the moment." You ask about the missing machine parts for Desert Island.
#
# "Oh, all of Gear Island is currently offline! Nothing is being manufactured at the moment, not until we get more lava to heat our forges. And our springs. The springs aren't very springy unless they're hot!"
#
# "Say, could you go up and see why the lava stopped flowing? The springs are too cold for normal operation, but we should be able to find one springy enough to launch you up there!"
#
# There's just one problem - many of the springs have fallen into disrepair, so they're not actually sure which springs would even be safe to use! Worse yet, their condition records of which springs are damaged (your puzzle input) are also damaged! You'll need to help them repair the damaged records.
#
# In the giant field just outside, the springs are arranged into rows. For each row, the condition records show every spring and whether it is operational (.) or damaged (#). This is the part of the condition records that is itself damaged; for some springs, it is simply unknown (?) whether the spring is operational or damaged.
#
# However, the engineer that produced the condition records also duplicated some of this information in a different format! After the list of springs for a given row, the size of each contiguous group of damaged springs is listed in the order those groups appear in the row. This list always accounts for every damaged spring, and each number is the entire size of its contiguous group (that is, groups are always separated by at least one operational spring: #### would always be 4, never 2,2).
#
# So, condition records with no unknown spring conditions might look like this:
#
# #.#.### 1,1,3
# .#...#....###. 1,1,3
# .#.###.#.###### 1,3,1,6
# ####.#...#... 4,1,1
# #....######..#####. 1,6,5
# .###.##....# 3,2,1
#
# However, the condition records are partially damaged; some of the springs' conditions are actually unknown (?). For example:
#
# ???.### 1,1,3
# .??..??...?##. 1,1,3
# ?#?#?#?#?#?#?#? 1,3,1,6
# ????.#...#... 4,1,1
# ????.######..#####. 1,6,5
# ?###???????? 3,2,1
#
# Equipped with this information, it is your job to figure out how many different arrangements of operational and broken springs fit the given criteria in each row.
#
# In the first line (???.### 1,1,3), there is exactly one way separate groups of one, one, and three broken springs (in that order) can appear in that row: the first three unknown springs must be broken, then operational, then broken (#.#), making the whole row #.#.###.
#
# The second line is more interesting: .??..??...?##. 1,1,3 could be a total of four different arrangements. The last ? must always be broken (to satisfy the final contiguous group of three broken springs), and each ?? must hide exactly one of the two broken springs. (Neither ?? could be both broken springs or they would form a single contiguous group of two; if that were true, the numbers afterward would have been 2,3 instead.) Since each ?? can either be #. or .#, there are four possible arrangements of springs.
#
# The last line is actually consistent with ten different arrangements! Because the first number is 3, the first and second ? must both be . (if either were #, the first number would have to be 4 or higher). However, the remaining run of unknown spring conditions have many different ways they could hold groups of two and one broken springs:
#
# ?###???????? 3,2,1
# .###.##.#...
# .###.##..#..
# .###.##...#.
# .###.##....#
# .###..##.#..
# .###..##..#.
# .###..##...#
# .###...##.#.
# .###...##..#
# .###....##.#
#
# In this example, the number of possible arrangements for each row is:
#
#     ???.### 1,1,3 - 1 arrangement
#     .??..??...?##. 1,1,3 - 4 arrangements
#     ?#?#?#?#?#?#?#? 1,3,1,6 - 1 arrangement
#     ????.#...#... 4,1,1 - 1 arrangement
#     ????.######..#####. 1,6,5 - 4 arrangements
#     ?###???????? 3,2,1 - 10 arrangements
#
# Adding all of the possible arrangement counts together produces a total of 21 arrangements.
#
# For each row, count all of the different arrangements of operational and broken springs that meet the given criteria. What is the sum of those counts?
#
# Your puzzle answer was 8419.
#
# Output:
# Answer: 8419
# (execute time: 54.776083ms, memory: 14173856 bytes)
#
# --- Part Two ---
#
# As you look out at the field of springs, you feel like there are way more springs than the condition records list. When you examine the records, you discover that they were actually folded up this whole time!
#
# To unfold the records, on each row, replace the list of spring conditions with five copies of itself (separated by ?) and replace the list of contiguous groups of damaged springs with five copies of itself (separated by ,).
#
# So, this row:
#
# .# 1
#
# Would become:
#
# .#?.#?.#?.#?.# 1,1,1,1,1
#
# The first line of the above example would become:
#
# ???.###????.###????.###????.###????.### 1,1,3,1,1,3,1,1,3,1,1,3,1,1,3
#
# In the above example, after unfolding, the number of possible arrangements for some rows is now much larger:
#
#     ???.### 1,1,3 - 1 arrangement
#     .??..??...?##. 1,1,3 - 16384 arrangements
#     ?#?#?#?#?#?#?#? 1,3,1,6 - 1 arrangement
#     ????.#...#... 4,1,1 - 16 arrangements
#     ????.######..#####. 1,6,5 - 2500 arrangements
#     ?###???????? 3,2,1 - 506250 arrangements
#
# After unfolding, adding all of the possible arrangement counts together produces 525152.
#
# Unfold your condition records; what is the new sum of possible arrangement counts?
#
# Your puzzle answer was 160500973317706.
#
# Output:
# Answer: 160500973317706
# (execute time: 1761.556167ms, memory: 360789728 bytes)

require "log"
require "../utils.cr"

module Day12
  extend self

  @@cache2 = Hash(Tuple(Array(Char), Array(Int32)), Int32).new

  # --- Part One ---
  def problem1(records : Array(String))
    rows = records.map do |instruction|
      schema_raw, damaged_raw = instruction.split
      schema : Array(Char) = schema_raw.chars
      damaged : Array(Int32) = damaged_raw.split(',').map &.to_i
      {schema, damaged}
    end
    # pp! rows

    rows.sum do |r|
      arrangements(r[0], r[1])
    end
  end

  def cache_clear
    @@cache2.clear
  end

  # arrangements2 search for all matches with bruetforce.
  # Build for testing performant version.
  def arrangements2(schema, damaged, i, compiled = nil)
    # puts "> arrangements2 (#{schema}, #{damaged} #{i})"
    cache_key = {schema, damaged}
    if @@cache2.has_key?(cache_key)
      return @@cache2[cache_key]
    end

    if compiled.nil?
      dst = damaged.map { |j| "#" * j }.join("\\.+")
      compiled = /^\.*#{dst}\.*$/
    end

    if schema.size == i
      r = compiled.match(schema.join) ? 1 : 0
      @@cache2[cache_key] = r
      return r
    end

    return 0 if damaged.size == 0 && schema.includes?('#')

    while i < schema.size && schema[i] != '?'
      i += 1
    end

    if i == schema.size
      return arrangements2(schema, damaged, i, compiled)
    end

    s1 = schema.dup
    s1[i] = '.'
    result = arrangements2(s1, damaged, i + 1, compiled)
    s2 = schema.dup
    s2[i] = '#'
    result += arrangements2(s2, damaged, i + 1, compiled)

    @@cache2[cache_key] = result
    result
  end

  def arrangements(schema : Array(Char), damaged : Array(Int32), cache : Hash(Tuple(Array(Char), Array(Int32)), UInt128) = Hash(Tuple(Array(Char), Array(Int32)), UInt128).new) : UInt128
    cache_key = {schema, damaged}
    if cache.has_key?(cache_key)
      return cache[cache_key]
    end

    stripped = lstrip(schema, damaged)
    if stripped.nil?
      cache[cache_key] = 0_u128
      return 0_u128
    end

    s, d = stripped
    if d.size == 0 && !s.includes?('#')
      cache[cache_key] = 1_u128
      return 1_u128
    end

    stripped = lstrip(s.reverse, d.reverse)
    if stripped.nil?
      cache[cache_key] = 0_u128
      return 0_u128
    end
    s, d = stripped[0].reverse, stripped[1].reverse

    result = 0_u128
    if d.size == 0
      result = s.includes?('#') ? 0_u128 : 1_u128
      cache[cache_key] = result
      return result
    end

    min_elements = d.sum + d.size - 1
    if min_elements > s.size
      cache[cache_key] = 0_u128
      return 0_u128
    end

    orig = s
    s = orig.dup
    s[0] = '.'
    result = arrangements(s, d, cache)
    s = orig.dup

    # If put # , it means should have the same size as for first element
    if !s[0...d.first].includes?('.')
      s.fill('#', 0, d.first)
      result += arrangements(s, d, cache)
    end

    cache[cache_key] = result
    result
  end

  def lstrip(schema, damaged)
    # puts "  > strip (#{schema}, #{damaged})"
    return nil if schema.size == 0 && damaged.size > 0
    s, d = schema.dup, damaged.dup
    i = 0
    num_damaged : Int32 = 0
    while i < s.size
      while i < s.size && s[i] == '.'
        i += 1
      end
      if i > 0
        s.delete_at(0, i)
        i = 0
        next
      end

      if d.size == 0
        return nil if s.includes?('#')
        return {[] of Char, d}
      end

      return nil if d.sum + (d.size - 1) > s.size

      num_damaged = d.first
      symbols = s[0...num_damaged].uniq.sort!
      if s[0] == '?'
        # The sample: ?.?# [2]
        if symbols.includes?('.')
          s.delete_at(0)
          next
        end
        return {s, d}
      end
      d.shift
      # From here s[0] -> '#'

      return nil if symbols.includes?('.')

      return nil if num_damaged > s.size

      if num_damaged == s.size
        return nil if d.size > 1
        return [] of Char, [] of Int32
      end

      # Not valid input if next one is not . or ?
      return nil if s[num_damaged] == '#'

      # Found a correct range and could be eliminated
      s.delete_at(0, num_damaged + 1)
    end

    if d.size == 0
      return nil if s.includes?('#')
      return [] of Char, d
    end
    # From here d.size > 0

    return nil if s.size == 0
    return {s, d}
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    schema, damaged = Array(Char).new, Array(Int32).new
    rows = records.map do |instruction|
      schema_raw, damaged_raw = instruction.split
      schema = schema_raw.chars
      damaged = damaged_raw.split(',').map &.to_i
      {schema, damaged}
    end

    rows.sum do |r|
      schema, damaged = unfold_records(r[0], r[1])
      arrangements(schema, damaged)
    end
  end

  def unfold_records(s, d)
    rs = s.dup
    4.times do
      rs += ['?'] + s
    end
    {rs, d * 5}
  end
end
