require "spec"
require "./day21"

describe Day21 do
  sample = <<-EOF.split("\n")
  ...........
  .....###.#.
  .###.##..#.
  ..#.#...#..
  ....#.#....
  .##..S####.
  .##..#...#.
  .......##..
  .##.#.####.
  .##..##.##.
  ...........
  EOF

  describe "problem1" do
    it "sample" do
      actual = Day21.problem1(sample, 6)
      actual.should eq(16)
    end
  end

  describe "problem2" do
    it "sample" do
      {
           6 => 16,
          10 => 50,  # +20
          12 => 74,  # +24
          14 => 99,  # +25
          16 => 129, # +30
          50 => 1594,
         100 => 6536,
         500 => 167004,
        1000 => 668697,
        # 5000 => 16733044,
      }.each do |steps, expected|
        Day21.problem2(sample, steps).should eq(expected)
      end
    end

    it "quadratic offset 0" do
      input_file_path = "input/day21.txt"
      pending("missing input file") if !File.exists?(input_file_path)
      input = File.open(input_file_path)
      entries = [] of String
      input.each_line do |line|
        entries << line
      end

      {
                65 => 3802,
        65 + 131   => 33732,
        65 + 131*2 => 93480,
      }.each do |steps, expected|
        Day21.problem2(entries, steps).should eq(expected)
      end
    end
  end
end
