# https://adventofcode.com/2023/day/13
#
# --- Day 13: Point of Incidence ---
#
# With your help, the hot springs team locates an appropriate spring which launches you neatly and precisely up to the edge of Lava Island.
#
# There's just one problem: you don't see any lava.
#
# You do see a lot of ash and igneous rock; there are even what look like gray mountains scattered around. After a while, you make your way to a nearby cluster of mountains only to discover that the valley between them is completely full of large mirrors. Most of the mirrors seem to be aligned in a consistent way; perhaps you should head in that direction?
#
# As you move through the valley of mirrors, you find that several of them have fallen from the large metal frames keeping them in place. The mirrors are extremely flat and shiny, and many of the fallen mirrors have lodged into the ash at strange angles. Because the terrain is all one color, it's hard to tell where it's safe to walk or where you're about to run into a mirror.
#
# You note down the patterns of ash (.) and rocks (#) that you see as you walk (your puzzle input); perhaps by carefully analyzing these patterns, you can figure out where the mirrors are!
#
# For example:
#
# #.##..##.
# ..#.##.#.
# ##......#
# ##......#
# ..#.##.#.
# ..##..##.
# #.#.##.#.
#
# #...##..#
# #....#..#
# ..##..###
# #####.##.
# #####.##.
# ..##..###
# #....#..#
#
# To find the reflection in each pattern, you need to find a perfect reflection across either a horizontal line between two rows or across a vertical line between two columns.
#
# In the first pattern, the reflection is across a vertical line between two columns; arrows on each of the two columns point at the line between the columns:
#
# 123456789
#     ><
# #.##..##.
# ..#.##.#.
# ##......#
# ##......#
# ..#.##.#.
# ..##..##.
# #.#.##.#.
#     ><
# 123456789
#
# In this pattern, the line of reflection is the vertical line between columns 5 and 6. Because the vertical line is not perfectly in the middle of the pattern, part of the pattern (column 1) has nowhere to reflect onto and can be ignored; every other column has a reflected column within the pattern and must match exactly: column 2 matches column 9, column 3 matches 8, 4 matches 7, and 5 matches 6.
#
# The second pattern reflects across a horizontal line instead:
#
# 1 #...##..# 1
# 2 #....#..# 2
# 3 ..##..### 3
# 4v#####.##.v4
# 5^#####.##.^5
# 6 ..##..### 6
# 7 #....#..# 7
#
# This pattern reflects across the horizontal line between rows 4 and 5. Row 1 would reflect with a hypothetical row 8, but since that's not in the pattern, row 1 doesn't need to match anything. The remaining rows match: row 2 matches row 7, row 3 matches row 6, and row 4 matches row 5.
#
# To summarize your pattern notes, add up the number of columns to the left of each vertical line of reflection; to that, also add 100 multiplied by the number of rows above each horizontal line of reflection. In the above example, the first pattern's vertical line has 5 columns to its left and the second pattern's horizontal line has 4 rows above it, a total of 405.
#
# Find the line of reflection in each of the patterns in your notes. What number do you get after summarizing all of your notes?
#
# Your puzzle answer was 26957.
#
# Output:
# Answer: 26957
# (execute time: 15.930083ms, memory: 1800096 bytes)
#
# --- Part Two ---
#
# You resume walking through the valley of mirrors and - SMACK! - run directly into one. Hopefully nobody was watching, because that must have been pretty embarrassing.
#
# Upon closer inspection, you discover that every mirror has exactly one smudge: exactly one . or # should be the opposite type.
#
# In each pattern, you'll need to locate and fix the smudge that causes a different reflection line to be valid. (The old reflection line won't necessarily continue being valid after the smudge is fixed.)
#
# Here's the above example again:
#
# #.##..##.
# ..#.##.#.
# ##......#
# ##......#
# ..#.##.#.
# ..##..##.
# #.#.##.#.
#
# #...##..#
# #....#..#
# ..##..###
# #####.##.
# #####.##.
# ..##..###
# #....#..#
#
# The first pattern's smudge is in the top-left corner. If the top-left # were instead ., it would have a different, horizontal line of reflection:
#
# 1 ..##..##. 1
# 2 ..#.##.#. 2
# 3v##......#v3
# 4^##......#^4
# 5 ..#.##.#. 5
# 6 ..##..##. 6
# 7 #.#.##.#. 7
#
# With the smudge in the top-left corner repaired, a new horizontal line of reflection between rows 3 and 4 now exists. Row 7 has no corresponding reflected row and can be ignored, but every other row matches exactly: row 1 matches row 6, row 2 matches row 5, and row 3 matches row 4.
#
# In the second pattern, the smudge can be fixed by changing the fifth symbol on row 2 from . to #:
#
# 1v#...##..#v1
# 2^#...##..#^2
# 3 ..##..### 3
# 4 #####.##. 4
# 5 #####.##. 5
# 6 ..##..### 6
# 7 #....#..# 7
#
# Now, the pattern has a different horizontal line of reflection between rows 1 and 2.
#
# Summarize your notes as before, but instead use the new different reflection lines. In this example, the first pattern's new horizontal line has 3 rows above it and the second pattern's new horizontal line has 1 row above it, summarizing to the value 400.
#
# In each pattern, fix the smudge and find the different line of reflection. What number do you get after summarizing the new reflection line in each pattern in your notes?
#
# Your puzzle answer was 42695.
#
# Output:
# Answer: 42695
# (execute time: 16.407625ms, memory: 1770736 bytes)

require "../utils.cr"

module Day13
  extend self

  def parse(records)
    plan = Hash(Coord, Char).new
    y = 0
    width = 0
    plan_by_col = Array(String).new
    plan_by_row = Array(String).new
    plans = Array(Tuple(Array(String), Array(String))).new
    records.each do |instruction|
      if instruction.size == 0
        width.times do |col|
          str = Array(Char).new(y)
          y.times do |row|
            str << plan[{x: col, y: row}]
          end
          plan_by_col << str.join
        end
        plans << {plan_by_row, plan_by_col}

        plan = Hash(Coord, Char).new
        y = 0
        width = 0
        plan_by_col = Array(String).new
        plan_by_row = Array(String).new
        next
      end

      plan_by_row << instruction
      instruction.chars.each_with_index do |c, x|
        plan[{x: x, y: y}] = c
      end
      width = [width, instruction.size].max
      y += 1
    end

    width.times do |col|
      str = Array(Char).new(y)
      y.times do |row|
        str << plan[{x: col, y: row}]
      end
      plan_by_col << str.join
    end
    plans << {plan_by_row, plan_by_col}

    plans
  end

  # --- Part One ---
  def problem1(records : Array(String))
    plans = parse(records)

    result = 0

    plans.each do |(plan_by_row, plan_by_col)|
      reflected = detect_reflection(plan_by_col)
      if reflected
        result += (reflected + 1)
        next
      end
      reflected = detect_reflection(plan_by_row)
      if reflected
        result += (reflected + 1) * 100
        next
      end
      raise "could not find reflection"
    end

    result
  end

  def detect_reflection(arr)
    k = 0
    last_indx = arr.size - 1
    found_middle = false
    while k < last_indx
      if arr[k] == arr[k + 1]
        found_middle = true

        i = k - 1
        j = k + 2
        while i >= 0 && j < arr.size
          if arr[i] == arr[j]
            i -= 1
            j += 1
            next
          end

          found_middle = false
          break
        end

        return k if found_middle
      end
      k += 1
    end
    nil
  end

  def detect_reflection_with_smuged(arr)
    k = 0
    last_indx = arr.size - 1
    found_smuged = nil
    found_middle = false
    while k < last_indx
      if arr[k] == arr[k + 1]
        found_middle = true
        found_smuged = false

        i = k - 1
        j = k + 2
        while i >= 0 && j < arr.size
          if arr[i] == arr[j]
            i -= 1
            j += 1
            next
          end

          if !found_smuged && similiar(arr[i], arr[j])
            found_smuged = true
            i -= 1
            j += 1
            next
          end

          found_middle = false
          break
        end

        if found_middle
          return k if found_smuged
          found_middle = false
        end
      elsif similiar(arr[k], arr[k + 1])
        found_middle = true
        found_smuged = true

        i = k - 1
        j = k + 2
        while i >= 0 && j < arr.size
          if arr[i] == arr[j]
            i -= 1
            j += 1
            next
          end

          found_middle = false
          break
        end

        return k if found_middle
      end
      k += 1
    end
    nil
  end

  def similiar(str, other)
    return false if (str.count('#') - other.count('#')).abs != 1
    diffs = 0
    str.size.times do |i|
      diffs += 1 if str[i] != other[i]
    end
    diffs == 1
  end

  # --- Part Two ---
  def problem2(records : Array(String))
    plans = parse(records)

    result = 0
    plans.each do |(plan_by_row, plan_by_col)|
      reflected = detect_reflection_with_smuged(plan_by_col)
      if reflected
        result += (reflected + 1)
        next
      end
      reflected = detect_reflection_with_smuged(plan_by_row)
      if reflected
        result += (reflected + 1) * 100
        next
      end
      raise "could not find reflection"
    end
    result
  end
end
