require "spec"
require "./day25"

describe Day25 do
  sample = <<-EOF.split("\n")
  jqt: rhn xhk nvd
  rsh: frs pzl lsr
  xhk: hfx
  cmg: qnr nvd lhk bvb
  rhn: xhk bvb hfx
  bvb: xhk hfx
  pzl: lsr hfx nvd
  qnr: nvd
  ntq: jqt hfx bvb xhk
  nvd: lhk
  lsr: lhk
  rzs: qnr cmg lsr rsh
  frs: qnr lhk lsr
  EOF

  describe "problem1" do
    it "sample" do
      actual = Day25.problem1(sample)
      actual.should eq(54)
    end
  end

  describe "problem2" do
    pending "sample" do
      input = <<-EOF
      <sample>
      EOF
      actual = Day25.problem2(input.split("\n"))
      actual.should eq(12)
    end
  end
end
