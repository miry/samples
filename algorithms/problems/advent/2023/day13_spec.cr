require "spec"
require "./day13"

describe Day13 do
  sample = <<-EOF
  #.##..##.
  ..#.##.#.
  ##......#
  ##......#
  ..#.##.#.
  ..##..##.
  #.#.##.#.

  #...##..#
  #....#..#
  ..##..###
  #####.##.
  #####.##.
  ..##..###
  #....#..#
  EOF

  describe "problem1" do
    it "sample" do
      actual = Day13.problem1(sample.split("\n"))
      actual.should eq(405)
    end
  end

  describe "#detect_reflection" do
    it "detect by col" do
      arr = <<-EOP.split("\n")
      ##...###..#.###
      ##...###..#.##.
      ##...###..#.##.
      ##...###..#.###
      ...###..###..#.
      #.###.##....#.#
      .#.#.#...#.#..#
      EOP
      actual = Day13.detect_reflection(arr)
      actual.should eq(1)
    end

    it "detect 0 by row" do
      arr = <<-EOP.split("\n")
      ##.#.##
      #.##...
      .######
      ##....#
      #.##.#.
      .#.####
      .#.####
      #.##.#.
      ##....#
      .######
      #.##..#
      ##.#.##
      ..###..
      ..###..
      ##.#.##
      #.##..#
      .######
      EOP
      actual = Day13.detect_reflection(arr)
      actual.should eq(12)
    end

    it "detect by last col" do
      arr = <<-EOP.split("\n")
      ###....
      ##..###
      .####..
      .#.....
      ####.##
      .....##
      #..####
      .#...##
      #...###
      .###...
      .#..###
      ####.##
      ....###
      ....###
      ###..##
      EOP
      actual = Day13.detect_reflection(arr)
      actual.should be_nil
    end

    it "detect by row 2" do
      arr = <<-EOP.split("\n")
      ###.#.##.#.##
      ..##.####.##.
      ##...#..#...#
      .....#..#....
      ####.####.#.#
      ..#.#.##.#.#.
      #####....####
      ..####..####.
      ..##########.
      ##..##..##..#
      ...#......#..
      ...#.####.#..
      ######..#####
      ###.######.##
      ##..##..##..#
      EOP
      actual = Day13.detect_reflection(arr)
      actual.should be_nil
    end

    it "detect 5 by col" do
      arr = <<-EOP.split("\n")
      #.#.#.#..#..###
      #.#.#.#..#..###
      ##..#####...##.
      .#..#.###.###..
      #....#####..###
      .####..###.####
      ##..##..#..#.#.
      ##..##..#..#.#.
      .####..###.####
      #....#####..###
      .#..#.###.###..
      ##...####...##.
      #.#.#.#..#..###
      EOP
      actual = Day13.detect_reflection(arr)
      actual.should eq(0)
    end
  end

  describe "problem2" do
    it "sample" do
      actual = Day13.problem2(sample.split("\n"))
      actual.should eq(400)
    end
  end
end
