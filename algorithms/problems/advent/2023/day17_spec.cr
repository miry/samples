require "spec"
require "./day17"

describe Day17 do
  samples = <<-EOF.split("\n")
  2413432311323
  3215453535623
  3255245654254
  3446585845452
  4546657867536
  1438598798454
  4457876987766
  3637877979653
  4654967986887
  4564679986453
  1224686865563
  2546548887735
  4322674655533
  EOF

  describe "problem1" do
    it "sample" do
      actual = Day17.problem1(samples)
      actual.should eq(102)
    end
  end

  describe "problem2" do
    it "sample" do
      actual = Day17.problem2(samples)
      actual.should eq(94)
    end

    it "sample 2" do
      input = <<-EOF.split("\n")
      111111111111
      999999999991
      999999999991
      999999999991
      999999999991
      EOF
      Day17.problem2(input).should eq(71)
    end
  end
end
