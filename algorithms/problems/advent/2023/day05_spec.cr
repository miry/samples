require "spec"
require "./day05"

describe Day5 do
  describe "problem1" do
    it "sample" do
      input = <<-EOF
      seeds: 79 14 55 13

      seed-to-soil map:
      50 98 2
      52 50 48

      soil-to-fertilizer map:
      0 15 37
      37 52 2
      39 0 15

      fertilizer-to-water map:
      49 53 8
      0 11 42
      42 0 7
      57 7 4

      water-to-light map:
      88 18 7
      18 25 70

      light-to-temperature map:
      45 77 23
      81 45 19
      68 64 13

      temperature-to-humidity map:
      0 69 1
      1 0 69

      humidity-to-location map:
      60 56 37
      56 93 4
      EOF
      actual = Day5.problem1(input.split("\n"))
      actual.should eq(35)
    end
  end

  describe "problem2" do
    it "sample" do
      input = <<-EOF
      seeds: 79 14 55 13

      seed-to-soil map:
      50 98 2
      52 50 48

      soil-to-fertilizer map:
      0 15 37
      37 52 2
      39 0 15

      fertilizer-to-water map:
      49 53 8
      0 11 42
      42 0 7
      57 7 4

      water-to-light map:
      88 18 7
      18 25 70

      light-to-temperature map:
      45 77 23
      81 45 19
      68 64 13

      temperature-to-humidity map:
      0 69 1
      1 0 69

      humidity-to-location map:
      60 56 37
      56 93 4
      EOF
      actual = Day5.problem2(input.split("\n"))
      actual.should eq(46)
    end

    it "fully contains and calculate min" do
      input = <<-EOF
      seeds: 79 14

      seed-to-soil map:
      50 98 2
      52 50 48
      EOF
      actual = Day5.problem2(input.split("\n"))
      actual.should eq(81)
    end

    it "fully contains" do
      input = <<-EOF
      seeds: 0 14

      seed-to-soil map:
      100 1 14
      EOF
      actual = Day5.problem2(input.split("\n"))
      actual.should eq(0)
    end

    it "one to one" do
      input = <<-EOF
      seeds: 1 14

      seed-to-soil map:
      100 1 14
      EOF
      actual = Day5.problem2(input.split("\n"))
      actual.should eq(100)
    end

    it "more rules" do
      input = <<-EOF
      seeds: 10 20

      seed-to-soil map:
      50 10 10
      1 20 50
      EOF
      actual = Day5.problem2(input.split("\n"))
      actual.should eq(1)
    end

    it "single seed split" do
      input = <<-EOF
      seeds: 100 100

      seed-to-soil map:
      10 110 20
      60 150 30
      EOF
      actual = Day5.problem2(input.split("\n"))
      actual.should eq(10)
    end
  end

  describe "#transform" do
    it "return min seed for level 0" do
      seed = 10_i64..20_i64
      destinations = [{10_i64...15_i64 => -5_i64, 15_i64...40_i64 => 20_i64}]
      Day5.transform(seed, destinations, 0).should eq(10)
    end

    it "match first" do
      seed = 10_i64...20_i64
      destinations = [{10_i64...15_i64 => -5_i64, 15_i64...40_i64 => 20_i64}]
      Day5.transform(seed, destinations, 1).should eq(5)
    end
  end
end
