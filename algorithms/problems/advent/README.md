# Advent of Code

### Start a new year advent of code

```shell
$ rake generate
```

Generate for specific Year:

```shell
$ rake generate YEAR=2015
```

## Solving

All tasks execute in the current year folder.

### Run solution for current day

```shell
$ rake run
```

### Run all solutions

```shell
$ rake run:all
```

### Run test for current day

```shell
$ rake test
```

### Run all tests

```shell
$ rake test:all
```

### Format code

```shell
$ rake fmt
```

### Download current day input

Before using the command first create file `cookies.txt` in the current advent folder with format:

```text
session=<value of session cookie>
```

The value could be extracted from Browser networking tab.
Then run command to create an input file:

```shell
$ rake download
```

Download for specific day. Example for day 4:

```shell
$ rake "download[4]"
```

Download base on input file name:

```shell
$ rake input/day04.txt
```

Refresh all input files:

```shell
$ for i in input/day* ; do rake $i ; done
```
