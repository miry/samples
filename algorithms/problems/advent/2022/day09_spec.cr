require "spec"
require "./day09"

describe "Day 9" do
  describe "problem9" do
    it "sample" do
      input = <<-EOF
      R 4
      U 4
      L 3
      D 1
      R 4
      D 1
      L 5
      R 2
      EOF
      actual = problem9(input.split("\n"))
      actual.should eq(13)
    end
  end

  describe "problem9_part_two" do
    it "sample" do
      input = <<-EOF
      R 5
      U 8
      L 8
      D 3
      R 17
      D 10
      L 25
      U 20
      EOF
      actual = problem9_part_two(input.split("\n"))
      actual.should eq(36)
    end
  end
end
