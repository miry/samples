require "spec"
require "./day19"

def create_blueprint
  robots = [
    Day19::Robot.new(Day19::ResourceType::Ore, costs: {Day19::ResourceType::Ore => 4_u8, Day19::ResourceType::Clay => 0_u8, Day19::ResourceType::Obsidian => 0_u8}),
    Day19::Robot.new(Day19::ResourceType::Clay, costs: {Day19::ResourceType::Ore => 2_u8, Day19::ResourceType::Clay => 0_u8, Day19::ResourceType::Obsidian => 0_u8}),
    Day19::Robot.new(Day19::ResourceType::Obsidian, costs: {Day19::ResourceType::Ore => 3_u8, Day19::ResourceType::Clay => 14_u8, Day19::ResourceType::Obsidian => 0_u8}),
    Day19::Robot.new(Day19::ResourceType::Geode, costs: {Day19::ResourceType::Ore => 2_u8, Day19::ResourceType::Clay => 0_u8, Day19::ResourceType::Obsidian => 7_u8}),
  ]
  blueprint = Day19::Blueprint.new(1_u8)
  robots.each do |robot|
    blueprint.add_robot(robot)
  end
  blueprint
end

def create_blueprint_two
  robots = [
    Day19::Robot.new(Day19::ResourceType::Ore, costs: {Day19::ResourceType::Ore => 2_u8, Day19::ResourceType::Clay => 0_u8, Day19::ResourceType::Obsidian => 0_u8}),
    Day19::Robot.new(Day19::ResourceType::Clay, costs: {Day19::ResourceType::Ore => 3_u8, Day19::ResourceType::Clay => 0_u8, Day19::ResourceType::Obsidian => 0_u8}),
    Day19::Robot.new(Day19::ResourceType::Obsidian, costs: {Day19::ResourceType::Ore => 3_u8, Day19::ResourceType::Clay => 8_u8, Day19::ResourceType::Obsidian => 0_u8}),
    Day19::Robot.new(Day19::ResourceType::Geode, costs: {Day19::ResourceType::Ore => 3_u8, Day19::ResourceType::Clay => 0_u8, Day19::ResourceType::Obsidian => 12_u8}),
  ] of Day19::Robot
  blueprint = Day19::Blueprint.new(1_u8)
  robots.each do |robot|
    blueprint.add_robot(robot)
  end
  blueprint
end

describe Day19 do
  describe "problem19" do
    pending "sample" do
      input = <<-EOF
      Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
      Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.
      EOF
      actual = problem19(input.split("\n"))
      actual.should eq(33)
    end
  end

  describe "problem19_part_two" do
    pending "sample" do
      input = <<-EOF
      Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
      Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.
      EOF
      actual = problem19_part_two(input.split("\n"))
      actual.should eq(62*56)
    end
  end

  describe Day19::Blueprint do
    describe ".initialize" do
      it "specify id" do
        blueprint = Day19::Blueprint.new(1_u8)
        blueprint.id.should eq(1)
      end
    end

    describe ".add_robot" do
      it "adds ore robot" do
        cost = {
          Day19::ResourceType::Ore      => 4_u8,
          Day19::ResourceType::Clay     => 0_u8,
          Day19::ResourceType::Obsidian => 0_u8,
        }
        robot = Day19::Robot.new(Day19::ResourceType::Ore, cost)
        blueprint = Day19::Blueprint.new(1_u8)
        blueprint.add_robot(robot)
      end
    end

    describe "sample blueprint 1" do
      it "verify 1st minute state" do
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(1_u8)
        geodes.should eq(0_u8)
      end

      it "verify 2nd minute state" do
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(2_u8)
        geodes.should eq(0_u8)
      end

      it "verify 3rd minute state" do
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(3_u8)
        geodes.should eq(0_u8)
      end

      it "verify 7th minute state, because it requires at least 7 obsiadian" do
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(7_u8)
        geodes.should eq(0_u8)
      end

      it "verify 11th minute state, because the first obsidian built" do
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(11_u8)
        geodes.should eq(0)
      end

      it "verify 18th minute state, because the first geode-cracking robot built" do
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(18_u8)
        geodes.should eq(0)
      end

      it "verify 19th minute state, because the first geode is cracked" do
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(19_u8)
        geodes.should eq(1)
      end

      it "verify 20th minute state should have 2 geodes" do
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(20_u8)
        geodes.should eq(2)
      end

      it "verify 21th minute state should have 3 geodes" do
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(21_u8)
        geodes.should eq(3)
      end

      it "verify 24th minute state should have 9 geodes" do
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(24_u8)
        geodes.should eq(9)
      end

      pending "verify 32nd minute state should have 9 geodes" do
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(32_u8)
        geodes.should eq(56)
      end
    end

    describe "sample blueprint 2" do
      it "verify 24th minute state should have 3 geodes" do
        blueprint = create_blueprint_two
        geodes = blueprint.open_geodes(24)
        geodes.should eq(12)
      end
    end

    describe "estimate production" do
      it "not enough obsidian with 4 days left" do
        # Each obsidian robot costs 3 ore and 14 clay.
        # cache_key = {4, {"ore" => 1, "clay" => 5, "obsidian" => 1, "geode" =>0_u8}, {"ore" => 1, "clay" => 21, "obsidian" =>0_u8, "geode" =>0_u8}}
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(4,
          robots: StaticArray[1_u8, 5_u8, 1_u8, 0_u8],
          resources: StaticArray[2_u8, 21_u8, 0_u8, 0_u8],
        )
        geodes.should eq(0)
      end

      it "crack first geode" do
        # Each obsidian robot costs 3 ore and 14 clay.
        # Each geode robot costs 2 ore and 7 obsidian.
        # cache_key = {2, {"ore" => 1, "clay" => 4, "obsidian" => 2, "geode" =>0_u8}, {"ore" => 3, "clay" => 16, "obsidian" => 7, "geode" =>0_u8}}
        blueprint = create_blueprint
        geodes = blueprint.open_geodes(2,
          robots: StaticArray[1_u8, 4_u8, 2_u8, 0_u8],
          resources: StaticArray[3_u8, 16_u8, 7_u8, 0_u8],
        )
        geodes.should eq(1_u8)
      end
    end
  end
end
