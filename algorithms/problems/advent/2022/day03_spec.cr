require "spec"
require "./day03"

describe "Day 3" do
  describe "problem3" do
    it "sample" do
      input = <<-EOF
      vJrwpWtwJgWrhcsFMMfFFhFp
      jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
      PmmdzqPrVvPwwTWBwg
      wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
      ttgJtRGJQctTZtZT
      CrZsJsPPZsGzwwsLwLmpwMDw
      EOF
      actual = problem3(input.split("\n"))
      actual.should eq(157)
    end
  end

  describe "problem3_part_two" do
    input = <<-EOF
      vJrwpWtwJgWrhcsFMMfFFhFp
      jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
      PmmdzqPrVvPwwTWBwg
      wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
      ttgJtRGJQctTZtZT
      CrZsJsPPZsGzwwsLwLmpwMDw
      EOF
    actual = problem3_part_two(input.split("\n"))
    actual.should eq(70)
  end
end
