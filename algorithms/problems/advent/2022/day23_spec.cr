require "spec"
require "./day23"

describe "Day 23" do
  describe "problem23" do
    it "sample1" do
      input = <<-EOF
      .....
      ..##.
      ..#..
      .....
      ..##.
      .....
      EOF
      actual = problem23(input.split("\n"))
      actual.should eq(25)
    end

    it "sample2" do
      input = <<-EOF
      ..............
      ..............
      .......#......
      .....###.#....
      ...#...#.#....
      ....#...##....
      ...#.###......
      ...##.#.##....
      ....#..#......
      ..............
      ..............
      ..............
      EOF
      actual = problem23(input.split("\n"))
      actual.should eq(110)
    end
  end

  describe "problem23_part_two" do
    it "sample" do
      input = <<-EOF
      ..............
      ..............
      .......#......
      .....###.#....
      ...#...#.#....
      ....#...##....
      ...#.###......
      ...##.#.##....
      ....#..#......
      ..............
      ..............
      ..............
      EOF
      actual = problem23_part_two(input.split("\n"))
      actual.should eq(20)
    end
  end
end
