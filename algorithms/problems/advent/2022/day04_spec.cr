require "spec"
require "./day04"
require "../utils"

describe "Day 4" do
  describe "Range" do
    it "overlaps another range" do
      r1 = Range.new(2, 10)
      r2 = Range.new(2, 9)
      r1.overlap?(r2).should be_true
    end

    it "overlaps with bigger range" do
      r1 = Range.new(2, 10)
      r2 = Range.new(2, 9)
      r2.overlap?(r1).should be_true
    end

    it "overlaps with bigger range within begin and end" do
      r1 = Range.new(2, 10)
      r2 = Range.new(3, 9)
      r2.overlap?(r1).should be_true
    end

    it "does not contain with bigger range" do
      r1 = Range.new(2, 10)
      r2 = Range.new(2, 9)
      r2.contains?(r1).should be_false
    end

    it "contains smaller range" do
      r1 = Range.new(2, 10)
      r2 = Range.new(2, 9)
      r1.contains?(r2).should be_true
    end

    it "contains same range" do
      r1 = Range.new(2, 9)
      r2 = Range.new(2, 9)
      r1.contains?(r2).should be_true
    end
  end

  describe "problem4" do
    it "sample" do
      input = <<-EOS
      2-4,6-8
      2-3,4-5
      5-7,7-9
      2-8,3-7
      6-6,4-6
      2-6,4-8
      EOS
      actual = problem4(input.split("\n"))
      actual.should eq(2)
    end
  end

  describe "problem4_part_two" do
    it "sample" do
      input = <<-EOS
      2-4,6-8
      2-3,4-5
      5-7,7-9
      2-8,3-7
      6-6,4-6
      2-6,4-8
      EOS
      actual = problem4_part_two(input.split("\n"))
      actual.should eq(4)
    end
  end
end
