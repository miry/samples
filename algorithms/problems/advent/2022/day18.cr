# https://adventofcode.com/2022/day/18
#
# --- Day 18: Boiling Boulders ---
#
# You and the elephants finally reach fresh air. You've emerged near the base of a large volcano that seems to be actively erupting! Fortunately, the lava seems to be flowing away from you and toward the ocean.
#
# Bits of lava are still being ejected toward you, so you're sheltering in the cavern exit a little longer. Outside the cave, you can see the lava landing in a pond and hear it loudly hissing as it solidifies.
#
# Depending on the specific compounds in the lava and speed at which it cools, it might be forming obsidian! The cooling rate should be based on the surface area of the lava droplets, so you take a quick scan of a droplet as it flies past you (your puzzle input).
#
# Because of how quickly the lava is moving, the scan isn't very good; its resolution is quite low and, as a result, it approximates the shape of the lava droplet with 1x1x1 cubes on a 3D grid, each given as its x,y,z position.
#
# To approximate the surface area, count the number of sides of each cube that are not immediately connected to another cube. So, if your scan were only two adjacent cubes like 1,1,1 and 2,1,1, each cube would have a single side covered and five sides exposed, a total surface area of 10 sides.
#
# Here's a larger example:
#
# 2,2,2
# 1,2,2
# 3,2,2
# 2,1,2
# 2,3,2
# 2,2,1
# 2,2,3
# 2,2,4
# 2,2,6
# 1,2,5
# 3,2,5
# 2,1,5
# 2,3,5
#
# In the above example, after counting up all the sides that aren't connected to another cube, the total surface area is 64.
#
# What is the surface area of your scanned lava droplet?
#
# Your puzzle answer was 4320.
# --- Part Two ---
#
# Something seems off about your calculation. The cooling rate depends on exterior surface area, but your calculation also included the surface area of air pockets trapped in the lava droplet.
#
# Instead, consider only cube sides that could be reached by the water and steam as the lava droplet tumbles into the pond. The steam will expand to reach as much as possible, completely displacing any air on the outside of the lava droplet but never expanding diagonally.
#
# In the larger example above, exactly one cube of air is trapped within the lava droplet (at 2,2,5), so the exterior surface area of the lava droplet is 58.
#
# What is the exterior surface area of your scanned lava droplet?
#
# Your puzzle answer was 2456.

# Generate plots with different dots: edge, hole, original. Debug with gnuplot for cases.
# $ gnuplot
# gnuplot> set xrange [14:16]; set yrange [6:10]; set zrange [17:19]; splot 'plot.txt' pt 6, 'plot_edges.txt' pt 5, 'plot_holes.txt' pt 4

require "../utils"

def problem18(records : Array(String))
  droplets = Array(Tuple(Int32, Int32, Int32)).new
  records.each do |instruction|
    next if instruction == ""
    droplets << Tuple(Int32, Int32, Int32).from(instruction.split(",").map(&.to_i32))
  end
  droplets

  # Need to build a global edges of dropplets and count sides.
  # Other thought is to count all edges and remove the one that has common to two dropplets.
  # Each dropplet has 6 sides. If two dropplets has common side -2.

  total = droplets.size * 6
  # Compare each dropllet with every other dropplet.
  droplets.each_combination(2) do |a|
    l, r = a
    found = 0
    not = false

    3.times do |i|
      if l[i] == r[i]
        found += 1
        next
      end

      if (l[i] - r[i]).abs > 1
        not = true
        break
      end
    end
    next if not

    total -= 2 if found == 2
  end

  total
end

# --- Part Two ---

def problem18_part_two(records : Array(String))
  droplets = Set(Tuple(Int32, Int32, Int32)).new
  records.each do |instruction|
    next if instruction == ""
    droplets.add Tuple(Int32, Int32, Int32).from(instruction.split(",").map(&.to_i32))
  end
  droplets

  # Find only edges and exclude internal dropplets.
  # How to find only edges?
  # The internal dropllets should have internal x,y,z.
  #
  #  x1,y1,z1 <-> xn,y1,z1
  #  x1,y1,z1 <-> x1,yn,z1
  #  x1,y1,z1 <-> x1,y1,zn
  #
  # Internal dropplet should have all sides conntacted with others.
  # Then exclude the one that is inside!

  edges = Hash(Tuple(Int32, Int32, Int32), Set(Tuple(Int32, Int32, Int32))).new
  xdropplets = Hash(Tuple(Int32, Int32), Array(Range(Int32, Int32))).new
  ydropplets = Hash(Tuple(Int32, Int32), Array(Range(Int32, Int32))).new
  zdropplets = Hash(Tuple(Int32, Int32), Array(Range(Int32, Int32))).new

  # Create a ranges of the object and replace dropplets with ranges
  droplets.each do |droplet|
    xdropplets[{droplet[1], droplet[2]}] ||= Array(Range(Int32, Int32)).new
    xdropplets[{droplet[1], droplet[2]}] << Range.new(droplet[0], droplet[0])
    xdropplets[{droplet[1], droplet[2]}].sort! { |a, b| a.begin <=> b.begin }
    xdropplets[{droplet[1], droplet[2]}] = xdropplets[{droplet[1], droplet[2]}].reduce(Array(Range(Int32, Int32)).new) do |a, b|
      l = a.size == 0 ? nil : a.pop
      l ? (a += (l + b)) : (a = [b])
      a
    end

    ydropplets[{droplet[0], droplet[2]}] ||= Array(Range(Int32, Int32)).new
    ydropplets[{droplet[0], droplet[2]}] << Range.new(droplet[1], droplet[1])
    ydropplets[{droplet[0], droplet[2]}].sort! { |a, b| a.begin <=> b.begin }
    ydropplets[{droplet[0], droplet[2]}] = ydropplets[{droplet[0], droplet[2]}].reduce(Array(Range(Int32, Int32)).new) do |a, b|
      l = a.size == 0 ? nil : a.pop
      l ? (a += (l + b)) : (a = [b])
      a
    end
    zdropplets[{droplet[0], droplet[1]}] ||= Array(Range(Int32, Int32)).new
    zdropplets[{droplet[0], droplet[1]}] << Range.new(droplet[2], droplet[2])
    zdropplets[{droplet[0], droplet[1]}].sort! { |a, b| a.begin <=> b.begin }
    zdropplets[{droplet[0], droplet[1]}] = zdropplets[{droplet[0], droplet[1]}].reduce(Array(Range(Int32, Int32)).new) do |a, b|
      l = a.size == 0 ? nil : a.pop
      l ? (a += (l + b)) : (a = [b])
      a
    end
  end

  # After analyze the output of x,y,z dropplets found it would be enough to have only single x
  # Find the line with holes in it
  # Add for each hole remove extra 6 sides! if it is single dropplet.
  # Assume for now it is single dropplet -> it works only for test input, but in my sample there are holes connected
  # think to fill the main dropplets with this holes and count again
  # filled_holes_droplets = droplets.dup

  xinverted = Hash(Tuple(Int32, Int32), Array(Range(Int32, Int32))).new
  xdropplets.each do |yz, xrange|
    if xrange.size > 1
      xinverted[yz] = Array(Range(Int32, Int32)).new
      xrange.each_cons_pair do |a, b|
        xinverted[yz] << Range.new(a.end + 1, b.begin - 1)
      end
    end
  end

  yinverted = Hash(Tuple(Int32, Int32), Array(Range(Int32, Int32))).new
  ydropplets.each do |yz, xrange|
    if xrange.size > 1
      yinverted[yz] = Array(Range(Int32, Int32)).new
      xrange.each_cons_pair do |a, b|
        yinverted[yz] << Range.new(a.end + 1, b.begin - 1)
      end
    end
  end

  zinverted = Hash(Tuple(Int32, Int32), Array(Range(Int32, Int32))).new
  zdropplets.each do |yz, xrange|
    if xrange.size > 1
      zinverted[yz] = Array(Range(Int32, Int32)).new
      xrange.each_cons_pair do |a, b|
        zinverted[yz] << Range.new(a.end + 1, b.begin - 1)
      end
    end
  end

  holes = Set(Tuple(Int32, Int32, Int32)).new
  # Fill holes with missing ranges
  xinverted.keys.sort! { |a, b| a[0] <=> b[0] }.each do |yz|
    xranges = xinverted[yz]
    xranges.each do |range|
      range.each do |x|
        droplet = {x, yz[0], yz[1]}
        # puts "Missing block: #{droplet}"
        # Check if this missing also in ydropplets
        if !yinverted.has_key?({droplet[0], droplet[2]})
          # puts "not such thing in yinverted!#{{droplet[0], droplet[2]}}"
          next
        end

        if !(yinverted[{droplet[0], droplet[2]}].any? { |r| r.includes?(droplet[1]) })
          # puts "found missed in x, but includes in y: #{yinverted[{droplet[0], droplet[2]}]}"
          next
        end

        if !(zinverted[{droplet[0], droplet[1]}].any? { |r| r.includes?(droplet[2]) })
          # puts "found missed in x, but includes in z: #{zinverted[{droplet[0], droplet[1]}]}"
          next
        end
        holes.add(droplet)
      end
    end
  end

  # Exclude wrong holes, that could have edge case when it is marked as valid, but does not have from sides wall or holes
  wrong_hole = {0, 0, 0}
  directions = [
    {0, 0, 1},
    {0, 0, -1},
    {0, 1, 0},
    {0, -1, 0},
    {1, 0, 0},
    {-1, 0, 0},
  ]
  while !wrong_hole.nil?
    wrong_hole = nil
    holes.each do |droplet|
      r = directions.all? do |direction|
        plot = {droplet[0] + direction[0], droplet[1] + direction[1], droplet[2] + direction[2]}
        holes.includes?(plot) || droplets.includes?(plot)
      end
      if !r
        wrong_hole = droplet
        break
      end
    end
    holes.delete(wrong_hole) if wrong_hole
  end

  filled_holes_droplets = holes + droplets

  # Compare each dropllet with every other dropplet.
  filled_holes_droplets.to_a.each_combination(2) do |a|
    l, r = a
    edges[l] ||= Set(Tuple(Int32, Int32, Int32)).new
    edges[r] ||= Set(Tuple(Int32, Int32, Int32)).new

    found = 0
    not = false

    3.times do |i|
      if l[i] == r[i]
        found += 1
        next
      end

      if (l[i] - r[i]).abs > 1
        not = true
        break
      end
    end
    next if not

    if found == 2
      edges[l].add(r)
      edges[r].add(l)
    end
  end

  # puts "--- Holes for gnuplot"
  # holes.each do |droplet|
  #   puts droplet.to_a.join(" ")
  # end
  # puts "---"

  # Exclude all dropplets with 6 sides
  total = 0
  edges.each do |_, v|
    total += 6 - v.size
  end

  total
end
