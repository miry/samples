require "spec"
require "./day13"

describe "Day 13" do
  describe "problem13" do
    it "sample" do
      input = <<-EOF
      [1,1,3,1,1]
      [1,1,5,1,1]

      [[1],[2,3,4]]
      [[1],4]

      [9]
      [[8,7,6]]

      [[4,4],4,4]
      [[4,4],4,4,4]

      [7,7,7,7]
      [7,7,7]

      []
      [3]

      [[[]]]
      [[]]

      [1,[2,[3,[4,[5,6,7]]]],8,9]
      [1,[2,[3,[4,[5,6,0]]]],8,9]
      EOF
      actual = problem13(input.split("\n"))
      actual.should eq(13)
    end
  end

  describe "problem13_part_two" do
    it "sample" do
      input = <<-EOF
      [1,1,3,1,1]
      [1,1,5,1,1]

      [[1],[2,3,4]]
      [[1],4]

      [9]
      [[8,7,6]]

      [[4,4],4,4]
      [[4,4],4,4,4]

      [7,7,7,7]
      [7,7,7]

      []
      [3]

      [[[]]]
      [[]]

      [1,[2,[3,[4,[5,6,7]]]],8,9]
      [1,[2,[3,[4,[5,6,0]]]],8,9]
      EOF
      actual = problem13_part_two(input.split("\n"))
      actual.should eq(140)
    end
  end
end
