# https://adventofcode.com/2022/day/19
#
# --- Day 19: Not Enough Minerals ---
#
# Your scans show that the lava did indeed form obsidian!
#
# The wind has changed direction enough to stop sending lava droplets toward you, so you and the elephants exit the cave. As you do, you notice a collection of geodes around the pond. Perhaps you could use the obsidian to create some geode-cracking robots and break them open?
#
# To collect the obsidian from the bottom of the pond, you'll need waterproof obsidian-collecting robots. Fortunately, there is an abundant amount of clay nearby that you can use to make them waterproof.
#
# In order to harvest the clay, you'll need special-purpose clay-collecting robots. To make any type of robot, you'll need ore, which is also plentiful but in the opposite direction from the clay.
#
# Collecting ore requires ore-collecting robots with big drills. Fortunately, you have exactly one ore-collecting robot in your pack that you can use to kickstart the whole operation.
#
# Each robot can collect 1 of its resource type per minute. It also takes one minute for the robot factory (also conveniently from your pack) to construct any type of robot, although it consumes the necessary resources available when construction begins.
#
# The robot factory has many blueprints (your puzzle input) you can choose from, but once you've configured it with a blueprint, you can't change it. You'll need to work out which blueprint is best.
#
# For example:
#
# Blueprint 1:
#   Each ore robot costs 4 ore.
#   Each clay robot costs 2 ore.
#   Each obsidian robot costs 3 ore and 14 clay.
#   Each geode robot costs 2 ore and 7 obsidian.
#
# Blueprint 2:
#   Each ore robot costs 2 ore.
#   Each clay robot costs 3 ore.
#   Each obsidian robot costs 3 ore and 8 clay.
#   Each geode robot costs 3 ore and 12 obsidian.
#
# (Blueprints have been line-wrapped here for legibility. The robot factory's actual assortment of blueprints are provided one blueprint per line.)
#
# The elephants are starting to look hungry, so you shouldn't take too long; you need to figure out which blueprint would maximize the number of opened geodes after 24 minutes by figuring out which robots to build and when to build them.
#
# Using blueprint 1 in the example above, the largest number of geodes you could open in 24 minutes is 9. One way to achieve that is:
#
# == Minute 1 ==
# 1 ore-collecting robot collects 1 ore; you now have 1 ore.
#
# == Minute 2 ==
# 1 ore-collecting robot collects 1 ore; you now have 2 ore.
#
# == Minute 3 ==
# Spend 2 ore to start building a clay-collecting robot.
# 1 ore-collecting robot collects 1 ore; you now have 1 ore.
# The new clay-collecting robot is ready; you now have 1 of them.
#
# == Minute 4 ==
# 1 ore-collecting robot collects 1 ore; you now have 2 ore.
# 1 clay-collecting robot collects 1 clay; you now have 1 clay.
#
# == Minute 5 ==
# Spend 2 ore to start building a clay-collecting robot.
# 1 ore-collecting robot collects 1 ore; you now have 1 ore.
# 1 clay-collecting robot collects 1 clay; you now have 2 clay.
# The new clay-collecting robot is ready; you now have 2 of them.
#
# == Minute 6 ==
# 1 ore-collecting robot collects 1 ore; you now have 2 ore.
# 2 clay-collecting robots collect 2 clay; you now have 4 clay.
#
# == Minute 7 ==
# Spend 2 ore to start building a clay-collecting robot.
# 1 ore-collecting robot collects 1 ore; you now have 1 ore.
# 2 clay-collecting robots collect 2 clay; you now have 6 clay.
# The new clay-collecting robot is ready; you now have 3 of them.
#
# == Minute 8 ==
# 1 ore-collecting robot collects 1 ore; you now have 2 ore.
# 3 clay-collecting robots collect 3 clay; you now have 9 clay.
#
# == Minute 9 ==
# 1 ore-collecting robot collects 1 ore; you now have 3 ore.
# 3 clay-collecting robots collect 3 clay; you now have 12 clay.
#
# == Minute 10 ==
# 1 ore-collecting robot collects 1 ore; you now have 4 ore.
# 3 clay-collecting robots collect 3 clay; you now have 15 clay.
#
# == Minute 11 ==
# Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
# 1 ore-collecting robot collects 1 ore; you now have 2 ore.
# 3 clay-collecting robots collect 3 clay; you now have 4 clay.
# The new obsidian-collecting robot is ready; you now have 1 of them.
#
# == Minute 12 ==
# Spend 2 ore to start building a clay-collecting robot.
# 1 ore-collecting robot collects 1 ore; you now have 1 ore.
# 3 clay-collecting robots collect 3 clay; you now have 7 clay.
# 1 obsidian-collecting robot collects 1 obsidian; you now have 1 obsidian.
# The new clay-collecting robot is ready; you now have 4 of them.
#
# == Minute 13 ==
# 1 ore-collecting robot collects 1 ore; you now have 2 ore.
# 4 clay-collecting robots collect 4 clay; you now have 11 clay.
# 1 obsidian-collecting robot collects 1 obsidian; you now have 2 obsidian.
#
# == Minute 14 ==
# 1 ore-collecting robot collects 1 ore; you now have 3 ore.
# 4 clay-collecting robots collect 4 clay; you now have 15 clay.
# 1 obsidian-collecting robot collects 1 obsidian; you now have 3 obsidian.
#
# == Minute 15 ==
# Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
# 1 ore-collecting robot collects 1 ore; you now have 1 ore.
# 4 clay-collecting robots collect 4 clay; you now have 5 clay.
# 1 obsidian-collecting robot collects 1 obsidian; you now have 4 obsidian.
# The new obsidian-collecting robot is ready; you now have 2 of them.
#
# == Minute 16 ==
# 1 ore-collecting robot collects 1 ore; you now have 2 ore.
# 4 clay-collecting robots collect 4 clay; you now have 9 clay.
# 2 obsidian-collecting robots collect 2 obsidian; you now have 6 obsidian.
#
# == Minute 17 ==
# 1 ore-collecting robot collects 1 ore; you now have 3 ore.
# 4 clay-collecting robots collect 4 clay; you now have 13 clay.
# 2 obsidian-collecting robots collect 2 obsidian; you now have 8 obsidian.
#
# == Minute 18 ==
# Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
# 1 ore-collecting robot collects 1 ore; you now have 2 ore.
# 4 clay-collecting robots collect 4 clay; you now have 17 clay.
# 2 obsidian-collecting robots collect 2 obsidian; you now have 3 obsidian.
# The new geode-cracking robot is ready; you now have 1 of them.
#
# == Minute 19 ==
# 1 ore-collecting robot collects 1 ore; you now have 3 ore.
# 4 clay-collecting robots collect 4 clay; you now have 21 clay.
# 2 obsidian-collecting robots collect 2 obsidian; you now have 5 obsidian.
# 1 geode-cracking robot cracks 1 geode; you now have 1 open geode.
#
# == Minute 20 ==
# 1 ore-collecting robot collects 1 ore; you now have 4 ore.
# 4 clay-collecting robots collect 4 clay; you now have 25 clay.
# 2 obsidian-collecting robots collect 2 obsidian; you now have 7 obsidian.
# 1 geode-cracking robot cracks 1 geode; you now have 2 open geodes.
#
# == Minute 21 ==
# Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
# 1 ore-collecting robot collects 1 ore; you now have 3 ore.
# 4 clay-collecting robots collect 4 clay; you now have 29 clay.
# 2 obsidian-collecting robots collect 2 obsidian; you now have 2 obsidian.
# 1 geode-cracking robot cracks 1 geode; you now have 3 open geodes.
# The new geode-cracking robot is ready; you now have 2 of them.
#
# == Minute 22 ==
# 1 ore-collecting robot collects 1 ore; you now have 4 ore.
# 4 clay-collecting robots collect 4 clay; you now have 33 clay.
# 2 obsidian-collecting robots collect 2 obsidian; you now have 4 obsidian.
# 2 geode-cracking robots crack 2 geodes; you now have 5 open geodes.
#
# == Minute 23 ==
# 1 ore-collecting robot collects 1 ore; you now have 5 ore.
# 4 clay-collecting robots collect 4 clay; you now have 37 clay.
# 2 obsidian-collecting robots collect 2 obsidian; you now have 6 obsidian.
# 2 geode-cracking robots crack 2 geodes; you now have 7 open geodes.
#
# == Minute 24 ==
# 1 ore-collecting robot collects 1 ore; you now have 6 ore.
# 4 clay-collecting robots collect 4 clay; you now have 41 clay.
# 2 obsidian-collecting robots collect 2 obsidian; you now have 8 obsidian.
# 2 geode-cracking robots crack 2 geodes; you now have 9 open geodes.
#
# However, by using blueprint 2 in the example above, you could do even better: the largest number of geodes you could open in 24 minutes is 12.
#
# Determine the quality level of each blueprint by multiplying that blueprint's ID number with the largest number of geodes that can be opened in 24 minutes using that blueprint. In this example, the first blueprint has ID 1 and can open 9 geodes, so its quality level is 9. The second blueprint has ID 2 and can open 12 geodes, so its quality level is 24. Finally, if you add up the quality levels of all of the blueprints in the list, you get 33.
#
# Determine the quality level of each blueprint using the largest number of geodes it could produce in 24 minutes. What do you get if you add up the quality level of all of the blueprints in your list?
#
# Your puzzle answer was 1294.
# Output:
# --- Part One ---
# Blueprint 1 opens 2 geodes 2 quality
# Blueprint 2 opens 0 geodes 0 quality
# Blueprint 3 opens 4 geodes 12 quality
# Blueprint 4 opens 4 geodes 16 quality
# Blueprint 5 opens 0 geodes 0 quality
# Blueprint 6 opens 1 geodes 6 quality
# Blueprint 7 opens 3 geodes 21 quality
# Blueprint 8 opens 5 geodes 40 quality
# Blueprint 9 opens 0 geodes 0 quality
# Blueprint 10 opens 0 geodes 0 quality
# Blueprint 11 opens 0 geodes 0 quality
# Blueprint 12 opens 0 geodes 0 quality
# Blueprint 13 opens 1 geodes 13 quality
# Blueprint 14 opens 0 geodes 0 quality
# Blueprint 15 opens 2 geodes 30 quality
# Blueprint 16 opens 2 geodes 32 quality
# Blueprint 17 opens 1 geodes 17 quality
# Blueprint 18 opens 3 geodes 54 quality
# Blueprint 19 opens 1 geodes 19 quality
# Blueprint 20 opens 4 geodes 80 quality
# Blueprint 21 opens 5 geodes 105 quality
# Blueprint 22 opens 8 geodes 176 quality
# Blueprint 23 opens 3 geodes 69 quality
# Blueprint 24 opens 3 geodes 72 quality
# Blueprint 25 opens 0 geodes 0 quality
# Blueprint 26 opens 5 geodes 130 quality
# Blueprint 27 opens 0 geodes 0 quality
# Blueprint 28 opens 9 geodes 252 quality
# Blueprint 29 opens 2 geodes 58 quality
# Blueprint 30 opens 3 geodes 90 quality
# Answer: 1294
# (execute time: 00:00:09.661575417)
# Memory:
#   before:   RSS 2960
#   after:    RSS 598864
#
# The first half of this puzzle is complete! It provides one gold star: *
# --- Part Two ---
#
# While you were choosing the best blueprint, the elephants found some food on their own, so you're not in as much of a hurry; you figure you probably have 32 minutes before the wind changes direction again and you'll need to get out of range of the erupting volcano.
#
# Unfortunately, one of the elephants ate most of your blueprint list! Now, only the first three blueprints in your list are intact.
#
# In 32 minutes, the largest number of geodes blueprint 1 (from the example above) can open is 56. One way to achieve that is:
#
# == Minute 1 ==
# 1 ore-collecting robot collects 1 ore; you now have 1 ore.
#
# == Minute 2 ==
# 1 ore-collecting robot collects 1 ore; you now have 2 ore.
#
# == Minute 3 ==
# 1 ore-collecting robot collects 1 ore; you now have 3 ore.
#
# == Minute 4 ==
# 1 ore-collecting robot collects 1 ore; you now have 4 ore.
#
# == Minute 5 ==
# Spend 4 ore to start building an ore-collecting robot.
# 1 ore-collecting robot collects 1 ore; you now have 1 ore.
# The new ore-collecting robot is ready; you now have 2 of them.
#
# == Minute 6 ==
# 2 ore-collecting robots collect 2 ore; you now have 3 ore.
#
# == Minute 7 ==
# Spend 2 ore to start building a clay-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 3 ore.
# The new clay-collecting robot is ready; you now have 1 of them.
#
# == Minute 8 ==
# Spend 2 ore to start building a clay-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 3 ore.
# 1 clay-collecting robot collects 1 clay; you now have 1 clay.
# The new clay-collecting robot is ready; you now have 2 of them.
#
# == Minute 9 ==
# Spend 2 ore to start building a clay-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 3 ore.
# 2 clay-collecting robots collect 2 clay; you now have 3 clay.
# The new clay-collecting robot is ready; you now have 3 of them.
#
# == Minute 10 ==
# Spend 2 ore to start building a clay-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 3 ore.
# 3 clay-collecting robots collect 3 clay; you now have 6 clay.
# The new clay-collecting robot is ready; you now have 4 of them.
#
# == Minute 11 ==
# Spend 2 ore to start building a clay-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 3 ore.
# 4 clay-collecting robots collect 4 clay; you now have 10 clay.
# The new clay-collecting robot is ready; you now have 5 of them.
#
# == Minute 12 ==
# Spend 2 ore to start building a clay-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 3 ore.
# 5 clay-collecting robots collect 5 clay; you now have 15 clay.
# The new clay-collecting robot is ready; you now have 6 of them.
#
# == Minute 13 ==
# Spend 2 ore to start building a clay-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 3 ore.
# 6 clay-collecting robots collect 6 clay; you now have 21 clay.
# The new clay-collecting robot is ready; you now have 7 of them.
#
# == Minute 14 ==
# Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 2 ore.
# 7 clay-collecting robots collect 7 clay; you now have 14 clay.
# The new obsidian-collecting robot is ready; you now have 1 of them.
#
# == Minute 15 ==
# 2 ore-collecting robots collect 2 ore; you now have 4 ore.
# 7 clay-collecting robots collect 7 clay; you now have 21 clay.
# 1 obsidian-collecting robot collects 1 obsidian; you now have 1 obsidian.
#
# == Minute 16 ==
# Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 3 ore.
# 7 clay-collecting robots collect 7 clay; you now have 14 clay.
# 1 obsidian-collecting robot collects 1 obsidian; you now have 2 obsidian.
# The new obsidian-collecting robot is ready; you now have 2 of them.
#
# == Minute 17 ==
# Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 2 ore.
# 7 clay-collecting robots collect 7 clay; you now have 7 clay.
# 2 obsidian-collecting robots collect 2 obsidian; you now have 4 obsidian.
# The new obsidian-collecting robot is ready; you now have 3 of them.
#
# == Minute 18 ==
# 2 ore-collecting robots collect 2 ore; you now have 4 ore.
# 7 clay-collecting robots collect 7 clay; you now have 14 clay.
# 3 obsidian-collecting robots collect 3 obsidian; you now have 7 obsidian.
#
# == Minute 19 ==
# Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 3 ore.
# 7 clay-collecting robots collect 7 clay; you now have 7 clay.
# 3 obsidian-collecting robots collect 3 obsidian; you now have 10 obsidian.
# The new obsidian-collecting robot is ready; you now have 4 of them.
#
# == Minute 20 ==
# Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
# 2 ore-collecting robots collect 2 ore; you now have 3 ore.
# 7 clay-collecting robots collect 7 clay; you now have 14 clay.
# 4 obsidian-collecting robots collect 4 obsidian; you now have 7 obsidian.
# The new geode-cracking robot is ready; you now have 1 of them.
#
# == Minute 21 ==
# Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
# 2 ore-collecting robots collect 2 ore; you now have 2 ore.
# 7 clay-collecting robots collect 7 clay; you now have 7 clay.
# 4 obsidian-collecting robots collect 4 obsidian; you now have 11 obsidian.
# 1 geode-cracking robot cracks 1 geode; you now have 1 open geode.
# The new obsidian-collecting robot is ready; you now have 5 of them.
#
# == Minute 22 ==
# Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
# 2 ore-collecting robots collect 2 ore; you now have 2 ore.
# 7 clay-collecting robots collect 7 clay; you now have 14 clay.
# 5 obsidian-collecting robots collect 5 obsidian; you now have 9 obsidian.
# 1 geode-cracking robot cracks 1 geode; you now have 2 open geodes.
# The new geode-cracking robot is ready; you now have 2 of them.
#
# == Minute 23 ==
# Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
# 2 ore-collecting robots collect 2 ore; you now have 2 ore.
# 7 clay-collecting robots collect 7 clay; you now have 21 clay.
# 5 obsidian-collecting robots collect 5 obsidian; you now have 7 obsidian.
# 2 geode-cracking robots crack 2 geodes; you now have 4 open geodes.
# The new geode-cracking robot is ready; you now have 3 of them.
#
# == Minute 24 ==
# Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
# 2 ore-collecting robots collect 2 ore; you now have 2 ore.
# 7 clay-collecting robots collect 7 clay; you now have 28 clay.
# 5 obsidian-collecting robots collect 5 obsidian; you now have 5 obsidian.
# 3 geode-cracking robots crack 3 geodes; you now have 7 open geodes.
# The new geode-cracking robot is ready; you now have 4 of them.
#
# == Minute 25 ==
# 2 ore-collecting robots collect 2 ore; you now have 4 ore.
# 7 clay-collecting robots collect 7 clay; you now have 35 clay.
# 5 obsidian-collecting robots collect 5 obsidian; you now have 10 obsidian.
# 4 geode-cracking robots crack 4 geodes; you now have 11 open geodes.
#
# == Minute 26 ==
# Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
# 2 ore-collecting robots collect 2 ore; you now have 4 ore.
# 7 clay-collecting robots collect 7 clay; you now have 42 clay.
# 5 obsidian-collecting robots collect 5 obsidian; you now have 8 obsidian.
# 4 geode-cracking robots crack 4 geodes; you now have 15 open geodes.
# The new geode-cracking robot is ready; you now have 5 of them.
#
# == Minute 27 ==
# Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
# 2 ore-collecting robots collect 2 ore; you now have 4 ore.
# 7 clay-collecting robots collect 7 clay; you now have 49 clay.
# 5 obsidian-collecting robots collect 5 obsidian; you now have 6 obsidian.
# 5 geode-cracking robots crack 5 geodes; you now have 20 open geodes.
# The new geode-cracking robot is ready; you now have 6 of them.
#
# == Minute 28 ==
# 2 ore-collecting robots collect 2 ore; you now have 6 ore.
# 7 clay-collecting robots collect 7 clay; you now have 56 clay.
# 5 obsidian-collecting robots collect 5 obsidian; you now have 11 obsidian.
# 6 geode-cracking robots crack 6 geodes; you now have 26 open geodes.
#
# == Minute 29 ==
# Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
# 2 ore-collecting robots collect 2 ore; you now have 6 ore.
# 7 clay-collecting robots collect 7 clay; you now have 63 clay.
# 5 obsidian-collecting robots collect 5 obsidian; you now have 9 obsidian.
# 6 geode-cracking robots crack 6 geodes; you now have 32 open geodes.
# The new geode-cracking robot is ready; you now have 7 of them.
#
# == Minute 30 ==
# Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
# 2 ore-collecting robots collect 2 ore; you now have 6 ore.
# 7 clay-collecting robots collect 7 clay; you now have 70 clay.
# 5 obsidian-collecting robots collect 5 obsidian; you now have 7 obsidian.
# 7 geode-cracking robots crack 7 geodes; you now have 39 open geodes.
# The new geode-cracking robot is ready; you now have 8 of them.
#
# == Minute 31 ==
# Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
# 2 ore-collecting robots collect 2 ore; you now have 6 ore.
# 7 clay-collecting robots collect 7 clay; you now have 77 clay.
# 5 obsidian-collecting robots collect 5 obsidian; you now have 5 obsidian.
# 8 geode-cracking robots crack 8 geodes; you now have 47 open geodes.
# The new geode-cracking robot is ready; you now have 9 of them.
#
# == Minute 32 ==
# 2 ore-collecting robots collect 2 ore; you now have 8 ore.
# 7 clay-collecting robots collect 7 clay; you now have 84 clay.
# 5 obsidian-collecting robots collect 5 obsidian; you now have 10 obsidian.
# 9 geode-cracking robots crack 9 geodes; you now have 56 open geodes.
#
# However, blueprint 2 from the example above is still better; using it, the largest number of geodes you could open in 32 minutes is 62.
#
# You no longer have enough blueprints to worry about quality levels. Instead, for each of the first three blueprints, determine the largest number of geodes you could open; then, multiply these three values together.
#
# Don't worry about quality levels; instead, just determine the largest number of geodes you could open using each of the first three blueprints. What do you get if you multiply these numbers together?
# Your puzzle answer was 13640.
# Output:
# --- Part Two ---
# Blueprint 1 cracks 31 geodes
# Blueprint 2 cracks 11 geodes
# Blueprint 3 cracks 40 geodes
# Answer: 13640
# (execute time: 00:00:52.592878500)
# Memory:
#   before:   RSS 3040
#   after:    RSS 1553376

module Day19
  enum ResourceType : UInt8
    Ore
    Clay
    Obsidian
    Geode
  end

  RESOURCE_BASIC_TYPES = [
    ResourceType::Ore,
    ResourceType::Clay,
    ResourceType::Obsidian,
  ]

  RESOURCE_TYPES = [
    ResourceType::Geode,
    ResourceType::Ore,
    ResourceType::Obsidian,
    ResourceType::Clay,
  ]

  alias RobotCost = Hash(ResourceType, UInt8)

  class Robot
    property costs : RobotCost
    property type : ResourceType

    def initialize(@type, @costs)
    end

    @[AlwaysInline]
    def enough?(resources)
      RESOURCE_BASIC_TYPES.all? do |t|
        resources[t.value] >= costs[t]
      end
    end

    @[AlwaysInline]
    def consume(resources : StaticArray(UInt8, 4)) : StaticArray(UInt8, 4)
      result = resources.dup
      RESOURCE_BASIC_TYPES.each do |t|
        result[t.value] -= costs[t]
      end
      result
    end
  end

  class Blueprint
    property id : UInt8
    property robot_costs : Hash(ResourceType, Robot)

    def initialize(@id)
      @robot_costs = Hash(ResourceType, Robot).new
      @max_robots = [0, 0, 0] of UInt8
      @cache = Hash(StaticArray(UInt8, 9), UInt8).new
    end

    def self.parse(records : Array(String)) : Array(Blueprint)
      blueprints = [] of Blueprint
      blueprint = Blueprint.new(0)
      records.each do |instruction|
        next if instruction == ""
        # Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 4 ore. Each obsidian robot costs 3 ore and 11 clay. Each geode robot costs 3 ore and 8 obsidian.
        blueprint_dirty, robot_costs_raw = instruction.split(":")
        blueprint = Blueprint.new(blueprint_dirty[10..].to_u8)

        # Remove extra space in front and punkt in the end and split lines by punkt.
        robot_costs_raws = robot_costs_raw[1..-2].split(". ")
        robot_costs_raws.each do |cost_raw|
          # "Each clay robot costs 2 ore"
          type, resources_raw = cost_raw[5..].split(" robot costs ")
          resources = Hash(String, UInt8).new
          resources_raw.split(" and ").each do |resource|
            # "2 ore"
            count, resource_type = resource.split(" ")
            resources[resource_type] ||= 0
            resources[resource_type] += count.to_i32
          end
          costs = {
            ResourceType::Ore      => resources.fetch("ore", 0_u8),
            ResourceType::Clay     => resources.fetch("clay", 0_u8),
            ResourceType::Obsidian => resources.fetch("obsidian", 0_u8),
          }
          robot = Robot.new(ResourceType.parse(type), costs)
          blueprint.add_robot(robot)
        end
        blueprints << blueprint
      end
      blueprints
    end

    def add_robot(robot : Robot)
      @robot_costs[robot.type] = robot
      calculate_max_required_robots
    end

    def calculate_max_required_robots
      return if !@robot_costs.has_key?(ResourceType::Geode)

      @max_robots[ResourceType::Obsidian.value] = @robot_costs[ResourceType::Geode].costs[ResourceType::Obsidian]
      @max_robots[ResourceType::Clay.value] = @robot_costs[ResourceType::Obsidian].costs[ResourceType::Clay]
      @max_robots[ResourceType::Ore.value] = [
        @robot_costs[ResourceType::Geode].costs[ResourceType::Ore],
        @robot_costs[ResourceType::Obsidian].costs[ResourceType::Ore],
        @robot_costs[ResourceType::Clay].costs[ResourceType::Ore],
        @robot_costs[ResourceType::Ore].costs[ResourceType::Ore],
      ].max
    end

    @[AlwaysInline]
    def required_a_new_robot?(check_robot_type : ResourceType, robots)
      return true if check_robot_type.geode?
      # It does not require anymore if enough obsidians and ores for geode
      if robots[ResourceType::Obsidian.value] >= @robot_costs[ResourceType::Geode].costs[ResourceType::Obsidian] &&
         robots[ResourceType::Ore.value] >= @robot_costs[ResourceType::Geode].costs[ResourceType::Ore]
        return false
      end

      return @max_robots[check_robot_type.value] > robots[check_robot_type.value]
    end

    def open_geodes(left : UInt8,
                    robots = StaticArray[1_u8, 0_u8, 0_u8, 0_u8],
                    resources = StaticArray[0_u8, 0_u8, 0_u8, 0_u8]) : UInt8
      return resources[ResourceType::Geode.value] + robots[ResourceType::Geode.value] if left == 1

      cache_key = StaticArray[left, robots[0], robots[1], robots[2], robots[3], resources[0], resources[1], resources[2], resources[3]]
      return @cache[cache_key] if @cache.has_key?(cache_key)

      result = 0_u8
      # Make sure allocate only single place
      next_robots = Array(UInt8).new(4)
      next_resources = Array(UInt8).new(4)
      robot = Robot.new(ResourceType::Ore, Hash(ResourceType, UInt8).new)

      RESOURCE_TYPES.each do |t|
        # Optimisation: Exclude creating new robots, that would not generate geode.
        next if !required_a_new_robot?(t, robots)

        robot = @robot_costs[t]
        next if !robot.enough?(resources)

        # Build a robot
        next_robots = robots.dup
        next_robots[robot.type.value] += 1

        # Produce resources
        next_resources = robot.consume(resources)
        robots.each_with_index do |count, t|
          next_resources[t] += count
        end

        result = [open_geodes(left - 1, next_robots, next_resources), result].max

        # Build only geode on this minute if possible. all other robots are not effective.
        if t.geode?
          @cache[cache_key] = result
          return result
        end
      end

      next_resources = resources.dup
      robots.each_with_index do |count, t|
        next_resources[t] += count
      end
      result = [open_geodes(left - 1, robots, next_resources), result].max

      @cache[cache_key] = result
      result
    end

    def clear_cache
      @cache = Hash(StaticArray(UInt8, 9), UInt8).new
    end
  end
end

def problem19(records : Array(String))
  blueprints = Day19::Blueprint.parse(records)

  result = 0
  channel = Channel(UInt8).new
  blueprints.each do |blueprint|
    spawn do
      r = blueprint.open_geodes(24_u8)
      puts "Blueprint #{blueprint.id} opens #{r} geodes #{r * blueprint.id} quality"
      channel.send r * blueprint.id
      blueprint.clear_cache
    end
  end

  blueprints.size.times do
    result += channel.receive
  end
  result
end

def problem19_part_two(records : Array(String))
  blueprints = Day19::Blueprint.parse(records[...3])

  result = 1
  channel = Channel(UInt8).new
  blueprints.each do |blueprint|
    spawn do
      r = blueprint.open_geodes(32_u8)
      puts "Blueprint #{blueprint.id} cracks #{r} geodes"
      channel.send r
    end
  end
  blueprints.size.times do
    result *= channel.receive
  end
  result
end
