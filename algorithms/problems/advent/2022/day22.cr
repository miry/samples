# https://adventofcode.com/2022/day/22
#
# --- Day 22: Monkey Map ---
#
# The monkeys take you on a surprisingly easy trail through the jungle. They're even going in roughly the right direction according to your handheld device's Grove Positioning System.
#
# As you walk, the monkeys explain that the grove is protected by a force field. To pass through the force field, you have to enter a password; doing so involves tracing a specific path on a strangely-shaped board.
#
# At least, you're pretty sure that's what you have to do; the elephants aren't exactly fluent in monkey.
#
# The monkeys give you notes that they took when they last saw the password entered (your puzzle input).
#
# For example:
#
#         ...#
#         .#..
#         #...
#         ....
# ...#.......#
# ........#...
# ..#....#....
# ..........#.
#         ...#....
#         .....#..
#         .#......
#         ......#.
#
# 10R5L5R10L4R5L5
#
# The first half of the monkeys' notes is a map of the board. It is comprised of a set of open tiles (on which you can move, drawn .) and solid walls (tiles which you cannot enter, drawn #).
#
# The second half is a description of the path you must follow. It consists of alternating numbers and letters:
#
#     A number indicates the number of tiles to move in the direction you are facing. If you run into a wall, you stop moving forward and continue with the next instruction.
#     A letter indicates whether to turn 90 degrees clockwise (R) or counterclockwise (L). Turning happens in-place; it does not change your current tile.
#
# So, a path like 10R5 means "go forward 10 tiles, then turn clockwise 90 degrees, then go forward 5 tiles".
#
# You begin the path in the leftmost open tile of the top row of tiles. Initially, you are facing to the right (from the perspective of how the map is drawn).
#
# If a movement instruction would take you off of the map, you wrap around to the other side of the board. In other words, if your next tile is off of the board, you should instead look in the direction opposite of your current facing as far as you can until you find the opposite edge of the board, then reappear there.
#
# For example, if you are at A and facing to the right, the tile in front of you is marked B; if you are at C and facing down, the tile in front of you is marked D:
#
#         ...#
#         .#..
#         #...
#         ....
# ...#.D.....#
# ........#...
# B.#....#...A
# .....C....#.
#         ...#....
#         .....#..
#         .#......
#         ......#.
#
# It is possible for the next tile (after wrapping around) to be a wall; this still counts as there being a wall in front of you, and so movement stops before you actually wrap to the other side of the board.
#
# By drawing the last facing you had with an arrow on each tile you visit, the full path taken by the above example looks like this:
#
#         >>v#
#         .#v.
#         #.v.
#         ..v.
# ...#...v..v#
# >>>v...>#.>>
# ..#v...#....
# ...>>>>v..#.
#         ...#....
#         .....#..
#         .#......
#         ......#.
#
# To finish providing the password to this strange input device, you need to determine numbers for your final row, column, and facing as your final position appears from the perspective of the original map. Rows start from 1 at the top and count downward; columns start from 1 at the left and count rightward. (In the above example, row 1, column 1 refers to the empty space with no tile on it in the top-left corner.) Facing is 0 for right (>), 1 for down (v), 2 for left (<), and 3 for up (^). The final password is the sum of 1000 times the row, 4 times the column, and the facing.
#
# In the above example, the final row is 6, the final column is 8, and the final facing is 0. So, the final password is 1000 * 6 + 4 * 8 + 0: 6032.
#
# Follow the path given in the monkeys' notes. What is the final password?
#
# Your puzzle answer was 146092.
#
# Output:
# Answer: 146092
# (execute time: 00:00:00.113938500 , RSS memory: 3008)

# --- Part Two ---
#
# As you reach the force field, you think you hear some Elves in the distance. Perhaps they've already arrived?
#
# You approach the strange input device, but it isn't quite what the monkeys drew in their notes. Instead, you are met with a large cube; each of its six faces is a square of 50x50 tiles.
#
# To be fair, the monkeys' map does have six 50x50 regions on it. If you were to carefully fold the map, you should be able to shape it into a cube!
#
# In the example above, the six (smaller, 4x4) faces of the cube are:
#
#         1111
#         1111
#         1111
#         1111
# 222233334444
# 222233334444
# 222233334444
# 222233334444
#         55556666
#         55556666
#         55556666
#         55556666
#
# You still start in the same position and with the same facing as before, but the wrapping rules are different. Now, if you would walk off the board, you instead proceed around the cube. From the perspective of the map, this can look a little strange. In the above example, if you are at A and move to the right, you would arrive at B facing down; if you are at C and move down, you would arrive at D facing up:
#
#         ...#
#         .#..
#         #...
#         ....
# ...#.......#
# ........#..A
# ..#....#....
# .D........#.
#         ...#..B.
#         .....#..
#         .#......
#         ..C...#.
#
# Walls still block your path, even if they are on a different face of the cube. If you are at E facing up, your movement is blocked by the wall marked by the arrow:
#
#         ...#
#         .#..
#      -->#...
#         ....
# ...#..E....#
# ........#...
# ..#....#....
# ..........#.
#         ...#....
#         .....#..
#         .#......
#         ......#.
#
# Using the same method of drawing the last facing you had with an arrow on each tile you visit, the full path taken by the above example now looks like this:
#
#         >>v#
#         .#v.
#         #.v.
#         ..v.
# ...#..^...v#
# .>>>>>^.#.>>
# .^#....#....
# .^........#.
#         ...#..v.
#         .....#v.
#         .#v<<<<.
#         ..v...#.
#
# The final password is still calculated from your final position and facing from the perspective of the map. In this example, the final row is 5, the final column is 7, and the final facing is 3, so the final password is 1000 * 5 + 4 * 7 + 3 = 5031.
#
# Fold the map into a cube, then follow the path given in the monkeys' notes. What is the final password?
#
# Your puzzle answer was 110342.
#
# Output:
#      0123
# 000   12
# 001   3
# 002  45
# 003  6
#
# Answer: 110342
# (execute time: 00:00:00.053444125)
# Memory:
#   before:   RSS 3392
#   after:    RSS 6432

require "../utils.cr"

module Day22
  extend self

  DIRECTIONS_ORDER            = ['>', 'v', '<', '^']
  DIRECTIONS_SYMBOL_TO_NUMBER = {
    '>' => 0,
    'v' => 1,
    '<' => 2,
    '^' => 3,
  }
  DIRECTIONS = {
    '>' => {x: 1, y: 0},
    'v' => {x: 0, y: 1},
    '<' => {x: -1, y: 0},
    '^' => {x: 0, y: -1},
  }

  def parse(records : Array(String))
    map = Hash(Coord, Char).new
    route = Array(Int32 | Char).new
    splitter_found = false
    y = 0
    width = 0
    records.each do |instruction|
      if instruction == ""
        break if splitter_found
        splitter_found = true
        next
      end

      if splitter_found
        instruction.scan(/(\d+)([LR])?/).each do |cmd|
          route << cmd[1].to_i
          route << cmd[2].chars[0] if cmd[2]?
        end
      else
        instruction.chars.each_with_index do |c, x|
          map[{x: x, y: y}] = c
        end
        width = [width, instruction.size].max
        y += 1
      end
    end
    height = y

    height.times do |y|
      width.times do |x|
        map[{x: x, y: y}] ||= ' '
      end
    end

    # Start direction
    direction = 0 # >
    position = {x: 0, y: 0}

    # Detect start position
    while map[position] == ' '
      position = {x: position[:x] + 1, y: 0}
    end

    direction_symbol = DIRECTIONS_ORDER[direction]
    map[position] = direction_symbol

    {map, height, width, route, direction, position}
  end

  def problem22(records : Array(String))
    map, height, width, route, direction, position = parse(records)

    # puts ">> Solution"
    # print_plan map, height, width, 'x'

    route.each do |cmd|
      if cmd.is_a?(Int32)
        # puts "Move #{cmd} steps in #{DIRECTIONS_ORDER[direction]}"
        position = move(map, position, direction, cmd.as(Int32), height, width)
      else
        # old = direction
        if cmd == 'R'
          direction = (direction + 1) % DIRECTIONS_ORDER.size
        else
          direction = direction - 1
          direction = DIRECTIONS_ORDER.size - 1 if direction < 0
        end
        # puts "Turn #{cmd} from #{DIRECTIONS_ORDER[old]} to #{DIRECTIONS_ORDER[direction]}"
        map[position] = DIRECTIONS_ORDER[direction]
      end
      # print_plan map, height, width, 'x'
      # print "press <ENTER> to continue"
      # gets
    end

    # puts "Finish:"
    # print_plan map, height, width

    (position[:y] + 1)*1000 + (position[:x] + 1)*4 + direction
  end

  def next_position(map, position, direction, height, width)
    # puts "> next_position(#{position}, #{height}, #{width})"
    direction_symbol = DIRECTIONS_ORDER[direction]
    direction_change = DIRECTIONS[direction_symbol]
    next_position = position

    loop_threshold_limit = 1000
    while true
      next_position = {
        x: next_position[:x] + direction_change[:x],
        y: next_position[:y] + direction_change[:y],
      }
      # pp! direction
      # pp! next_position
      if !map.has_key?(next_position)
        if direction_change[:x] > 0
          next_position = {x: 0, y: next_position[:y]}
        elsif direction_change[:x] < 0
          next_position = {x: width - 1, y: next_position[:y]}
        elsif direction_change[:y] > 0
          next_position = {x: next_position[:x], y: 0}
        else
          next_position = {x: next_position[:x], y: height - 1}
        end
      end

      return position if map[next_position] == '#'
      return next_position if map[next_position] != ' '

      loop_threshold_limit -= 1
      raise "Loop" if loop_threshold_limit == 0
    end
  end

  def move(map, position, direction, steps, height, width)
    # puts "> move()"
    direction_symbol = DIRECTIONS_ORDER[direction]
    direction_change = DIRECTIONS[direction_symbol]
    valid_position = position
    next_position = valid_position
    step = 0
    while true
      next_position = {x: next_position[:x] + direction_change[:x], y: next_position[:y] + direction_change[:y]}
      if !map.has_key?(next_position) || map[next_position] == ' '
        next_position = next_position(map, valid_position, direction, height, width)
      end
      return valid_position if map[next_position] == '#'
      valid_position = next_position
      map[valid_position] = direction_symbol
      step += 1
      break if step == steps
    end
    valid_position
  end

  class CubeSide
    property id : Int32
    property x_base : Int32
    property y_base : Int32
    property width : Int32
    property map : Hash(Coord, Char)

    # Connections with edges
    property connections : Hash(Int32, NamedTuple(id: Int32, direction: Int32))
    @@cubes : Hash(Int32, CubeSide) = Hash(Int32, CubeSide).new
    @@cubes_schemas : Hash(Coord, CubeSide) = Hash(Coord, CubeSide).new
    @@cube_size : Int32 = 0

    def initialize(@id, @x_base, @y_base, @width)
      @map = Hash(Coord, Char).new
      @connections = Hash(Int32, NamedTuple(id: Int32, direction: Int32)).new
    end

    def self.from(num, global, x_base, y_base, cube_width)
      cube = self.new(num, x_base, y_base, cube_width)
      map = Hash(Coord, Char).new
      cube_width.times do |y|
        cube_width.times do |x|
          map[{x: x, y: y}] = global[{x: x_base + x, y: y_base + y}]
        end
      end
      cube.map = map
      cube
    end

    def self.cube_size
      @@cube_size
    end

    def self.cube_size=(val)
      @@cube_size = val
    end

    def self.cubes=(val)
      @@cubes = val
    end

    def self.cubes_schemas=(val)
      @@cubes_schemas = val
    end

    def self.get_by_position(position : Coord)
      base_postion : Coord = {x: position[:x] // @@cube_size, y: position[:y] // @@cube_size}
      find(base_postion)
    end

    def self.find(id : Int32)
      @@cubes[id]
    end

    def self.find(base_position : Coord)
      @@cubes_schemas[base_position]
    end

    def to_s
      id.to_s
    end
  end

  def problem22_part_two(records : Array(String))
    map, height, width, route, direction, position = parse(records)

    cubes = Hash(Int32, CubeSide).new
    cubes_schemas = Hash(Coord, CubeSide).new
    side_number = 1
    # For tests use small cube
    cube_size = height > 50 ? 50 : 4

    (height // cube_size).times do |k|
      y_base = cube_size * k

      (width // cube_size).times do |i|
        x_base = cube_size * i
        next if map[{x: x_base, y: y_base}] == ' '

        side = CubeSide.from(side_number, map, x_base, y_base, cube_size)
        cubes[side_number] = side
        cubes_schemas[{x: i, y: k}] = side
        side_number += 1
      end
    end
    CubeSide.cubes = cubes
    CubeSide.cubes_schemas = cubes_schemas
    CubeSide.cube_size = cube_size

    print_plan cubes_schemas, 4, 4

    # # Connect edges
    # cubes_schemas.each do |k, cube_side|
    #   # # Connect directly connected
    #   DIRECTIONS_ORDER.each_with_index do |direction_symbol, direction_num|
    #     next if cube_side.connections.has_key?(direction_num)
    #     direction = DIRECTIONS[direction_symbol]
    #     neighbour = {x: k[:x] + direction[:x], y: k[:y] + direction[:y]}
    #     if cubes_schemas.has_key?(neighbour)
    #       other = cubes_schemas[neighbour]
    #       # puts "Found direction from #{cube_side.id} to #{other.id} with #{direction_symbol}"
    #       cube_side.connections[direction_num] = {id: other.id, direction: direction_num}
    #       oposite_direction = (direction_num + 2) % DIRECTIONS_ORDER.size
    #       other.connections[oposite_direction] = {id: cube_side.id, direction: oposite_direction}
    #     end
    #   end
    # end

    # NOTE: Replace with automatic algorithm to build a cube connections
    transitions = {
      1 => {
        '>' => {2, '>'},
        '<' => {4, '>'},
        '^' => {6, '>'},
        'v' => {3, 'v'},
      },

      2 => {
        '<' => {1, '<'},
        '>' => {5, '<'},
        '^' => {6, '^'},
        'v' => {3, '<'},
      },

      3 => {
        '<' => {4, 'v'},
        '>' => {2, '^'},
        '^' => {1, '^'},
        'v' => {5, 'v'},
      },

      4 => {
        '<' => {1, '>'},
        '>' => {5, '>'},
        '^' => {3, '>'},
        'v' => {6, 'v'},
      },

      5 => {
        '<' => {4, '<'},
        '>' => {2, '<'},
        '^' => {3, '^'},
        'v' => {6, '<'},
      },
      6 => {
        '<' => {1, 'v'},
        '>' => {5, '^'},
        '^' => {4, '^'},
        'v' => {2, 'v'},
      },
    }

    if cube_size == 4
      transitions = {
        1 => {
          '>' => {6, '<'},
          'v' => {4, 'v'},
          '<' => {3, 'v'},
          '^' => {2, 'v'},
        },
        2 => {
          '>' => {3, '>'},
          'v' => {5, '^'},
          '<' => {6, '^'},
          '^' => {1, 'v'},
        },
        3 => {
          '>' => {4, '>'},
          'v' => {5, '>'},
          '<' => {2, '<'},
          '^' => {1, '>'},
        },
        4 => {
          '>' => {6, 'v'},
          'v' => {5, 'v'},
          '<' => {3, '<'},
          '^' => {1, '^'},
        },
        5 => {
          '>' => {3, '>'},
          'v' => {2, '^'},
          '<' => {6, '^'},
          '^' => {1, 'v'},
        },
        6 => {
          '>' => {1, '<'},
          'v' => {2, '>'},
          '<' => {5, '<'},
          '^' => {4, '<'},
        },
      }
    end

    # Base on transitions table fill connections
    transitions.each do |side_number, directions|
      cube_side = cubes[side_number]
      directions.each do |direction, dest|
        direction_num = DIRECTIONS_SYMBOL_TO_NUMBER[direction]
        cube_side.connections[direction_num] = {
          id:        dest[0],
          direction: DIRECTIONS_SYMBOL_TO_NUMBER[dest[1]],
        }
      end
    end

    direction_symbol = DIRECTIONS_ORDER[direction]
    map[position] = direction_symbol

    # puts "Start:"
    # print_plan map, height, width

    route.each do |cmd|
      if cmd.is_a?(Int32)
        # puts "Move #{cmd} steps in #{DIRECTIONS_ORDER[direction]}"
        position, direction = move_in_cube(map, position, direction, cmd.as(Int32), height, width)
        # print_plan map, height, width
      else
        # old = direction
        if cmd == 'R'
          direction = (direction + 1) % DIRECTIONS_ORDER.size
        else
          direction = direction - 1
          direction = DIRECTIONS_ORDER.size - 1 if direction < 0
        end
        # puts "Turn #{cmd} from #{DIRECTIONS_ORDER[old]} to #{DIRECTIONS_ORDER[direction]}"
        map[position] = DIRECTIONS_ORDER[direction]
        # print_plan map, height, width
      end
    end

    # puts "Finish:"
    # print_plan map, height, width
    (position[:y] + 1)*1000 + (position[:x] + 1)*4 + direction
  end

  def move_in_cube(map, position : Coord, direction : Int32, steps, height, width)
    # puts "> move_in_cube()"
    next_direction = direction
    direction_symbol = DIRECTIONS_ORDER[direction]
    direction_change = DIRECTIONS[direction_symbol]
    valid_position : Coord = position
    next_position : Coord = valid_position
    step = 0
    while true
      next_position = {x: next_position[:x] + direction_change[:x], y: next_position[:y] + direction_change[:y]}
      if !map.has_key?(next_position) || map[next_position] == ' '
        # puts "  missing dot for #{next_position} with #{direction}"
        next_position, next_direction = next_position_in_cube(
          map,
          valid_position,
          direction,
          height,
          width)
        # puts "  next_position: #{next_position}"
        # puts "  next_direction: #{DIRECTIONS_ORDER[next_direction]}"
        # puts "  next_position_symbol: #{map[next_position]}"
      end
      # next if map[next_position] == ' '
      if map[next_position] == '#'
        # puts "  met the wall keep last position: #{valid_position}, #{direction_symbol}"
        return {valid_position, direction}
      end

      # Update current position and direction
      valid_position = next_position
      direction = next_direction
      direction_symbol = DIRECTIONS_ORDER[direction]
      direction_change = DIRECTIONS[direction_symbol]

      # Mark the passed step
      map[valid_position] = direction_symbol
      step += 1
      break if step == steps
    end
    {valid_position, direction}
  end

  def next_position_in_cube(
    map : Hash(Coord, Char),
    position : Coord,
    direction : Int32,
    height : Int32,
    width : Int32
  ) : Tuple(Coord, Int32)
    # puts "  > next_position_in_cube(map, #{position})"
    direction_symbol = DIRECTIONS_ORDER[direction]
    direction_change = DIRECTIONS[direction_symbol]
    next_position = position

    while true
      next_position = {x: next_position[:x] + direction_change[:x], y: next_position[:y] + direction_change[:y]}
      # puts "    checking next_position: #{next_position}"
      if !map.has_key?(next_position) || map[next_position] == ' '
        src_cube_side = CubeSide.get_by_position(position)
        # puts "    src_cube_side_num: #{src_cube_side.id}"
        # puts "    direction: #{direction_symbol} (#{direction})"
        transformation : Hash(Int32, NamedTuple(id: Int32, direction: Int32)) = src_cube_side.connections

        dst_cube_side_num = transformation[direction][:id]
        # puts "    dst_cube_side_num: #{dst_cube_side_num}"
        next_direction = transformation[direction][:direction]
        # puts "    next_direction: #{DIRECTIONS_ORDER[next_direction]} (#{next_direction})"
        dst_cube_side = CubeSide.find(dst_cube_side_num)

        case next_direction
        when 0 # >
          case direction
          when 0 # >
            next_position = {x: dst_cube_side.x_base, y: dst_cube_side.y_base + CubeSide.cube_size - 1 - position[:y] % CubeSide.cube_size}
          when 2 # <
            next_position = {x: dst_cube_side.x_base, y: dst_cube_side.y_base + CubeSide.cube_size - position[:y] % CubeSide.cube_size - 1}
          when 3 # ^
            next_position = {x: dst_cube_side.x_base, y: dst_cube_side.y_base + position[:x] % CubeSide.cube_size}
          else
            raise "not implemented"
          end
        when 1 # v
          case direction
          when 0 # >
            next_position = {x: dst_cube_side.x_base + CubeSide.cube_size - position[:y] % CubeSide.cube_size - 1, y: dst_cube_side.y_base}
          when 1 # v
            next_position = {x: dst_cube_side.x_base + position[:x] % CubeSide.cube_size, y: dst_cube_side.y_base}
          when 2 # <
            next_position = {x: dst_cube_side.x_base + position[:y] % CubeSide.cube_size, y: dst_cube_side.y_base}
          else
            raise "not implemented"
          end
        when 2 # <
          case direction
          when 0 # >
            next_position = {x: dst_cube_side.x_base + CubeSide.cube_size - 1, y: dst_cube_side.y_base + CubeSide.cube_size - 1 - position[:y] % CubeSide.cube_size}
          when 1 # v
            next_position = {x: dst_cube_side.x_base + CubeSide.cube_size - 1, y: dst_cube_side.y_base + position[:x] % CubeSide.cube_size}
          when 3 # ^
            next_position = {x: dst_cube_side.x_base + CubeSide.cube_size - 1, y: dst_cube_side.y_base + CubeSide.cube_size - 1 - position[:x] % CubeSide.cube_size}
          else
            raise "not implemented"
          end
        when 3 # ^
          case direction
          when 0 # >
            next_position = {x: dst_cube_side.x_base + position[:y] % CubeSide.cube_size, y: dst_cube_side.y_base + CubeSide.cube_size - 1}
          when 1 # v
            next_position = {x: dst_cube_side.x_base + CubeSide.cube_size - position[:x] % CubeSide.cube_size - 1, y: dst_cube_side.y_base + CubeSide.cube_size - 1}
          when 2 # <
            next_position = {x: dst_cube_side.x_base + CubeSide.cube_size - position[:y] % CubeSide.cube_size - 1, y: dst_cube_side.y_base + CubeSide.cube_size - 1}
          when 3 # ^
            next_position = {x: dst_cube_side.x_base + position[:x] % CubeSide.cube_size, y: dst_cube_side.y_base + CubeSide.cube_size - 1}
          else
            raise "not implemented"
          end
        else
          raise "not implemented"
        end
        # print_plan map, height, width
        # puts "    from_position: #{position}"
        # puts "    direction_symbol: #{direction_symbol}"
        # puts "    next_position: #{next_position}"
        # puts "    next_position_symbol: #{map[next_position]}"
        # print "Test values: <press ENTER to continue>"
        # gets
        # puts "\e[H\e[2J" # clear screen
      end

      return {next_position.not_nil!, next_direction.not_nil!} if ![' '].includes?(map[next_position])
      raise "never rechead point"
    end
    return {position, direction}
  end
end

def problem22(records : Array(String))
  Day22.problem22(records)
end

def problem22_part_two(records : Array(String))
  Day22.problem22_part_two(records)
end
