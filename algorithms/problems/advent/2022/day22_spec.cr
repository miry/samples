require "spec"
require "./day22"

describe "Day 22" do
  describe "problem22" do
    it "sample" do
      input = <<-EOF
              ...#
              .#..
              #...
              ....
      ...#.......#
      ........#...
      ..#....#....
      ..........#.
              ...#....
              .....#..
              .#......
              ......#.

      10R5L5R10L4R5L5
      EOF
      actual = problem22(input.split("\n"))
      actual.should eq(6032)
    end

    it "real problem" do
      input_file_path = "./input/day22.txt"
      pending!("input/day22.txt is not exists") if !File.exists?(input_file_path)
      input = File.read_lines(input_file_path)
      pending!("input/day22.txt is encrypted or empty") if input[0][0] != ' '
      actual = problem22(input)
      actual.should eq(146092)
    end
  end

  describe "problem22_part_two" do
    it "sample" do
      input = <<-EOF
              ...#
              .#..
              #...
              ....
      ...#.......#
      ........#...
      ..#....#....
      ..........#.
              ...#....
              .....#..
              .#......
              ......#.

      10R5L5R10L4R5L5
      EOF
      actual = problem22_part_two(input.split("\n"))
      actual.should eq(5031)
    end

    it "real problem" do
      input_file_path = "./input/day22.txt"
      pending!("input/day22.txt is not exists") if !File.exists?(input_file_path)
      input = File.read_lines(input_file_path)
      pending!("input/day22.txt is encrypted or empty") if input[0][0] != ' '
      actual = problem22_part_two(input)
      actual.should eq(110342)
    end
  end
end
