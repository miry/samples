require "spec"
require "./day06"

describe "Day 6" do
  describe "problem6" do
    it "sample" do
      input = <<-EOF
      mjqjpqmgbljsphdztnvjfqwrcgsmlb
      EOF
      actual = problem6(input.split("\n"))
      actual.should eq(7)
    end
  end

  describe "problem6_part_two" do
    it "sample" do
      input = <<-EOF
      mjqjpqmgbljsphdztnvjfqwrcgsmlb
      EOF
      actual = problem6_part_two(input.split("\n"))
      actual.should eq(19)
    end
    it "sample2" do
      input = <<-EOF
      bvwbjplbgvbhsrlpgdmjqwftvncz
      EOF
      actual = problem6_part_two(input.split("\n"))
      actual.should eq(23)
    end
  end
end
