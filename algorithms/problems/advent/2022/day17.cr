# https://adventofcode.com/2022/day/17
#
# --- Day 17: Pyroclastic Flow ---
#
# Your handheld device has located an alternative exit from the cave for you and the elephants. The ground is rumbling almost continuously now, but the strange valves bought you some time. It's definitely getting warmer in here, though.
#
# The tunnels eventually open into a very tall, narrow chamber. Large, oddly-shaped rocks are falling into the chamber from above, presumably due to all the rumbling. If you can't work out where the rocks will fall next, you might be crushed!
#
# The five types of rocks have the following peculiar shapes, where # is rock and . is empty space:
#
# ####
#
# .#.
# ###
# .#.
#
# ..#
# ..#
# ###
#
# #
# #
# #
# #
#
# ##
# ##
#
# The rocks fall in the order shown above: first the - shape, then the + shape, and so on. Once the end of the list is reached, the same order repeats: the - shape falls first, sixth, 11th, 16th, etc.
#
# The rocks don't spin, but they do get pushed around by jets of hot gas coming out of the walls themselves. A quick scan reveals the effect the jets of hot gas will have on the rocks as they fall (your puzzle input).
#
# For example, suppose this was the jet pattern in your cave:
#
# >>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>
#
# In jet patterns, < means a push to the left, while > means a push to the right. The pattern above means that the jets will push a falling rock right, then right, then right, then left, then left, then right, and so on. If the end of the list is reached, it repeats.
#
# The tall, vertical chamber is exactly seven units wide. Each rock appears so that its left edge is two units away from the left wall and its bottom edge is three units above the highest rock in the room (or the floor, if there isn't one).
#
# After a rock appears, it alternates between being pushed by a jet of hot gas one unit (in the direction indicated by the next symbol in the jet pattern) and then falling one unit down. If any movement would cause any part of the rock to move into the walls, floor, or a stopped rock, the movement instead does not occur. If a downward movement would have caused a falling rock to move into the floor or an already-fallen rock, the falling rock stops where it is (having landed on something) and a new rock immediately begins falling.
#
# Drawing falling rocks with @ and stopped rocks with #, the jet pattern in the example above manifests as follows:
#
# The first rock begins falling:
# |..@@@@.|
# |.......|
# |.......|
# |.......|
# +-------+
#
# Jet of gas pushes rock right:
# |...@@@@|
# |.......|
# |.......|
# |.......|
# +-------+
#
# Rock falls 1 unit:
# |...@@@@|
# |.......|
# |.......|
# +-------+
#
# Jet of gas pushes rock right, but nothing happens:
# |...@@@@|
# |.......|
# |.......|
# +-------+
#
# Rock falls 1 unit:
# |...@@@@|
# |.......|
# +-------+
#
# Jet of gas pushes rock right, but nothing happens:
# |...@@@@|
# |.......|
# +-------+
#
# Rock falls 1 unit:
# |...@@@@|
# +-------+
#
# Jet of gas pushes rock left:
# |..@@@@.|
# +-------+
#
# Rock falls 1 unit, causing it to come to rest:
# |..####.|
# +-------+
#
# A new rock begins falling:
# |...@...|
# |..@@@..|
# |...@...|
# |.......|
# |.......|
# |.......|
# |..####.|
# +-------+
#
# Jet of gas pushes rock left:
# |..@....|
# |.@@@...|
# |..@....|
# |.......|
# |.......|
# |.......|
# |..####.|
# +-------+
#
# Rock falls 1 unit:
# |..@....|
# |.@@@...|
# |..@....|
# |.......|
# |.......|
# |..####.|
# +-------+
#
# Jet of gas pushes rock right:
# |...@...|
# |..@@@..|
# |...@...|
# |.......|
# |.......|
# |..####.|
# +-------+
#
# Rock falls 1 unit:
# |...@...|
# |..@@@..|
# |...@...|
# |.......|
# |..####.|
# +-------+
#
# Jet of gas pushes rock left:
# |..@....|
# |.@@@...|
# |..@....|
# |.......|
# |..####.|
# +-------+
#
# Rock falls 1 unit:
# |..@....|
# |.@@@...|
# |..@....|
# |..####.|
# +-------+
#
# Jet of gas pushes rock right:
# |...@...|
# |..@@@..|
# |...@...|
# |..####.|
# +-------+
#
# Rock falls 1 unit, causing it to come to rest:
# |...#...|
# |..###..|
# |...#...|
# |..####.|
# +-------+
#
# A new rock begins falling:
# |....@..|
# |....@..|
# |..@@@..|
# |.......|
# |.......|
# |.......|
# |...#...|
# |..###..|
# |...#...|
# |..####.|
# +-------+
#
# The moment each of the next few rocks begins falling, you would see this:
#
# |..@....|
# |..@....|
# |..@....|
# |..@....|
# |.......|
# |.......|
# |.......|
# |..#....|
# |..#....|
# |####...|
# |..###..|
# |...#...|
# |..####.|
# +-------+
#
# |..@@...|
# |..@@...|
# |.......|
# |.......|
# |.......|
# |....#..|
# |..#.#..|
# |..#.#..|
# |#####..|
# |..###..|
# |...#...|
# |..####.|
# +-------+
#
# |..@@@@.|
# |.......|
# |.......|
# |.......|
# |....##.|
# |....##.|
# |....#..|
# |..#.#..|
# |..#.#..|
# |#####..|
# |..###..|
# |...#...|
# |..####.|
# +-------+
#
# |...@...|
# |..@@@..|
# |...@...|
# |.......|
# |.......|
# |.......|
# |.####..|
# |....##.|
# |....##.|
# |....#..|
# |..#.#..|
# |..#.#..|
# |#####..|
# |..###..|
# |...#...|
# |..####.|
# +-------+
#
# |....@..|
# |....@..|
# |..@@@..|
# |.......|
# |.......|
# |.......|
# |..#....|
# |.###...|
# |..#....|
# |.####..|
# |....##.|
# |....##.|
# |....#..|
# |..#.#..|
# |..#.#..|
# |#####..|
# |..###..|
# |...#...|
# |..####.|
# +-------+
#
# |..@....|
# |..@....|
# |..@....|
# |..@....|
# |.......|
# |.......|
# |.......|
# |.....#.|
# |.....#.|
# |..####.|
# |.###...|
# |..#....|
# |.####..|
# |....##.|
# |....##.|
# |....#..|
# |..#.#..|
# |..#.#..|
# |#####..|
# |..###..|
# |...#...|
# |..####.|
# +-------+
#
# |..@@...|
# |..@@...|
# |.......|
# |.......|
# |.......|
# |....#..|
# |....#..|
# |....##.|
# |....##.|
# |..####.|
# |.###...|
# |..#....|
# |.####..|
# |....##.|
# |....##.|
# |....#..|
# |..#.#..|
# |..#.#..|
# |#####..|
# |..###..|
# |...#...|
# |..####.|
# +-------+
#
# |..@@@@.|
# |.......|
# |.......|
# |.......|
# |....#..|
# |....#..|
# |....##.|
# |##..##.|
# |######.|
# |.###...|
# |..#....|
# |.####..|
# |....##.|
# |....##.|
# |....#..|
# |..#.#..|
# |..#.#..|
# |#####..|
# |..###..|
# |...#...|
# |..####.|
# +-------+
#
# To prove to the elephants your simulation is accurate, they want to know how tall the tower will get after 2022 rocks have stopped (but before the 2023rd rock begins falling). In this example, the tower of rocks will be 3068 units tall.
#
# How many units tall will the tower of rocks be after 2022 rocks have stopped falling?
#
# Your puzzle answer was 3157.
#
# --- Part Two ---
#
# The elephants are not impressed by your simulation. They demand to know how tall the tower will be after 1000000000000 rocks have stopped! Only then will they feel confident enough to proceed through the cave.
#
# In the example above, the tower would be 1514285714288 units tall!
#
# How tall will the tower be after 1000000000000 rocks have stopped?
#
# Your puzzle answer was 1581449275319.

def detect_pattern(sequence, value_to_index)
  last = sequence.last
  n = sequence.size
  return nil if !value_to_index.has_key?(last) || value_to_index[last].size <= 1

  indexes = value_to_index[last]
  current_index = n - 1

  result = nil
  1.upto(indexes.size - 1) do |index_id|
    prev_index = indexes[index_id]
    len = (current_index - prev_index)

    return result if prev_index - len + 1 < 0

    if sequence[prev_index + 1..current_index] == sequence[prev_index - len + 1..prev_index]
      result = len
    end
  end
  result
end

def problem17(records : Array(String), count = 2022)
  moves = records.first.chars

  blocks : Array(Array(Tuple(Int128, Int128))) = [
    [
      {0_i128, 0_i128},
      {0_i128, 1_i128},
      {0_i128, 2_i128},
      {0_i128, 3_i128},
    ],
    [
      {0_i128, 1_i128},
      {1_i128, 0_i128},
      {1_i128, 1_i128},
      {1_i128, 2_i128},
      {2_i128, 1_i128},
    ],
    [
      {0_i128, 0_i128},
      {0_i128, 1_i128},
      {0_i128, 2_i128},
      {1_i128, 2_i128},
      {2_i128, 2_i128},
    ],
    [
      {0_i128, 0_i128},
      {1_i128, 0_i128},
      {2_i128, 0_i128},
      {3_i128, 0_i128},
    ],
    [
      {0_i128, 0_i128},
      {0_i128, 1_i128},
      {1_i128, 0_i128},
      {1_i128, 1_i128},
    ],
  ]

  min_height = 0
  height : Int128 = 0
  state = Hash(Tuple(Int128, Int128), Char).new

  i = 0
  n = moves.size
  block_position : Hash(Tuple(Int128, Int128), Char) = Hash(Tuple(Int128, Int128), Char).new
  new_block_position : Hash(Tuple(Int128, Int128), Char) = Hash(Tuple(Int128, Int128), Char).new

  block_width : Int128 = 0

  # Optimisation section. States required for calculation patterns.
  # Keep the height of previous first blocks
  last_height : Int128 = 0_i128
  diff_heights = Array(Int128).new
  diff_heights_map = Hash(Int128, Array(Int128)).new
  pattern_detected_length = 0_i128
  pattern_detected_start = 0_i128
  pattern_detected = false
  pattern_calculated = false
  pattern_calculated_height = 0_i128

  step = 0_i128
  while step < count
    # print "\rstep: #{step}" # Live counter

    block_id = step % 5
    block = blocks[block_id]

    # Optimization section.
    # It tries to detect pattern for each previous height changes on first block.
    # In case it identifies a pattern. It should be 2 similar sequence and third one for confirmation.
    if block_id == 0
      diff_height = height - last_height
      diff_heights << diff_height
      diff_heights_map[diff_height] ||= Array(Int128).new
      diff_heights_map[diff_height].insert(0, diff_heights.size - 1)
      last_height = height

      # Check if previous pattern detected and the new valid covers the pattern
      if pattern_detected
        # If pattern detected but still it does not cover the end of rounds.
        # Then we skip searching for new patterns and confirm previous.
        if !pattern_calculated
          # Compare element by element per each round. The first sequence were detected before.
          if diff_height != diff_heights[-pattern_detected_length - 1]
            pattern_detected = false
            # pp! diff_height
            # pp! diff_heights[-pattern_detected_length-1]
            # puts diff_heights[-3*pattern_detected_length..].join(", ")
          else
            if pattern_detected_length + pattern_detected_start == diff_heights.size - 1
              # puts diff_heights[-3*pattern_detected_length..].join(", ")

              pattern_height = diff_heights[pattern_detected_start + 1..].sum
              # puts "Pattern height: #{pattern_height}"
              # puts "Pattern size: #{pattern_detected_length}"
              # puts "Pattern: #{diff_heights[pattern_detected_start+1..]}"
              # puts "Height: #{height}"
              # puts "Step: #{step}"
              # puts "Left: #{count - step - 2}"
              repeats : Int128 = ((count - step - 1) / (pattern_detected_length * blocks.size)).to_i128
              # puts "Repeats (399-3): #{repeats}"
              # puts "Estimate height: #{repeats * pattern_height + height}"
              count -= repeats * pattern_detected_length * blocks.size
              pattern_calculated_height = repeats * pattern_height
              pattern_calculated = true
              # puts "---\n\n\n"
            end
          end
        end
      else
        length = detect_pattern(diff_heights, diff_heights_map)
        if !length.nil? && length > 4
          pattern_detected = true
          pattern_detected_start = diff_heights.size - 1
          pattern_detected_length = length
          # puts "\nPattern detected"
          # pp! length
          # puts "Pattern: " + diff_heights[-2*length..].join(", ")
        end
      end
    end

    block_position = Hash(Tuple(Int128, Int128), Char).new

    # Draw block
    start = {height + 3, 2}
    x : Int128 = 0
    y : Int128 = 0

    block_width = 0
    block.each do |k|
      x = k[0] + start[0]
      y = k[1] + start[1]
      block_width = [block_width, k[1] + 1].max
      block_position[{x, y}] = '@'
    end
    # puts "The #{step} rock begins falling:"
    # print_tunnel(state, block_position, start[0])

    move = moves[i % n]
    i += 1
    new_start = start
    case move
    when '>'
      if start[1] + block_width < 7
        new_start = {start[0], start[1] + 1}
      end
    when '<'
      if start[1] > 0
        new_start = {start[0], start[1] - 1}
      end
    else
      raise "not implemented"
    end
    new_block_position = Hash(Tuple(Int128, Int128), Char).new
    fit = true
    block.each do |k|
      x = k[0] + new_start[0]
      y = k[1] + new_start[1]
      new_block_position[{x, y}] = '@'
      if state.has_key?({x, y})
        fit = false
        break
      end
    end
    if fit
      block_position = new_block_position
      start = new_start
    end

    # Moving block
    while true
      # Move block 1 line down
      start = {start[0] - 1, start[1]}
      new_block_position = Hash(Tuple(Int128, Int128), Char).new
      fit = true
      block.each do |k|
        x = k[0] + start[0]
        y = k[1] + start[1]
        new_block_position[{x, y}] = '@'
        if state.has_key?({x, y}) || x < min_height
          fit = false
          break
        end
      end
      break if !fit
      block_position = new_block_position

      # Move left or right
      move = moves[i % n]
      i += 1
      new_start = start
      case move
      when '>'
        if start[1] + block_width < 7
          new_start = {start[0], start[1] + 1}
        end
      when '<'
        if start[1] > 0
          new_start = {start[0], start[1] - 1}
        end
      else
        raise "not implemented"
      end
      new_block_position = Hash(Tuple(Int128, Int128), Char).new
      fit = true
      block.each do |k|
        x = k[0] + new_start[0]
        y = k[1] + new_start[1]
        new_block_position[{x, y}] = '@'
        if state.has_key?({x, y})
          fit = false
          break
        end
      end
      if fit
        block_position = new_block_position
        start = new_start
      end
    end

    # puts "Rock falls 1 unit, causing it to come to rest:"
    # Add result block to the main picture
    block_position.keys.each do |coord|
      height = [height, coord[0] + 1].max
      state[coord] = block_id.to_s[0]
    end
    step += 1
  end
  height + pattern_calculated_height
end

# --- Part Two ---

def problem17_part_two(records : Array(String), count = 1_000_000_000_000)
  problem17(records, count)
end

def print_tunnel(state, block, line, min = 0)
  top = line + 4
  bottom = min
  puts "    +" + "-"*7 + "+"
  top.downto([bottom, min].max) do |x|
    print "%03d |" % x
    7.times do |y|
      print state.fetch({x, y}, block.fetch({x, y}, "."))
    end
    print "|\n"
  end
  puts "    +" + "-"*7 + "+"
  puts
  puts
end
