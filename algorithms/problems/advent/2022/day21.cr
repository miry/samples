# https://adventofcode.com/2022/day/21
#
# --- Day 21: ... ---
#

def monkey_value(monkeys, monkey_id : String, cache, path = Set(String).new)
  path.add(monkey_id) if monkey_id == "humn"

  return cache[monkey_id] if cache.has_key?(monkey_id)
  monkey = monkeys[monkey_id]
  return monkey if monkey.is_a?(Int64)

  mpath = path
  mpath = path + Set{monkey_id} if !path.includes?("humn")
  a = monkey_value(monkeys, monkey[0].as(String), cache, mpath)
  op = monkey[1]
  b = monkey_value(monkeys, monkey[2].as(String), cache, mpath)
  path.concat(mpath) if mpath.includes?("humn")

  result = case op
           when "+"
             a + b
           when "-"
             a - b
           when "*"
             a * b
           when "/"
             a // b
           else
             raise "Operation #{op} is not implementated"
           end

  cache[monkey_id] = result
  result
end

def problem21(records : Array(String))
  monkeys = Hash(String, Array(String | Int64) | Int64).new

  records.each do |instruction|
    next if instruction == ""
    monkey, operation = instruction.split(": ")
    monkeys[monkey] = if !operation.includes?(" ")
                        operation.to_i64
                      else
                        operation.split(" ").map { |e| e.to_i64? ? e.to_i64 : e }.as(Array(Int64 | String))
                      end
  end

  monkey_value(monkeys, "root", Hash(String, Int64).new)
end

# --- Part Two ---

def detect_number(monkeys, path, monkey_id, value, cache)
  return value if monkey_id == "humn"

  monkey = monkeys[monkey_id]

  if monkey.is_a?(Int64)
    raise "found int64 and expect other value and not monkey_id humn"
  end

  lmonkey = monkey[0].as(String)
  rmonkey = monkey[2].as(String)
  op = monkey[1]

  nmonkey = ""
  omonkey = ""

  if path.includes?(lmonkey)
    raise "Both branches uses human" if path.includes?(rmonkey)
    nmonkey, omonkey = lmonkey, rmonkey
  else
    raise "Both branches does not include humn" if !path.includes?(rmonkey)
    nmonkey, omonkey = rmonkey, lmonkey
  end
  nvalue = monkey_value(monkeys, omonkey, cache)

  nvalue = case op
           when "+"
             value - nvalue
           when "/"
             if nmonkey == lmonkey
               value * nvalue
             else
               nvalue / value
             end
           when "*"
             value // nvalue
           when "-"
             if nmonkey == lmonkey
               value + nvalue
             else
               -(value - nvalue)
             end
           else
             raise "Operation #{op} is not implementated"
           end

  detect_number(monkeys, path, nmonkey, nvalue, cache)
end

def problem21_part_two(records : Array(String))
  monkeys = Hash(String, Array(String | Int64) | Int64).new

  records.each do |instruction|
    next if instruction == ""
    monkey, operation = instruction.strip.split(": ")
    monkeys[monkey] = if !operation.includes?(" ")
                        operation.to_i64
                      else
                        operation.split(/ +/).map { |e| e.to_i64? ? e.to_i64 : e }.as(Array(Int64 | String))
                      end
  end

  root = monkeys["root"].as(Array(Int64 | String))
  lmonkey = root[0].as(String)
  rmonkey = root[2].as(String)

  cache = Hash(String, Int64).new

  lpath = Set(String).new
  lvalue = monkey_value(monkeys, lmonkey, cache, lpath)

  rpath = Set(String).new
  rvalue = monkey_value(monkeys, rmonkey, cache, rpath)

  cache = Hash(String, Int64).new
  if lpath.size > 0
    detect_number(monkeys, lpath, lmonkey, rvalue, cache)
  else
    detect_number(monkeys, rpath, rmonkey, lvalue, cache)
  end
end
