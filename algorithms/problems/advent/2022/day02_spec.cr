require "spec"
require "./day02"

describe "Day 2" do
  describe "problem2" do
    it "works for sample" do
      input = <<-EOS
      A Y
      B X
      C Z
      EOS
      actual = problem2(input.split("\n"))
      actual.should eq(15) # 8 + 1 + 6
    end

    it "win single line" do
      input = <<-EOS
      B Z
      EOS
      actual = problem2(input.split("\n"))
      actual.should eq(9) # 6 + 3
    end

    it "sample 2" do
      input = <<-EOS
      B Z
      C Z
      B X
      A Y
      EOS
      actual = problem2(input.split("\n"))
      actual.should eq(24)
    end
  end

  describe "problem2_part_two" do
    it "works for sample" do
      input = <<-EOS
      A Y
      B X
      C Z
      EOS
      actual = problem2_part_two(input.split("\n"))
      actual.should eq(12)
    end

    it "easy" do
      input = <<-EOS
      A X
      A Y
      A Z
      B X
      B Y
      B Z
      C X
      C Y
      C Z
      EOS
      actual = problem2_part_two(input.split("\n"))
      actual.should eq(45)
    end
  end
end
