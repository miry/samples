# https://adventofcode.com/2022/day/12
#
# --- Day 12: Hill Climbing Algorithm ---
#
# You try contacting the Elves using your handheld device, but the river you're following must be too low to get a decent signal.
#
# You ask the device for a heightmap of the surrounding area (your puzzle input). The heightmap shows the local area from above broken into a grid; the elevation of each square of the grid is given by a single lowercase letter, where a is the lowest elevation, b is the next-lowest, and so on up to the highest elevation, z.
#
# Also included on the heightmap are marks for your current position (S) and the location that should get the best signal (E). Your current position (S) has elevation a, and the location that should get the best signal (E) has elevation z.
#
# You'd like to reach E, but to save energy, you should do it in as few steps as possible. During each step, you can move exactly one square up, down, left, or right. To avoid needing to get out your climbing gear, the elevation of the destination square can be at most one higher than the elevation of your current square; that is, if your current elevation is m, you could step to elevation n, but not to elevation o. (This also means that the elevation of the destination square can be much lower than the elevation of your current square.)
#
# For example:
#
# Sabqponm
# abcryxxl
# accszExk
# acctuvwj
# abdefghi
#
# Here, you start in the top-left corner; your goal is near the middle. You could start by moving down or right, but eventually you'll need to head toward the e at the bottom. From there, you can spiral around to the goal:
#
# v..v<<<<
# >v.vv<<^
# .>vv>E^^
# ..v>>>^^
# ..>>>>>^
#
# In the above diagram, the symbols indicate whether the path exits each square moving up (^), down (v), left (<), or right (>). The location that should get the best signal is still E, and . marks unvisited squares.
#
# This path reaches the goal in 31 steps, the fewest possible.
#
# What is the fewest steps required to move from your current position to the location that should get the best signal?
#
# Your puzzle answer was 517.
# --- Part Two ---
#
# As you walk up the hill, you suspect that the Elves will want to turn this into a hiking trail. The beginning isn't very scenic, though; perhaps you can find a better starting point.
#
# To maximize exercise while hiking, the trail should start as low as possible: elevation a. The goal is still the square marked E. However, the trail should still be direct, taking the fewest steps to reach its goal. So, you'll need to find the shortest path from any square at elevation a to the square marked E.
#
# Again consider the example from above:
#
# Sabqponm
# abcryxxl
# accszExk
# acctuvwj
# abdefghi
#
# Now, there are six choices for starting position (five marked a, plus the square marked S that counts as being at elevation a). If you start at the bottom-left square, you can reach the goal most quickly:
#
# ...v<<<<
# ...vv<<^
# ...v>E^^
# .>v>>>^^
# >^>>>>>^
#
# This path reaches the goal in only 29 steps, the fewest possible.
#
# What is the fewest steps required to move starting from any square with elevation a to the location that should get the best signal?
#
# Your puzzle answer was 512.

def problem12(records : Array(String))
  heightmap = Hash(Tuple(Int32, Int32), Int32).new
  start = {0, 0}
  finish = {0, 0}
  records.each_with_index do |instruction, row|
    next if instruction == ""
    instruction.chars.each_with_index do |height, col|
      coord = {row, col}
      heightmap[coord] = case height
                         when 'S'
                           start = coord
                           'a'.ord
                         when 'E'
                           finish = coord
                           'z'.ord
                         else
                           height.ord
                         end
    end
  end

  queue = [start] of Tuple(Int32, Int32)
  visited = Hash(Tuple(Int32, Int32), Int32).new
  visited[start] = 0

  neighbours = {
    "up":    {-1, 0},
    "down":  {1, 0},
    "left":  {0, -1},
    "right": {0, 1},
  }

  distance = 0
  while true
    distance += 1
    new_queue = [] of Tuple(Int32, Int32)
    queue.each do |coord|
      val = heightmap[coord]
      neighbours.values.map { |c| {coord[0] + c[0], coord[1] + c[1]} }.select { |c| heightmap.has_key?(c) && !visited.has_key?(c) && (heightmap[c] - val <= 1) }.each do |ncoord|
        return distance if finish == ncoord
        new_queue << ncoord
        visited[ncoord] = distance
      end
    end
    break if new_queue.empty?
    queue = new_queue
  end
  distance
end

# --- Part Two ---

def problem12_part_two(records : Array(String))
  heightmap = Hash(Tuple(Int32, Int32), Int32).new
  finish = {0, 0}
  queue = [] of Tuple(Int32, Int32)
  records.each_with_index do |instruction, row|
    next if instruction == ""
    instruction.chars.each_with_index do |height, col|
      coord = {row, col}
      heightmap[coord] = case height
                         when 'a'
                           queue << coord
                           'a'.ord
                         when 'S'
                           queue << coord
                           'a'.ord
                         when 'E'
                           finish = coord
                           'z'.ord
                         else
                           height.ord
                         end
    end
  end

  visited = Hash(Tuple(Int32, Int32), Int32).new
  queue.each do |c|
    visited[c] = 0
  end

  neighbours = {
    "up":    {-1, 0},
    "down":  {1, 0},
    "left":  {0, -1},
    "right": {0, 1},
  }

  distance = 0
  while true
    distance += 1
    new_queue = [] of Tuple(Int32, Int32)
    queue.each do |coord|
      val = heightmap[coord]
      neighbours.values.map { |c| {coord[0] + c[0], coord[1] + c[1]} }.select { |c| heightmap.has_key?(c) && !visited.has_key?(c) && (heightmap[c] - val <= 1) }.each do |ncoord|
        return distance if finish == ncoord
        new_queue << ncoord
        visited[ncoord] = distance
      end
    end
    break if new_queue.empty?
    queue = new_queue
  end
  distance
end
