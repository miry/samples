require "spec"
require "./day12"

describe "Day 12" do
  describe "problem12" do
    it "sample" do
      input = <<-EOF
      Sabqponm
      abcryxxl
      accszExk
      acctuvwj
      abdefghi
      EOF
      actual = problem12(input.split("\n"))
      actual.should eq(31)
    end
  end

  describe "problem12_part_two" do
    it "sample" do
      input = <<-EOF
      Sabqponm
      abcryxxl
      accszExk
      acctuvwj
      abdefghi
      EOF
      actual = problem12_part_two(input.split("\n"))
      actual.should eq(29)
    end
  end
end
