require "spec"
require "./day05"

describe "Day 5" do
  describe "problem5" do
    it "sample" do
      input = <<-EOF
          [D]
      [N] [C]
      [Z] [M] [P]
       1   2   3

      move 1 from 2 to 1
      move 3 from 1 to 3
      move 2 from 2 to 1
      move 1 from 1 to 2
      EOF
      actual = problem5(input.split("\n"))
      actual.should eq("CMZ")
    end
  end

  describe "problem5_part_two" do
    it "sample" do
      input = <<-EOF
          [D]
      [N] [C]
      [Z] [M] [P]
       1   2   3

      move 1 from 2 to 1
      move 3 from 1 to 3
      move 2 from 2 to 1
      move 1 from 1 to 2
      EOF
      actual = problem5_part_two(input.split("\n"))
      actual.should eq("MCD")
    end
  end
end
