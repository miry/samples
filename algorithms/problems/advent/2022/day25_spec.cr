require "spec"
require "./day25"

describe SNAFU do
  it "converts from decimal to snafu" do
    [
      {1, "1"},
      {2, "2"},
      {3, "1="},
      {4, "1-"},
      {5, "10"},
      {6, "11"},
      {7, "12"},
      {8, "2="},
      {9, "2-"},
      {10, "20"},
      {15, "1=0"},
      {20, "1-0"},
      {2022, "1=11-2"},
      {12345, "1-0---0"},
      {314159265, "1121-1110-1=0"},
    ].each do |examplar|
      subject = SNAFU.from_int(examplar[0])
      subject.decimal.should eq(examplar[0])

      actual = subject.snafu
      actual.should eq(examplar[1]), "Decimal #{examplar[0]} to SNAFU got #{actual}, expected #{examplar[1]}"
    end
  end

  it "converts from snafu to decimal" do
    [
      {1, "1"},
      {2, "2"},
      {3, "1="},
      {4, "1-"},
      {5, "10"},
      {6, "11"},
      {7, "12"},
      {8, "2="},
      {9, "2-"},
      {10, "20"},
      {15, "1=0"},
      {20, "1-0"},
      {2022, "1=11-2"},
      {12345, "1-0---0"},
      {314159265, "1121-1110-1=0"},
    ].each do |examplar|
      subject = SNAFU.from_str(examplar[1])
      subject.snafu.should eq(examplar[1])
      actual = subject.decimal
      expected = examplar[0]
      actual.should eq(expected), "SNAFU #{examplar[1]} to Decimal got #{actual}, expected #{expected}"
    end
  end
end

describe "Day 25" do
  describe "problem25" do
    it "sample" do
      input = <<-EOF
      1=-0-2
      12111
      2=0=
      21
      2=01
      111
      20012
      112
      1=-1=
      1-12
      12
      1=
      122
      EOF
      actual = problem25(input.split("\n"))
      actual.should eq("2=-1=0")
    end
  end
end
