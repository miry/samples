require "spec"
require "./day01"

describe "Day 1" do
  describe "problem1" do
    it "sample 1" do
      input = <<-EOS
      1000
      2000
      3000

      4000

      5000
      6000

      7000
      8000
      9000

      10000
      EOS

      actual = problem1(input.split("\n"))
      actual.should eq(24000)
    end
  end

  describe "problem1_part_two" do
    it "sample 1" do
      input = <<-EOS
      1000
      2000
      3000

      4000

      5000
      6000

      7000
      8000
      9000

      10000
      EOS

      actual = problem1_part_two(input.split("\n"))
      actual.should eq(45000)
    end
  end
end
