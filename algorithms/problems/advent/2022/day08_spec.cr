require "spec"
require "./day08"

describe "Day 8" do
  describe "problem8" do
    it "sample" do
      input = <<-EOF
      30373
      25512
      65332
      33549
      35390
      EOF
      actual = problem8(input.split("\n"))
      actual.should eq(21)
    end
  end

  describe "problem8_part_two" do
    it "sample" do
      input = <<-EOF
      30373
      25512
      65332
      33549
      35390
      EOF
      actual = problem8_part_two(input.split("\n"))
      actual.should eq(8)
    end
  end
end
