require "spec"
require "./day21"

describe "Day 21" do
  describe "problem21" do
    it "sample" do
      input = <<-EOF
      root: pppw + sjmn
      dbpl: 5
      cczh: sllz + lgvd
      zczc: 2
      ptdq: humn - dvpt
      dvpt: 3
      lfqf: 4
      humn: 5
      ljgn: 2
      sjmn: drzm * dbpl
      sllz: 4
      pppw: cczh / lfqf
      lgvd: ljgn * ptdq
      drzm: hmdt - zczc
      hmdt: 32
      EOF
      actual = problem21(input.split("\n"))
      actual.should eq(152)
    end
  end

  describe "problem21_part_two" do
    it "sample" do
      input = <<-EOF
      root: pppw + sjmn
      dbpl: 5
      cczh: sllz + lgvd
      zczc: 2
      ptdq: humn - dvpt
      dvpt: 3
      lfqf: 4
      humn: 5
      ljgn: 2
      sjmn: drzm * dbpl
      sllz: 4
      pppw: cczh / lfqf
      lgvd: ljgn * ptdq
      drzm: hmdt - zczc
      hmdt: 32
      EOF
      actual = problem21_part_two(input.split("\n"))
      actual.should eq(301)
    end

    it "when minus" do
      input = <<-EOF
      root: aaaa + bbbb
      aaaa: dddd - humn
      bbbb: 10
      dddd: 5
      humn: 5
      EOF
      actual = problem21_part_two(input.split("\n"))
      actual.should eq(-5)
    end

    it "when div" do
      input = <<-EOF
      root: aaaa + bbbb
      aaaa: dddd / humn
      bbbb: 10
      dddd: 50
      humn: 1
      EOF
      actual = problem21_part_two(input.split("\n"))
      actual.should eq(5)
    end
  end
end
