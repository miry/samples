require "spec"
require "./day20"

describe "Day 20" do
  describe "problem20" do
    it "sample" do
      input = <<-EOF
      1
      2
      -3
      3
      -2
      0
      4
      EOF
      actual = problem20(input.split("\n"))
      actual.should eq(3)
    end

    it "sample2" do
      input = <<-EOF
      1
      0
      2
      -2
      EOF
      actual = problem20(input.split("\n"))
      actual.should eq(0)
    end

    it "sample2" do
      input = <<-EOF
      6
      7
      8
      9
      0
      10
      EOF
      actual = problem20(input.split("\n"))
      actual.should eq(19)
    end
  end

  describe "problem20_part_two" do
    it "sample" do
      input = <<-EOF
      1
      2
      -3
      3
      -2
      0
      4
      EOF
      actual = problem20_part_two(input.split("\n"))
      actual.should eq(1623178306)
    end
  end
end
