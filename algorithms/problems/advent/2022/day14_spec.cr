require "spec"
require "./day14"

describe "Day 14" do
  describe "problem14" do
    it "sample" do
      input = <<-EOF
      498,4 -> 498,6 -> 496,6
      503,4 -> 502,4 -> 502,9 -> 494,9
      EOF
      actual = problem14(input.split("\n"))
      actual.should eq(24)
    end
  end

  describe "problem14_part_two" do
    it "sample" do
      input = <<-EOF
      498,4 -> 498,6 -> 496,6
      503,4 -> 502,4 -> 502,9 -> 494,9
      EOF
      actual = problem14_part_two(input.split("\n"))
      actual.should eq(93)
    end
  end
end
