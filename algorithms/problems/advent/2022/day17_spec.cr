require "spec"
require "./day17"

describe "Day 17" do
  describe "problem17" do
    it "sample" do
      input = <<-EOF
      >>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>
      EOF
      actual = problem17(input.split("\n"))
      actual.should eq(3068)
    end
  end

  describe "problem17_part_two" do
    it "sample1" do
      input = <<-EOF
      >>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>
      EOF
      actual = problem17_part_two(input.split("\n"), 2022)
      actual.should eq(3068)
    end

    it "sample2" do
      input = <<-EOF
      >>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>
      EOF
      actual = problem17_part_two(input.split("\n"))
      actual.should eq(1514285714288)
    end
  end

  describe "detect_pattern" do
    it "could not find anyting" do
      sequence = [1, 2]
      sequense_map = Hash(Int128, Array(Int128)).new
      sequence.each_with_index do |v, i|
        sequense_map[v] ||= Array(Int128).new
        sequense_map[v].insert(0, i)
      end

      actual = detect_pattern(sequence, sequense_map)
      actual.should be_nil
    end

    it "find single element sequence" do
      sequence = [1, 1]
      sequense_map = Hash(Int128, Array(Int128)).new
      sequence.each_with_index do |v, i|
        sequense_map[v] ||= Array(Int128).new
        sequense_map[v].insert(0, i)
      end

      actual = detect_pattern(sequence, sequense_map)
      actual.should eq(1)
    end

    it "find double element sequence" do
      sequence = [1, 2, 1, 2]
      sequense_map = Hash(Int128, Array(Int128)).new
      sequence.each_with_index do |v, i|
        sequense_map[v] ||= Array(Int128).new
        sequense_map[v].insert(0, i)
      end

      actual = detect_pattern(sequence, sequense_map)
      actual.should eq(2)
    end

    it "mixed partial element sequence" do
      sequence = [1, 2, 1]
      sequense_map = Hash(Int128, Array(Int128)).new
      sequence.each_with_index do |v, i|
        sequense_map[v] ||= Array(Int128).new
        sequense_map[v].insert(0, i)
      end

      actual = detect_pattern(sequence, sequense_map)
      actual.should be_nil
    end

    it "extra element between" do
      sequence = [1, 2, 3, 1, 2]
      sequense_map = Hash(Int128, Array(Int128)).new
      sequence.each_with_index do |v, i|
        sequense_map[v] ||= Array(Int128).new
        sequense_map[v].insert(0, i)
      end

      actual = detect_pattern(sequence, sequense_map)
      actual.should be_nil
    end

    it "extra element in the start" do
      sequence = [4, 1, 2, 1, 2]
      sequense_map = Hash(Int128, Array(Int128)).new
      sequence.each_with_index do |v, i|
        sequense_map[v] ||= Array(Int128).new
        sequense_map[v].insert(0, i)
      end

      actual = detect_pattern(sequence, sequense_map)
      actual.should eq(2)
    end

    it "duplicated symbols in single sequence" do
      sequence = [4, 1, 2, 2, 3, 2, 1, 2, 2, 3, 2]
      sequense_map = Hash(Int128, Array(Int128)).new
      sequence.each_with_index do |v, i|
        sequense_map[v] ||= Array(Int128).new
        sequense_map[v].insert(0, i)
      end

      actual = detect_pattern(sequence, sequense_map)
      actual.should eq(5)
    end

    it "duplicated symbols in single sequence and extra element" do
      sequence = [4, 1, 2, 2, 3, 2, 5, 1, 2, 2, 3, 2]
      sequense_map = Hash(Int128, Array(Int128)).new
      sequence.each_with_index do |v, i|
        sequense_map[v] ||= Array(Int128).new
        sequense_map[v].insert(0, i)
      end

      actual = detect_pattern(sequence, sequense_map)
      actual.should be_nil
    end

    it "real sample" do
      sequence =
        [
          3, 6, 9, 7, 9, 7, 11, 7, 10, 9, 5, 8, 3, 9, 10, 8, 5, 7, 8, 4, 8, 9, 9, 10, 5, 9, 9, 9, 7, 5, 9, 4, 9, 8, 4, 7, 7, 7, 6, 9, 11, 5, 7, 8, 6, 4, 10, 10, 9, 6, 9,
          13, 9, 6, 8, 6, 8, 10, 10, 6, 7, 8, 9, 7, 10, 6, 7, 11, 7, 7, 9, 8, 9, 8, 11, 8, 11, 7, 8, 7, 9, 10, 8, 9, 10, 9, 6, 8, 9, 9, 9, 10, 6, 13, 8, 9, 6, 8, 9, 8, 4, 5, 7, 7, 7, 8, 11, 12, 9, 10, 6, 9, 11, 8, 9, 9, 6, 8, 7, 9, 11, 7, 5, 10, 8, 9, 6, 8, 13, 7, 11, 7, 7, 9, 8, 10, 7, 6, 7, 9, 5, 7, 4, 7, 9, 8, 10, 11, 9, 8, 6, 7, 9, 6, 7, 7, 6, 7, 9, 11, 11, 8, 11, 11, 11, 10, 7, 5, 7, 7, 7, 9, 10, 8, 7, 5, 8, 10, 8, 5, 7, 10, 9, 7, 8, 6, 8, 5, 8, 8, 13, 7, 10, 9, 7, 10, 6, 7, 11, 10, 10, 6, 6, 9, 7, 8, 9, 4, 8, 11, 6, 10, 7, 8, 6, 5, 8, 9, 10, 11, 7, 6, 11, 8, 5, 6, 6, 12, 9, 3, 7, 7, 4, 7, 7, 6, 7, 6, 7, 7, 6, 11, 8, 8, 6, 7, 6, 9, 9, 8, 7, 7, 6, 8, 7, 6, 11, 10, 9, 8, 8, 11, 8, 11, 4, 10, 9, 11, 7, 12, 6, 7, 8, 6, 7, 8, 10, 8, 12, 6, 8, 7, 8, 7, 8, 11, 7, 8, 4, 6, 7, 6, 11, 11, 10, 7, 12, 7, 9, 6, 9, 7, 9, 8, 5, 7, 10, 9, 7, 10, 4, 10, 7, 8, 6, 5, 11, 9, 7, 7, 7, 7, 5, 7, 7, 10, 6, 8, 9, 7, 4, 8, 8, 9, 11, 6, 9, 7, 6, 5, 10, 10, 8, 4, 8, 3, 6, 9, 7, 9, 7, 11, 7, 10, 9, 5, 8, 3, 9, 10, 8, 5, 7, 8, 4, 8, 9, 9, 10, 5, 9, 9, 9, 7, 5, 9, 4, 9, 8, 4, 7, 7, 7, 6, 9, 11, 5, 7, 8, 6, 4, 10, 10, 9, 6, 9,
          13, 9, 6, 8, 6, 8, 10, 10, 6, 7, 8, 9, 7, 10, 6, 7, 11, 7, 7, 9, 8, 9, 8, 11, 8, 11, 7, 8, 7, 9, 10, 8, 9, 10, 9, 6, 8, 9, 9, 9, 10, 6, 13, 8, 9, 6, 8, 9, 8, 4, 5, 7, 7, 7, 8, 11, 12, 9, 10, 6, 9, 11, 8, 9, 9, 6, 8, 7, 9, 11, 7, 5, 10, 8, 9, 6, 8, 13, 7, 11, 7, 7, 9, 8, 10, 7, 6, 7, 9, 5, 7, 4, 7, 9, 8, 10, 11, 9, 8, 6, 7, 9, 6, 7, 7, 6, 7, 9, 11, 11, 8, 11, 11, 11, 10, 7, 5, 7, 7, 7, 9, 10, 8, 7, 5, 8, 10, 8, 5, 7, 10, 9, 7, 8, 6, 8, 5, 8, 8, 13, 7, 10, 9, 7, 10, 6, 7, 11, 10, 10, 6, 6, 9, 7, 8, 9, 4, 8, 11, 6, 10, 7, 8, 6, 5, 8, 9, 10, 11, 7, 6, 11, 8, 5, 6, 6, 12, 9, 3, 7, 7, 4, 7, 7, 6, 7, 6, 7, 7, 6, 11, 8, 8, 6, 7, 6, 9, 9, 8, 7, 7, 6, 8, 7, 6, 11, 10, 9, 8, 8, 11, 8, 11, 4, 10, 9, 11, 7, 12, 6, 7, 8, 6, 7, 8, 10, 8, 12, 6, 8, 7, 8, 7, 8, 11, 7, 8, 4, 6, 7, 6, 11, 11, 10, 7, 12, 7, 9, 6, 9, 7, 9, 8, 5, 7, 10, 9, 7, 10, 4, 10, 7, 8, 6, 5, 11, 9, 7, 7, 7, 7, 5, 7, 7, 10, 6, 8, 9, 7, 4, 8, 8, 9, 11, 6, 9, 7, 6, 5, 10, 10, 8, 4, 8, 3, 6, 9, 7, 9, 7, 11, 7, 10, 9, 5, 8, 3, 9, 10, 8, 5, 7, 8, 4, 8, 9, 9, 10, 5, 9, 9, 9, 7, 5, 9, 4, 9, 8, 4, 7, 7, 7, 6, 9, 11, 5, 7, 8, 6, 4, 10, 10, 9, 6, 9,
          13, 9, 6, 8, 6, 8, 10, 10, 6, 7, 8, 9, 7, 10, 6, 7, 11, 7, 7, 9, 8, 9, 8, 11, 8, 11, 7, 8, 7, 9, 10, 8, 9, 10, 9, 6, 8, 9, 9, 9, 10, 6, 13, 8, 9, 6, 8, 9, 8, 4, 5, 7, 7, 7, 8, 11, 12, 9, 10, 6, 9, 11, 8, 9, 9, 6, 8, 7, 9, 11, 7, 5, 10, 8, 9, 6, 8, 13, 7, 11, 7, 7, 9, 8, 10, 7, 6, 7, 9, 5, 7, 4, 7, 9, 8, 10, 11, 9, 8, 6, 7, 9, 6, 7, 7, 6, 7, 9, 11, 11, 8, 11, 11, 11, 10, 7, 5, 7, 7, 7, 9, 10, 8, 7, 5, 8, 10, 8, 5, 7, 10, 9, 7, 8, 6, 8, 5, 8, 8, 13, 7, 10, 9, 7, 10, 6, 7, 11, 10, 10, 6, 6, 9, 7, 8, 9, 4, 8, 11, 6, 10, 7, 8, 6, 5, 8, 9, 10, 11, 7, 6, 11, 8, 5, 6, 6, 12, 9, 3, 7, 7, 4, 7, 7, 6, 7, 6, 7, 7, 6, 11, 8, 8, 6, 7, 6, 9, 9, 8, 7, 7, 6, 8, 7, 6, 11, 10, 9, 8, 8, 11, 8, 11, 4, 10, 9, 11, 7, 12, 6, 7, 8, 6, 7, 8, 10, 8, 12, 6, 8, 7, 8, 7, 8, 11, 7, 8, 4, 6, 7, 6, 11, 11, 10, 7, 12, 7, 9, 6, 9, 7, 9, 8, 5, 7, 10, 9, 7, 10, 4, 10, 7, 8, 6, 5, 11, 9, 7, 7, 7, 7, 5, 7, 7, 10, 6, 8, 9, 7, 4, 8, 8, 9, 11, 6, 9, 7, 6, 5, 10, 10, 8, 4, 8, 3, 6, 9, 7, 9, 7, 11, 7, 10, 9, 5, 8, 3, 9, 10, 8, 5, 7, 8, 4, 8, 9, 9, 10, 5, 9, 9, 9, 7, 5, 9, 4, 9, 8, 4, 7, 7, 7, 6, 9, 11, 5, 7, 8, 6, 4, 10, 10, 9, 6, 9,
        ]
      sequense_map = Hash(Int128, Array(Int128)).new
      sequence.each_with_index do |v, i|
        sequense_map[v] ||= Array(Int128).new
        sequense_map[v].insert(0, i)
      end

      actual = detect_pattern(sequence, sequense_map)
      actual.should eq(345)
    end
  end
end
