require "spec"
require "./day18"

describe "Day 18" do
  describe "problem18" do
    it "sample" do
      input = <<-EOF
      2,2,2
      1,2,2
      3,2,2
      2,1,2
      2,3,2
      2,2,1
      2,2,3
      2,2,4
      2,2,6
      1,2,5
      3,2,5
      2,1,5
      2,3,5
      EOF
      actual = problem18(input.split("\n"))
      actual.should eq(64)
    end
  end

  describe "problem18_part_two" do
    it "sample" do
      input = <<-EOF
      2,2,2
      1,2,2
      3,2,2
      2,1,2
      2,3,2
      2,2,1
      2,2,3
      2,2,4
      2,2,6
      1,2,5
      3,2,5
      2,1,5
      2,3,5
      EOF
      actual = problem18_part_two(input.split("\n"))
      actual.should eq(58)
    end
  end
end
