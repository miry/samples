require "spec"
require "./day24"

describe "Day 24" do
  describe "move_blizards" do
    it "single move" do
      blizards = Array(Tuple(Coord, Char)).new
      blizards << { {x: 1, y: 2}, '>' }
      blizards << { {x: 4, y: 4}, 'v' }
      wall = Set{
        {x: 0, y: 0},
        {x: 2, y: 0},
        {x: 3, y: 0},
        {x: 4, y: 0},
        {x: 5, y: 0},
        {x: 6, y: 0},
        {x: 0, y: 1},
        {x: 6, y: 1},
        {x: 0, y: 2},
        {x: 6, y: 2},
        {x: 0, y: 3},
        {x: 6, y: 3},
        {x: 0, y: 4},
        {x: 6, y: 4},
        {x: 0, y: 5},
        {x: 6, y: 5},
        {x: 0, y: 6},
        {x: 1, y: 6},
        {x: 2, y: 6},
        {x: 3, y: 6},
        {x: 4, y: 6},
        {x: 6, y: 6},
      }
      field_size = {x: 7, y: 7}
      blizards, field = move_blizards blizards, wall, field_size
      field.should eq({ {x: 2, y: 2} => '>', {x: 4, y: 5} => 'v' })
      blizards.should eq([{ {x: 2, y: 2}, '>' }, { {x: 4, y: 5}, 'v' }])
    end
  end

  describe "problem24" do
    it "sample" do
      input = <<-EOF
      #.#####
      #.....#
      #>....#
      #.....#
      #...v.#
      #.....#
      #####.#
      EOF
      actual = problem24(input.split("\n"))
      actual.should eq(10)
    end

    it "sample 2" do
      input = <<-EOF
      #.######
      #>>.<^<#
      #.<..<<#
      #>v.><>#
      #<^v^^>#
      ######.#
      EOF
      actual = problem24(input.split("\n"))
      actual.should eq(18)
    end
  end

  describe "problem24_part_two" do
    it "sample" do
      input = <<-EOF
      #.######
      #>>.<^<#
      #.<..<<#
      #>v.><>#
      #<^v^^>#
      ######.#
      EOF
      actual = problem24_part_two(input.split("\n"))
      actual.should eq(54)
    end
  end
end
