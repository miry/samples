require "spec"
require "./day07"

describe "Day 7" do
  describe "problem7" do
    it "sample" do
      input = <<-EOF
      $ cd /
      $ ls
      dir a
      14848514 b.txt
      8504156 c.dat
      dir d
      $ cd a
      $ ls
      dir e
      29116 f
      2557 g
      62596 h.lst
      $ cd e
      $ ls
      584 i
      $ cd ..
      $ cd ..
      $ cd d
      $ ls
      4060174 j
      8033020 d.log
      5626152 d.ext
      7214296 k
      EOF
      actual = problem7(input.split("\n"))
      actual.should eq(95437)
    end
  end

  describe "problem7_part_two" do
    it "sample" do
      input = <<-EOF
      $ cd /
      $ ls
      dir a
      14848514 b.txt
      8504156 c.dat
      dir d
      $ cd a
      $ ls
      dir e
      29116 f
      2557 g
      62596 h.lst
      $ cd e
      $ ls
      584 i
      $ cd ..
      $ cd ..
      $ cd d
      $ ls
      4060174 j
      8033020 d.log
      5626152 d.ext
      7214296 k
      EOF
      actual = problem7_part_two(input.split("\n"))
      actual.should eq(24933642)
    end
  end
end
