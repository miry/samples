require "option_parser"
require "big"
require "benchmark"

require "../utils"
require "../progress"

require "./day01"
require "./day02"
require "./day03"
require "./day04"
require "./day05"
require "./day06"
require "./day07"
require "./day08"
require "./day09"
require "./day10"
require "./day11"
require "./day12"
require "./day13"
require "./day14"
require "./day15"
require "./day16"
require "./day17"
require "./day18"
require "./day19"
require "./day20"
require "./day21"
require "./day22"
require "./day23"
require "./day24"
require "./day25"

def run
  exit = false
  day : Int32 = Time.local.day
  part : Int64 = 1
  input_file_path = ""
  input = STDIN

  OptionParser.parse do |parser|
    parser.banner = "Usage: advent [arguments]"
    parser.on("-d DAY", "--day=DAY", "Advent Day from 1 to 25. Deafult: current day of month") { |d| day = d.to_i32 }
    parser.on("-p PART", "--part=PART", "Advent Day Part from 1 to 2. Default: 1") { |p| part = p.to_i64 }
    parser.on("-i FILE", "--input=FILE", "Input data file path.") { |p| input_file_path = p }
    parser.on("-h", "--help", "Show this help") { puts parser; exit = true }

    parser.unknown_args do |before, after|
      (before | after).join(" ")
    end
  end

  return if exit

  if day < 1 && day > 24
    raise "Day should be from 1 to 25"
  end

  if part != 1 && part != 2
    raise "Part should be 1 or 2"
  end

  # Read from parameter path or default path
  if !input_file_path.empty?
    if !File.exists?(input_file_path)
      raise "Could not open #{input_file_path}"
    end
    input = File.open(input_file_path)
  elsif STDIN.tty?
    input_file_path = "./input/day%02d.txt" % day
    if File.exists?(input_file_path)
      input = File.open(input_file_path)
    else
      raise "Missing input: neither stdin neither #{input_file_path}"
    end
  else
    input = STDIN
  end

  entries = [] of String
  input.each_line do |line|
    entries << line
  end

  task = day + 0.1 * part
  answer = 0
  {% begin %}
  answer = case task
           {% for i in (1..24) %}
           when {{i.id}}.1
             puts "--- Day {{i.id}} ---"
             puts "--- Part One ---"
             Day{{i.id}}.problem1(entries)
           when {{i.id}}.2
             puts "--- Day {{i.id}}: ---"
             puts "--- Part Two ---"
             Day{{i.id}}.problem2(entries)
           {% end %}
           when 25.1
             puts "--- Day 25: ---"
             puts "--- Part One ---"
             Day25.problem1(entries)
           when 25.2
             puts "--- Day 25: ---"
             puts "--- Part Two ---"
             puts "Complete all other tasks!"
           else
             raise "Day #{day} is not implemented"
           end
  {% end %}
  if answer.is_a?(String) && answer.includes?("\n")
    puts "Answer:\n---\n#{answer}\n---"
  else
    puts "Answer: #{answer}"
  end
end

elapsed_time = Time::Span.new
elapsed_memory = Benchmark.memory do
  elapsed_time = Time.measure do
    run()
  end
end
puts "(execute time: #{elapsed_time.total_milliseconds} ms, memory: #{elapsed_memory} bytes)"

# vim: filetype=crystal syntax=crystal
