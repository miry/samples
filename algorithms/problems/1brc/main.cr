# https://1brc.dev/#%F0%9F%92%AA-the-challenge

class Metric
  property name

  @min : Float64?
  @mean : Float64?
  @max : Float64?

  def initialize(@name : String)
    @data = Array(Float64).new
  end

  def <<(val : Float64)
    @data << val
    @data.sort!
    if @max.nil? || val > @max.not_nil!
      @max = val
    end
    if @min.nil? || val < @min.not_nil!
      @min = val
    end
  end

  def calculate
    @mean = @data[@data.size // 2]
  end

  def to_s
    calculate
    "#{@name}=#{@min}/#{@mean}/#{@max}"
  end
end

def read_file(path = "input.txt")
  File.read_lines(path)
end

def parse(line)
  line.split(";", 2)
end

def run(path)
  metrics = Hash(String, Metric).new(initial_capacity: 10_000)
  read_file(path).each do |line|
    next if line == ""
    name, val = parse(line)
    if !metrics.has_key?(name)
      metrics[name] = Metric.new(name)
    end
    metrics[name] << val.to_f
  end
  puts "-----------"

  print "{"
  metrics.keys.sort.each_with_index do |name, i|
    metric = metrics[name]
    print ", " if i != 0
    print metric.to_s
  end
  print "}\n"
end

run("target.txt")
# run("small.txt")
# run("input.txt")
