// # Input:
// array1 = [10, 20, 30, 40]
// array2 = [50, 60, 70, 80, 90]
// target = 100
//
// # Output:
// [[10, 90], [20, 80], [30, 70], [40, 60]]
//
//
// # Input:
// array1 = [3, 5, 9, 2]
// array2 = [8, 1, 4, 7, 6]
// target = 10
//
// # Output:
// [[3, 7], [9, 1], [2, 8]]

// arr1 , arr2
// target = 10
//

package problems_test

import (
	"testing"
	"fmt"

	"codeberg.org/miry/samples/algorithms/problems"
)

func TestTwoSumClosest(t *testing.T) {
	tt := []struct {
		arr1     []int
		arr2     []int
		target   int
		expected [][]int
	}{
		{
			[]int{10, 20, 30, 40},
			[]int{50, 60, 70, 80, 90},
			100,
			[][]int{
				[]int{10, 90},
				[]int{20, 80},
				[]int{30, 70},
				[]int{40, 60},
			},
		},
		{
			[]int{3, 5, 9, 2},
			[]int{8, 1, 4, 7, 6},
			10,
			[][]int{
				[]int{3, 7},
				[]int{9, 1},
				[]int{2, 8},
			},
		},
		{
			[]int{3, 3, 5, 9, 2},
			[]int{8, 1, 4, 7, 6},
			10,
			[][]int{
				[]int{3, 7},
				[]int{3, 7},
				[]int{9, 1},
				[]int{2, 8},
			},
		},
	}

	fmt.Println("TwoSums:")
	for _, tc := range tt {
		actual := problems.TwoSums(tc.arr1, tc.arr2, tc.target)
		fmt.Printf("actual: %v, expected: %v\n", actual, tc.expected)
	}

	fmt.Println("TwoSumsClosestBruetForce:")
	actual := problems.TwoSumsClosestBruetForce([]int{-2, -1, 1, 3}, []int{0, 1, 3}, 5)
	fmt.Printf("actual: %v, expected: [[1 3] [3 3] [3 1]]\n", actual)

	fmt.Println("TwoSumsClosestSorting:")
	actual = problems.TwoSumsClosestSorting([]int{-2, -1, 1, 3}, []int{0, 1, 3}, 5)
	fmt.Printf("actual: %v, expected: [[1 3] [3 3] [3 1]]\n", actual)

	actual = problems.TwoSumsClosestSorting([]int{-2, -1, 1, 3}, []int{0, 1, 3, 7, 8}, 6)
	fmt.Printf("actual: %v, expected: [[-2 8] [-1 7] [3 3]]\n", actual)
}
