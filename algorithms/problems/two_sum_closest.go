// # Input:
// array1 = [10, 20, 30, 40]
// array2 = [50, 60, 70, 80, 90]
// target = 100
//
// # Output:
// [[10, 90], [20, 80], [30, 70], [40, 60]]
//
//
// # Input:
// array1 = [3, 5, 9, 2]
// array2 = [8, 1, 4, 7, 6]
// target = 10
//
// # Output:
// [[3, 7], [9, 1], [2, 8]]

// arr1 , arr2
// target = 10
//

package problems

import (
	"fmt"
	"math"
	"sort"
)

func TwoSums(arr1, arr2 []int, target int) [][]int {
	// fmt.Printf("> TwoSums(%v, %v, %v)", arr1, arr2, target)
	data := make(map[int]int)

	for _, num := range arr1 {
		data[num]++
	}

	result := [][]int{}

	for _, num := range arr2 {
		comp := target - num
		if _, ok := data[comp]; ok {
			for i := 0; i < data[comp]; i++ {
				result = append(result, []int{comp, num})
			}
		}

	}

	return result
}

func TwoSumsClosestBruetForce(arr1, arr2 []int, target int) [][]int {
	closestSum := int(math.Abs(float64(target - arr2[0] - arr1[0])))
	result := [][]int{
		[]int{arr2[0], arr1[0]},
	}
	var sum, distance int
	for _, num1 := range arr1 {
		for _, num2 := range arr2 {
			sum = num1 + num2
			distance = int(math.Abs(float64(target - sum)))

			if distance < closestSum {
				closestSum = distance
				result = [][]int{[]int{num1, num2}}
			} else if distance == closestSum {
				result = append(result, []int{num1, num2})
			}
		}
	}

	return result
}

func TwoSumsClosestSorting(arr1, arr2 []int, target int) [][]int {
	sort.Ints(arr1)
	sort.Ints(arr2)

	dist := math.MaxInt

	cur1 := 0
	cur2 := len(arr2) - 1

	result := [][]int{}

	for cur1 < len(arr1) && cur2 >= 0 {
		sum := arr1[cur1] + arr2[cur2]
		curDist := int(math.Abs(float64(target - sum)))
		if curDist == 0 {
			if dist == 0 {
				fmt.Printf("append [%v, %v]\n", arr1[cur1], arr2[cur2])
				result = append(result, []int{arr1[cur1], arr2[cur2]})
			} else {
				dist = 0
				result = [][]int{[]int{arr1[cur1], arr2[cur2]}}
			}
		} else if curDist < dist {
			dist = curDist
			result = [][]int{[]int{arr1[cur1], arr2[cur2]}}
		} else if curDist == dist {
			result = append(result, []int{arr1[cur1], arr2[cur2]})
		}

		if sum < target {
			cur1++
		} else {
			cur2--
		}
	}

	return result
}
