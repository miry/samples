# TODO:
# https://en.wikipedia.org/wiki/Karger%27s_algorithm
# https://www.redblobgames.com/pathfinding/a-star/introduction.html
# http://theory.stanford.edu/~amitp/GameProgramming/MapRepresentations.html
#

class UndirectedGraph(C)
  def initialize
    # adjacency-list implementation
    @graph = Hash(C, Hash(C, Int32)).new
  end

  def add(src : C, target : C, weight : Int32 = 0)
    @graph[src] ||= Hash(C, Int32).new
    @graph[src][target] = weight

    @graph[target] ||= Hash(C, Int32).new
    @graph[target][src] = weight
  end

  def neighbours(node : C)
    @graph[node]
  end

  def empty?
    @graph.empty?
  end

  def bfs(start : C, & : C -> Nil)
    return if !@graph.has_key?(start)

    queue = [start]
    visited = Set(C).new
    visiting = Set(C).new
    while queue.size > 0
      visiting = Set(C).new
      queue.each do |node|
        yield node

        visited.add(node)
        @graph[node].each do |cell|
          next if visited.includes?(cell) || queue.includes?(cell) || !@graph.has_key?(cell)
          visiting.add(cell)
        end
      end
      queue = visiting
    end
  end

  def dfs(start : C, goal : C, & : Array(Tuple(C, Int32)) -> Nil)
    return if !@graph.has_key?(start) || !@graph.has_key?(goal)

    queue : Array(Array(C)) = [[start]]
    weights = [[0]]
    while queue.size > 0
      branch = queue.pop
      branch_weights = weights.pop
      node_id = branch.last

      neighbours = neighbours(node_id).reject { |neighbour_id| branch.includes?(neighbour_id) }
      neighbours.each do |neighbour_id, neighbour_weight|
        if neighbour_id == goal
          route = Array(Tuple(C, Int32)).new
          branch.each_with_index do |n, i|
            route << {n, branch_weights[i]}
          end
          route += [{neighbour_id, neighbour_weight}]
          yield route
        else
          queue << (branch + [neighbour_id])
          weights << (branch_weights + [neighbour_weight])
        end
      end
    end
  end

  # DOT format. References https://graphviz.org/doc/info/lang.html
  def to_s(io : IO) : Nil
    io << "strict graph {\n"
    @graph.each do |s, dsts|
      dsts.each do |dst, weight|
        io << "  \"#{s}\" -> \"#{dst}\" [ label = \"#{weight}\" ]\n"
      end
    end
    io << "}\n"
  end
end
