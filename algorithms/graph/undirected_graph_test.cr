require "minitest/autorun"

require "./undirected_graph"

class UndirectedGraphTest < Minitest::Test
  def graph
    @graph ||= UndirectedGraph(String).new
  end

  def test_empty_on_init
    assert_equal true, graph.empty?
  end

  def test_add_nodes
    graph.add("A", "B")
    graph.add("B", "C")
    graph.add("A", "C")
  end

  #   def test_bfs
  #     graph.add("A", "B")
  #     graph.add("B", "C")
  #     graph.add("A", "C")
  #
  #     actual = [] of String
  #     graph.bfs("A") do |node|
  #       actual << node
  #     end
  #     assert_equal ["A", "B", "C"], actual
  #   end
end
