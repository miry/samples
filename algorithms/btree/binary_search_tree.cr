class BinarySearchTree(K, T)
  getter left : self | Nil
  getter right : self | Nil
  getter key
  getter value

  def initialize(@key : K, @value : T)
  end

  def put(key : K, value : T) : self
    cmp = key <=> @key
    if cmp < 0
      l = @left
      if l.nil?
        @left = BinarySearchTree(K, T).new(key, value)
      else
        l.put(key, value)
      end
    elsif cmp > 0
      r = @right
      if r.nil?
        @right = BinarySearchTree(K, T).new(key, value)
      else
        r.put(key, value)
      end
    else
      @value = value
      self
    end
  end

  def get(key : K)
    x = self
    while !x.nil?
      cmp = key <=> x.key
      if cmp < 0
        x = x.left
      elsif cmp > 0
        x = x.right
      else
        return x.value
      end
    end
    return nil
  end

  def min : self
    x = self
    while !x.left.nil?
      x = x.left.not_nil!
    end
    x
  end

  def max : self
    x = self
    while !x.right.nil?
      x = x.right.not_nil!
    end
    x
  end

  # Experiment to use solution base on recursion.
  # The benchmark shows for small size of the tree it has a better performance.
  def get_recursion(key : K)
    return @value if key == @key
    node = key < @key ? @left : @right
    node.nil? ? nil : node.get(key)
  end
end
