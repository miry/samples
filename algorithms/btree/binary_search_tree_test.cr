require "minitest/autorun"

require "./binary_search_tree"

class BinarySearchTreeTest < Minitest::Test
  def test_initialize_root
    root = BinarySearchTree(Char, Int32).new('A', 0)
    assert_equal 'A', root.key
    assert_equal 0, root.value
  end

  def test_put
    root = BinarySearchTree(Char, Int32).new('A', 0)
    ['E', 'X', 'C', 'R', 'H', 'M'].each do |k|
      root.put(k, 1)
    end
    assert_equal 'A', root.key
    assert_equal 0, root.value
  end

  def test_put_overwrite
    root = BinarySearchTree(Char, Int32).new('A', 0)
    ['E', 'X', 'C', 'R', 'H', 'M'].each do |k|
      root.put(k, 1)
    end
    node = root.put('X', 10)

    assert_equal 'X', node.key
    assert_equal 10, node.value

    assert_equal 10, root.get('X')
  end

  def test_get
    root = BinarySearchTree(Char, Int32).new('A', 0)
    ['E', 'X', 'A', 'C', 'R', 'H', 'M'].each do |k|
      root.put(k, 1)
    end
    assert_equal nil, root.get('G')
    assert_equal 1, root.get('H')
  end

  def test_min
    root = BinarySearchTree(Char, Int32).new('S', 0)
    ['E', 'X', 'A', 'C', 'R', 'H', 'M'].each do |k|
      root.put(k, 1)
    end
    node = root.min
    assert_equal 'A', node.key
  end

  def test_max
    root = BinarySearchTree(Char, Int32).new('S', 0)
    ['E', 'X', 'A', 'C', 'R', 'H', 'M'].each do |k|
      root.put(k, 1)
    end
    node = root.max
    assert_equal 'X', node.key
  end
end
