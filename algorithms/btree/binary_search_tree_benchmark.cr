require "minitest/autorun"
require "benchmark"

require "./binary_search_tree"

class BinarySearchTreeTest < Minitest::Test
  def test_get
    size = 1_000_000_0
    root = BinarySearchTree(Int32, Int32).new(0, 0)
    r = Random.new
    size.times { root.put(r.next_int, 1) }

    r = Benchmark.ips(interactive: false) do |x|
      x.report("normal") { root.get(1024); root.get(-1024) }
      x.report("recursion") { root.get_recursion(1024); root.get_recursion(-1024) }
    end

    slow = r.items.find! &.label.starts_with?("normal")
    fast = r.items.find! &.label.starts_with?("recursion")

    assert fast.slower < slow.slower, "Expected #{slow.label} (#{slow.slower}) be slower than #{fast.label} (#{fast.slower})"
  end
end
