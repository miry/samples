require 'async/scheduler'

scheduler = Async::Scheduler.new
Fiber.set_scheduler(scheduler)

def run
  n_threads = 1_000
  fibers = Array.new(n_threads)
  n_threads.times do |i|
    fibers[i] = Fiber.schedule do
      puts "working #{i}"
      sleep 1
    end
  end
  fibers
end

run.each do |f|
  pp f
  pp f.blocking?
  pp f.alive?
end
