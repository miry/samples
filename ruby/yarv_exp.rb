# Save binary

puts "\n== attr_reader == "
example_attr_reader = <<~CODE
class A
  attr_reader :a
end
CODE
puts RubyVM::InstructionSequence.compile(example_attr_reader).disasm

puts "\n== getter == "
example_getter = <<~CODE
class A
  def a
    @a
  end
end
CODE
puts RubyVM::InstructionSequence.compile(example_getter).disasm
