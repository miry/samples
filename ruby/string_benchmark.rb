# frozen_string_literal: true

require "stringio"
require "benchmark/ips"
require_relative "gc_suit"

RubyVM::YJIT.enable if defined? RubyVM::YJIT.enable

size = 10_000_000
half = size / 2
r = Random.new(1234)
arr = []
size.times do
  arr.push(r.rand(size) - half)
end

iio = StringIO.new
sio = StringIO.new
a = Time.now.utc.strftime("I%m%d %T.%6N")
b = Process.pid
m = +"message that is not"

Benchmark.ips do |x|
  x.config(time: 30, warmup: 10, suite: GCSuite.new)

  x.report("interpolation") { iio << "I#{a} #{b}] #{m}" }
  x.report("stringio") { sio << "I" << a << " " << b << "] " << m }

  x.compare!
end
