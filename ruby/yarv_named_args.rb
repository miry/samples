def compile(code)
  puts "=== Code =="
  puts code
  puts
  puts "=== Disassemble ==="
  puts RubyVM::InstructionSequence.compile(code).disasm
  puts "=" * 80
  puts
end

# Show disassemble code
[
  "def foo(a)\n  a\nend",
  "def foo(a:)\n  a\nend",
  "def foo(a: 1)\n  a\nend",
  "def foo(opts={})\n  opts[:a]\nend"
].each { |c| compile(c) }
