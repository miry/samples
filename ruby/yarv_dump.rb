# Save binary

code = <<~CODE
def run
  puts "Running my code from binary!"
end
run
CODE

dump = RubyVM::InstructionSequence.compile(code).to_binary
File.binwrite 'yarv_dump.bin', dump

# Eval binary
RubyVM::InstructionSequence.load_from_binary(dump).eval
