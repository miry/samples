# frozen_string_literal: true

require "net/https"

# Consider to set to https://about.gitlab.com/ as https://gitlab.com response with 301 Moved Permanetly
uri = URI.parse("https://gitlab.com")
# uri = URI.parse("https://about.gitlab.com")
http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true

SECONDS_IN_MIN = 60
WINDOW = 1 * SECONDS_IN_MIN

# It is a primitive measure base on block, but for better accurate latency
# it should mesure the performance on Net::HTTP#transport_request
def measure
	start = Process.clock_gettime(Process::CLOCK_MONOTONIC)
	yield
	Process.clock_gettime(Process::CLOCK_MONOTONIC) - start
end

total = 0
buckets = {}
http.start
latencies = []

start = Process.clock_gettime(Process::CLOCK_MONOTONIC)
limit = start + WINDOW
# It will take longer than 5 mins, i have not implement a concurent solution to stop a process in the middle.
# In this case could be used standard ruby library Timeout.
while Process.clock_gettime(Process::CLOCK_MONOTONIC) < limit do
	t = measure {
    # I am taking HEAD, as it should return 0 content,
    # in case need to check full download content, can replace with GET
    http.head("/")
  }
	# store only pre 5 sec bucket
	t = (t * 1000).to_i # convert seconds to ms
  latencies << t
	t -= t % 10 # sample latency by buckets with step 10 ms
	buckets[t] ||= 0
	buckets[t] += 1
	total += 1
end

# puts "Latencies"
# ruby script.rb | gnuplot -p -e 'set boxwidth 1; plot "-" title "HTTP latency in (ms)" with histogram'
# puts latencies

puts "Latencies distribution"
# ruby script.rb | gnuplot -p -e 'set boxwidth 1; set style fill solid; plot "-" title "HTTP latency in (ms)" with boxes'
buckets.each do |latency, count|
  puts "#{latency}ms #{count} times"
end

latencies.sort!
puts "Stats"
p50 = latencies[(latencies.size * 0.5).to_i]
puts "p50: #{p50} ms"
p90 = latencies[(latencies.size * 0.9).to_i]
puts "p90: #{p90} ms"
p95 = latencies[(latencies.size * 0.95).to_i]
puts "p95: #{p95} ms"

http.finish
