# gem install vernier profile-viewer benchmark-ips profiler stackprof memory_profiler
class FifoArray
	# processor is a wrapped solution, we would use it to test results
	# It should be a callable, that enough for our experiments
	def initialize(dst, batch_size)
		@buffer = []
		@batch_size = batch_size
    @dst = dst
	end

	def push(obj)
		@buffer << obj
		flush if @buffer.size >= @batch_size
	end

	def pop(size=1)
		@buffer.shift(size)
	end

	def flush
		@dst.write @buffer.shift(@buffer.size)
	end
end

class App
	def initialize(queue)
		@queue = queue
	end

	def process(batch)
		puts batch.size
	end

	def run(size)
		while size > 0
			# because we don't know what it could be,
			# we need provide some object, every objects in Ruby has the same structure and size
			@queue.push(Object.new)
			size -= 1
		end
    @queue.flush
	end
end

fifo = FifoArray.new(File::NULL, 512) { }
queue_arr = App.new(fifo)

thread_queue = Thread::Queue.new
queue_thread_queue = App.new(thread_queue)
size = 2048*1000

require 'benchmark/ips'
require 'vernier'
# https://github.com/evanphx/benchmark-ips
Benchmark.ips do |x|
	x.report("Array") { queue_arr.run(size) }
	x.report("Thread::Queue") { queue_thread_queue.run(size) }
end

# # Profile CPU
# # https://github.com/jhawthorn/vernier
# # profile-viewer time_profile_array.json
# Vernier.trace(out: "time_profile_array.json") { queue_arr.run(size) }
#
# # Profile Memory
# # profile-viewer memory_allocations_array.json
# Vernier.trace_retained(out: "memory_allocations_array.json") { queue_arr.run(size) }
#

# Malloc
# require 'profile'
# queue_arr.run(size)

# require 'memory_profiler'
# report = MemoryProfiler.report do
#   queue_arr.run(size)
# end
# report.pretty_print
