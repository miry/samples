require "objspace"

ObjectSpace.trace_object_allocations_start

require_relative "lib/logging"

ObjectSpace.dump_all output: File.new("heap.dump", "w")
