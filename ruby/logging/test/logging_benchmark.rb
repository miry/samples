# frozen_string_literal: true

require "test_helper"
require "objspace"
require "json"
require "minitest/benchmark"

class TestLogging < Minitest::Benchmark
  def bench_my_algorithm
    logger = Logging.new(File::NULL)
    msg = ("Lorium" * 1000).freeze
    assert_performance_linear 0.9999 do |n| # n is a range value
      i = 0
      while i < n
        logger.log { msg }
        i += 1
      end
    end
  end

  def bench_object_memory_consuption
   logger = Logging.new(File::NULL)
   actual = ObjectSpace.memsize_of(logger)
   assert 40 >= actual, "Expecting max 40 bytes for object allocations"
  end

  def bench_logging_nodes
    GC.disable
    ObjectSpace::trace_object_allocations_clear
    ObjectSpace::trace_object_allocations_start
    GC.start
    gc_generation = GC.count
    shape_generation = RubyVM.stat(:next_shape_id)
    Logging.new(File::NULL)
    report_raw = ObjectSpace.dump_all(output: :string, since: gc_generation, shapes: shape_generation)
    report = report_raw.split("\n")
    report.reject!{|l|
      l.include?('"method":"bench_logging_nodes"') ||
      l.include?('objspace.rb') ||
      l.include?(__FILE__)
    }

    memory = report.sum {|str| obj= JSON.parse(str); obj["memsize"].to_i }
    assert_equal 8, report.size
    assert_equal 320, memory
  end
  # def bench_vernier_objects
  #   GC.disable
  #   ObjectSpace::trace_object_allocations_clear
  #   ObjectSpace::trace_object_allocations_start
  #   GC.start
  #   gc_generation = GC.count
  #   shape_generation = RubyVM.stat(:next_shape_id)
  #   actual = ObjectSpace.dump_all(output: :string, since: gc_generation, shapes: shape_generation)
  #   assert_equal 1, actual
  # end
end

