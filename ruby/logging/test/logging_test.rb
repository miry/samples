# frozen_string_literal: true

require "test_helper"

class LoggingTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Logging::VERSION
  end

  def test_io_string
    io = StringIO.new
    log = Logging.new(io, v: 4)
    log.log { "something new" }
    assert_includes io.string, "something new"
  end

  def test_low_default_verbosity
    log = Logging.new(File::NULL, v: 1)
    log.log { raise "should not happen" }
  end

  def test_low_default_verbosity_1
    log = Logging.new(File::NULL)
    log.log(v: 5) { raise "should not happen" }
  end

  def test_null_destination
    log = Logging.new(File::NULL, v: 5)
    log.log(v: 5) { raise "should not happen" }
  end

  def test_file_destination
    file_path = 'tmp/foo.log'
    FileUtils.rm(file_path)  if File.exist?(file_path)
    file = File.open(file_path, "w")
    log = Logging.new(file)
    log.log { "line in file" }
    file.close
    actual = File.new(file_path).readline
    assert_includes actual, "line in file"
  end
end
