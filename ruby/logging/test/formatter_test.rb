# frozen_string_literal: true

require "test_helper"

class LoggingFormatterTest < Minitest::Test
  def test_default_formatting
    formatter = Logging::Formatter.new
    io = StringIO.new
    formatter.call(io, Time.new(2002, 10, 31, 2, 2, 2), [], "foo")
    actual = io.string
    assert_match /^I1031 02:02:02.000000 [0-9]{5}\] "foo"/, actual
  end
end
