n_threads = 100
iterate = 100_000
total = n_threads * iterate

puts "using #{n_threads} threads"

class Account
  def initialize(saldo)
    @balance = saldo
  end

  def credit(amount)
    @balance += amount
  end

  attr_reader :balance
end

account = Account.new(0)

threads = Array.new(n_threads)
n_threads.times do |i|
  threads[i] = Thread.new do
    iterate.times { account.credit(1) }
  end
end

threads.each(&:join)
puts "Balance: #{account.balance}"
puts "Expected: #{total}"
puts "Missed #{total - account.balance}"
