require "benchmark/ips"
require_relative './gc_suit'

# $ ruby --yjit --yjit-call-threshold=1 --yjit-exec-mem-size=512 str_loop_benchmark.rb
# ruby 3.3.0 (2023-12-25 revision 5124f9ac75) +YJIT [arm64-darwin23]
# Warming up --------------------------------------
#            each_byte   159.000 i/100ms
#                while   373.000 i/100ms
# Calculating -------------------------------------
#            each_byte      1.601k (± 0.9%) i/s -     48.018k in  30.002977s
#                while      3.733k (± 6.1%) i/s -    111.900k in  30.130775s

def each_byte(str)
  str.each_byte {|c| c}
end

def while_loop(str)
  j = 0
  b = str.bytes
  while j < b.size
    b[j]
    j += 1
  end
end

s = "42" * 10_000
s.freeze

Benchmark.ips do |x|
  x.config(time: 30, warmup: 10, suite: GCSuite.new)
  x.report( 'each_byte') { each_byte(s) }
  x.report( 'while') { while_loop(s) }
end
