require 'benchmark/ips'

RubyVM::YJIT.enable if defined? RubyVM::YJIT.enable

size = 10_000_000
half = size / 2
r = Random.new(1234)
arr = []
size.times do
  arr.push(r.rand(size) - half)
end

Benchmark.ips do |x|
  x.config(time: 30, warmup: 10)

  x.report('compare with 0') { arr.each { |i| i > 0 } }
  x.report('positive?') { arr.each { |i| i.positive? } }

  x.compare!
end
