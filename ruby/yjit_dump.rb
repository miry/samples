RubyVM::YJIT.enable

def a(i)
  puts i + i
end

100000.times do |i|
  a(i)
end

%i[inline_code_size yjit_insns_count vm_insns_count side_exit_count total_exit_count].each do |k|
  print "#{k}: #{RubyVM::YJIT.runtime_stats[k]}\n"
end
