# frozen_string_literal: true

# Sample running shows no difference between Struct and Class
#
#     ruby 3.3.4 (2024-07-09 revision be1089c8ec) +YJIT [arm64-darwin23]
#     Warming up --------------------------------------
#                    class   622.288k i/100ms
#                   struct   767.983k i/100ms
#     Calculating -------------------------------------
#                    class      5.495M (±73.0%) i/s -    101.433M in  30.282227s
#                   struct      5.598M (±57.3%) i/s -    111.358M in  30.310027s
#
#     Comparison:
#                   struct:  5597788.4 i/s
#                    class:  5495355.3 i/s - same-ish: difference falls within error
#
#     Class allocations: {:TOTAL=>116400452, :FREE=>5887, :T_OBJECT=>1001266, :T_CLASS=>1018, :T_MODULE=>80, :T_FLOAT=>4, :T_STRING=>4015641, :T_REGEXP=>275, :T_ARRAY=>3951, :T_HASH=>468, :T_STRUCT=>111357564, :T_BIGNUM=>1, :T_FILE=>3, :T_DATA=>639, :T_COMPLEX=>1, :T_SYMBOL=>157, :T_IMEMO=>13379, :T_ICLASS=>118}
#     Struct allocations: {:TOTAL=>75704525, :FREE=>70667540, :T_OBJECT=>1255, :T_CLASS=>1016, :T_MODULE=>80, :T_FLOAT=>4, :T_STRING=>4015631, :T_REGEXP=>275, :T_ARRAY=>3933, :T_HASH=>466, :T_STRUCT=>1000029, :T_BIGNUM=>1, :T_FILE=>3, :T_DATA=>636, :T_COMPLEX=>1, :T_SYMBOL=>157, :T_IMEMO=>13380, :T_ICLASS=>118}

require "objspace"
require "benchmark/ips"
require_relative "gc_suit"

RubyVM::YJIT.enable if defined? RubyVM::YJIT.enable

class Location
  attr_reader :name, :city

  def initialize(name, city)
    @name = name
    @city = city
  end
end

Slocation = Struct.new(:name, :city)

Benchmark.ips do |x|
  x.config(time: 30, warmup: 10, suite: GCSuite.new)

  x.report("class") {
    obj = Location.new("John Doe", "New York")
    obj.name
    obj.city
  }
  x.report("struct") {
    obj = Slocation.new("John Doe", "New York")
    obj.name
    obj.city
  }

  x.compare!
end

# Allocations

NUM = 1_000_000
retained = Array.new(NUM)

GC.disable
ObjectSpace.trace_object_allocations_start
NUM.times do |i|
  obj = Location.new("John Doe #{i}", "New York #{i}")
  obj.name
  obj.city
  retained[i] = obj
end
ObjectSpace.trace_object_allocations_stop
h = {}
ObjectSpace.count_objects(h)
puts "Class allocations: #{h}"

retained = Array.new(NUM)
GC.start
GC.disable

ObjectSpace.trace_object_allocations_clear
ObjectSpace.trace_object_allocations_start
NUM.times do |i|
  obj = Slocation.new("John Doe #{i}", "New York #{i}")
  obj.name
  obj.city
  retained[i] = obj
end
ObjectSpace.trace_object_allocations_stop
h = {}
ObjectSpace.count_objects(h)
puts "Struct allocations: #{h}"
